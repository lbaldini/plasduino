#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Configuration'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4']
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """System-wide configuration facility.

Launch the plasduino configuration editor in standalone mode, providing access
to every single setting available in the package.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                            __dependencies__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END


def launch():
    """ Launch the main application.
    """
    from plasduino.gui.gConfigurationEditor import gConfigurationEditor
    mainWindow = gConfigurationEditor()
    mainWindow.exec_()


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = ['gui.style', 'gui.skin']
    import plasduino.__cfgparse__ as __cfgparse__
    application = __cfgparse__.getapp()
    __cfgparse__.TOP_LEVEL_MODULE_OPTIONS = None
    launch()
