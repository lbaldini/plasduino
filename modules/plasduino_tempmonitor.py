#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Temperature Monitor'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'PyQwt', 'pyserial', 'avrdude',
                    'arduino']
__shield__       = 'lab1'
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """A general purpose, two-channels temperature monitor.

Read and display on a strip chart the temperature readout through a pair of
thermistors connected to the analog inputs A0 and A1.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, __shield__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END

import sys
import math

import plasduino.arduino.protocol_h as ptcl

from PyQt4 import Qt, QtCore, QtGui
from plasduino.gui.gAnalogMonitorWindow import gAnalogMonitorWindow
from plasduino.gui.gStripChart import gStripChart
from plasduino.daq.gArduinoRunControl import gArduinoRunControl
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.gui.gQt import connect
from plasduino.__logging__ import logger
from plasduino.sensors.__sensors__ import getSensor
from plasduino.sensors.gThermistor import gThermistor



class tempmonitorEventBuffer(gEventBuffer):
    
    """ Specialized data buffer for the tempmonitor module.
    """

    PROCESSED_DATA_FIELDS = ['Time 0', 'Temp A0', 'Time 1', 'Temp A1']
    PROCESSED_DATA_UNITS = ['s', 'deg C', 's', 'deg C']
    PROCESSED_DATA_FORMAT = ['%.3f', '%.2f', '%.3f', '%.2f']
    MIN_NUM_EVENTS = 2

    def fillDataTable(self):
        """ Reimplemented from the base class.
        """
        for i in xrange(1, len(self), 2):
            t1 = self[i-1]['seconds']
            T1 = self[i-1]['temperature']
            t2 = self[i]['seconds']
            T2 = self[i]['temperature']
            row = (t1, T1, t2, T2)
            self.DataTable.addRow(row)



class tempmonitorRunControl(gArduinoRunControl):

    SKETCH_NAME = 'sktchAnalogSampling'
    PIN_LIST = [0, 1]
    SAMPLING_INTERVAL = 500

    """ Specialized RunControl class for the tempmonitor module.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        eventBuffer = tempmonitorEventBuffer()
        gArduinoRunControl.__init__(self, eventBuffer, maxSeconds)
        logger.info('Retrieving thermistor information...')
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        a0sensor = TOP_LEVEL_CONFIGURATION.get('sensors.a0-sensor')
        a1sensor = TOP_LEVEL_CONFIGURATION.get('sensors.a1-sensor')
        self.ThermistorDict = {}
        if a0sensor is not None:
            thermistor = getSensor(a0sensor)
            if isinstance(thermistor, gThermistor):
                self.ThermistorDict[0] = thermistor
        if a1sensor is not None:
            thermistor = getSensor(a1sensor)
            if isinstance(thermistor, gThermistor):
                self.ThermistorDict[1] = thermistor
        logger.info('A0 analog input: %s' % self.getThermistor(0))
        logger.info('A1 analog input: %s' % self.getThermistor(1))
        if (self.getNumThermistors() == 0):
            logger.warn('No thermistors defined in the configuration.')

    def connectToArduino(self, timeout = None, autoUpload = True):
        """ Overloaded method.
        """
        errCode = gArduinoRunControl.connectToArduino(self, timeout, autoUpload)
        if not errCode:
            self.ArduinoManager.setupAnalogSamplingSketch(self.PIN_LIST,
                                                 self.SAMPLING_INTERVAL)
        return errCode
 
    def getNumThermistors(self):
        """ Return the number of thermistors defined in the configuration file.
        """
        return len(self.ThermistorDict)

    def getThermistor(self, pinNumber):
        """ Return the thermistor object corresponding to a given analog input.
        """
        try:
            return self.ThermistorDict[pinNumber]
        except KeyError:
            return None

    def poll(self):
        """ Implementation of the polling cycle on the PC side.

        Here we emit an additional readout() signal passing along the event
        number as an argument---so that the even itself can be easily retrieved
        in the update() slot of the main window, which is connected to the
        signal.
        """
        ctrl = self.readCtrlByte()
        if ctrl != ptcl.ANALOG_READOUT_HEADER:
            return ctrl
        readout = self.ArduinoManager.sreadAnalogReadout()
        self.addEvent(readout)
        thermistor = self.getThermistor(readout['pinNumber'])
        if thermistor is not None:
            readout['temperature'] = thermistor.adc2celsius(readout['adc'])
        else:
            readout['temperature'] = gThermistor.ABSOLUTE_ZERO
        self.emit(QtCore.SIGNAL('readout(int)'), self.getNumEvents() - 1)
        return ctrl



class tempmonitorWindow(gAnalogMonitorWindow):

    """ Main window for the tempmonitor module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.*', 'daq.*', 'arduino.*',
                      'sensors.a0-sensor', 'sensors.a1-sensor']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gAnalogMonitorWindow.__init__(self, **kwargs)
        self.PlotWidget.setYTitle('Temperature [deg C]')
        self.RunControl = tempmonitorRunControl()
        self.connectRunControl()
        self.RunControl.connectToArduino(timeout = 0.1)
        if self.RunControl.ArduinoManager.connected():
            self.setupConnections()
        if (self.RunControl.getNumThermistors() == 0):
            msg = 'There are no thermistors defined in the top-level '\
                'configuration file.\n\n'\
                'Please select "Change settings" from the "Configuration" '\
                'menu and select the appropriate thermistors for the A0 '\
                'and/or A1 analog inputs in the "sensors" panel.\n\n'
            self.popupFatalError(msg)
            return
        self.__ActivePins = []
        for pinNumber in [0, 1]:
            thermistor = self.RunControl.getThermistor(pinNumber)
            if thermistor is not None:
                self.__ActivePins.append(pinNumber)
                chartName = '%s on A%d' % (thermistor.Model, pinNumber)
                self.addChart(chartName, pinNumber)

    def loadData(self, table):
        """ Overloaded class method.

        Load data from a table.
        """
        cols = table.getColumns()
        self.clearCharts()
        chartRight = self.StripChartGroup[0]
        chartLeft = self.StripChartGroup[1]
        chartRight.setData(cols[0], cols[1])
        chartLeft.setData(cols[2], cols[3])
        self.PlotWidget.replot()
        self.enableZooming()

    def setupConnections(self):
        """ Setup the connections.
        """
        connect(self.RunControl, 'readout(int)', self.updateCharts)
        connect(self.RunControl, 'started()', self.clearCharts)
        connect(self.RunControl, 'started()', self.disableZooming)
        connect(self.RunControl, 'stopped()', self.enableZooming)

    def updateCharts(self, eventNumber):
         """ Synchronous update of the data display.
         """
         readout = self.RunControl.getEvent(eventNumber)
         pinNumber = readout['pinNumber']
         if pinNumber in self.__ActivePins:
             seconds = readout['seconds']
             adc = readout['adc']
             temperature = readout['temperature']
             chart = self.StripChartGroup.getChartByPin(pinNumber)
             chart.addDataPoint(seconds, temperature)
             self.StripChartGroup.adjustXRange()
             self.PlotWidget.replot()
             msg = 'A%d: %d ADC counts (%.3f deg C)' %\
                 (pinNumber, adc, temperature)
             self.showMessage(msg, -1)



def launch():
    """ Launch the main application.
    """
    mainWindow = tempmonitorWindow()
    mainWindow.show()
    return mainWindow



if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = tempmonitorWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
