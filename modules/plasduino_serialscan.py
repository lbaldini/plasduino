#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Serial Scan'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'pyserial']
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Serial port scanner.

Small utility module polling on the serial interfaces and logging the attached
devices.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END



import time

import plasduino.arduino.__boards__ as __boards__

from plasduino.daq.__serial__ import scan_posix
from plasduino.gui.gMessageBox import gMessageBox
from PyQt4 import QtCore
from plasduino.gui.gQt import connect


class doctorWindow(gMessageBox):

    """ Main window for the plasduino_doctor module.
    """
    
    MODULE_OPTIONS = ['gui.style']

    def __init__(self):
        """ Constructor.
        """
        gMessageBox.__init__(self, title = 'Serial port scanner', width = 550)
        self.update()
        self.__Timer = QtCore.QTimer(self)
        self.__Timer.setInterval(1000)
        connect(self.__Timer, 'timeout()', self.update)
        self.__Timer.start()

    def update(self):
        """ Show the (formatted) output of the serial scan.
        """
        text = 'Last update on %s.\n\n' % time.asctime()
        for folder, port, vid, pid in scan_posix():
            text += '%s (' % folder
            if port is not None:
                text += '%s, ' % port
            text += 'vid %s, pid %s' % (vid, pid)
            try:
                model = __boards__.SERIAL_ID_DICT[(vid, pid)]
                text += ', arduino %s' % model
            except KeyError:
                model = None
            text += ')\n'
        self.setText(text)



def launch():
    """ Main function.
    """
    msgBox = doctorWindow()
    msgBox.show()
    return msgBox


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = doctorWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_()
