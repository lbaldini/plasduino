#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Audio Track'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'PyQwt', 'pyaudio']
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """A general purpose audio card tracking application.

Minimal application tracking in real time the (stereo) input of the
audiocard. Mainly for testing purposes.
"""

#__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
#                             __dependencies__, author = __author__,
#                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END


import sys
import PyQt4.Qwt5 as Qwt

from plasduino.__logging__ import logger
from plasduino.gui.gAnalogMonitorWindow import gAnalogMonitorWindow
from plasduino.gui.gStripChart import gStripChart
from plasduino.gui.gQt import connect
from plasduino.daq.gAudioStereoEventBuffer import gAudioStereoEventBuffer
from plasduino.daq.gAudioStereoRunControl import gAudioStereoRunControl
from PyQt4 import Qt, QtCore, QtGui


class audiotrackRunControl(gAudioStereoRunControl):

    """ Specialized RunControl class for the audiotrack module.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        self.SampleRate = TOP_LEVEL_CONFIGURATION['audio.sample-rate']
        self.BufferSize = TOP_LEVEL_CONFIGURATION['audio.buffer-size']
        self.PrescaleFactor = TOP_LEVEL_CONFIGURATION['audio.prescale-factor']
        eventBuffer = gAudioStereoEventBuffer(self.SampleRate,
                                              self.PrescaleFactor)
        gAudioStereoRunControl.__init__(self, eventBuffer, maxSeconds)
        
    def poll(self):
        """ Implementation of the polling cycle on the PC side.
        """
        self.Mutex.lock()
        try:
            data = self.AudioStream.read(self.BufferSize)
        except IOError:
            return 
        self.Mutex.unlock()
        # Check whether the run control has been stopped before filling the
        # data buffer and sending out the readout() signal.
        if self.isRunning():
            (startIndex, numSamples) = self.EventBuffer.fill(data)
            self.emit(QtCore.SIGNAL('readout(int, int)'), startIndex,
                      numSamples)



class audiotrackWindow(gAnalogMonitorWindow):

    """ Main window for the audiotrack module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.style', 'gui.skin', 'gui.language',
                      'daq.*', 'audio.*']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gAnalogMonitorWindow.__init__(self, **kwargs)
        self.PlotWidget.setYTitle('Analog input [a. u.]')
        self.RunControl = audiotrackRunControl()
        self.connectRunControl()
        kwargs = {'markerSize': 0, 'lineWidth': 2}
        self.addChart('Right channel', **kwargs)
        self.addChart('Left channel', **kwargs)
        self.setupConnections()

    def setupConnections(self):
        """ Setup the connections.
        """
        connect(self.RunControl, 'started()', self.clearCharts)
        connect(self.RunControl, 'readout(int, int)', self.updateCharts)
        connect(self.RunControl, 'started()', self.disableZooming)
        connect(self.RunControl, 'stopped()', self.showAllDataPoints)
        connect(self.RunControl, 'stopped()', self.enableZooming)

    def updateCharts(self, startIndex, numSamples):
        """ Synchronous update of the data display.
        """
        chartRight = self.StripChartGroup[0]
        chartLeft = self.StripChartGroup[1]
        for i in xrange(startIndex, startIndex + numSamples):
            evt = self.RunControl.EventBuffer[i]
            chartRight.addDataPoint(evt['seconds'], evt['right'])
            chartLeft.addDataPoint(evt['seconds'], evt['left'])
        self.PlotWidget.replot()

    def showAllDataPoints(self):
        """ Show all the data points in the strip charts, irrespectively of the
        MAX_DISPLAY_POINTS class member.

        Note that we have to set the x-axis scale explicitely and call a replot
        at the end.
        """
        self.StripChartGroup[0].showAllDataPoints()
        self.StripChartGroup[1].showAllDataPoints()
        self.PlotWidget.replot()



def launch():
    """ Launch the main application.
    """
    mainWindow = audiotrackWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = audiotrackWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
