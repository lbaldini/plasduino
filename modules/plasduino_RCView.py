#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2015   Carmelo Sgro' (carmelo.sgro@pi.infn.it)      *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'RC View'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'PyQwt', 'pyserial', 'avrdude',
                    'arduino']
__shield__       = 'lab1'
__author__       = 'Carmelo Sgro'
__contact__      = 'carmelo.sgro@pi.infn.it'
__abstract__     = """Visualize the charge/discharge of a RC circuit

Details TBD...
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, __shield__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END

import sys
import math
import atexit

import plasduino.arduino.protocol_h as ptcl

from PyQt4 import Qt, QtCore, QtGui
from plasduino.gui.gAnalogMonitorWindow import gAnalogMonitorWindow
from plasduino.gui.gPlotWidget import gPlotWidget
from plasduino.gui.gAcquisitionDataDisplay import gSynchAcquisitionDataDisplay
from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gToggleWidget import gToggleWidget
from plasduino.daq.gArduinoRunControl import gArduinoRunControl
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.gui.gQt import connect, disconnect
from plasduino.__logging__ import logger



class rcviewEventBuffer(gEventBuffer):
    
    """ Specialized data buffer for the rcview module.
    """

    PROCESSED_DATA_FIELDS = ['Time A', 'Vout A', 'Time B', 'Vout B']
    PROCESSED_DATA_UNITS = ['s', 'adc', 's', 'adc']
    PROCESSED_DATA_FORMAT = ['%.3f', '%d', '%.3f', '%d']
    MIN_NUM_EVENTS = 2

    def fillDataTable(self):
        """ Reimplemented from the base class.

        """
        samplePos = self[0]['position']
        self.DataTable.FormatStrings = ['%.3f', '%d', '%.3f', '%d']
        for i in xrange(1, len(self), 2):
            t1 = self[i-1]['seconds']
            P1 = self[i-1]['position'] 
            t2 = self[i]['seconds']
            P2 = self[i]['position']
            row = (t1, P1, t2, P2)
            self.DataTable.addRow(row)


class rcviewRunControl(gArduinoRunControl):

    SKETCH_NAME = 'sktchRCView'
    INPUT_PIN_LIST = [4, 5]
    DRIVER_PIN  = 12
    SAMPLING_INTERVAL = 20

    """ Specialized RunControl class for the rcview module.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        eventBuffer = rcviewEventBuffer()
        gArduinoRunControl.__init__(self, eventBuffer, maxSeconds)
        
    def connectToArduino(self, timeout = None, autoUpload = True):
        """ Overloaded method.
        """
        if len(self.INPUT_PIN_LIST) !=2:
            logger.error('Sketch can handle only 2 ADC pins, here we set %d' 
                         % len(self.INPUT_PIN_LIST))
            return 10
        errCode = gArduinoRunControl.connectToArduino(self, timeout, autoUpload)
        if not errCode:
            self.ArduinoManager.swritecmd(ptcl.OP_CODE_SELECT_ANALOG_PIN,
                                          'B', self.INPUT_PIN_LIST[0])
            self.ArduinoManager.swritecmd(ptcl.OP_CODE_SELECT_ANALOG_PIN,
                                          'B', self.INPUT_PIN_LIST[1])
            self.ArduinoManager.swritecmd(ptcl.OP_CODE_SELECT_SAMPLING_INTERVAL,
                                          'I', self.SAMPLING_INTERVAL)
            self.ArduinoManager.swritecmd(ptcl.OP_CODE_SELECT_DIGITAL_PIN,
                                          'B', self.DRIVER_PIN)

        return errCode

    def toggleDigitalPin(self):
        """ change status to Driver pin
        """
        logger.info('Toggle Driver Pin')
        self.ArduinoManager.swriteuint8(ptcl.OP_CODE_TOGGLE_DIGITAL_PIN) 


    def poll(self):
        """ Implementation of the polling cycle on the PC side.

        Here we emit an additional readout() signal passing along the event
        number as an argument---so that the even itself can be easily retrieved
        in the update() slot of the main window, which is connected to the
        signal.
        """
        ctrl = self.readCtrlByte()
        if ctrl != ptcl.ANALOG_READOUT_HEADER:
            return ctrl
        readout = self.ArduinoManager.sreadAnalogReadout()
        self.addEvent(readout)
        readout['position'] = readout['adc']
        self.emit(QtCore.SIGNAL('readout(int)'), self.getNumEvents() - 1)
        return ctrl



class rcviewWindow(gAnalogMonitorWindow):

    """ Main window for the rcview module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.*', 'daq.*', 'arduino.*']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        kwargs['numLayoutColumns'] = 5
        gAnalogMonitorWindow.__init__(self, **kwargs)
        self.PlotWidget.setYTitle('Voltage [a.u.]')
        self.RunControl = rcviewRunControl()
        self.connectRunControl()
        self.RunControl.connectToArduino(timeout = 0.2)
        if self.RunControl.ArduinoManager.connected():
            self.setupConnections()
        for pinNumber in self.RunControl.INPUT_PIN_LIST:
            chartName = 'Input pin A%d' % pinNumber
            self.addChart(chartName, pinNumber, markerSize = 0)

    def loadData(self, table):
        """ Overloaded class method.

        Load data from a table.
        """
        cols = table.getColumns()
        self.clearCharts()
        chartA = self.StripChartGroup[0]
        chartB = self.StripChartGroup[1]
        chartA.setData(cols[0], cols[1])
        chartB.setData(cols[2], cols[3])
        self.PlotWidget.replot()
        self.enableZooming()

    def disableControls(self):
        """ Overloaded method.
        """
        gAnalogMonitorWindow.disableControls(self)
        self.ToggleWidget.setEnabled(False)

    def setup(self):
        """ Setup the widgets on the gui.
        """
        self.PlotWidget = gPlotWidget(self, xTitle = 'Time [s]')
        self.DataDisplay = gSynchAcquisitionDataDisplay(self)
        self.TransportBar = gTransportBar(self, tooltips = \
                                              gAcquisitionWindow.TOOLTIP_DICT)
        connect(self.TransportBar, 'quit()', self.close)
        self.ToggleWidget = gToggleWidget(self, 'RC control', 'RC Input')
        self.ToggleWidget.setTooltip('Change RC input status')
        self.addWidget(self.PlotWidget, 3, 0, 1, 4)
        self.addWidget(self.ToggleWidget, 4, 3, align = QtCore.Qt.AlignTop)
        self.addWidget(self.TransportBar, 4, 0, align = QtCore.Qt.AlignTop)
        self.TransportBar.setMaximumWidth(350)
        self.addItem(QtGui.QSpacerItem(150, 0), 4, 1)
        self.addWidget(self.DataDisplay, 4, 2, align = QtCore.Qt.AlignTop)
            
    def setupConnections(self):
        """ Setup the connections.
        """
        connect(self.RunControl, 'readout(int)', self.updateCharts)
        connect(self.RunControl, 'started()', self.clearCharts)
        connect(self.RunControl, 'started()', self.disableZooming)
        connect(self.RunControl, 'stopped()', self.enableZooming)
        connect(self.ToggleWidget, 'toggle()',
                self.RunControl.toggleDigitalPin)
 
    def updateCharts(self, eventNumber):
         """ Synchronous update of the data display.
         """
         readout = self.RunControl.getEvent(eventNumber)
         pinNumber = readout['pinNumber']
         seconds = readout['seconds']
         position  = readout['position']
         try:
             chart = self.StripChartGroup.getChartByPin(pinNumber)
             chart.addDataPoint(seconds, position)
             self.StripChartGroup.adjustXRange()
             self.PlotWidget.replot()
         except KeyError:
             pass


def launch():
    """ Launch the main application.
    """
    mainWindow = rcviewWindow()
    mainWindow.show()
    return mainWindow



if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = rcviewWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
