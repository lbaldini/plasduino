#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

from plasduino.modulemanager.__modulemanager__ import dumpModuleInfo
from plasduino.__plasduino__ import PLASDUINO_DIST_MODULEINFO_FILE_PATH

""" Dump the pickle file with the module information for all the modules
shipped with the distribution.
"""


if __name__ == '__main__':
    folderPath = os.path.dirname(os.path.abspath(__file__))
    dumpModuleInfo([folderPath], PLASDUINO_DIST_MODULEINFO_FILE_PATH)
