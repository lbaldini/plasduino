#!/usr/bin/python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Pendulum Audio'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'PyQwt', 'pyaudio']
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Track the motion of a pendulum continously in real time.

This application acquires data through the audiocard and needs a specific,
dedicated hardware setup.
"""

#__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
#                             __dependencies__, author = __author__,
#                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END


import sys
import time
import PyQt4.Qwt5 as Qwt

from plasduino.__logging__ import logger
from plasduino.gui.gAnalogMonitorWindow import gAnalogMonitorWindow
from plasduino.gui.gStripChart import gStripChart
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gQt import connect, disconnect
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.daq.gAudioStereoEvent import gAudioStereoEvent
from plasduino.daq.gAudioStereoEventBuffer import gAudioStereoEventBuffer
from plasduino.daq.gAudioStereoRunControl import gAudioStereoRunControl
from PyQt4 import Qt, QtCore, QtGui


""" Brief description of the experimental setup.

We track the position of the pendulum through the PC audiocard by measuring
the resistance between the pendulum tip in water and an electrode.
There are two pendulums, connected with the L and R channels of the audiocard.

The output signal is modulated with a square wave with period ~2 ms and ~50%
duty cycle in order to be able to bypass the audiocard input capacitor
(we're interested in frequencies of the order of ~1 Hz or less).
"""


class pendulumaudioEventBuffer(gAudioStereoEventBuffer):

    def __init__(self, sampleRate, offsetRight = 0, offsetLeft = 0,
                 maxNumEvents = None):
        """ Overloaded constructor.
        """
        gAudioStereoEventBuffer.__init__(self, sampleRate, 1, maxNumEvents)
        self.setOffsets(offsetRight, offsetLeft)

    def setOffsets(self, offsetRight, offsetLeft):
        """ Set the offset for the data buffer.
        """
        self.OffsetRight = offsetRight
        self.OffsetLeft = offsetLeft

    def fill(self, data):
        """ Overloaded fill() method.

        Processing the chunk of events and getting a reasonable position out
        is not completely trivial as we have to average the difference between
        the high- and low-levels of the input square wave with an offset that
        changes in time and with the PC sampling times being asynchronous with
        the input wave itself.

        We do a first loop and calculate the average vaule of the input samples.

        Then we loop again and we keep track of the subset of the samples above
        and below the local offset.
        """
        startIndex = len(self)
        numSamples = 0
        roff = 0
        loff = 0
        for index in xrange(0, len(data), self.EVENT_LENGTH):
            event = gAudioStereoEvent(data[index:index + self.EVENT_LENGTH])
            roff += event['right']
            loff += event['left']
            numSamples += 1
        roff /= numSamples
        loff /= numSamples
        rp = []
        rm = []
        lp = []
        lm = []
        for index in xrange(0, len(data), self.EVENT_LENGTH):
            event = gAudioStereoEvent(data[index:index + self.EVENT_LENGTH])
            r = event['right']
            if r > roff:
                rp.append(r)
            else:
                rm.append(r)
            l = event['left']
            if l > loff:
                lp.append(l)
            else:
                lm.append(l)
            self.CurrentIndex += 1
        try:
            r = sum(rp)/len(rp) - sum(rm)/len(rm) - self.OffsetRight
            l = sum(lp)/len(lp) - sum(lm)/len(lm) - self.OffsetLeft
            t = self.TimeStep*(self.CurrentIndex - numSamples/2)
            event = {'seconds': t, 'right': r, 'left': l}
            gEventBuffer.fill(self, event)
        except ZeroDivisionError:
            logger.warn('ZeroDivisionError in pendulumaudioEventBuffer.fill(), '
                        'skipping event...')

    def average(self):
        """ Return the average values for the right and left channels across
        the entire buffer.
        """
        logger.info('Calculating average across data buffer...')
        right = 0
        left = 0
        for event in self:
            right += event['right'] 
            left += event['left']
        if len(self):
            right /= float(len(self))
            left /= float(len(self))
        logger.info('Done. R channel: %.2f, L channel: %.2f.' % (right, left))
        return (right, left)

    def zeroOffset(self):
        """
        """
        (right, left) = self.average()
        self.setOffsets(right, left)



class pendulumaudioRunControl(gAudioStereoRunControl):

    """ Specialized RunControl class for the pendulumaudio module.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        self.SampleRate = 4000
        self.BufferSize = 200
        eventBuffer = pendulumaudioEventBuffer(self.SampleRate)
        gAudioStereoRunControl.__init__(self, eventBuffer, maxSeconds)

    def poll(self):
        """ Implementation of the polling cycle on the PC side.

        TODO: we might probably do a better job in terms of synchronization
        than checking whenther the run control is running in the polling
        loop.
        """
        self.Mutex.lock()
        try:
            data = self.AudioStream.read(self.BufferSize)
        except IOError:
            self.Mutex.unlock()
            return
        except AttributeError:
            self.Mutex.unlock()
            return
        self.Mutex.unlock()
        # Check whether the run control has been stopped before filling the
        # data buffer and sending out the readout() signal.
        if self.isRunning():
            self.EventBuffer.fill(data)
            self.emit(QtCore.SIGNAL('readout(int)'), len(self.EventBuffer) - 1)

    def zeroOffsetInit(self, acquisitionSeconds = 3):
        """ Calibrate the offsets for the right and left channels.
        """
        logger.info('Starting zero-offset calibration...')
        self.EventBuffer.setOffsets(0, 0)
        timer = QtCore.QTimer(self)
        timer.setInterval(acquisitionSeconds*1000)
        timer.setSingleShot(True)
        connect(timer, 'timeout()', self.zeroOffsetDone)
        timer.start()
        self.emit(QtCore.SIGNAL('zeroOffsetInit()'))
        self.setRunning()
        
    def zeroOffsetDone(self):
        """ Finalize the zero offset procedure.
        """
        self.setStopped()
        self.emit(QtCore.SIGNAL('zeroOffsetDone()'))
        self.EventBuffer.zeroOffset()
        logger.info('Zero-offset calibration correctly performed.')



class pendulumaudioWindow(gAnalogMonitorWindow):

    """ Main window for the pendulumaudio module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.style', 'gui.skin', 'gui.language', 'daq.*']
    MAX_DISPLAY_POINTS = 600

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gAnalogMonitorWindow.__init__(self, **kwargs)
        self.RunControl = pendulumaudioRunControl()
        self.connectRunControl()
        self.insertMenu('Calibration', 'Configuration')
        self.addMenuAction('Calibration', 'Zero offset',
                           self.RunControl.zeroOffsetInit)
        self.PlotWidget.setYTitle('Analog input [a. u.]')
        kwargs = {'markerSize': 0, 'lineWidth': 2}
        self.addChart('Right channel', self.MAX_DISPLAY_POINTS, **kwargs)
        self.addChart('Left channel', self.MAX_DISPLAY_POINTS, **kwargs)
        self.setupConnections()

    def loadData(self, table):
        """ Overloaded class method.

        Load data from a table.
        """
        cols = table.getColumns()
        self.clearCharts()
        chartRight = self.StripChartGroup[0]
        chartLeft = self.StripChartGroup[1]
        chartRight.setData(cols[0], cols[1])
        chartLeft.setData(cols[0], cols[2])
        self.PlotWidget.replot()
        self.enableZooming()

    def setupConnections(self):
        """ Setup the connections.
        """
        connect(self.RunControl, 'started()', self.clearCharts)
        connect(self.RunControl, 'readout(int)', self.updateCharts)
        connect(self.RunControl, 'zeroOffsetInit()', self.zeroOffsetInit)
        connect(self.RunControl, 'zeroOffsetDone()', self.zeroOffsetDone)
        connect(self.RunControl, 'started()', self.disableZooming)
        connect(self.RunControl, 'stopped()', self.showAllDataPoints)
        connect(self.RunControl, 'stopped()', self.enableZooming)

    def zeroOffsetInit(self):
        """ Disable the transport bar and, if necessary, disconnect the
        promptCopyFile slot from the rawDataProcessed(QString) signal.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        if TOP_LEVEL_CONFIGURATION['daq.prompt-save-dialog']:
            disconnect(self.RunControl, 'rawDataProcessed(QString)',
                       self.promptCopyFile)
        self.TransportBar.setStatus(gTransportBar.STATUS_DISABLED)

    def zeroOffsetDone(self):
        """ Set the transport bar in the stopped state and, if necessary,
        reconnect the promptCopyFile slot to the rawDataProcessed(QString)
        signal.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        if TOP_LEVEL_CONFIGURATION['daq.prompt-save-dialog']:
            connect(self.RunControl, 'rawDataProcessed(QString)',
                    self.promptCopyFile)
        self.TransportBar.setStatus(gTransportBar.STATUS_STOPPED)

    def updateCharts(self, index):
        """ Synchronous update of the data display.
        """
        chartRight = self.StripChartGroup[0]
        chartLeft = self.StripChartGroup[1]
        evt = self.RunControl.EventBuffer[index]
        seconds = evt['seconds']
        chartRight.addDataPoint(seconds, evt['right'])
        chartLeft.addDataPoint(seconds, evt['left'])
        self.PlotWidget.replot()

    def showAllDataPoints(self):
        """ Show all the data points in the strip charts, irrespectively of the
        MAX_DISPLAY_POINTS class member.

        Note that we have to set the x-axis scale explicitely and call a replot
        at the end.
        """
        self.StripChartGroup[0].showAllDataPoints()
        self.StripChartGroup[1].showAllDataPoints()
        self.PlotWidget.replot()



def launch():
    """ Launch the main application.
    """
    mainWindow = pendulumaudioWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = pendulumaudioWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
