#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Wheel'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'pyserial', 'avrdude', 'arduino']
__shield__       = 'lab1'
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Module for tracking the motion of a wheel.

The hardware setup is based on a 20-sector encoder mounted on an aluminum wheel
free to rotate in a vertical plane. The goal of the experiment is to track the
angular velocity of the wheel.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, __shield__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END

import sys
import math

import plasduino.arduino.protocol_h as ptcl

from PyQt4 import QtGui
from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.daq.gArduinoRunControl import gArduinoRunControl
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.__logging__ import logger
from plasduino.arduino.plasduino_h import INTERRUPT_MODE_DISABLE,\
    INTERRUPT_MODE_ENABLE_CHANGE



class wheelEventBuffer(gEventBuffer):

    NUM_SECTORS = 20
    SECTOR_WIDTH = (2*math.pi)/NUM_SECTORS
    PROCESSED_DATA_FIELDS = ['Time', 'Angular velocity']
    PROCESSED_DATA_UNITS = ['s', 'rad/s']
    PROCESSED_DATA_FORMAT = ['%.6f', '%.6f']
    MIN_NUM_EVENTS = 4
    
    """ Specialized data buffer for the wheel module.

    In the actual implementation this module was developed for, the angular
    velocity of the wheel is measured through a 20-sector encoder. We do
    record both the rising and the falling edge of the transitions so that
    we effectively have 4 data points for each 1/20 revolution (two transitions
    up and two down), 2 of which in common with the previous/next part of the
    revolution.
    """

    def fillDataTable(self):
        """ Reimplemented from the base class.
        """
        for i in xrange(2, len(self) - 1, 2):
            dt1 = self[i]['seconds'] - self[i-2]['seconds']
            dt2 = self[i+1]['seconds'] - self[i-1]['seconds']
            t = 0.5*(self[i+1]['seconds'] + self[i-2]['seconds'])
            omega = 2*self.SECTOR_WIDTH/(dt1 + dt2)
            row = (t, omega)
            self.DataTable.addRow(row)



class wheelRunControl(gArduinoRunControl):

    SKETCH_NAME = 'sktchDigitalTimer'
    INTERRUPT_MODE_0 = INTERRUPT_MODE_ENABLE_CHANGE
    INTERRUPT_MODE_1 = INTERRUPT_MODE_DISABLE

    """ Specialized RunControl class for the wheel module.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        eventBuffer = wheelEventBuffer()
        gArduinoRunControl.__init__(self, eventBuffer, maxSeconds)

    def connectToArduino(self, timeout = None, autoUpload = True):
        """ Overloaded method.
        """
        errCode = gArduinoRunControl.connectToArduino(self, timeout, autoUpload)
        if not errCode:
            self.ArduinoManager.setupDigitalTimerSketch(self.INTERRUPT_MODE_0,
                                                        self.INTERRUPT_MODE_1)
        return errCode

    def poll(self):
        """ Implementation of the polling cycle on the PC side.
        """
        return gArduinoRunControl.pollAlternateDigitalTransitions(self)



class wheelWindow(gAcquisitionWindow):

    """ Main window for the wheel module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.*', 'daq.*', 'arduino.*']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gAcquisitionWindow.__init__(self, **kwargs)
        self.RunControl = wheelRunControl()
        self.connectRunControl()
        self.RunControl.connectToArduino(timeout = 0.2)



def launch():
    """ Launch the main application.
    """
    mainWindow = wheelWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = wheelWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
