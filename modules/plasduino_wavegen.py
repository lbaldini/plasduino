#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo


#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Waveform Generator'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'pyserial', 'avrdude', 'arduino']
__shield__       = None
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Waveform generator.

Simple waveform generator based on the AD9833 chip.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, __shield__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END


import atexit

import plasduino.arduino.protocol_h as ptcl

from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gWavegenWidget import gWavegenWidget
from plasduino.gui.gQt import connect, disconnect
from plasduino.daq.gArduinoManager import gArduinoManager
from plasduino.devices.__AD9833__ import AD9833
from plasduino.__logging__ import logger


class wavegenArduinoManager(gArduinoManager):

    SKETCH_NAME = 'sktchWavegenAD9833'

    """ Specialized arduino manager for the wavegen module.

    We don't write data, we have no real concept of "running" (or polling
    thread) on the PC side and we don't need the runId, so having a full
    gRunControl is overkill. We still need to send commands to Arduino, so
    we do need at least a gArduinoManager.
    """

    def __init__(self):
        """ Constructor.

        We do the handshaking right away, here.
        """
        gArduinoManager.__init__(self)
        self.__AD9833 = AD9833()

    def connect(self, timeout = 0.2):
        gArduinoManager.connect(self, self.SKETCH_NAME, timeout)

    def __cmd(self, fmt, payload):
        """ Send a serial command to the AD9833 onboard.
        """
        self.swritecmd(ptcl.OP_CODE_AD9833_CMD, fmt, payload)

    def powerUp(self, mode = AD9833.OUTPUT_MODE_SINUSOIDAL):
        """ Power up the AD9833.
        """
        self.__cmd('H', self.__AD9833.payloadPowerUp(mode))

    def powerDown(self):
        """ Power down the AD9833.
        """
        self.__cmd('H', self.__AD9833.payloadPowerDown())

    def setFrequency(self, value):
        """ Set the frequency on the AD9833.
        """
        control, lsbs, msbs = self.__AD9833.payloadSetFrequency(value)
        self.__cmd('H', control)
        self.__cmd('H', lsbs)
        self.__cmd('H', msbs)

    def setPhase(self, value):
        """ Set the phase on the AD9833.
        """
        self.__cmd('H', self.__AD9833.payloadSetPhase(value))



class wavegenWindow(gAcquisitionWindow):

    """ Main window for the wheel module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.*', 'daq.*', 'arduino.*']

    def __init__(self, **kwargs):
        """ Constructor.

        Mind we do have to call self.setupConnections() before
        self.ArduinoManager.connect() is order for the main GUI to be able
        to intercept possible error messages in the serial connection stage.
        """
        gAcquisitionWindow.__init__(self, **kwargs)
        self.ArduinoManager = wavegenArduinoManager()
        self.setupConnections()
        self.ArduinoManager.connect()
        if self.ArduinoManager.connected():
            self.powerDown()
            atexit.register(self.powerDown)
        else:
            self.disableControls()
        self.__Powered = False

    def setup(self):
        """ Overloaded setup method.
        """
        self.WavegenWidget = gWavegenWidget(self)
        self.addWidget(self.WavegenWidget)
        self.TransportBar = gTransportBar(self, tooltips = self.TOOLTIP_DICT)
        self.TransportBar.setTooltips(self.TOOLTIP_DICT)
        self.addWidget(self.TransportBar)

    def setupConnections(self):
        """ Setup the connections.
        """
        connect(self.TransportBar, 'quit()', self.close)
        connect(self.ArduinoManager, 'fatalError(QString)',
                self.popupFatalError)
        connect(self.TransportBar, 'start()', self.powerUp)
        connect(self.TransportBar, 'stop()', self.powerDown)
        connect(self.WavegenWidget.FrequencyDial, 'updated(float)',
                self.setFrequency)
        connect(self.WavegenWidget.PhaseDial, 'updated(float)',
                self.ArduinoManager.setPhase)

    def disableControls(self):
        """ Overloaded method.
        """
        gAcquisitionWindow.disableControls(self)
        self.WavegenWidget.setEnabled(False)

    def powerUp(self):
        """ Power up the wave generator.

        Note that we grab the appropriate output mode (i.e. square,
        triangular or sinusoidal) from the radio group on the main window.
        """
        self.ArduinoManager.powerUp(self.WavegenWidget.getMode())
        self.__Powered = True
        for button in [self.WavegenWidget.TriangleRadioButton,
                       self.WavegenWidget.SineRadioButton,
                       self.WavegenWidget.SquareRadioButton]:
            connect(button, 'clicked()', self.powerUp)

    def powerDown(self):
        """ Power down the wave generator.
        """
        self.ArduinoManager.powerDown()
        self.__Powered = False
        for button in [self.WavegenWidget.TriangleRadioButton,
                       self.WavegenWidget.SineRadioButton,
                       self.WavegenWidget.SquareRadioButton]:
            disconnect(button, 'clicked()', self.powerUp)

    def setFrequency(self, value):
        """ Set the output frequency.
        """
        self.ArduinoManager.setFrequency(value)
        if self.__Powered:
            self.powerUp()



def launch():
    """ Launch the main application.
    """
    mainWindow = wavegenWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = wavegenWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
