#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012-2013 Carmelo Sgro' (carmelo.sgro@pi.infn.it)   *
# * Copyright (C) 2012-2013 Luca Baldini (luca.baldini@pi.infn.it)    *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Pendulum View'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'PyQwt', 'pyserial', 'avrdude',
                    'arduino']
__shield__       = 'lab1'
__author__       = 'Carmelo Sgro'
__contact__      = 'carmelo.sgro@pi.infn.it'
__abstract__     = """Pendulum analog monitor.

Monitor the position of a (pair of) pendulum(s) whose tip is (are) immersed in
a water container acting as a voltage divider between ground and Vcc.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, __shield__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END

import sys
import math

import plasduino.arduino.protocol_h as ptcl

from PyQt4 import Qt, QtCore, QtGui
from plasduino.gui.gAnalogMonitorWindow import gAnalogMonitorWindow
from plasduino.gui.gStripChart import gStripChart
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.daq.gArduinoRunControl import gArduinoRunControl
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.daq.gRunningStat import gRunningStat
from plasduino.gui.gQt import connect, disconnect
from plasduino.__logging__ import logger



class pendulumviewEventBuffer(gEventBuffer):
    
    """ Specialized data buffer for the pendulumview module.
    """

    PROCESSED_DATA_FIELDS = ['Time A', 'Pos A', 'Time B', 'Pos B']
    PROCESSED_DATA_UNITS = ['s', 'au', 's', 'au']
    PROCESSED_DATA_FORMAT = ['%.3f', '%d', '%.3f', '%d']
    MIN_NUM_EVENTS = 2

    def fillDataTable(self):
        """ Reimplemented from the base class.

        Note that we change the format strings on the fly depending on
        whether the zero offset is applied (in which case the positions are
        float) or not (in which case they are integers).
        """
        samplePos = self[0]['position']
        if isinstance(samplePos, int):
            self.DataTable.FormatStrings = ['%.3f', '%d', '%.3f', '%d']
        elif isinstance(samplePos, float):
            self.DataTable.FormatStrings = ['%.3f', '%.2f', '%.3f', '%.2f']
        for i in xrange(1, len(self), 2):
            t1 = self[i-1]['seconds']
            P1 = self[i-1]['position'] 
            t2 = self[i]['seconds']
            P2 = self[i]['position']
            row = (t1, P1, t2, P2)
            self.DataTable.addRow(row)

    def getStats(self):
        """ Return the sample statistics of the data points.
        """
        stat1 = gRunningStat()
        stat2 = gRunningStat()
        for i in xrange(1, len(self), 2):
            stat1.fill(self[i-1]['adc'])
            stat2.fill(self[i]['adc'])
        return stat1, stat2



class pendulumviewRunControl(gArduinoRunControl):

    SKETCH_NAME = 'sktchAnalogSampling'
    INPUT_PIN_LIST = [4, 5]
    SAMPLING_INTERVAL = 50

    """ Specialized RunControl class for the pendulumview module.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        eventBuffer = pendulumviewEventBuffer()
        gArduinoRunControl.__init__(self, eventBuffer, maxSeconds)
        self.OffsetDict = {}
        for pin in self.INPUT_PIN_LIST:
            self.OffsetDict[pin] = 0

    def connectToArduino(self, timeout = None, autoUpload = True):
        errCode = gArduinoRunControl.connectToArduino(self, timeout, autoUpload)
        if not errCode:
            self.ArduinoManager.setupAnalogSamplingSketch(self.INPUT_PIN_LIST,
                                                       self.SAMPLING_INTERVAL)
        return errCode

    def poll(self):
        """ Implementation of the polling cycle on the PC side.

        Here we emit an additional readout() signal passing along the event
        number as an argument---so that the even itself can be easily retrieved
        in the update() slot of the main window, which is connected to the
        signal.
        """
        ctrl = self.readCtrlByte()
        if ctrl != ptcl.ANALOG_READOUT_HEADER:
            return ctrl
        readout = self.ArduinoManager.sreadAnalogReadout()
        self.addEvent(readout)
        readout['position'] = readout['adc'] -\
                              self.OffsetDict[readout['pinNumber']]
        self.emit(QtCore.SIGNAL('readout(int)'), self.getNumEvents() - 1)
        return ctrl

    def zeroOffsetInit(self, acquisitionSeconds = 3):
        """ Calibrate the offsets for the right and left channels.
        """
        logger.info('Starting zero-offset calibration...')
        timer = QtCore.QTimer(self)
        timer.setInterval(acquisitionSeconds*1000)
        timer.setSingleShot(True)
        connect(timer, 'timeout()', self.zeroOffsetDone)
        timer.start()
        self.emit(QtCore.SIGNAL('zeroOffsetInit()'))
        self.setRunning()
        
    def zeroOffsetDone(self):
        """ Finalize the zero offset procedure.
        """
        self.setStopped()
        self.emit(QtCore.SIGNAL('zeroOffsetDone()'))
        stats = self.EventBuffer.getStats()
        logger.info('Data sample statistics following...')
        for stat in stats:
            logger.info(stat)
        for i, pin in enumerate(self.INPUT_PIN_LIST):
            average = stats[i].getAverage()
            if average is not None:
                self.OffsetDict[pin] = average
                logger.info('Offset for pin %d set to %.2f' % (pin, average))
        logger.info('Zero-offset calibration finished.')
        


class pendulumviewWindow(gAnalogMonitorWindow):

    """ Main window for the pendulumview module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.*', 'daq.*', 'arduino.*']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gAnalogMonitorWindow.__init__(self, **kwargs)
        self.PlotWidget.setYTitle('Position [a.u.]')
        self.RunControl = pendulumviewRunControl()
        self.connectRunControl()
        self.insertMenu('Calibration', 'Configuration')
        self.addMenuAction('Calibration', 'Zero offset',
                           self.RunControl.zeroOffsetInit)
        self.RunControl.connectToArduino(timeout = 0.2)
        if self.RunControl.ArduinoManager.connected():
            self.setupConnections()
        for pinNumber in self.RunControl.INPUT_PIN_LIST:
            chartName = 'Pendulum on input pin A%d' % pinNumber
            self.addChart(chartName, pinNumber, markerSize = 0)

    def loadData(self, table):
        """ Overloaded class method.

        Load data from a table.
        """
        cols = table.getColumns()
        self.clearCharts()
        chartA = self.StripChartGroup[0]
        chartB = self.StripChartGroup[1]
        chartA.setData(cols[0], cols[1])
        chartB.setData(cols[2], cols[3])
        self.PlotWidget.replot()
        self.enableZooming()
            
    def setupConnections(self):
        """ Setup the connections.
        """
        connect(self.RunControl, 'readout(int)', self.updateCharts)
        connect(self.RunControl, 'started()', self.clearCharts)
        connect(self.RunControl, 'started()', self.disableZooming)
        connect(self.RunControl, 'stopped()', self.enableZooming)
        connect(self.RunControl, 'zeroOffsetInit()', self.zeroOffsetInit)
        connect(self.RunControl, 'zeroOffsetDone()', self.zeroOffsetDone)
  
    def zeroOffsetInit(self):
        """ Disable the transport bar and, if necessary, disconnect the
        promptCopyFile slot from the rawDataProcessed(QString) signal.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        if TOP_LEVEL_CONFIGURATION['daq.prompt-save-dialog']:
            logger.info('Disconnecting promptCopyFile()...')
            disconnect(self.RunControl, 'rawDataProcessed(QString)',
                       self.promptCopyFile)
        self.TransportBar.setStatus(gTransportBar.STATUS_DISABLED)

    def zeroOffsetDone(self):
        """ Set the transport bar in the stopped state and, if necessary,
        reconnect the promptCopyFile slot to the rawDataProcessed(QString)
        signal.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        if TOP_LEVEL_CONFIGURATION['daq.prompt-save-dialog']:
            logger.info('Reconnecting promptCopyFile()...')
            connect(self.RunControl, 'rawDataProcessed(QString)',
                    self.promptCopyFile)
        self.TransportBar.setStatus(gTransportBar.STATUS_STOPPED)

    def updateCharts(self, eventNumber):
         """ Synchronous update of the data display.
         """
         readout = self.RunControl.getEvent(eventNumber)
         pinNumber = readout['pinNumber']
         seconds = readout['seconds']
         position  = readout['position']
         try:
             chart = self.StripChartGroup.getChartByPin(pinNumber)
             chart.addDataPoint(seconds, position)
             self.StripChartGroup.adjustXRange()
             self.PlotWidget.replot()
         except KeyError:
             pass



def launch():
    """ Launch the main application.
    """
    mainWindow = pendulumviewWindow()
    mainWindow.show()
    return mainWindow



if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = pendulumviewWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
