#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Reaction Time'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4']
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Standalone module for measuring the human reaction time.

This is a simulation of the process of measuring the period of an
oscillator with a chronometer.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END


from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.daq.gRunControl import gRunControl
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.gui.gAnimationWindow import gAnimationWindow
from plasduino.gui.gQt import connect
from plasduino.__logging__ import logger



class reactiontimeEventBuffer(gEventBuffer):

    PROCESSED_DATA_FIELDS = ['Time']
    PROCESSED_DATA_UNITS = ['s']
    PROCESSED_DATA_FORMAT = ['%.3f']
    MIN_NUM_EVENTS = 1
    
    """ Specialized data buffer for the reactiontime module.
    """

    def fillDataTable(self):
        """ Reimplemented from the base class.
        """
        for event in self:
            row = (event['seconds'],)
            self.DataTable.addRow(row)

    def write(self, filePath):
        """ Do-nothing overload method (we don't have raw data to write).
        """
        pass



class reactiontimeRunControl(gRunControl):

    """ Specialized RunControl class for the reactiontime module.
    """

    def __init__(self,  maxSeconds = None):
        """ Constructor.
        """
        eventBuffer = reactiontimeEventBuffer()
        gRunControl.__init__(self, eventBuffer, maxSeconds)

    def run(self):
        """ Do-nothing overloaded method.

        As the animation (the equivalent of the polling cycle) is handled
        through a timer and a few signal/slots, we don't need any explicit
        multi-threading capability.
        """
        pass



class reactiontimeWindow(gAcquisitionWindow):

    """ Main window for the reactiontime module.
    """

    WINDOW_TITLE = __modulename__
    TOOLTIP_DICT = {'stop' : 'Stop the pendulum animation',
                    'start': 'Start the pendulum animation'
                    }
    MODULE_OPTIONS = ['gui.style', 'gui.skin', 'gui.language',
                      'daq.run-id-file-path', 'daq.log-file-dir',
                      'daq.data-file-dir', 'daq.prompt-save-dialog',
                      'daq.disable-log-file']

    def __init__(self, **kwargs):
        """ Constructor.

        Note that we need to add the data display in place of the transport
        bar and then re-allocate the transport bar itself to shift it down one
        place.
        """
        gAcquisitionWindow.__init__(self, **kwargs)
        self.RunControl = reactiontimeRunControl()
        self.connectRunControl()
        self.AnimationWindow = gAnimationWindow(self)
        connect(self.TransportBar, 'start()', self.showAnimation)
        connect(self.TransportBar, 'stop()', self.hideAnimation)
        connect(self.AnimationWindow, 'chronoStopped(double)', self.addEvent)
        connect(self.AnimationWindow, 'chronoDiscarded()', self.popEvent)

    def addEvent(self, t):
        """
        """
        event = {'seconds': t}
        self.RunControl.EventBuffer.fill(event)

    def popEvent(self):
        """
        """
        logger.info('Erasing the last time measurement from the data buffer...')
        self.RunControl.EventBuffer.pop()

    def showAnimation(self):
        self.AnimationWindow.show()
        self.AnimationWindow.start()

    def hideAnimation(self):
        """
        """
        self.AnimationWindow.stop()
        self.AnimationWindow.hide()

    #def setup(self):
    #    """ Setup the main window.
    #    """
    #    self.DataDisplay = diceDataDisplay(self)
    #    self.addWidget(self.DataDisplay)
    #    self.DataInput = diceDataInput(self)
    #    self.addWidget(self.DataInput)
    #    self.TransportBar = gTransportBar(self)
    #    self.TransportBar.setTooltips(self.TOOLTIP_DICT)
    #    self.addWidget(self.TransportBar)
    #    connect(self.TransportBar, 'quit()', self.close)

    #def synchronousUpdate(self):
    #    """ Update the data display.
    #    """
    #    self.DataDisplay.setNumEvents(self.RunControl.getNumEvents())
    #    self.DataDisplay.setElapsedTime(self.RunControl.getElapsedTime())



def launch():
    """ Launch the main application.
    """
    mainWindow = reactiontimeWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = reactiontimeWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
