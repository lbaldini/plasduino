#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Jacopo Nespolo <j.nespolo@gmail.com            *
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os.path
import imp

from PyQt4 import QtCore, QtGui
from plasduino.gui.gChooseModule import gChooseModuleDialog
from plasduino.gui.gQt import connect
from plasduino.__logging__ import logger, abort
from plasduino.modulemanager.__modulemanager__ import dumpModuleInfo,\
    loadModuleInfo, retrieveModuleInfo
from plasduino.__plasduino__ import PLASDUINO_DIST_MODULEINFO_FILE_PATH
from plasduino.__install__ import PLASDUINO_CUSTOM_MODULEINFO_FILE_PATH,\
    PLASDUINO_CUSTOM_MODULES

   
class plasduinoDialog(gChooseModuleDialog):

    """ Specialized dialog for plasduino launcher window.
    """
    
    WINDOW_TITLE = "Plasduino!"

    def __init__(self):
        """ Constructor.
        """
        gChooseModuleDialog.__init__(self, showStatusBar = False)
        # The pickle file with the module information for the modules
        # shipped with the distribution should be created by the post-install
        # script, so it should exists after an installation. This is really
        # for developers that clone the repo, so that they don't have
        # to run the update script manually.
        if not os.path.exists(PLASDUINO_DIST_MODULEINFO_FILE_PATH):
            folder = os.path.dirname(os.path.abspath(__file__))
            dumpModuleInfo(folder, PLASDUINO_DIST_MODULEINFO_FILE_PATH)
        self.DistInfoDict = loadModuleInfo(PLASDUINO_DIST_MODULEINFO_FILE_PATH)
        self.CustomInfoDict =\
            loadModuleInfo(PLASDUINO_CUSTOM_MODULEINFO_FILE_PATH)
        self.populateListWidget()
        connect(self.AddButton, 'released()',  self.addModules)
        connect(self.RemoveButton, 'released()', self.removeModule)
        connect(self.ListWidget, 'itemDoubleClicked(QListWidgetItem*)',
                self.listWidgetDoubleClicked)
        self.reset()

    def listWidgetDoubleClicked(self, item):
        """ Slot connected to the itemDoubleClicked(QListWidgetItem*) signal
        of the QListWidget in the dialog.
        
        Upon double click we just terminate the event loop and launch the
        selected module.
        """
        self.done(1)

    def populateListWidget(self):
        """ Scans the dictionary with module names and info and populates
        the listWidget.

        We also deselect all items, to start with.

        TODO: For the time being we put the distribution and custom module in
        the same list, but we can separate them if we want to.
        """
        self.ListWidget.clear()
        items = list(self.DistInfoDict.keys() + self.CustomInfoDict.keys())
        items.sort()
        self.ListWidget.addItems(items)
        self.ListWidget.clearSelection()

    def getModuleInfo(self, key):
        """ Utility function to search for a module info in both dist and
        custom dictionaries.
        """
        if key in self.DistInfoDict:
            return self.DistInfoDict[key]
        elif key in self.CustomInfoDict:
            return self.CustomInfoDict[key]
        else:
            abort('Could not retrieve module info for module %s.' % key)

    def getCurrentModuleInfo(self):
        """ Return the module information for the selected module.
        """
        key = str(self.ListWidget.currentItem().text())
        return self.getModuleInfo(key)

    def showDescription(self):
        """ Reads the active element of the listWidget and displays info in
        the right box.

        Also, enable/disable the remove module button depending on whether the
        selected module is custom or not.
        """
        self.setOpenButtonEnabled(True)
        key = str(self.ListWidget.currentItem().text())
        text = '%s' % (self.getModuleInfo(key))
        self.DescriptionLabel.setText(text)
        self.RemoveButton.setEnabled(key in self.CustomInfoDict)

    def dumpCustomInfoDict(self):
        """ Dump the custom info dictionary to file.
        """
        import pickle
        logger.info('Dumping module information to %s...' %\
                        PLASDUINO_CUSTOM_MODULEINFO_FILE_PATH)
        pickle.dump(self.CustomInfoDict,
                    open(PLASDUINO_CUSTOM_MODULEINFO_FILE_PATH, 'w'))
        logger.info('Done.')

    def addModules(self):
        """ Prompts a file browser from which to choose one or more files
        to install as plasduino modules.
        """
        from plasduino.gui.gFileDialog import getOpenFileList, FILE_TYPE_PY
        import plasduino.__utils__ as __utils__
        __utils__.createFolder(PLASDUINO_CUSTOM_MODULES)
        fileList = getOpenFileList([FILE_TYPE_PY], self, 'Select module(s)')
        for filePath in fileList:
            moduleInfo = retrieveModuleInfo(filePath)
            if moduleInfo is not None:
                moduleName = moduleInfo['name']
                logger.info('Adding %s to the custom modules...' % moduleName)
                self.CustomInfoDict[moduleName] = moduleInfo
        if len(fileList):
            self.dumpCustomInfoDict()
            self.populateListWidget()
            self.setOpenButtonEnabled(False)

    def removeModule(self):
        """ Removes currently selected module.

        Note that the remove button is only enabled when the current selection
        is a custom module.
        """
        moduleName = str(self.ListWidget.currentItem().text())
        self.ListWidget.clearSelection()
        self.setOpenButtonEnabled(False)
        try:
            del self.CustomInfoDict[moduleName]
            self.dumpCustomInfoDict()
            self.populateListWidget()
        except KeyError as error:
            logger.error(error)



if __name__ == '__main__':
    import plasduino.__cfgparse__ as __cfgparse__
    application = __cfgparse__.getapp()
    dialog = plasduinoDialog()
    if dialog.exec_():
        moduleInfo = dialog.getCurrentModuleInfo()
        executable = moduleInfo['executable']
        moduleName = os.path.basename(executable).replace('.py', '')
        try:
            module = imp.load_source(moduleName, executable)
        except IOError:
            logger.error('Could not import %s from %s.' %\
                             (moduleName, executable))
        mainWindow = module.launch()
        if mainWindow is not None:
            __cfgparse__.TOP_LEVEL_MODULE_OPTIONS = mainWindow.MODULE_OPTIONS
            application.exec_()
        __cfgparse__.TOP_LEVEL_MODULE_OPTIONS = None
        dialog.reset()


