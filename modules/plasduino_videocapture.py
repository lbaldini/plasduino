#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Video Capture'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4']
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Standalone module for capturing video from a webcam.

This is still experimental and far from being fully functional.
"""

#__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
#                             __dependencies__, author = __author__,
#                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END


import gobject

from plasduino.gui.gMainWindow import gMainWindow
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gVideocaptureWindow import gVideocaptureWindow
from plasduino.gui.gQt import connect


class videocaptureWindow(gMainWindow):

    """ Main window for the videocapture module.
    """

    #WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.*', 'daq.*']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gMainWindow.__init__(self, **kwargs)
        self.setup()
        self.CaptureWindow = gVideocaptureWindow(self)
        self.popupInfo('This module is largely experimental and you should '
                       'not expect it to be full (or even partially) '
                       'functional. Use it at the risk of your frustration.')

    def setup(self):
        """ Set up the necessary widgets in the window.
        """
        self.TransportBar = gTransportBar(self, tooltips = self.TOOLTIP_DICT)
        self.TransportBar.setTooltips(self.TOOLTIP_DICT)
        self.addWidget(self.TransportBar)
        connect(self.TransportBar, 'quit()', self.close)
        connect(self.TransportBar, 'start()', self.startCapture)
        connect(self.TransportBar, 'stop()', self.stopCapture)

    def startCapture(self):
        """ Start video capture.
        """
        self.CaptureWindow.show()
        self.CaptureWindow.open('/dev/video0')
        self.CaptureWindow.capture()

    def stopCapture(self):
        """ Stop video capture.
        """
        self.CaptureWindow.pause()
        self.CaptureWindow.hide()




def launch():
    """ Launch the main application.
    """
    # Note this is peculiar to the video capturing and cannot be removed
    # (at least not simply removed).
    gobject.threads_init()
    mainWindow = videocaptureWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = videocaptureWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 

