#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Dice'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4']
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Standalone module for throwing dice.

Based on the python pseudo-random number generator, the program allows to
simulate an arbitrary number of throws of an arbitrary number of dice,
the sum of the dice being the variable of interest.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END

import time
import random

from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.gui.gDataDisplay import gDataDisplay
from plasduino.gui.gDataInput import gDataInput
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gSpinBox import gSpinBox
from plasduino.daq.gRunControl import gRunControl
from plasduino.daq.gRunningStat import gRunningStat
from plasduino.__logging__ import logger
from plasduino.gui.gQt import connect



class diceEventBuffer(dict):
    
    """ Specialized data buffer for the dice module.

    This is very different from the data buffers germane to the data
    acquisition. It is essentially an hitogram in which we doi accumulate the
    occurrences from the rolling of the dice.
    """

    MIN_NUM_EVENTS = 1

    def __init__(self, numDice = 1, maxNumEvents = 100):
        """ Constructor.

        Fill an empty dictionary whose keys are the possible sums of the
        ouctomes of rolling a fixed number of dice. Effectively this is really
        a one-dimensional histogram with integer bins.

        Note that for this application---though we inherit from QObject since
        the RunControl needs it to setup the connections, we don't emit any
        full() signal, as the random number generator is too fast and we would
        have a whole bunch of extra events if we had to wait for the slot to
        receive and digest the signal. Rather, we check the full() condition
        directly in the RunControl polling loop.

        Also note that, though this class does not inherit from gEventBuffer
        (as the underlying container is a dictionary rather than a list), we
        tried to religiously adhere to the class layout in terms of interfaces.
        """
        dict.__init__(self)
        self.__RunningStat = gRunningStat()
        self.setNumDice(numDice)
        self.setMaxNumEvents(maxNumEvents)

    def getRunningStat(self):
        """ Return the gRunningStat object.
        """
        return self.__RunningStat

    def clear(self):
        """ Clear the buffer.
        
        Reset the bin contents and the total number of entries.
        """
        for i in range(self.__NumDice, 6*self.__NumDice + 1):
            self[i] = 0
        self.__NumRolls = 0
        self.__RunningStat.reset()

    def __len__(self):
        """ Overloaded method: return the number of rolls rather than the
        number of key/item pairs in the underlying dictionary (i.e., the number
        of bins in the histogram).
        """
        return self.__NumRolls

    def full(self):
        """ Return True if the buffer is full.
        """
        return len(self) >= self.__MaxNumEvents

    def setNumDice(self, numDice):
        """ Set the number of dices and clear the buffer.
        """
        self.__NumDice = numDice
        self.clear()

    def setMaxNumEvents(self, numEvents):
        """ Set the maximum number of events to be acquired.
        """
        logger.info('Setting the data buffer maximum size to %d.' % numEvents)
        self.__MaxNumEvents = numEvents

    def getNumDice(self):
        """ Return the number of dice.
        """
        return self.__NumDice

    def fill(self, value):
        """ Fill the buffer with one value.

        Increment the appropriate bin and the total number of entries.
        """
        self.__RunningStat.fill(value)
        self[value] += 1
        self.__NumRolls += 1

    def write(self, filePath):
        """ Do-nothing overloaded method.
        """
        pass

    def process(self, filePath):
        """ Nothing much to do, here, beside writing out the occurrences.
        """
        logger.info('Writing processed data to %s...' % filePath)
        outputFile = open(filePath, 'w')
        timestamp = time.asctime()
        header = '#\n# Written by plasduino.%s on %s.\n#\n' %\
            (self.__class__.__name__, timestamp)
        header += '# %s\n#\n' % self.__RunningStat
        outputFile.writelines(header)
        for i in range(self.__NumDice, 6*self.__NumDice + 1):
            outputFile.writelines('%d\t%d\n' % (i, self[i]))
        outputFile.close()
        logger.info('Done.')

    def __str__(self):
        """ String representation.
        """
        text = ''
        for i in range(self.__NumDice, 6*self.__NumDice + 1):
            text += '%4d: %d\n' % (i, self[i])
        return text.strip('\n')



class diceRunControl(gRunControl):

    """ Specialized RunControl class for the dice module.

    This is a fairly peculiar, stripped-down version of a run control as
    it does not have to communicate at all with arduino.

    We still need the gFiniteStateMachine- and QThread-like behavior, though.
    """

    MAX_NUM_DICE = 10000
    MAX_NUM_ROLL = 10000000

    def __init__(self,  maxSeconds = None):
        """ Constructor.
        """
        eventBuffer = diceEventBuffer()
        gRunControl.__init__(self, eventBuffer, maxSeconds)

    def _stop(self):
        """ Overloaded method.
        """
        gRunControl._stop(self)
        print('Occurrences:\n%s' % self.EventBuffer)
        print(self.EventBuffer.getRunningStat())

    def poll(self):
        """ Implementation of the polling cycle on the PC side.
        """
        numDice = self.EventBuffer.getNumDice()
        self.addEvent(sum([random.randint(1,6) for i in range(numDice)]))



class diceDataDisplay(gDataDisplay):
    
    """ Custom data display for the dice module.
    """
    
    def __init__(self, parent = None):
        """ Constructor.
        """
        gDataDisplay.__init__(self, parent)
        self.addRunIdField()
        self.addNumEventsField()
        self.addElapsedTimeField()
        self.setFieldDataFormat(self.ELAPSED_TIME_FIELD_NAME, '%.2f')



class diceDataInput(gDataInput):

    """ Custom data input for the dice module.
    """

    NUM_DICE_FIELD_NAME = 'num_dice'
    NUM_ROLLS_FIELD_NAME = 'num_rolls'
    
    def __init__(self, parent = None):
        """ Constructor.
        """
        gDataInput.__init__(self, parent)
        dataWidget = gSpinBox(self, minimum = 1, maximum = 100000, value = 1)
        self.addField(self.NUM_DICE_FIELD_NAME, 'Number of dice', dataWidget,
                      checkBox = False)
        dataWidget = gSpinBox(self, minimum = 1, maximum = 100000000,
                              value = 100, step = 10)
        self.addField(self.NUM_ROLLS_FIELD_NAME, 'Number of rolls', dataWidget,
                      checkBox = False)

    def getNumDice(self):
        """ Get the number of dice from the input widget.
        """
        return self.getFieldValue(self.NUM_DICE_FIELD_NAME)

    def getNumRolls(self):
        """ Get the number of rolls from the input widget.
        """
        return self.getFieldValue(self.NUM_ROLLS_FIELD_NAME)



class diceWindow(gAcquisitionWindow):

    """ Main window for the wheel module.
    """

    WINDOW_TITLE = __modulename__
    TOOLTIP_DICT = {'stop' : 'Stop rolling dice',
                    'start': 'Start rolling dice'
                    }
    MODULE_OPTIONS = ['gui.style', 'gui.skin', 'gui.language',
                      'daq.run-id-file-path', 'daq.log-file-dir',
                      'daq.data-file-dir', 'daq.prompt-save-dialog',
                      'daq.disable-log-file']

    def __init__(self, **kwargs):
        """ Constructor.

        Note that we need to add the data display in place of the transport
        bar and then re-allocate the transport bar itself to shift it down one
        place.
        """
        gAcquisitionWindow.__init__(self, **kwargs)
        self.RunControl = diceRunControl()
        connect(self.TransportBar, 'start()', self.setupEventBuffer)
        self.connectRunControl()

    def setupEventBuffer(self):
        """ Setup the run control parameters that are controlled by the
        widgets on the main window.
        """
        eventBuffer = self.RunControl.EventBuffer
        eventBuffer.setNumDice(self.DataInput.getNumDice())
        eventBuffer.setMaxNumEvents(self.DataInput.getNumRolls())

    def setup(self):
        """ Setup the main window.
        """
        self.DataDisplay = diceDataDisplay(self)
        self.addWidget(self.DataDisplay)
        self.DataInput = diceDataInput(self)
        self.addWidget(self.DataInput)
        self.TransportBar = gTransportBar(self)
        self.TransportBar.setTooltips(self.TOOLTIP_DICT)
        self.addWidget(self.TransportBar)
        connect(self.TransportBar, 'quit()', self.close)

    def synchronousUpdate(self):
        """ Update the data display.
        """
        self.DataDisplay.setNumEvents(self.RunControl.getNumEvents())
        self.DataDisplay.setElapsedTime(self.RunControl.getElapsedTime())



def launch():
    """ Launch the main application.
    """
    mainWindow = diceWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = diceWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
