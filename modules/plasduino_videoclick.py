#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import math

from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Video Click'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'videodec']
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Standalone module for analyzing video clips.

Fully-fledged module for opening video clips, exracting frames and tracking
the position of objects in the canvas by means of mouse clicks. The data can
be exported in plain text for offline analysis.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END


from PyQt4 import QtGui, QtCore
from plasduino.gui.gVideoclickWindow import gVideoclickWindow
from plasduino.gui.gVideoclickZoomPixmapWidget import \
    gVideoclickZoomPixmapWidget
from plasduino.gui.gVideoclickScaleDialog import gVideoclickScaleDialog
from plasduino.gui.gVideoclickOriginDialog import gVideoclickOriginDialog
from plasduino.gui.gQt import connect, disconnect
from plasduino.daq.gDataTable import gDataTable
from plasduino.daq.gVideoclickPoint import gVideoclickPoint
from plasduino.__logging__ import logger



class videoclickEventBuffer:
    
    """ Event buffer fot the videpoint module.

    Needless to say, this has nothing to do with the event buffers for the
    other modules, except for the fact that we didn't bother searching for
    a different name.

    This module is peculiar in that frames can be clicked in random order and
    can be overwritten one or more time. This would suggest some sparse data
    structure like a dictionary. Still, in the end we want to save the
    data preserving the frame ordering, so we go for a list (a list of lists,
    actually) instead. The event list is filled at the beginning with a
    bunch of None which get overwritten as the user clicks on the frame.
    This is highly inefficient, memor-wise, but with the numbers we're
    dealing with nobody will ever notice it.

    Note that the frames and the objects are numbered starting from 1
    rathen than 0, so we need to take this into account in the get/set methods.
    """

    def __init__(self, numFrames, numObjects, frameRate):
        """ Constructor.
        """
        self.NumFrames = numFrames
        self.NumObjects = numObjects
        self.FrameRate = frameRate
        self.RefSystemScale = None
        self.RefSystemOrigin = gVideoclickPoint(0, 0)
        self.clear()

    def empty(self):
        """ Return True if the buffer is empty (i.e. no position has been
        added, yet).
        """
        return self.__Empty

    def clear(self):
        """ Clear the data buffer (i.e., set all the positions to None).
        """
        logger.info('Clearing up the videoclick event buffer...')
        self.__TimestampList = []
        self.__PositionList = []
        for frameNumber in range(self.NumFrames):
            self.__TimestampList.append(frameNumber/float(self.FrameRate))
            self.__PositionList.append([None]*self.NumObjects)
        self.__Empty = True
        logger.info('Done.')

    def clearFrame(self, frameNumber):
        """ Clear a frame (i.e., set all the positions to None).
        """
        logger.info('Clearing frame %s...' % frameNumber)
        self.__PositionList[frameNumber - 1] = [None]*self.NumObjects

    def getTimestamp(self, frameNumber):
        """ Return the timestamp for a given frame.
        """
        return self.__TimestampList[frameNumber - 1]

    def getPosition(self, frameNumber, objectId):
        """ Return the position of a given object in a given frame.
        """
        return self.__PositionList[frameNumber - 1][objectId - 1]

    def getPositions(self, frameNumber):
        """ Return the list of object positions for a given frame.
        """
        return [self.getPosition(frameNumber, objectId) for \
                    objectId in range(1, self.NumObjects + 1)]
    
    def getAllPositions(self):
        """ Return a plain list of all the valid object positions register
        so far.
        """
        positions = []
        for frameNumber in range(self.NumFrames):
            for objectId in range(self.NumObjects):
                position = self.__PositionList[frameNumber - 1][objectId - 1]
                if position is not None:
                    positions.append(position)
        return positions

    def setPosition(self, frameNumber, objectId, position):
        """ Set the position for a given object in a given frame.
        """
        self.__PositionList[frameNumber - 1][objectId - 1] = position
        if self.hasCompleteFrameData(frameNumber):
            self.__Empty = False

    def hasNoFrameData(self, frameNumber):
        """ Return true if the buffer does not contain data for any of the
        objects in a given frame.
        """
        return self.getPositions(frameNumber) == [None]*self.NumObjects

    def hasFrameData(self, frameNumber):
        """ Return true if the buffer contains some data.
        """
        return not self.hasFrameData(frameNumber)

    def hasCompleteFrameData(self, frameNumber):
        """ Return true if the buffer contains valid data for all the objects in
        a given frame.
        """
        return not (None in self.getPositions(frameNumber))

    def hasIncompleteFrameData(self, frameNumber):
        """ Return true if the buffer contains incomplete data for a given
        frame.
        """
        return not self.hasNoFrameData(frameNumber) and \
            not self.hasCompleteFrameData(frameNumber)

    def getDataTable(self):
        """ Return a gDataTable object with the appropriate dimensions, header
        units and formats storing the underlying data to be written to file.
        """
        logger.info('Processing frame data and filling data table.')
        header = ['Frame', 'Time']
        units = ['', 's']
        fmtStrings = ['%d', '%.3f']
        for objectId in range(1, self.NumObjects + 1):
            header += ['x%d' % objectId, 'y%d' % objectId]
            if self.RefSystemScale is None:
                units += ['pixel', 'pixel']
                fmtStrings += ['%d', '%d']
            else:
                units += ['cm', 'cm']
                fmtStrings += ['%.2f', '%.2f']
        table = gDataTable(self.__class__.__name__, header, units, fmtStrings)
        for frameNumber in range(1, self.NumFrames + 1):
            if self.hasCompleteFrameData(frameNumber):
                row = [frameNumber, self.getTimestamp(frameNumber)]
                for position in self.getPositions(frameNumber):
                    row += position.getPhysicalCoordinates(self.RefSystemOrigin,
                                                           self.RefSystemScale)
                table.addRow(row)
        return table

    def writeTxt(self, filePath):
        """ Write the buffer to file in text format.
        """
        self.getDataTable().writeTxt(filePath)

    def writeCsv(self, filePath):
        """ Write the buffer to file in text format.
        """
        self.getDataTable().writeCsv(filePath)



class videoclickWindow(gVideoclickWindow):
    
    """ Main window for the videoclick module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.style', 'gui.skin', 'gui.language',
                      'daq.tmp-file-dir', 'video.*']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gVideoclickWindow.__init__(self, **kwargs)
        self.insertMenuAction('File', 'Save data to file', self.save, 2)
        self.getMenuAction('File', 'Save data to file').setEnabled(False)
        self.insertMenu('Reference system', 'Configuration')
        self.addMenuAction('Reference system', 'Set scale factor',
                           self.setRefSystemScale)
        self.addMenuAction('Reference system', 'Set origin',
                           self.setRefSystemOrigin)
        self.getMenu('Reference system').setEnabled(False)
        self.CurrentObject = None
        self.EventBuffer = None
        self.acceptMouseEvents()
        checkBox = self.OptionDisplay.getPhysicalUnitsCheckBox()
        connect(checkBox, 'stateChanged(int)', self.refreshDisplayPositions)
        checkBox = self.OptionDisplay.getAllPointsCheckBox()
        connect(checkBox, 'stateChanged(int)', self.refreshPixmapPositions)

    def frameContextMenu(self, point):
        """ Pop up the context menu to clear the current frame or the entire
        data buffer.
        """
        menu = QtGui.QMenu(self)
        action = menu.addAction('Clear current frame', self.clearCurrentFrame)
        if self.EventBuffer.hasNoFrameData(self.CurrentFrame):
            action.setEnabled(False)
        action = menu.addAction('Clear all frames', self.clearAllFrames)
        if self.EventBuffer.empty():
            action.setEnabled(False)
        menu.exec_(self.PixmapWidget.mapToGlobal(point))

    def clearCurrentFrame(self):
        """ Clear the current frame.
        """
        self.EventBuffer.clearFrame(self.CurrentFrame)
        self.setFrame(self.CurrentFrame)
        self.CurrentObject = 1

    def clearAllFrames(self):
        """ Clear all the frame in the event buffer.
        """
        self.EventBuffer.clear()
        self.setFrame(self.CurrentFrame)
        self.CurrentObject = 1

    def acceptMouseEvents(self):
        """ Setup the signal/slot connections for processing mouse events
        from the main pixmap widget.
        """
        connect(self.PixmapWidget, 'customContextMenuRequested(QPoint)',
                self.frameContextMenu)
        connect(self.PixmapWidget, 'frameLeftClicked(int, int)', self.leftClick)

    def ignoreMouseEvents(self):
        """ Disconnect from the mouse event signals from the main pixmap widget.
        """
        disconnect(self.PixmapWidget, 'customContextMenuRequested(QPoint)',
                   self.frameContextMenu)
        disconnect(self.PixmapWidget, 'frameLeftClicked(int, int)',
                   self.leftClick)

    def updateSystemDisplay(self):
        """ Update the system display according to the settings of the event
        buffer.
        """
        self.SystemDisplay.update(self.EventBuffer)

    def setRefSystemScale(self):
        """ Show the dialog to set the reference system scale.

        Mind that if the widgets to show the coordinates in physical units
        are not enabled and the dialog is closed through the Save button,
        we do enable the widgets themselves as the dialog disappear.
        """
        # Disable normal mouse events on the main pixmap.
        self.ignoreMouseEvents()
        # Get rid of the data points, if any.
        self.refreshPixmap(False)
        # Create the dialog and show it.
        dialog = gVideoclickScaleDialog(self)
        connect(dialog, 'finished(int)', self.scaleDialogClosed)
        dialog.show()

    def scaleDialogClosed(self, status):
        """ Slot to be executed as soon as the reference system scale dialog
        is closed.
        """
        # Put back the data points on the main pixmap.
        self.refreshPixmap(True)
        # Restore the mouse events.
        self.acceptMouseEvents()
        # Do we need to enable the check box for the physical units?
        if not self.OptionDisplay.isPhysicalUnitsEnabled():
            self.OptionDisplay.setPhysicalUnitsEnabled(status)
            self.OptionDisplay.setPhysicalUnitsChecked(status)

    def setRefSystemOrigin(self):
        """ Show the dialog to set the reference system origin.
        """
        # Disable normal mouse events on the main pixmap.
        self.ignoreMouseEvents()
        # Get rid of the data points, if any.
        self.refreshPixmap(False)
        # Create the dialog and show it.
        dialog = gVideoclickOriginDialog(self)
        connect(dialog, 'finished(int)', self.originDialogClosed)
        dialog.show()

    def originDialogClosed(self, status):
        """ Slot to be executed as soon as the reference system origin dialog
        is closed.
        """
        # Put back the data points on the main pixmap.
        self.refreshPixmap(True)
        # Restore the mouse events.
        self.acceptMouseEvents()

    def openClipFinalize(self):
        """ Overloaded method from the base class.

        Note that the base class always calls its own setFrame() method
        and, as the overloaded one involved the event buffer, we have
        to call again self.setFrame(1) after the buffer has been creted.
        """
        gVideoclickWindow.openClipFinalize(self)
        numFrames = self.VideoDecoder.getNumFrames()
        frameRate = self.VideoDecoder.FrameRate
        self.EventBuffer = videoclickEventBuffer(numFrames, self.NumObjects,
                                                 frameRate)
        self.updateSystemDisplay()
        self.setFrame(1)
        self.getMenu('Reference system').setEnabled(True)
        
    def closeClip(self):
        """ Overloaded method from the base class.
        """
        gVideoclickWindow.closeClip(self)
        self.EventBuffer = None
        self.getMenuAction('File', 'Save data to file').setEnabled(False)
        self.getMenu('Reference system').setEnabled(False)

    def save(self):
        """ Save the current data file.
        """
        from plasduino.gui.gFileDialog import getSaveFilePath, FILE_TYPE_TXT,\
            FILE_TYPE_CSV
        filePath = getSaveFilePath([FILE_TYPE_TXT, FILE_TYPE_CSV], self,
                                   'Save data')
        if filePath is not None:
            if filePath.endswith(FILE_TYPE_TXT.Extension):
                self.EventBuffer.writeTxt(filePath)
            elif filePath.endswith(FILE_TYPE_CSV.Extension):
                self.EventBuffer.writeCsv(filePath)

    def setFrame(self, frameNumber):
        """ Set the frame to be displayed.
        
        While all the logic related to the GUI is encapsulated in the base
        class, here we update in addition the stuff related to the
        actual data.
        """
        if self.EventBuffer.hasIncompleteFrameData(self.CurrentFrame):
            msg = 'You are about to leave a frame with incomplete data '\
                '(i.e., you did not click on all of the objects). '\
                'Recall that frames with incomplete data won\'t be written '\
                'to file. If you need to change the number of objects to be '\
                'tracked you can do so from the "Video" tab in the '\
                '"Configuration->Change settings" drop-down menu.'
            self.popupInfo(msg)
        gVideoclickWindow.setFrame(self, frameNumber)
        self.drawReferenceSystem()
        self.CurrentObject = 1
        self.DataDisplay.setFrameNumber('%d/%d' % (frameNumber,
                                                   self.EventBuffer.NumFrames))
        timestamp = self.EventBuffer.getTimestamp(frameNumber)
        self.DataDisplay.setElapsedTime(timestamp)
        for objectId in range(1, self.NumObjects + 1):
            position = self.EventBuffer.getPosition(frameNumber, objectId)
            self.displayObjectPosition(objectId, position)
            if position is not None:
                self.drawPoint(position, 'Object %d' % objectId)
        if self.OptionDisplay.isAllPointsChecked():
            self.drawAllPoints()

    def drawAllPoints(self):
        """ Overimpose all the data points on the current frame.
        """
        currentPositions = self.EventBuffer.getPositions(self.CurrentFrame)
        for position in self.EventBuffer.getAllPositions():
            if position not in currentPositions:
                self.drawPoint(position, coordinates = False, cross = True)

    def drawPoint(self, point, coordinates = True, text = '', cross = False):
        """ Draw a point on the pixmap, possibly with a text label.
        """
        crossSize = 3
        pixmap = self.PixmapWidget.pixmap()
        painter =  QtGui.QPainter(pixmap)
        if cross:
            painter.setPen(QtGui.QPen(QtCore.Qt.white, 1))
            painter.drawLine(point.X - crossSize, point.Y,
                             point.X + crossSize, point.Y)
            painter.drawLine(point.X, point.Y - crossSize,
                             point.X, point.Y + crossSize)
        else:
            painter.setPen(QtGui.QPen(QtCore.Qt.white, 2))
            painter.drawEllipse(point.X, point.Y, 2, 2)
        if coordinates:
            text = '%s %s' % (text, point)
            painter.drawText(point.X - 10, point.Y - 10, text.strip())
        del painter
        self.repaint()

    def drawLine(self, point1, point2):
        """ Draw a line connectring two points on the pixmap widget.
        """
        pixmap = self.PixmapWidget.pixmap()
        painter =  QtGui.QPainter(pixmap)
        painter.setPen(QtGui.QPen(QtCore.Qt.white, 1))
        painter.drawLine(point1.X, point1.Y, point2.X, point2.Y)
        del painter
        self.repaint()

    def drawReferenceSystem(self, offset = 10, length = 30, padding = 4):
        """ Draw the reference system.
        """
        pixmap = self.PixmapWidget.pixmap()
        painter =  QtGui.QPainter(pixmap)
        painter.setPen(QtGui.QPen(QtCore.Qt.white, 1))
        painter.drawLine(offset, offset, offset + length, offset)
        painter.drawLine(offset, offset, offset, offset + length)
        painter.drawText(offset + length + padding, offset + padding, 'x')
        painter.drawText(offset - padding, offset + length + 3*padding, 'y')
        del painter
        self.repaint()        

    def displayObjectPosition(self, objectId, position):
        """ Display the position of a given object in the data display.

        This has become more complicated when we decided to be able to
        switch between raw and physical coordinates. At that point we
        decided to wrap the code into a separate function.
        """
        # If the position is not set, this will show the default value
        # of the data display widget.
        if position is None:
            self.DataDisplay.setObjectPosition(objectId, position)
            return
        # Otherwise we need to figure out whether we are displaying
        # calibrated or raw coordinates.
        if self.OptionDisplay.isPhysicalUnitsChecked():
            origin = self.EventBuffer.RefSystemOrigin
            scale = self.EventBuffer.RefSystemScale
            coords = position.getPhysicalCoordinates(origin, scale)
            value = '(%.1f, %.1f)' % coords
        else:
            coords = position.getRawCoordinates()
            value = '(%d, %d)' % coords
        # Finally, we have to take care of the units.
        enabled = self.OptionDisplay.isPhysicalUnitsChecked()
        self.DataDisplay.setPhysicalUnitsEnabled(enabled)
        self.DataDisplay.setObjectPosition(objectId, value)

    def refreshDisplayPositions(self):
        """ Refresh the object positions in the data display.
        """
        for objectId in range(1, self.NumObjects + 1):
            position = self.EventBuffer.getPosition(self.CurrentFrame, objectId)
            self.displayObjectPosition(objectId, position)

    def refreshPixmap(self, drawDataPoints):
        """ Refresh the current pixmap, optionally drawing the data in the
        event buffer for the current frame.
        """
        gVideoclickWindow.setFrame(self, self.CurrentFrame)
        self.drawReferenceSystem()
        if drawDataPoints:
            for objectId in range(1, self.NumObjects + 1):
                position = self.EventBuffer.getPosition(self.CurrentFrame,
                                                        objectId)
                if position is not None:
                    self.drawPoint(position, 'Object %d' % objectId)

    def refreshPixmapPositions(self, drawAllPoints):
        """ Refrest the pixmap and redraw the data points.
        """
        self.refreshPixmap(True)
        if drawAllPoints:
            self.drawAllPoints()

    def leftClick(self, x, y):
        """ Slot connected to a left mouse click on the pixmap widget.

        This is actually one of the main methods of the derived class.
        When we click on the pixmap we want to:
        * retrieve the position of the mouse click;
        * display the position on the data display;
        * set the position in the proper object in the event buffer;
        * advance the current object and, if necessary, the current frame.

        Note that we do buffer in time the operations via a QTimer that
        is instantiated at each click and only lives in this scope (so that
        we don't have to bother disconnecting signals and slots).
        We disconnect the slots receiving the mouse events as soon as we enter
        the slot and we reconnect them at the timeout of the timer. This is
        to ensure a minimum time between two valid clicks and avoid
        double-clicks, that we don't want in this context.
        At the last object we connect the timer to the next() slot and set
        the timeout to a larger value so that the last position pertists for
        some time on the data display.
        """
        # Disconnect the mouse signals.
        self.ignoreMouseEvents()
        # Are we clicking on the first object of a frame for which the first
        # object has already a position set? If so: clear the frame in the
        # event buffer and call setFrame()---which repaints the frame and
        # set the data display values.
        if self.CurrentObject == 1 and \
                self.EventBuffer.getPosition(self.CurrentFrame, 1) is not None:
            self.EventBuffer.clearFrame(self.CurrentFrame)
            self.setFrame(self.CurrentFrame)
        # Setup the timer.
        timer = QtCore.QTimer(self)
        timer.setSingleShot(True)
        connect(timer, 'timeout()', self.acceptMouseEvents)
        self.showMessage('Left mouse click at (%d, %d)' % (x, y))
        # Grab the position, update the data display, update the event buffer
        # and draw the point.
        position = gVideoclickPoint(x, y)
        self.displayObjectPosition(self.CurrentObject, position)
        self.EventBuffer.setPosition(self.CurrentFrame, self.CurrentObject,
                                     position)
        self.drawPoint(position, 'Object %d' % self.CurrentObject)
        # Do we have enough points for the File->Save action to make sense?
        if not self.getMenuAction('File', 'Save data to file').isEnabled() and \
                not self.EventBuffer.empty():
            self.getMenuAction('File', 'Save data to file').setEnabled(True)
        # Update the object counter and start the timer.
        if self.CurrentObject == self.NumObjects and \
                self.CurrentFrame != self.EventBuffer.NumFrames:
            self.CurrentObject = 1
            timer.setInterval(750)
            connect(timer, 'timeout()', self.next)
        else:
            self.CurrentObject += 1
            timer.setInterval(250)
        timer.start()



def launch():
    """ Launch the main application.
    """
    mainWindow = videoclickWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = videoclickWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_()
