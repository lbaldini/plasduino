#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Pendulum'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'pyserial', 'avrdude', 'arduino']
__shield__       = 'lab1'
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Module for studying the period of a pendulum.

Based on a single photo-gate, it allows the measurement of the period of a
pendulum and the transit time at the bottom of the oscillation.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, __shield__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END

import sys

from PyQt4 import QtGui
from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.daq.gArduinoRunControl import gArduinoRunControl
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.daq.gDigitalTransition import gDigitalTransition
from plasduino.__logging__ import logger
from plasduino.arduino.plasduino_h import INTERRUPT_MODE_DISABLE,\
    INTERRUPT_MODE_ENABLE_CHANGE

class pendulumSimpleEventBuffer(gEventBuffer):
    
    """ Specialized data buffer for the pendulum module.
    """

    PROCESSED_DATA_FIELDS = ['Id', 'Edge', 'Time']
    PROCESSED_DATA_UNITS = ['', 'b', 's']
    PROCESSED_DATA_FORMAT = ['%d', '%d', '%.6f']
    MIN_NUM_EVENTS = 1

    def fillDataTable(self):
        """Ultra simple implementation of the data processing: do nothing.
        Just store Id, absolute time, and tansition egde: 
        0 when flag enter the gante and 1 when exit.

        Mind we skip the first edge if the polarity is wrong---i.e. the
        data acquisition is started with the flag covering the optical gate.
        """
        logger.info('Processing algorithm: simple.')
        start = 0
        if self[0]['edge'] == gDigitalTransition.RISING_EDGE:
            logger.info('Wrong edge on the first transition, skipping...')
            start += 1
        for i in xrange(start, len(self), 1):
            row = (i, self[i]['edge'], self[i]['seconds'])
            self.DataTable.addRow(row)
            
            
class pendulumEventBuffer(gEventBuffer):
    
    """ Specialized data buffer for the pendulum module.
    """

    PROCESSED_DATA_FIELDS = ['Time', 'Period', 'Transit time']
    PROCESSED_DATA_UNITS = ['s', 's', 's']
    PROCESSED_DATA_FORMAT = ['%.6f', '%.6f', '%.6f']
    MIN_NUM_EVENTS = 6

    
    def __fillDataTablePlain(self):
        """ Basic implementation of the data processing. We do attempt any
        average for the transit time and, therefore, expect to observe
        small "jumps" up and down if the optical gate is not aligned with the
        pendulum.

        Mind we skip the first edge if the polarity is wrong---i.e. the
        data acquisition is started with the flag covering the optical gate.
        """
        logger.info('Processing algorithm: plain.')
        start = 3
        if self[0]['edge'] == gDigitalTransition.RISING_EDGE:
            logger.info('Wrong edge on the first transition, skipping...')
            start += 1
        for i in xrange(start, len(self) - 1, 2):
            t = 0.5*(self[i]['seconds'] + self[i-1]['seconds'])
            period = self[i+1]['seconds'] - self[i-3]['seconds']
            transitTime = self[i]['seconds'] - self[i-1]['seconds']
            row = (t, period, transitTime)
            self.DataTable.addRow(row)        

    def __fillDataTableAdvanced(self):
        """ Slightly more advanced implementation where do some
        averaging.

        Mind we skip the first edge if the polarity is wrong---i.e. the
        data acquisition is started with the flag covering the optical gate.
   
        """
        logger.info('Processing algorithm: advanced.')
        start = 5
        if self[0]['edge'] == gDigitalTransition.RISING_EDGE:
            logger.info('Wrong edge on the first transition, skipping...')
            start += 1
        for i in xrange(start, len(self) - 3, 2):
            t1 = 0.5*(self[i-4]['seconds'] + self[i-5]['seconds'])
            t2 = 0.5*(self[i-2]['seconds'] + self[i-3]['seconds'])
            t3 = 0.5*(self[i  ]['seconds'] + self[i-1]['seconds'])
            t4 = 0.5*(self[i+2]['seconds'] + self[i+1]['seconds'])
            dt2 = self[i-2]['seconds'] - self[i-3]['seconds']
            dt3 = self[i]['seconds'] - self[i-1]['seconds']
            t = 0.5*(t2 + t3)
            period = 0.5*(t3 - t1 + t4 - t2)
            transitTime = 0.5*(dt2 + dt3)
            row = (t, period, transitTime)
            self.DataTable.addRow(row)
        

    def fillDataTable(self):
        """ Reimplemented from the base class.

        You get to choose from the methods available above.
        """
        self.__fillDataTableAdvanced()
        



class pendulumRunControl(gArduinoRunControl):

    SKETCH_NAME = 'sktchDigitalTimer'
    INTERRUPT_MODE_0 = INTERRUPT_MODE_ENABLE_CHANGE
    INTERRUPT_MODE_1 = INTERRUPT_MODE_DISABLE

    """ Specialized RunControl class for the pendulum module.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        AdvancedProc = TOP_LEVEL_CONFIGURATION.get('daq.advanced-processing')
        if AdvancedProc:
            eventBuffer = pendulumEventBuffer()
        else:
            eventBuffer = pendulumSimpleEventBuffer()
        gArduinoRunControl.__init__(self, eventBuffer, maxSeconds)

    def connectToArduino(self, timeout = None, autoUpload = True):
        """ Overloaded method.
        """
        errCode = gArduinoRunControl.connectToArduino(self, timeout, autoUpload)
        if not errCode:
            self.ArduinoManager.setupDigitalTimerSketch(self.INTERRUPT_MODE_0,
                                                        self.INTERRUPT_MODE_1)
        return errCode

    def poll(self):
        """ Implementation of the polling cycle on the PC side.
        """
        return gArduinoRunControl.pollAlternateDigitalTransitions(self)



class pendulumWindow(gAcquisitionWindow):

    """ Main window for the pendulum module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.*', 'daq.*', 'arduino.*']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gAcquisitionWindow.__init__(self, **kwargs)
        self.RunControl = pendulumRunControl()
        self.connectRunControl()
        self.RunControl.connectToArduino(timeout = 0.2)



def launch():
    """ Launch the main application.
    """
    mainWindow = pendulumWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = pendulumWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
