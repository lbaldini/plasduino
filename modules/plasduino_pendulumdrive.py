#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012-2013 Carmelo Sgro' (carmelo.sgro@pi.infn.it)   *
# * Copyright (C) 2012-2013 Luca Baldini (luca.baldini@pi.infn.it)    *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Pendulum Drive'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'PyQwt', 'pyserial', 'avrdude',
                    'arduino']
__shield__       = 'lab1'
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Driven pendulum analog monitor.

This is essentially acting as the plasduino_pendulumview monitor, except
that it also drives a motor acting as an external force.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, __shield__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END

import sys
import math
import atexit

import plasduino.arduino.protocol_h as ptcl

from PyQt4 import Qt, QtCore, QtGui
from plasduino.gui.gAnalogMonitorWindow import gAnalogMonitorWindow
from plasduino.gui.gPlotWidget import gPlotWidget
from plasduino.gui.gAcquisitionDataDisplay import gSynchAcquisitionDataDisplay
from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gPwmDialWidget import gPwmDialWidget
from plasduino.daq.gArduinoRunControl import gArduinoRunControl
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.gui.gQt import connect, disconnect
from plasduino.__logging__ import logger



class pendulumdriveEventBuffer(gEventBuffer):
    
    """ Specialized data buffer for the pendulumdrive module.
    """

    PROCESSED_DATA_FIELDS = ['Time', 'Pos']
    PROCESSED_DATA_UNITS = ['s', 'au']
    PROCESSED_DATA_FORMAT = ['%.3f', '%d']
    MIN_NUM_EVENTS = 1

    def fillDataTable(self):
        """ Reimplemented from the base class.

        Note that we change the format strings on the fly depending on
        whether the zero offset is applied (in which case the positions are
        float) or not (in which case they are integers).
        """
        samplePos = self[0]['position']
        if isinstance(samplePos, int):
            self.DataTable.FormatStrings = ['%.3f', '%d']
        elif isinstance(samplePos, float):
            self.DataTable.FormatStrings = ['%.3f', '%.2f']
        for event in self:
            row = (event['seconds'], event['position'])
            self.DataTable.addRow(row)



class pendulumdriveRunControl(gArduinoRunControl):

    SKETCH_NAME = 'sktchPendulumDrive'
    INPUT_PIN = 5
    SAMPLING_INTERVAL = 50

    """ Specialized RunControl class for the pendulumdrive module.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        eventBuffer = pendulumdriveEventBuffer()
        gArduinoRunControl.__init__(self, eventBuffer, maxSeconds)
        self.Offset = 0

    def connectToArduino(self, timeout = None, autoUpload = True):
        """ Overloaded method.
        """
        errCode = gArduinoRunControl.connectToArduino(self, timeout, autoUpload)
        if not errCode:
            self.ArduinoManager.swritecmd(ptcl.OP_CODE_SELECT_ANALOG_PIN,
                                          'B', self.INPUT_PIN)
            self.ArduinoManager.swritecmd(ptcl.OP_CODE_SELECT_SAMPLING_INTERVAL,
                                          'I', self.SAMPLING_INTERVAL)
        return errCode

    def setPwmDutyCycle(self, value):
        """ Set the PWM duty cycle on the proper pin.
        """
        payload = int(value)
        logger.info('Setting PWM duty cycle to %d/255...' % payload)
        self.ArduinoManager.swriteuint8(ptcl.OP_CODE_SELECT_PWM_DUTY_CYCLE)
        self.ArduinoManager.swriteuint8(payload)

    def disablePwmOutput(self):
        """ Disable the PWM, i.e. set the duty cycle to 0.
        """
        self.setPwmDutyCycle(0)

    def poll(self):
        """ Implementation of the polling cycle on the PC side.

        Here we emit an additional readout() signal passing along the event
        number as an argument---so that the even itself can be easily retrieved
        in the update() slot of the main window, which is connected to the
        signal.
        """
        ctrl = self.readCtrlByte()
        if ctrl != ptcl.ANALOG_READOUT_HEADER:
            return ctrl
        readout = self.ArduinoManager.sreadAnalogReadout()
        self.addEvent(readout)
        readout['position'] = readout['adc'] - self.Offset
        self.emit(QtCore.SIGNAL('readout(int)'), self.getNumEvents() - 1)
        return ctrl

    def zeroOffsetInit(self, acquisitionSeconds = 3):
        """ Calibrate the offsets for the right and left channels.
        """
        logger.info('Starting zero-offset calibration...')
        timer = QtCore.QTimer(self)
        timer.setInterval(acquisitionSeconds*1000)
        timer.setSingleShot(True)
        connect(timer, 'timeout()', self.zeroOffsetDone)
        timer.start()
        self.emit(QtCore.SIGNAL('zeroOffsetInit()'))
        self.setRunning()
        
    def zeroOffsetDone(self):
        """ Finalize the zero offset procedure.
        """
        self.setStopped()
        self.emit(QtCore.SIGNAL('zeroOffsetDone()'))
        stat = self.EventBuffer.getStat('adc')
        logger.info(stat)
        average = stat.getAverage()
        if average is not None:
            self.Offset = average
            logger.info('Offset set to %.2f' % self.Offset)
        logger.info('Zero-offset calibration finished.')
        


class pendulumdriveWindow(gAnalogMonitorWindow):

    """ Main window for the pendulumdrive module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.*', 'daq.*', 'arduino.*']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        kwargs['numLayoutColumns'] = 5
        gAnalogMonitorWindow.__init__(self, **kwargs)
        self.PlotWidget.setYTitle('Position [a.u.]')
        self.RunControl = pendulumdriveRunControl()
        self.connectRunControl()
        self.insertMenu('Calibration', 'Configuration')
        self.addMenuAction('Calibration', 'Zero offset',
                           self.RunControl.zeroOffsetInit)
        self.RunControl.connectToArduino(timeout = 0.2)
        if self.RunControl.ArduinoManager.connected():
            self.setupConnections()
            self.RunControl.disablePwmOutput()
            atexit.register(self.RunControl.disablePwmOutput)
        chartName = 'Pendulum on input pin A%d' % self.RunControl.INPUT_PIN
        self.addChart(chartName, self.RunControl.INPUT_PIN, markerSize = 0)

    def loadData(self, table):
        """ Overloaded class method.

        Load data from a table.
        """
        cols = table.getColumns()
        self.clearCharts()
        chart = self.StripChartGroup[0]
        chart.setData(cols[0], cols[1])
        self.PlotWidget.replot()
        self.enableZooming()

    def disableControls(self):
        """ Overloaded method.
        """
        gAnalogMonitorWindow.disableControls(self)
        self.PwmDialWidget.setEnabled(False)

    def setup(self):
        """ Setup the widgets on the gui.
        """
        self.PlotWidget = gPlotWidget(self, xTitle = 'Time [s]')
        self.DataDisplay = gSynchAcquisitionDataDisplay(self)
        self.TransportBar = gTransportBar(self, tooltips = \
                                              gAcquisitionWindow.TOOLTIP_DICT)
        self.PwmDialWidget = gPwmDialWidget(self, title = '', minDac = 75,
                                            dialTitle = 'Motor speed [DAC]')
        connect(self.TransportBar, 'quit()', self.close)
        self.addWidget(self.PlotWidget, 3, 0, 1, 4)
        self.addWidget(self.PwmDialWidget, 3, 4, align = QtCore.Qt.AlignTop)
        self.addWidget(self.TransportBar, 4, 0, align = QtCore.Qt.AlignTop)
        self.TransportBar.setMaximumWidth(350)
        self.addItem(QtGui.QSpacerItem(150, 0), 4, 1)
        self.addWidget(self.DataDisplay, 4, 3, align = QtCore.Qt.AlignTop)
            
    def setupConnections(self):
        """ Setup the connections.
        """
        connect(self.RunControl, 'readout(int)', self.updateCharts)
        connect(self.RunControl, 'started()', self.clearCharts)
        connect(self.RunControl, 'started()', self.disableZooming)
        connect(self.RunControl, 'stopped()', self.enableZooming)
        connect(self.RunControl, 'zeroOffsetInit()', self.zeroOffsetInit)
        connect(self.RunControl, 'zeroOffsetDone()', self.zeroOffsetDone)
        connect(self.PwmDialWidget, 'updated(int)',
                self.RunControl.setPwmDutyCycle)
 
    def zeroOffsetInit(self):
        """ Disable the transport bar and, if necessary, disconnect the
        promptCopyFile slot from the rawDataProcessed(QString) signal.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        if TOP_LEVEL_CONFIGURATION['daq.prompt-save-dialog']:
            disconnect(self.RunControl, 'rawDataProcessed(QString)',
                       self.promptCopyFile)
        self.TransportBar.setStatus(gTransportBar.STATUS_DISABLED)

    def zeroOffsetDone(self):
        """ Set the transport bar in the stopped state and, if necessary,
        reconnect the promptCopyFile slot to the rawDataProcessed(QString)
        signal.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        if TOP_LEVEL_CONFIGURATION['daq.prompt-save-dialog']:
            connect(self.RunControl, 'rawDataProcessed(QString)',
                    self.promptCopyFile)
        self.TransportBar.setStatus(gTransportBar.STATUS_STOPPED)

    def updateCharts(self, eventNumber):
         """ Synchronous update of the data display.
         """
         readout = self.RunControl.getEvent(eventNumber)
         pinNumber = readout['pinNumber']
         seconds = readout['seconds']
         position  = readout['position']
         try:
             chart = self.StripChartGroup.getChartByPin(pinNumber)
             chart.addDataPoint(seconds, position)
             self.StripChartGroup.adjustXRange()
             self.PlotWidget.replot()
         except KeyError:
             pass



def launch():
    """ Launch the main application.
    """
    mainWindow = pendulumdriveWindow()
    mainWindow.show()
    return mainWindow



if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = pendulumdriveWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
