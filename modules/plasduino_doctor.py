#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'doctor'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4']
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Installation diagnostic.

Small utility module making sure that all the python packages, along with the
other programs necessary to succesfully run plasduino, are correctly installed
on the system.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END


import plasduino.__utils__ as __utils__

from plasduino.__logging__ import logger
from plasduino.gui.gMessageBox import gMessageBox


UNDEFINED = -1
SUCCESS   = 0
WARNING   = 1
ERROR     = 2


class DiagnosticInfo:

    """ Small utility class encapsulating some diagnostic informations
    related to the python modules and programs necessary to plasduino.
    """

    def __init__(self):
        """ Constructor.
        """
        self.Notes = []
        self.Warnings = []
        self.Errors = []

    def note(self, message):
        """ Log a note message.
        """
        self.Notes.append(message)

    def warn(self, message):
        """ Log a warning message.
        """
        self.Warnings.append(message)

    def error(self, message):
        """ Log an error message.
        """
        self.Errors.append(message)

    def __str__(self):
        """ Log out the summary.
        """
        if len(self.Errors) or len(self.Warnings) or len(self.Notes):
            text = 'Summary of diagnostic information\n\n'
        else:
            text = 'Congratulations, you have everything you need!'
        if len(self.Errors):
            text += 'Errors:\n'
            for i, message in enumerate(self.Errors):
                text += '%d) %s\n' % (i + 1, message)
        if len(self.Warnings):
            text += 'Warnings:\n'
            for i, message in enumerate(self.Warnings):
                text += '%d) %s\n' % (i + 1, message)
        if len(self.Notes):
            text += 'Notes:\n'
            for i, message in enumerate(self.Notes):
                text += '%d) %s\n' % (i + 1, message)
        return text


DIAG_INFO = DiagnosticInfo()


def checkPythonModule(moduleName, required = True):
    """ Check the availability of a python module.
    """
    logger.info('Checking for python module %s...' % moduleName)
    cmd =\
"""
try:
    import %s
    module = eval(moduleName)
except ImportError, exception:
    module = None
""" % (moduleName)
    exec(cmd)
    if module is not None:
        modulePath = os.path.dirname(module.__file__)
        logger.info('Module %s found in %s!' % (moduleName, modulePath))
        return SUCCESS
    else:
        message = 'python module %s not installed.' % moduleName
        if required:
            logger.error('Could not find %s.' % moduleName)
            DIAG_INFO.error(message)
            return ERROR
        else:
            logger.warn('Could not find %s.' % moduleName)
            DIAG_INFO.warn(message)
            return WARNING

def checkProgram(programName, required = True):
    """ Check the availability of a program.
    """
    logger.info('Checking for program %s...' % programName)
    exists, absPath = __utils__.programInstalled(programName)
    if exists:
        logger.info('Program %s found in %s!' % (programName, absPath))
    else:
        if required:
            logger.error('%s does not seem to be installed.' % programName)
            DIAG_INFO.error('%s not installed.' % programName)
        else:
            logger.warn('%s does not seem to be installed.' % programName)
            DIAG_INFO.warn('%s not installed.' % programName)

def checkProgramGroup(programNames, required = True):
    """
    """
    logger.info('Searching for at least one program in %s...' % programNames)
    flagList = []
    for programName in programNames:
        exists, absPath = __utils__.programInstalled(programName)
        flagList.append(exists)
        if exists:
            logger.info('--- Program %s found in %s.' % (programName, absPath))
    if True in flagList:
        logger.info('At least one of them found!')
    else:
        logger.info('None of them found.')
        if required:
            DIAG_INFO.error('None of %s found.' % programNames)
            return ERROR
        else:
            DIAG_INFO.warn('None of %s found.' % programNames)
            return WARNING

def checkGroup(group):
    """
    """
    if os.name == 'posix':
        user = __utils__.getcmdoutput('whoami')
        logger.info('Checking if %s belongs to group %s...' % (user, group))
        groups = __utils__.getcmdoutput('groups').split()
        if group in groups:
            logger.info('Yes!')
            return SUCCESS
        else:
            logger.info('No.')
            message = '%s is not in the %s group.' % (user, group)
            DIAG_INFO.warn(message)
            return WARNING
    else:
        return UNDEFINED


class doctorWindow(gMessageBox):

    """ Main window for the plasduino_doctor module.
    """
    
    MODULE_OPTIONS = ['gui.style']

    def __init__(self, msg):
        """ Constructor.
        """
        gMessageBox.__init__(self, title = 'Up and running with plasduino?',
                             text = msg)



def launch():
    """ Main function.
    """
    checkPythonModule('PyQt4')    
    checkPythonModule('PyQt4.Qwt5', False)
    checkPythonModule('serial')
    checkPythonModule('pyaudio', False)
    if os.name == 'posix':
        checkGroup('dialout')
        checkProgram('avrdude')
        checkProgram('arduino')
        checkProgram('scons', False)
        checkProgramGroup(['ffmpeg', 'avconv'], False)
    else:
        logger.info('Pladuino doctor not fully implemented for os %s.' %\
                        os.name)
        DIAG_INFO.note('Pladuino doctor not fully implemented for os %s.' %\
                           os.name)
    print('\n%s' % DIAG_INFO)
    msg = 'Here is the output from the plasduino_doctor:\n\n%s' % DIAG_INFO
    msgBox = doctorWindow(msg)
    msgBox.show()
    return msgBox


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = doctorWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_()

