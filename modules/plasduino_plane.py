#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'Plane'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'pyserial', 'avrdude', 'arduino']
__shield__       = 'lab1'
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """Studying the motion of a sphere on a plane.

The setup is based on a simple steel profile with a v-shaped cross section and
two optical gates to measure the time it takes to a sphere to travel a given
distance.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, __shield__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END
import sys

import plasduino.arduino.protocol_h as ptcl

from PyQt4 import QtGui, QtCore
from plasduino.gui.gMainWindow import gMainWindow
from plasduino.gui.gDataDisplay import gDataDisplay
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gQt import connect
from plasduino.daq.gArduinoRunControl import gArduinoRunControl
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.__logging__ import logger
from plasduino.arduino.plasduino_h import INTERRUPT_MODE_ENABLE_FALLING



class planeEventBuffer(gEventBuffer):
    
    """ Specialized data buffer object to study the time resolution of the
    data acquisition system.
    """

    MIN_NUM_EVENTS = 0

    def write(self, filePath):
        """ Do-nothing overloaded method.
        
        We're not writing an output file for the plane module.
        """
        pass
    
    def process(self, filePath):
        """ Do-nothing overloaded method.
        
        We're not writing an output file for the plane module.
        """
        pass



class planeRunControl(gArduinoRunControl):

    SKETCH_NAME = 'sktchDigitalTimer'
    INTERRUPT_MODE_0 = INTERRUPT_MODE_ENABLE_FALLING
    INTERRUPT_MODE_1 = INTERRUPT_MODE_ENABLE_FALLING

    """ Specialized RunControl class for the plane module.

    Note that, though we're not writing out data, we still create log files
    and therefore we need all the machinery for reading/incrementing/writing
    the run number. The corresponing runs won't have data files associated.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        eventBuffer = planeEventBuffer()
        gArduinoRunControl.__init__(self, eventBuffer, maxSeconds)
        self.__LastTransition = None
        self.__Measuring = False

    def connectToArduino(self, timeout = None, autoUpload = True):
        """ Overloaded method.
        """
        errCode = gArduinoRunControl.connectToArduino(self, timeout, autoUpload)
        if not errCode:
            self.ArduinoManager.setupDigitalTimerSketch(self.INTERRUPT_MODE_0,
                                                        self.INTERRUPT_MODE_1)
        return errCode

    def poll(self):
        """ Implementation of the polling cycle on the PC side.

        This is a super-minimal implementation as the polling cycle should
        be streamlined for speed.
        """
        ctrl = self.readCtrlByte()
        if ctrl != ptcl.DIGITAL_TRANSITION_HEADER:
            return ctrl
        transition = self.ArduinoManager.sreadDigitalTransition()
        logger.debug(transition)
        if transition.falling():
            if not self.__Measuring:
                self.LastTransition = transition
                self.__Measuring = True
                self.emit(QtCore.SIGNAL('measuring()'))
            else:
                if self.LastTransition is not None:
                    transitTime = transition['seconds'] -\
                                  self.LastTransition['seconds']
                    self.__Measuring = False
                    self.emit(QtCore.SIGNAL('transitTime(float)'),
                              transitTime)
        return ctrl



class planeDataDisplay(gDataDisplay):
    
    """ Data display for the plane module.
    """
    
    TRANSIT_TIME_FIELD_NAME = 'transit_time'

    def __init__(self, parent = None):
        """ Constructor.
        """
        gDataDisplay.__init__(self, parent)
        self.addRunIdField()
        self.addField(self.TRANSIT_TIME_FIELD_NAME, 'Transit time')
        self.setFieldDataFormat(self.TRANSIT_TIME_FIELD_NAME, '%.6f')

    def setMeasuring(self):
        """ Display "Measuring..." on the data display.
        """
        self.setFieldValue(self.TRANSIT_TIME_FIELD_NAME, 'Measuring...')

    def setTransitTime(self, transitTime):
        """ Set the transit time.
        """
        self.setFieldValue(self.TRANSIT_TIME_FIELD_NAME, transitTime)



class planeWindow(gMainWindow):

    """ Main window for the plane module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.*', 'daq.*', 'arduino.*']

    def __init__(self, **kwargs):
        """ Constructor.

        As the RunControl is started immediately we don't bother adding
        the arduino menu for this particular module.
        """
        gMainWindow.__init__(self, **kwargs)
        self.setup(**kwargs)
        self.RunControl = planeRunControl()
        connect(self.RunControl.ArduinoManager, 'fatalError(QString)',
                self.popupFatalError)
        connect(self.RunControl, 'measuring()', self.DataDisplay.setMeasuring)
        connect(self.RunControl, 'transitTime(float)',
                self.DataDisplay.setTransitTime)
        self.RunControl.connectToArduino(timeout = 0.2)
        if self.RunControl.ArduinoManager.connected():
            connect(self.RunControl, 'runIdChanged(int)', self.setRunId)
            self.RunControl.setRunning()

    def setRunId(self, runId):
        """ Set the runId field in the data display.
        """
        self.DataDisplay.setRunId(runId)

    def disableControls(self):
        """ Disable all the controls on the GUI.
        """
        self.DataDisplay.setEnabled(False)
        self.TransportBar.setStatus(gTransportBar.STATUS_QUIT)
        gMainWindow.disableControls(self)

    def setup(self):
        """ Setup function.
        """
        self.DataDisplay = planeDataDisplay(self)
        self.addWidget(self.DataDisplay)
        self.TransportBar = gTransportBar(self, mask = gTransportBar.MASK_QUIT)
        self.addWidget(self.TransportBar)
        connect(self.TransportBar, 'quit()', self.close)



def launch():
    """ Launch the main application.
    """
    mainWindow = planeWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = planeWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
