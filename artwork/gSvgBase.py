#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.__logging__ import logger


SVG_HEADER =\
    """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="%s" height="%s">
"""


class gColorScheme(dict):
    
    """ See http://colorschemedesigner.com/.
    """
    
    WHITE = '#ffffff'
    LIGHT_GRAY = '#bbbbbb'
    DARK_GRAY = '#555555'
    BLACK = '#000000'

    def __init__(self, lighter, light, dark, darker):
        """
        """
        dict.__init__(self)
        self['lighter'] = lighter
        self['light'] = light
        self['dark'] = dark
        self['darker'] = darker



SCHEME_COLD = gColorScheme('#6a92d4', '#4479d4', '#1049a9', '#052c6e')
SCHEME_WARM = gColorScheme('#ffb173', '#ff9540', '#ff7100', '#a64a00')


class gSvgBase(dict):

    """
    """
    
    def __init__(self):
        """
        """
        self.Text = ''

    def append(self, item, newline = True):
        """
        """
        self.Text += str(item)
        if newline:
            self.Text += '\n'

    def __str__(self):
        """
        """
        return self.Text



class gSvgEntity(gSvgBase, dict):

    """
    """

    def __init__(self, name, value = None, **kwargs):
        """
        """
        gSvgBase.__init__(self)
        self.Name = name
        dict.__init__(self)
        self.append('<%s' % name, False)
        for key, val in kwargs.items():
            self.append(' %s="%s"' % (key, val), False)
            self[key] = val
        if value is None:
            self.append('/>')
        else:
            self.append('>%s</%s>' % (value, name))

    def close(self):
        """
        """
        self.append('<%s/>' % self.Name)



class gSvgImage(gSvgBase):

    """
    """
    
    def __init__(self, width, height, **kwargs):
        """
        """
        gSvgBase.__init__(self)
        self.Width = width
        self.Height = height
        self.append(SVG_HEADER % (width, height))

    def close(self):
        """
        """
        self.append('</svg>')

    def write(self, filePath):
        """
        """
        logger.info('Writing output file %s...' % filePath)
        open(filePath, 'w').writelines(self.Text)
