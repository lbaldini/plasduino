#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import plasduino.__utils__ as __utils__

from plasduino.artwork.gSvgBase import SCHEME_COLD, SCHEME_WARM
from plasduino.artwork.gSvgButtonSet import gSvgButtonSet
from plasduino.artwork.gPlasduinoLogo import gPlasduinoLogo, gPlasduinoIcon


if __name__ == '__main__':
    WIDTH = 35
    HEIGHT = WIDTH
    BORDER_WIDTH = 1
    CORNER_RADIUS = 4

    SCHEME = SCHEME_COLD
    BORDER_COLOR = SCHEME['darker']
    FILL = SCHEME['lighter']
    OUTPUT_FOLDER = os.path.join('skins','cold')
    btnSet = gSvgButtonSet(WIDTH, HEIGHT, BORDER_COLOR, BORDER_WIDTH,
                           CORNER_RADIUS, FILL, OUTPUT_FOLDER)

    SCHEME = SCHEME_WARM
    BORDER_COLOR = SCHEME['darker']
    FILL = SCHEME['lighter']
    OUTPUT_FOLDER = os.path.join('skins', 'warm')
    btnSet = gSvgButtonSet(WIDTH, HEIGHT, BORDER_COLOR, BORDER_WIDTH,
                           CORNER_RADIUS, FILL, OUTPUT_FOLDER)
