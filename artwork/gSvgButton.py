#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.artwork.gSvgBase import gSvgEntity, gSvgImage, gColorScheme


class gSvgButton(gSvgImage):

    """ Base class for a button.
    """
    
    INNER_SCALE = 0.4

    def __init__(self, width, height = None, **kwargs):
        """ Constructor.
        """
        height = height or width
        gSvgImage.__init__(self, width, height)
        self.Fill = kwargs.get('fill', '#888888')
        self.BorderColor = kwargs.get('borderColor', '#000000')
        self.BorderWidth = kwargs.get('borderWidth', 2)
        self.CornerRadius = kwargs.get('cornerRadius', 4)
        self.Opacity = kwargs.get('opacity', 1)
        outputFilePath = kwargs.get('outputFilePath', None)
        params = {
            'x': 0.5*self.BorderWidth,
            'y': 0.5*self.BorderWidth,
            'width': self.Width - self.BorderWidth,
            'height': self.Height - self.BorderWidth,
            'rx': self.CornerRadius,
            'ry': self.CornerRadius,
            'fill': self.Fill,
            'stroke': self.BorderColor,
            'stroke-width': self.BorderWidth,
            'opacity': self.Opacity
            }
        self.MainRectangle = gSvgEntity('rect', **params)
        self.append(self.MainRectangle)
        params = {
            'x': self.BorderWidth,
            'y': self.BorderWidth,
            'width': self.Width - 2*self.BorderWidth,
            'height': int(0.5*(self.Height - self.BorderWidth) + 0.5),
            'rx': self.CornerRadius,
            'ry': self.CornerRadius,
            'fill': gColorScheme.WHITE,
            'stroke-width': 0,
            'opacity': 0.5
            }
        self.UpperShade = gSvgEntity('rect', **params)
        self.append(self.UpperShade)
        self.shapeup()
        self.close()
        if outputFilePath is not None:
            self.write(outputFilePath)

    def shapeup(self):
        """ Do-nothing function to be overloaded by derived classes.
        """
        pass



class gSvgStartButton(gSvgButton):

    """
    """

    def shapeup(self):
        """
        """
        w = int(0.9*self.INNER_SCALE*self.Width + 0.5)
        h = int(1.2*self.INNER_SCALE*self.Height + 0.5)
        x1 = 0.55*(self.Width - w)
        y1 = 0.5*(self.Height - h)
        x2 = x1 + w
        y2 = y1 + h/2.
        x3 = x1
        y3 = y1 + h
        points = '%f,%f %f,%f %f,%f' % (x1, y1, x2, y2, x3, y3)
        params = {
            'points': points,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'stroke-linejoin': 'round'
            }
        self.append(gSvgEntity('polygon', **params))



class gSvgStopButton(gSvgButton):

    """
    """

    def shapeup(self):
        """
        """
        w = int(self.INNER_SCALE*self.Width + 0.5)
        h = int(self.INNER_SCALE*self.Height + 0.5)
        params = {
            'x': 0.5*(self.Width - w),
            'y': 0.5*(self.Height - h),
            'width': w,
            'height': h,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'rx': 1,
            'ry': 1
            }
        self.append(gSvgEntity('rect', **params))



class gSvgPauseButton(gSvgButton):

    """
    """

    def shapeup(self):
        """
        """
        w = int(0.4*self.INNER_SCALE*self.Width + 0.5)
        h = int(self.INNER_SCALE*self.Height + 0.5)
        params = {
            'x': 0.5*(self.Width - w) - 0.7*w,
            'y': 0.5*(self.Height - h),
            'width': w,
            'height': h,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'rx': 1,
            'ry': 1
            }
        self.append(gSvgEntity('rect', **params))
        params['x'] = 0.5*(self.Width - w) + 0.7*w
        self.append(gSvgEntity('rect', **params))



class gSvgQuitButton(gSvgButton):

    """
    """

    def shapeup(self):
        """
        """
        r = int(0.7*self.INNER_SCALE*self.Height + 0.5)
        params = {
            'cx': 0.5*self.Width,
            'cy': 0.5*self.Height,
            'rx': r,
            'ry': r,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity
            }
        self.append(gSvgEntity('ellipse', **params))
        r *= 0.55
        params['rx'] = r
        params['ry'] = r
        params['fill'] = self.Fill
        self.append(gSvgEntity('ellipse', **params))
        w = int(0.35*self.INNER_SCALE*self.Width + 0.5)
        h = int(self.INNER_SCALE*self.Height + 0.5)
        params = {
            'x': 0.5*(self.Width - w),
            'y': 0.5*(self.Height - h) - 0.4*h,
            'width': w,
            'height': h,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'rx': 1,
            'ry': 1
            }
        self.append(gSvgEntity('rect', **params))



class gSvgNextButton(gSvgButton):

    """
    """

    def shapeup(self):
        """
        """
        w = int(0.7*self.INNER_SCALE*self.Width + 0.5)
        h = int(1.0*self.INNER_SCALE*self.Height + 0.5)
        x1 = 0.35*(self.Width - w)
        y1 = 0.5*(self.Height - h)
        x2 = x1 + w
        y2 = y1 + h/2.
        x3 = x1
        y3 = y1 + h
        points = '%f,%f %f,%f %f,%f' % (x1, y1, x2, y2, x3, y3)
        params = {
            'points': points,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'stroke-linejoin': 'round'
            }
        self.append(gSvgEntity('polygon', **params))
        x1 += w
        x2 = x1 + w
        x3 = x1
        points = '%f,%f %f,%f %f,%f' % (x1, y1, x2, y2, x3, y3)
        params['points'] = points
        self.append(gSvgEntity('polygon', **params))



class gSvgEndButton(gSvgButton):

    """
    """

    def shapeup(self):
        """
        """
        w = int(0.7*self.INNER_SCALE*self.Width + 0.5)
        h = int(1.0*self.INNER_SCALE*self.Height + 0.5)
        x1 = 0.4*(self.Width - w)
        y1 = 0.5*(self.Height - h)
        x2 = x1 + w
        y2 = y1 + h/2.
        x3 = x1
        y3 = y1 + h
        points = '%f,%f %f,%f %f,%f' % (x1, y1, x2, y2, x3, y3)
        params = {
            'points': points,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'stroke-linejoin': 'round'
            }
        self.append(gSvgEntity('polygon', **params))
        w = int(0.34*self.INNER_SCALE*self.Width + 0.5)
        params = {
            'x': x2,
            'y': 0.5*(self.Height - h),
            'width': w,
            'height': h,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'rx': 1,
            'ry': 1
            }
        self.append(gSvgEntity('rect', **params))



class gSvgPreviousButton(gSvgButton):

    """
    """

    def shapeup(self):
        """
        """
        w = int(0.7*self.INNER_SCALE*self.Width + 0.5)
        h = int(1.0*self.INNER_SCALE*self.Height + 0.5)
        x1 = self.Width - 0.35*(self.Width - w)
        y1 = 0.5*(self.Height - h)
        x2 = x1 - w
        y2 = y1 + h/2.
        x3 = x1
        y3 = y1 + h
        points = '%f,%f %f,%f %f,%f' % (x1, y1, x2, y2, x3, y3)
        params = {
            'points': points,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'stroke-linejoin': 'round'
            }
        self.append(gSvgEntity('polygon', **params))
        x1 -= w
        x2 = x1 - w
        x3 = x1
        points = '%f,%f %f,%f %f,%f' % (x1, y1, x2, y2, x3, y3)
        params['points'] = points
        self.append(gSvgEntity('polygon', **params))


class gSvgBeginButton(gSvgButton):

    """
    """

    def shapeup(self):
        """
        """
        w = int(0.7*self.INNER_SCALE*self.Width + 0.5)
        h = int(1.0*self.INNER_SCALE*self.Height + 0.5)
        x1 = self.Width - 0.4*(self.Width - w)
        y1 = 0.5*(self.Height - h)
        x2 = x1 - w
        y2 = y1 + h/2.
        x3 = x1
        y3 = y1 + h
        points = '%f,%f %f,%f %f,%f' % (x1, y1, x2, y2, x3, y3)
        params = {
            'points': points,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'stroke-linejoin': 'round'
            }
        self.append(gSvgEntity('polygon', **params))
        w = int(0.34*self.INNER_SCALE*self.Width + 0.5)
        params = {
            'x': x2 - w,
            'y': 0.5*(self.Height - h),
            'width': w,
            'height': h,
            'fill': gColorScheme.WHITE,
            'stroke': self.BorderColor,
            'stroke-width': 1,
            'opacity': self.Opacity,
            'rx': 1,
            'ry': 1
            }
        self.append(gSvgEntity('rect', **params))
