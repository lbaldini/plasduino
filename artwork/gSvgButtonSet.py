#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.artwork.gSvgButton import *
from plasduino.__logging__ import logger
import os


class gSvgButtonSet:

    """ 
    """
    
    ACTION_LIST  = ['Begin', 'Previous','Stop', 'Pause', 'Start', 'Next', 'End',
                    'Quit']

    def __init__(self, width, height, borderColor, borderWidth, cornerRadius,
                 fillColor, outputFolder):
        if not os.path.exists(outputFolder):
            logger.info('Creating folder %s...' % outputFolder)
            os.makedirs(outputFolder)
        kwargs = 'borderWidth = %d, cornerRadius = %d' %\
            (borderWidth, cornerRadius)
        for action in self.ACTION_LIST:
            # Generate the "enabled" version of the buttons.
            outputFileName = '%s_enabled.svg' % (action.lower())
            outputFilePath = os.path.join(outputFolder, outputFileName)
            args = '%s, borderColor = "%s", fill = "%s"' %\
                (kwargs, borderColor, fillColor)
            args = '%d, %d, %s, outputFilePath = "%s"' %\
                (width, height, args, outputFilePath)
            try:
                btn = eval('gSvg%sButton(%s)' % (action, args))
            except:
                btn = eval('gSvgButton(%s)' % (args))
            # And the "disabled" one.
            outputFileName = '%s_disabled.svg' % (action.lower())
            outputFilePath = os.path.join(outputFolder, outputFileName)
            args = '%s, borderColor = "%s", fill = "%s", opacity = 0.35' %\
                (kwargs, borderColor, fillColor)
            args = '%d, %d, %s, outputFilePath = "%s"' %\
                (width, height, args, outputFilePath)
            try:
                btn = eval('gSvg%sButton(%s)' % (action, args))
            except:
                btn = eval('gSvgButton(%s)' % (args))

