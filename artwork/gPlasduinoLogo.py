#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.artwork.gSvgBase import gSvgImage, gSvgEntity


LOGO_CSS =\
"""
<defs>
  <linearGradient id="vgrad" x1="0%%" y1="0%%" x2="0%%" y2="100%%">
    <stop offset="0%%" style="stop-color:#ffffff;stop-opacity:0.5" />
    <stop offset="50%%" style="stop-color:#ffffff;stop-opacity:0.5" />
    <stop offset="50%%" style="stop-color:#ffffff;stop-opacity:0.0" />
    <stop offset="100%%" style="stop-color:#ffffff;stop-opacity:0.0" />
  </linearGradient>
  <style type="text/css"><![CDATA[
  text {
  font-size:%dpx;
  font-style:normal;
  font-variant:normal;
  font-stretch:normal;
  font-family:%s;
  stroke:%s;
  stroke-width:%dpx
  }
  text.normal {
  font-weight:normal;
  }
  text.bold {
  font-weight:bold;
  }
  text.small {
  font-weight:normal;
  font-size:%dpx;
  fill:%s;
  stroke:"none";
  }
  ]]></style>
</defs>
"""


class gPlasduinoLogo(gSvgImage):

    """
    """
    
    TEXT = 'Open source data acquisition framework'

    def __init__(self, borderColor, borderWidth, fillColor):
        """
        """
        # Ugly stuff that might be parametrized.
        width = 410
        height = 83
        textSize = 83
        textSizeSmall = 10
        font = 'Ubuntu'
        x = -5
        y = 66
        xc = -395
        xO = 237
        yO = 64
        xt = 15
        yt = 64
        #
        gSvgImage.__init__(self, width, height)
        self.append(LOGO_CSS %\
                        (textSize, font, borderColor, borderWidth,
                         textSizeSmall, borderColor))
        params = {'x': x,
                  'y': y,
                  'class': 'bold'
                  }
        params['fill'] = fillColor
        self.append(gSvgEntity('text', 'plasduin', **params))
        params['fill'] = 'url(#vgrad)'
        self.append(gSvgEntity('text', 'plasduin', **params))
        params['fill'] = fillColor
        params['x'] = xc
        params['transform'] = 'scale(-1, 1)'
        self.append(gSvgEntity('text', 'c', **params))
        params['fill'] = 'url(#vgrad)'
        self.append(gSvgEntity('text', 'c', **params))
        params['fill'] = fillColor
        params['x'] = xO
        params['y'] = yO
        params['class'] = 'normal'
        params['transform'] = 'scale(1.38, 1.25)'
        self.append(gSvgEntity('text', 'O', **params))
        params['fill'] = 'url(#vgrad)'
        self.append(gSvgEntity('text', 'O', **params))
        params['fill'] = borderColor
        params['x'] = xt
        params['y'] = yt
        params['class'] = 'small'
        self.append(gSvgEntity('text', self.TEXT, **params))
        self.close()


class gPlasduinoIcon(gSvgImage):
    
    """
    """

    def __init__(self,  borderColor, borderWidth, fillColor):
        """
        """
        width = 15
        height = width
        textSize = 16
        textSizeSmall = 8
        font = 'Ubuntu'
        #
        gSvgImage.__init__(self, width, height)
        self.append(LOGO_CSS %\
                        (textSize, font, borderColor, borderWidth,
                         textSizeSmall, borderColor))
        params = {'x': -0.025*textSize,
                  'y': height/1.5
                  }
        self.append(gSvgEntity('text', 'p', **params))
        params = {'x': 0.375*textSize,
                  'y': height/1.12
                  }
        self.append(gSvgEntity('text', 'd', **params))
        self.close()
