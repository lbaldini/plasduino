#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


SENSOR_DICT = {}

def addThermistor(thermistor):
    """ Add a thermistor to the dictionary.
    """
    SENSOR_DICT[thermistor.Model] = thermistor


from plasduino.sensors.__NXFT15XH103FA2B__ import NXFT15XH103FA2B
addThermistor(NXFT15XH103FA2B())

from plasduino.sensors.__IKE02XE1500XXXXX__ import IKE02XE1500XXXXX
addThermistor(IKE02XE1500XXXXX())


def getSensor(model):
    """ Retrieve thermistors by model.
    """
    try:
        return SENSOR_DICT[model]
    except KeyError:
        return None

def listSensors():
    """ Retrieve the (sorted) list of available thermistor models.
    """
    keys = SENSOR_DICT.keys()
    keys.sort()
    return keys



if __name__ == '__main__':
    print(getSensor('NXFT15XH103FA2B'))
    print(listSensors())
