
#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.sensors.gThermistor import gThermistor, beta2pars


class IKE02XE1500XXXXX(gThermistor):

    """ The IKE02XE1500XXXXX thermistor.
    """

    def __init__(self):
        """ Constructor.

        The data sheets only provides the beta parameter, so we use that and
        plug it into a 2-parameters Steinhart-Hart equation.
        """
        c0, c1, c2 = beta2pars(3435., 298.15, 10000.)
        gThermistor.__init__(self, 'IKE02XE1500XXXXX', 'Italcoppie',
                             c0, c1, c2)



if __name__ == '__main__':
    thermistor = IKE02XE1500XXXXX()
    print(thermistor)
