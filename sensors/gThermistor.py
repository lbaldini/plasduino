#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from plasduino.sensors.gSensor import gSensor


def beta2pars(beta, T0, R0):
    """ Convert the beta parameters to the three parameters of the
    Steinhart-Hart equation, see:
    http://en.wikipedia.org/wiki/Thermistor#B_or_.CE.B2_parameter_equation
    """
    c0 = 1./T0 - (1./beta)*math.log(R0)
    c1 = 1./beta
    c2 = 0.
    return (c0, c1, c2)
    


class gThermistor(gSensor):

    """ Small class describing a thermistor.
    """

    ABSOLUTE_ZERO = -273.15
    
    def __init__(self, model, manufacturer, c0, c1, c2):
        """ Constructor.

        The class members Ci are the three coefficiencts of the
        Steinhart-Hart equation, see:
        http://en.wikipedia.org/wiki/Thermistor
        """
        gSensor.__init__(self, model, manufacturer)
        self.C0 = c0
        self.C1 = c1
        self.C2 = c2

    def adc2celsius(self, adc, shuntResistor = 10000.0):
        """ Convert ADC counts to degrees C.

        The conversion is base on the Steinhart-Hart equation, with the
        three coefficients being fixed in the constructor.
        """
        if adc == 0:
            return self.ABSOLUTE_ZERO
        logR = math.log(shuntResistor*(1023./adc - 1))
        T = 1./(self.C0 + self.C1*logR + self.C2*logR**3) + self.ABSOLUTE_ZERO
        return T

    def tolerance(self, R):
        """ Do nothing __tolerance() method.

        To be overloaded by derived classes.
        """
        pass

    def getMeasurementError(self, adc, shuntResistor = 10000.0):
        """ Return the maximum error on the temperature measurement
        at a given ADC count value.

        Mind you have to divide the value by sqrt(12) if you want to
        turn this into a sigma.
        """
        if adc == 0:
            return 0
        R = shuntResistor*(1023./adc - 1)
        dR = self.tolerance(R)
        logRmax = math.log(R*(1 + dR))
        logRmin = math.log(R*(1 - dR))
        Tmin = 1./(self.C0 + self.C1*logRmax + self.C2*logRmax**3)
        Tmax = 1./(self.C0 + self.C1*logRmin + self.C2*logRmin**3)
        return 0.5*(Tmax - Tmin)



if __name__ == '__main__':
    thermistor = gThermistor('H725', 'ACME', 8.650e-04, 2.554e-04, 1.745e-07)
    print(thermistor)
    print(thermistor.adc2celsius(560))
