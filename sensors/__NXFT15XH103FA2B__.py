#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from plasduino.sensors.gThermistor import gThermistor


class NXFT15XH103FA2B(gThermistor):

    """ The NXFT15XH103FA2B thermistor.
    """

    def __init__(self):
        """ Constructor.

        The coefficients for the Steinhart-Hart equation are based on the
        output of the fitData() function below.
        """
        gThermistor.__init__(self, 'NXFT15XH103FA2B', 'MURATA MANUFACTURING',
                             8.65082e-04, 2.55459e-04, 1.74569e-07)

    def tolerance(self, R):
        """ Overloaded method.
        """
        return 0.01 + 0.0230281*abs(math.log10(R) - 4)

    def fitData(self):
        """ Read the manufacturer data and find the coefficients for the
        Steinhart-Hart equation.
        """
        import os
        from plasduino.analysis.__ROOT__ import ROOT
        from plasduino.analysis.gGraphErrors import gGraphErrors
        from plasduino.analysis.gResidualCanvas import gResidualCanvas
        from plasduino.analysis.gCanvas import gCanvas
        from plasduino.analysis.gF1 import gF1
        from plasduino.__plasduino__ import PLASDUINO_SENSORS
        from plasduino.__logging__ import logger

        filePath = os.path.join(PLASDUINO_SENSORS, 'NXFT15XH103FA2B.dat')
        gAbs = gGraphErrors('gabs')
        gTol = gGraphErrors('gtol')
        dataT = []
        dataR = []
        for line in open(filePath):
            if not line.startswith('#'):
                line = line.strip()
                T, Rmin, R, Rmax = [float(item) for item in line.split()]
                tol = 0.5*(Rmax - Rmin)/R
                logger.info('%.3f deg: %.2f--%.2f (%.5f)' %\
                            (T, Rmin, Rmax, tol))
                T -= self.ABSOLUTE_ZERO
                R *= 1000.
                dataT.append(T)
                dataR.append(R)
                gAbs.SetNextPoint(R, T)
                gTol.SetNextPoint(R, tol)
        gAbs.GetXaxis().SetTitle('R [k#Omega]')
        gAbs.GetYaxis().SetTitle('T [#circ K]')
        gTol.GetXaxis().SetTitle('R [k#Omega]')
        gTol.GetYaxis().SetTitle('Tolerance')

        c = gResidualCanvas('NXFT15XH103FA2B calibration', Logx = True)
        f = gF1('f', '1./([0] +[1]*log(x) +[2]*(log(x)**3))')
        f.SetParameter(0, 1e-3)
        f.SetParameter(1, 1e-4)
        f.SetParameter(2, 1e-7)
        gAbs.Fit('f')
        gRes = gAbs.GetResiduals(f)
        c.drawPlots(gAbs, gRes)
        c.Update()
        
        c1 = gCanvas('ctol', Logx = True)
        f1 = gF1('f1', '[0] + [1]*abs(log10(x) - 4)')
        gTol.Fit(f1)
        gTol.Draw()
        c1.Update()
        
        raw_input('Press enter to quit.')



if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-f', action = 'store_true', dest = 'fit',
                      help = 'Fit the manufacturer data.')
    (opts, args) = parser.parse_args()
    thermistor = NXFT15XH103FA2B()
    print(thermistor)
    print(thermistor.tolerance(10000.))
    if opts.fit:
        thermistor.fitData()
    
