//-*- Mode:c -*-
// *********************************************************************
// * Copyright (C) 2015 Carmelo Sgro (carmelo.sgro@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "RunControl.h"
#include "shield_lab1.h"

/*
  This sketch is largely similar to the sktchPendulumDrive and 
  the sktchAnalogSampling  sketch, but
  at runtime it rises and lower a defined pin to drive and read a RC circuit
*/
#define SKETCH_ID SKETCH_ID_RC_VIEW
#define SKETCH_VERSION 1

#define MIN_SAMPLING_INTERVAL 10


/*
  Initialize global variables.
*/ 
RunControl RUN_CONTROL;
uint8_t  INPUT_PINS[2];
uint8_t  DRIVER_PIN;
uint32_t SAMPLING_INTERVAL = 0;
uint32_t POLL_DELAY = 0;
uint8_t  _opcode;
uint8_t  _pin;
uint16_t _adc;
uint32_t _timestamp;
bool     _ishigh;

/*
  Basic sketch configuration.
 */
void _config() {
  INPUT_PINS[0]  = sreadcmd8(OP_CODE_SELECT_ANALOG_PIN);
  INPUT_PINS[1]  = sreadcmd8(OP_CODE_SELECT_ANALOG_PIN); // expect 2 pins
  SAMPLING_INTERVAL = sreadcmd32(OP_CODE_SELECT_SAMPLING_INTERVAL);
  if (SAMPLING_INTERVAL < MIN_SAMPLING_INTERVAL) {
    SAMPLING_INTERVAL = MIN_SAMPLING_INTERVAL;
  }
  POLL_DELAY = SAMPLING_INTERVAL/2.;
  DRIVER_PIN = sreadcmd8(OP_CODE_SELECT_DIGITAL_PIN);
  pinMode(DRIVER_PIN , OUTPUT);
  digitalWrite(DRIVER_PIN, LOW); // start in LOW state
  _ishigh = false;
}


/*
  setup() implementation.

  Here we read the input pin number and the sampling interval to set up
  the sketch operations.
 */
void setup() {
  setupBoard();
  handshake(SKETCH_ID, SKETCH_VERSION);
  _config();
}


/*
  loop() implementation.

  Here we do all the rest, namely wait for the run control to be
  started/stopped and wait for commands changing state to the driver pin.

 */
void loop() {
  _opcode = RUN_CONTROL.echo();
  if (_opcode == OP_CODE_TOGGLE_DIGITAL_PIN) {
    if (_ishigh){
      digitalWrite(DRIVER_PIN, LOW); 
      _ishigh = false;
    }
    else {
      digitalWrite(DRIVER_PIN, HIGH); 
      _ishigh = true;
    }
  }
  if (RUN_CONTROL.running()) {
    //delay(SAMPLING_INTERVAL);
    //_adc = analogRead(INPUT_PIN);
    //_timestamp = RUN_CONTROL.millisSinceStart();
    //swriteAnalogReadout(INPUT_PIN, _adc, _timestamp);

    // pin 0
    _pin = INPUT_PINS[0];
    _adc = analogRead(_pin);
    delay(POLL_DELAY);
    _adc = analogRead(_pin);
    _timestamp = RUN_CONTROL.millisSinceStart();
    swriteAnalogReadout(_pin, _adc, _timestamp);

    // pin 1
    _pin = INPUT_PINS[1];
    _adc = analogRead(_pin);
    delay(POLL_DELAY);
    _adc = analogRead(_pin);
    _timestamp = RUN_CONTROL.millisSinceStart();
    swriteAnalogReadout(_pin, _adc, _timestamp);


  }
}
