// *********************************************************************
// * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "shield_lab1.h"

void setupBoard() {
  // Setup the digital pins for the LEDs.
  pinMode(PIN_GREEN_LED, OUTPUT);
  digitalWrite(PIN_GREEN_LED, LOW);
  pinMode(PIN_YELLOW_LED, OUTPUT);
  digitalWrite(PIN_YELLOW_LED, LOW);
  // Setup the digital inputs.
  pinMode(PIN_DIGITAL_INPUT_A, INPUT);
  pinMode(PIN_DIGITAL_INPUT_B, INPUT);
  // Turn on the internal pullup resistors for the digital inputs.  
  digitalWrite(PIN_DIGITAL_INPUT_A, HIGH);
  digitalWrite(PIN_DIGITAL_INPUT_B, HIGH);
}
