#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import glob
import os
import plasduino.__utils__ as __utils__

from plasduino.__logging__ import logger, abort
from plasduino.arduino.__arduino__ import getSketchInfo
from plasduino.__plasduino__ import PLASDUINO_ARDUINO


LIBS  = ['plasduino.h', 'plasduino.cpp', 'protocol.h', 'sketches.h',
         'RunControl.h', 'RunControl.cpp']
LIBS += glob.glob('shield_*.h')
LIBS += glob.glob('shield_*.cpp')
AUX_SOURCES = ['SConstruct'] + LIBS


def compile(target, board = 'uno', verbose = False, clean = True):
    """ Compile the target sketch.

    This actually implies copying the SConstruct file into the target folder
    and calling scons from within it. Unless otherwise specifies scons cleans
    up the mess after itself.

    Note that each arduino board (i.e., uno, diecimila has its own
    microcontroller, therefore the compiled file is board-specific).
    """
    target = target.strip('/')
    logger.info('Compiling %s...' % target)
    if not os.path.isdir(target):
        abort('Could not find folder %s' % target)
    for source in AUX_SOURCES:
        __utils__.cp(source, target)
    command = 'scons'
    command += ' ARDUINO_MODEL=%s' % board
    command = 'cd %s; %s' % (target, command)
    logFilePath = os.path.join(target, 'compile.log')
    if __utils__.cmd(command, verbose = verbose, logFilePath = logFilePath):
        abort()
    logger.info('Compilation succesfully finished!')
    parseSketch(target)
    if clean:
        cleanup(target, verbose)

def parseSketch(target):
    """ Parse the sketch c file and write a small python module with the
    sketch info to be used on the PC side for the handshake.
    """
    logger.info('Parsing sketch information...')
    target = target.strip('/')
    filePath = os.path.join(PLASDUINO_ARDUINO, '%s.py' % target)
    sketchId, sketchVersion = getSketchInfo(target)
    logger.info('%s: sketch ID = %s, version = %s.' %\
                    (target, sketchId, sketchVersion))
    text = 'SKETCH_ID = %s\nSKETCH_VERSION = %s\n' % (sketchId, sketchVersion)
    open(filePath, 'w').writelines(text)
    logger.info('Done.')

def cleanup(target, verbose = False):
    """ Cleanup the scons mess.

    Again, this implies cd-ing into the target directory and invoking
    "scons --clean". Note that the binary .hex file is *not* removed in view
    of the possibility of a standalone upload routine to be called on the fly
    (we don't want to install scons on all the machines we run the system on).
    """
    target = target.strip('/')
    logger.info('Cleaning up %s...' % target)
    if not os.path.isdir(target):
        abort('Could not find folder %s' % target)
    logFilePath = os.path.join(target, 'compile.log')
    __utils__.rm(logFilePath)
    command = 'cd %s; scons --clean' % target
    status = __utils__.cmd(command, verbose = verbose)
    for source in AUX_SOURCES:
        __utils__.rm(os.path.join(target, source))
    filePath = os.path.join(target, '%s.pde' % target)
    if os.path.exists(filePath):
        __utils__.rm(filePath)
    if not status:
        logger.info('Cleanup succesfully finished!')
    else:
        abort()
    logger.info('Cleanup finished.')



if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-b', type = str, action = 'append', default = None,
                      dest = 'boards',
                      help = 'The board(s) the sketch must be compiled for')
    parser.add_option('-v', action = 'store_true', dest = 'verbose',
                      help = 'Print out compilation messages.')
    parser.add_option('-s', type = str, action = 'append', dest = 'skip',
                      default = None,
                      help = 'Skip selected sketch(es).')
    (opts, args) = parser.parse_args()
    from gCodeParser import parseAll
    parseAll()
    if not len(args):
        targets = []
        for item in os.listdir(os.curdir):
            if os.path.isdir(item):
                targets.append(item)
    else:
        targets = args
    if opts.boards is None:
        import __arduino__
        opts.boards = __arduino__.SUPPORTED_BOARDS
    for target in targets:
        if opts.skip is not None and target in opts.skip:
            logger.info('Skipping sketch %s...' % target)
        else:
            for board in opts.boards:
                compile(target, board, opts.verbose)

