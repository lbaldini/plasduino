//-*- Mode:c -*-
// *********************************************************************
// * Copyright (C) 2012 Carmelo Sgro (carmelo.sgro@pi.infn.it)         *
// * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "RunControl.h"
#include "shield_lab1.h"

/*
  This sketch is largely similar to the sktchAnalogSampling sketch, except
  that is only supports a single analog input and it allows to set 
  at runtime the duty cycle of the PWM on a predefined pin.
*/
#define SKETCH_ID SKETCH_ID_PENDULUM_DRIVE
#define SKETCH_VERSION 3

#define MIN_SAMPLING_INTERVAL 10


/*
  Initialize global variables.
*/ 
RunControl RUN_CONTROL;
uint8_t INPUT_PIN;
uint32_t SAMPLING_INTERVAL;
uint8_t _opcode;
uint8_t _payload;
uint16_t _adc;
uint32_t _timestamp;


/*
  Basic sketch configuration.
 */
void _config() {
  INPUT_PIN = sreadcmd8(OP_CODE_SELECT_ANALOG_PIN);
  SAMPLING_INTERVAL = sreadcmd32(OP_CODE_SELECT_SAMPLING_INTERVAL);
  if (SAMPLING_INTERVAL < MIN_SAMPLING_INTERVAL) {
    SAMPLING_INTERVAL = MIN_SAMPLING_INTERVAL;
  }
}


/*
  setup() implementation.

  Here we read the input pin number and the sampling interval to set up
  the sketch operations.
 */
void setup() {
  setupBoard();
  handshake(SKETCH_ID, SKETCH_VERSION);
  _config();
}


/*
  loop() implementation.

  Here we do all the rest, namely wait for the run control to be
  started/stopped and wait for commands setting the PWM duty cycle on the
  proper pin.

  Note that writing back the opcode and the payload read in the PWM block
  to make sure they're right is not completely trivial as the output order
  of the analog readout and diagnostic data depends on the timing of the
  command and would require some thoughts on the python side. Therefore, at
  least for the time being, we only write analog readouts to the serial port.
 */
void loop() {
  _opcode = RUN_CONTROL.echo();
  if (_opcode == OP_CODE_SELECT_PWM_DUTY_CYCLE) {
    while (Serial.available() < 1)
      ;
    _payload = _sreaduint8();
    analogWrite(PIN_PWM_MOTOR, _payload);
  }
  if (RUN_CONTROL.running()) {
    delay(SAMPLING_INTERVAL);
    _adc = analogRead(INPUT_PIN);
    _timestamp = RUN_CONTROL.millisSinceStart();
    swriteAnalogReadout(INPUT_PIN, _adc, _timestamp);
  }
}
