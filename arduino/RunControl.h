// *********************************************************************
// * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#ifndef RunControl_h
#define RunControl_h

#include "plasduino.h"

/*
  Minimal class to standardize the flow of data acquisition on the arduino
  side.

  The RunControl keeps track of the status of the DAQ (e.g., running or
  stopped) and of the time of the start run.

  Both the micros() and millis() Arduino library functions return 32-bit
  integers (i.e. 0 to 2**32 - 1 = 4,294,967,295). This implies that 
  * micros() saturates at 4,294,967,295 us or about 71.58 minutes;
  * millis() saturates at 4,294,967,295 us or about 49.71 days.
  We put quite a bit of thought into how to keep track of the rollovers in the
  timing counters and came to the conclusions that implementing a complete and
  reliable solution on the arduino side (including making sure that we actually
  latch the micros() register value at least ~once an hour) would be way too
  intrusive. We decided instead of providing convenience functions in the
  RunControl class returning the difference between the values returned by the
  micros() and millis() library functions and the corresponding values latched
  at the beginning of the run. At the cost of an integer subtraction this
  gurantees that:
  * the values returned are, by definition, referred to the start of the run
  so that the initial offset doesn't need to be subtracted on the python side;
  * the micros() rollover always happens some 70 minutes after the start of the
  run, no matter what the relative timing wrt to the power-up of the board is.
  
  For applications where the masimum (i.e. us) time resolution is needed for
  more than ~1 hour, it is responsibility of the python code on the PC side to
  take care of that.
 */

class RunControl
{

 public:
  
  // Constructor.
  RunControl();

  // Start the data acquisition. 
  void start();

  // Stop the data acquisition.
  void stop();

  // Return true of the RunControl is running.
  inline const bool running() const { return m_running; }

  // Return the value returned by micros(), latched at the start run.
  inline const uint32_t startMicros() const { return m_startMicros; }

  // Return the value returned by millis(), latched at the start run.
  inline const uint32_t startMillis() const { return m_startMillis; }

  // Return the number of us since the start of the run.
  uint32_t microsSinceStart();

  // Return the number of ms since the start of the run.
  uint32_t millisSinceStart();

  // Listen to the serial port for a change-state command.
  void listen();

  // Listen to the serial port for a change-state command and pass
  // along the op code.
  uint8_t echo();

 private:

  // Status of the acquisition.
  bool m_running;

  // Value returned by micros() at the start of the run.
  uint32_t m_startMicros;

  // Value returned by millis() at the start of the run.
  uint32_t m_startMillis;
};

#endif
