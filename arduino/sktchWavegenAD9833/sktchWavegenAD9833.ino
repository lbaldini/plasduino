//-*- Mode:c -*-
// *********************************************************************
// * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "plasduino.h"
#include <SPI.h>

#define SKETCH_ID SKETCH_ID_WAVEGEN_AD9833
#define SKETCH_VERSION 2

#define FSYNC 2


uint16_t _payload;


/*
Send a command to the AD9833 with the SPI protocol.

This actually implies three separate steps, namely:
(i)   set the FSYNC low;
(ii)  send the actual message;
(iii) set the FSYNC back high.

The short delays are meant to ensure everything has time to settle.
*/
void transfer(unsigned int payload)
{
  digitalWrite(FSYNC, LOW);
  delay(10);
  SPI.transfer(highByte(payload));
  SPI.transfer(lowByte(payload));
  delay(10);
  digitalWrite(FSYNC, HIGH);
}


/*
  setup() implementation.
 */
void setup() {
  handshake(SKETCH_ID, SKETCH_VERSION);
  pinMode(FSYNC, OUTPUT);
  digitalWrite(FSYNC, HIGH);
  SPI.setDataMode(SPI_MODE2);
  SPI.begin();
  delay(100);
}


/*
  loop() implementation.
 */
void loop() {
  if (Serial.available() >= 3) {
    _payload = sreadcmd16(OP_CODE_AD9833_CMD);
    transfer(_payload);
  }
}
