# Automatically created by gCodeParser, do not edit.

A0_SHUNT_RESISTOR = 10000.
A1_SHUNT_RESISTOR = 10000.
PIN_GREEN_LED = 13
PIN_YELLOW_LED = 12
PIN_DIGITAL_INPUT_A = 2
PIN_DIGITAL_INPUT_B = 3
PIN_ANALOG_INPUT_A = 0
PIN_ANALOG_INPUT_B = 1
PIN_ANALOG_INPUT_C = 2
PIN_ANALOG_INPUT_D = 3
PIN_PWM_MOTOR = 9
