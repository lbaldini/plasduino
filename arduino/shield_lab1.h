// *********************************************************************
// * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#ifndef shield_lab1_h
#define shield_lab1_h

#include "plasduino.h"

// Board hardware setup.
#define A0_SHUNT_RESISTOR 10000.
#define A1_SHUNT_RESISTOR 10000.

// Basic pinout definition.
#define PIN_GREEN_LED  13
#define PIN_YELLOW_LED 12
#define PIN_DIGITAL_INPUT_A 2
#define PIN_DIGITAL_INPUT_B 3
#define PIN_ANALOG_INPUT_A 0
#define PIN_ANALOG_INPUT_B 1
#define PIN_ANALOG_INPUT_C 2
#define PIN_ANALOG_INPUT_D 3
#define PIN_PWM_MOTOR 9


// Setup the board according to the pinout defined above.
void setupBoard();

#endif
