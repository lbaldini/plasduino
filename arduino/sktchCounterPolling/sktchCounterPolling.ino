//-*- Mode:c -*-
// *********************************************************************
// * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "RunControl.h"
#include "shield_lab1.h"

/*
  Configurable sketch...
  
  Up to 6 pins (i.e., as many as available on the uno board) are supported.
*/
#define SKETCH_ID SKETCH_ID_COUNTER_POLLING
#define SKETCH_VERSION 2

#define MAX_NUM_INPUT_PINS 6
#define MIN_COUNTING_INTERVAL 10


/*
  Initialize global variables.
*/ 
RunControl RUN_CONTROL;
uint8_t NUM_INPUT_PINS = 0;
uint8_t INPUT_PINS[MAX_NUM_INPUT_PINS];
uint32_t COUNTING_INTERVAL = 0;
int8_t POLLING_MODE = 0;

uint8_t _opcode;
uint8_t _pin;
uint8_t _state;
int8_t _delta;
uint8_t _prevState[MAX_NUM_INPUT_PINS];
uint32_t _counter[MAX_NUM_INPUT_PINS];
uint32_t _startTime;
uint32_t _elapsedTime;

/*
  Reset the counters.
 */
void reset() {
  for (uint8_t i = 0; i < NUM_INPUT_PINS; i++) {
    _counter[i] = 0;
  }
  _startTime = millis();
  _elapsedTime = 0;
}


/*
  Write data to the serial port.
 */
void writeData() {
  for (uint8_t i = 0; i < NUM_INPUT_PINS; i++) {
    swrite(_counter[i]);
  }
  reset();
}


/*
  Polling function.
 */
void poll() {
  for (uint8_t i = 0; i < NUM_INPUT_PINS; i++) {
    _pin = INPUT_PINS[i];
    _state = digitalRead(_pin);
    _delta = _state - _prevState[_pin];
    if ((POLLING_MODE == POLLING_MODE_ENABLE_CHANGE && _delta != 0) ||
	(_delta == POLLING_MODE)) {
      _counter[_pin] += 1;
    }
    _prevState[_pin] = _state;
  }
  _elapsedTime = millis() - _startTime;  
}


/*
  Basic sketch configuration.
 */
void _config() {
  NUM_INPUT_PINS = sreadcmd8(OP_CODE_SELECT_NUM_ANALOG_PINS);
  if (NUM_INPUT_PINS) {
    for (uint8_t i = 0; i < NUM_INPUT_PINS; i++) {
      _pin = sreadcmd8(OP_CODE_SELECT_DIGITAL_PIN);
      INPUT_PINS[i] = _pin;
      pinMode(_pin, INPUT);
      digitalWrite(_pin, HIGH);
    }
    COUNTING_INTERVAL = sreadcmd32(OP_CODE_SELECT_SAMPLING_INTERVAL);
    if (COUNTING_INTERVAL < MIN_COUNTING_INTERVAL) {
      COUNTING_INTERVAL = MIN_COUNTING_INTERVAL;
    }
    POLLING_MODE = sreadcmd8(OP_CODE_SELECT_POLLING_MODE);
  }
}


/*
  setup() implementation.

  Here we essentially read the parameters to be configured from the
  python application and setup things for the polling cycle.
 */
void setup() {
  setupBoard();
  handshake(SKETCH_ID, SKETCH_VERSION);
  _config();
}


/*
  loop() implementation.

 */
void loop() {
  _opcode = RUN_CONTROL.echo();
  // Reset the elapsed time and the counters at start run;
  if (_opcode == OP_CODE_START_RUN) {
    reset();
    }
  // If the counting interval has expired, write the data over the serial port.
  if (_elapsedTime > COUNTING_INTERVAL) {
    writeData();
  }
  // Read the actual pin values for state changes.
  if (RUN_CONTROL.running()) {
    poll();
  }
}
