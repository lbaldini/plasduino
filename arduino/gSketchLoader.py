#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
import re
import os
import plasduino.arduino.__arduino__ as __arduino__
import plasduino.arduino.__boards__ as __boards__
import plasduino.__utils__

from plasduino.__logging__ import logger, abort
from plasduino.__plasduino__ import PLASDUINO_ARDUINO




class gSketchLoader:

    """ Small utility class to handle the uploading process for the arduino
    sketches.
    """

    def __init__(self, port, model):
        """ Constructor.
        """
        self.Port = port
        self.Board = __boards__.getBoardByModel(model)
        if self.Board is None:
            abort('Unrecongnized board %s while uploading the sketch.' % model)
        logger.info('Retrieving upload information...')
        logger.info('Board info: %s' % self.Board)
       
    def sendReset(self):
        """ Send a reset command.
        """
        __arduino__.pulseDTR(self.Port)
 
    def upload(self, sketchName):
        """ Upload a sketch on the board.
        
        Note that we actually:
        (*) send a reset signal on the serial port (i.e., pulse the DTR).
        (*) upload the sketch to the flash memory;
        (*) send another reset so that we're ready to read again.
        """
        model = self.Board['model']
        uploadProtocol = self.Board['upload.protocol']
        if uploadProtocol == 'stk500':
            uploadProtocol = 'stk500v1'
            logger.info('Upload protocol changed from stk500 to stk500v1.')
        uploadSpeed = self.Board['upload.speed']
        buildMcu = self.Board['build.mcu']
        logger.info('Preparing to upload the sketch "%s"...' % sketchName)
        targetDirPath = os.path.join(PLASDUINO_ARDUINO, sketchName)
        targetFileName = __arduino__.getHexFileName(sketchName, model)
        targetFilePath = os.path.join(targetDirPath, targetFileName)
        if not os.path.exists(targetFilePath):
            sys.exit('Could not find file %s. Abort.' % targetFilePath)
        else:
            logger.info('File %s found.' % targetFilePath)
        cmd = 'cd %s' % targetDirPath
        cmd = '%s; avrdude -V -F -c %s -b %s -p %s -P %s' %\
            (cmd, uploadProtocol, uploadSpeed, buildMcu, self.Port)
        cmd = '%s -U flash:w:%s' % (cmd, targetFileName)
        self.sendReset()
        status = plasduino.__utils__.cmd(cmd)
        if status:
            logger.warn('Giving up on the last reset...')
            return status
        self.sendReset()
        return 0



if __name__ == '__main__':
    loader = gSketchLoader('/dev/ttyACM0', 'uno')
    loader.upload('sktchDigitalTimer')
