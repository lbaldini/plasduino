#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys

from plasduino.arduino.gSketchLoader import gSketchLoader
from plasduino.daq.__serial__ import getArduinoInfo


def upload(target):
    """ Upload a compiled (.hex) sketch to the microcontroller.
    """
    # This is in case one takes advantage of the autocomplition and gets
    # a trailing slash into the sketch name.
    target = target.strip('/')
    port, board, vip, pid = getArduinoInfo()
    loader = gSketchLoader(port, board)
    loader.upload(target)



if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    (opts, args) = parser.parse_args()
    if len(args) != 1:
        parser.error('Please privide a single sketch name to be uploaded.')
    # We need this otherwise the sketch name is passed along the way to
    # the plasduino option parser implicitly invoked in the getArduinoInfo()
    # call---and we end up with a parser error due to the unrecongnized
    # option.
    sys.argv = sys.argv[:-1]
    upload(args[0])
