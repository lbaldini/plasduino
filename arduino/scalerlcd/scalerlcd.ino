//-*- Mode:c -*-
// *********************************************************************
// * Copyright (C) 2012 Carmelo Sgro (carmelo.sgro@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include "wiring_private.h"

//include interrupt for start trigger
#include <avr/interrupt.h>

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 10, 9, 8, 7);

// this constant won't change:
#define ledPin  13   // the pin that the LED is attached to
// pins for scaler counters: Port D 
// port readout is hardcoded!!!
#define inPin0  3
#define inPin1  4
#define inPin2  5
#define inPin3  6
// run start pin
#define runPin  2    // the pin start condition: Interrupt
// analog read for time
int analogPin = A0;

// Variables will change:
uint8_t inPinState;
int inCounter0   = 0;   // counter for the number of pin change
uint8_t inState0     = 0;   // current state of the input
uint8_t inLastState0 = 0;   // previous state of the input
int inCounter1   = 0;
uint8_t inState1     = 0;
uint8_t inLastState1 = 0;
int inCounter2   = 0;
uint8_t inState2     = 0;
uint8_t inLastState2 = 0;
int inCounter3   = 0;
uint8_t inState3     = 0;
uint8_t inLastState3 = 0;

// status variables
boolean running      = false;  // running flag
//int runState         = 0;
//int runLastState     = 0;
int runId            = 0;
unsigned long runTime   = 5000;   // integration time in ms
unsigned long startTime = 0;      // start time in ms
uint8_t tt;

void setup() 
{ 
  // init start+4 channels as a input:
  pinMode(runPin, INPUT);
  pinMode(inPin0, INPUT);
  pinMode(inPin1, INPUT);
  pinMode(inPin2, INPUT);
  pinMode(inPin3, INPUT);
  // turnOffPWM at the setup. Really necessary?
  tt = digitalPinToTimer(inPin0);
  if (tt != NOT_ON_TIMER) myturnOffPWM(tt);
  tt = digitalPinToTimer(inPin1);
  if (tt != NOT_ON_TIMER) myturnOffPWM(tt);
  tt = digitalPinToTimer(inPin2);
  if (tt != NOT_ON_TIMER) myturnOffPWM(tt);
  tt = digitalPinToTimer(inPin3);
  if (tt != NOT_ON_TIMER) myturnOffPWM(tt);

  // initialize the LED as an output:
  pinMode(ledPin, OUTPUT);

  // set up the LCD's number of columns and rows: 
  lcd.begin(20, 4);
  // Print a test message to the LCD.
  lcd.print("12345678901234567890");
  lcd.setCursor(0, 1);
  lcd.print("Init LCD Scaler");  
  delay(1000); 
  lcd.clear();
  delay(10); 
  attachInterrupt(runPin - 2, startCounting, RISING);
} 
 

// read analog input and convert it to integration time
unsigned long getIntegrationTime(){
  int aVal = analogRead(analogPin);
  //lcd.setCursor(13, 1);
  //lcd.print(aVal);
  // change this function for a different mapping  
  unsigned long rTime = ((unsigned long)aVal/10 + 1)*1000;
  // milliseconds
  return rTime;
}
 
void loop() 
{ 
  if (running){// count until time elapsed
    unsigned long currTime = millis() - startTime;
    if ( currTime < runTime){
        updateCounters();
    }
    else {
      running = false;
      // Update LCD with measurements
      digitalWrite(ledPin, LOW);
      writeLCD();
    }
  }
  else {
    // Get new time and write it:
    // TBD using analog input
    runTime = getIntegrationTime();
    //runTime = 5000;
    // rewrite integration time
    lcd.setCursor(10, 0);
    lcd.print("T=");
    lcd.setCursor(13, 0);
     // there must be a better way !!!
    unsigned long writeTime = runTime/1000; // write it in seconds
    if      (writeTime<10)  { lcd.print(writeTime); lcd.print("   "); }
    else if (writeTime<100) { lcd.print(writeTime); lcd.print("  ");  }
    else if (writeTime<1000){ lcd.print(writeTime); lcd.print(" ");   }
    else                    { lcd.print(writeTime);                   }
    lcd.setCursor(18, 0);
    lcd.print("s"); // seconds

    delay(100);// do not update to frequently
  }
} 

// function called when START button is pressed
void startCounting(){
  if (!running){ // START!
    resetCounters();
    digitalWrite(ledPin, HIGH);
    runId++;
    startTime = millis(); // reference time, issues when it resets!
    running = true;
  }
}

// write counter status
void writeLCD(){
  // remove old readout
  lcd.clear();
  // add new redout
  lcd.setCursor(0, 0);
  lcd.print(inCounter0);
  lcd.setCursor(0, 1);
  lcd.print(inCounter1);
  lcd.setCursor(0, 2);
  lcd.print(inCounter2);
  lcd.setCursor(0, 3);
  lcd.print(inCounter3);
  // rewrite integration time
  lcd.setCursor(10, 0);
  lcd.print("T=");
  lcd.setCursor(13, 0);
  lcd.print(runTime);
  // write runId
  lcd.setCursor(10, 2);
  lcd.print("R=");
  lcd.setCursor(13, 2);
  lcd.print(runId);
}


void resetCounters(){
  inCounter0 = 0;
  inCounter1 = 0;
  inCounter2 = 0;
  inCounter3 = 0;
}

// check port status and update counters
void updateCounters(){
  //read full port D
  inPinState = PIND;
  
  // read the input pins:
  inState0 = (inPinState&8 )>0;
  inState1 = (inPinState&16)>0;
  inState2 = (inPinState&32)>0;
  inState3 = (inPinState&64)>0;
  
  // check if pin value changed  
  if (inState0>0 && inLastState0==0) {  inCounter0++;  }
  inLastState0=inState0;
  if (inState1>0 && inLastState1==0) {  inCounter1++;  }
  inLastState1=inState1;
  if (inState2>0 && inLastState2==0) {  inCounter2++;  }
  inLastState2=inState2;
  if (inState3>0 && inLastState3==0) {  inCounter3++;  }
  inLastState3=inState3;

}


void myturnOffPWM(uint8_t timer)
{ // this will depend on the type of ATmel
  if (timer == TIMER1A) cbi(TCCR1A, COM1A1);
  if (timer == TIMER1B) cbi(TCCR1A, COM1B1);
  if (timer == TIMER0A) cbi(TCCR0A, COM0A1);
  if (timer == TIMER0B) cbi(TCCR0A, COM0B1);
  if (timer == TIMER2A) cbi(TCCR2A, COM2A1);
  if (timer == TIMER2B) cbi(TCCR2A, COM2B1);
}

