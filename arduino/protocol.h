// *********************************************************************
// * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#ifndef protocol_h
#define protocol_h


/*
  This header file contains a bunch of constants implementing the backbone
  of the communication protocol between the arduino board and the plasduino
  software running on the personal computer controlling the data acquisition.
 */


/*
  Definition of the control bytes for the arduino-to-host-PC serial
  communication. (This includes the headers for the basic data types.)
 */

#define NO_OP_HEADER                     0xA0
#define DIGITAL_TRANSITION_HEADER        0xA1
#define ANALOG_READOUT_HEADER            0xA2
#define GPS_MEASSGE_HEADER               0xA3
#define RUN_END_MARKER                   0xB0

/*
  Definition of the basic operational codes.
*/

#define OP_CODE_NO_OP                    0x00
#define OP_CODE_START_RUN                0x01
#define OP_CODE_STOP_RUN                 0x02
#define OP_CODE_SELECT_NUM_DIGITAL_PINS  0x03
#define OP_CODE_SELECT_DIGITAL_PIN       0x04
#define OP_CODE_SELECT_NUM_ANALOG_PINS   0x05
#define OP_CODE_SELECT_ANALOG_PIN        0x06
#define OP_CODE_SELECT_SAMPLING_INTERVAL 0x07
#define OP_CODE_SELECT_INTERRUPT_MODE    0x08
#define OP_CODE_SELECT_PWM_DUTY_CYCLE    0x09
#define OP_CODE_SELECT_POLLING_MODE      0x0A
#define OP_CODE_AD9833_CMD               0x0B
#define OP_CODE_TOGGLE_LED               0x0C
#define OP_CODE_TOGGLE_DIGITAL_PIN       0x0D



#endif
