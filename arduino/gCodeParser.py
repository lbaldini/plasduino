#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


""" Small utility module to extract the #define statements from a C/C++
header file and create a corresponding python module.
"""

import os
import glob

from plasduino.__plasduino__ import PLASDUINO_ARDUINO
from plasduino.__logging__ import logger


def parseHeader(inputFilePath):
    """ Parse a C/C++ header file and create a python module.
    """
    if not inputFilePath.endswith('.h'):
        logger.error('%s does not look like a C/C++ header file.' %\
                         inputFilePath)
        return
    outputFilePath = inputFilePath.replace('.h', '_h.py')
    outputFile = open(outputFilePath, 'w')
    creator = os.path.basename(__file__).split('.')[0]
    outputFile.write('# Automatically created by %s, do not edit.\n\n' %\
                         creator)
    logger.info('Parsing %s...' % inputFilePath)
    for line in open(inputFilePath):
        line = line.strip()
        if line.startswith('#define'):
            try:
                cmd, name, value = [item.strip() for item in line.split()]
                text = '%s = %s' % (name, value)
                logger.info('Found "%s".' % text)
                outputFile.write('%s\n' % text)
            except:
                logger.info('Skipping line "%s"...' % line)
    outputFile.close()
    logger.info('Output file %s closed.' % outputFilePath)

def parseAll():
    """ Parse all the header files.
    """
    for filePath in glob.glob(os.path.join(PLASDUINO_ARDUINO, '*.h')):
        parseHeader(filePath)



if __name__ == '__main__':
    parseAll()
