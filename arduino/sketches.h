// *********************************************************************
// * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#ifndef sketches_h
#define sketches_h

/*
  Basic definition of the sketch IDs.
  
  These are used in the hand-shaking between the arduino board and the
  python software controlling the data acquisition in order to decide whether
  a new sketch needs to be uploaded.

  We conventionally use the numbers 0--127 (0x0--0x7F) for the sketch that
  are used by real modules and the others for the sketch that are either
  for testing purposes or under development.
*/

#define SKETCH_ID_NO_OP             0x00
#define SKETCH_ID_DIGITAL_TIMER     0x01
#define SKETCH_ID_ANALOG_SAMPLING   0x02
#define SKETCH_ID_WAVEGEN_AD9833    0x03
#define SKETCH_ID_PENDULUM_DRIVE    0x04
#define SKETCH_ID_COUNTER_POLLING   0x05
#define SKETCH_ID_COUNTER_INTERRUPT 0x06
#define SKETCH_ID_RC_VIEW           0x07

#define SKETCH_ID_TEST_PROTOCOL     0x80
#define SKETCH_ID_TEST_LED          0x81

#define SKETCH_ID_PENDULUM          0xA0
#define SKETCH_ID_SCALERLCD         0xA1


#endif
