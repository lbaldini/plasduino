//-*- Mode:c -*-
// *********************************************************************
// * Copyright (C) 2012 Carmelo Sgro (carmelo.sgro@pi.infn.it)         *
// * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "RunControl.h"
#include "shield_lab1.h"


#define SKETCH_ID SKETCH_ID_DIGITAL_TIMER
#define SKETCH_VERSION 2


/*
  This is a sketch for a digital timer with the possibility of reading
  an analog input each time an interrupt is triggered and parsing the
  output of a GPS receiver through a software serial port.

  In typical operation the analog input pin is used to read a thermistor
  so that we can keep track of the temperature as the data acquisition 
  proceeds.
*/


/*
  Initialize global variables.
*/
RunControl RUN_CONTROL;
uint8_t ANALOG_INPUT_PIN;
uint32_t _timestamp;
uint32_t _state;
uint16_t _adc;


/*
  Basic sketch configuration.
 */
void _config() {
  uint8_t mode;
  mode = sreadcmd8(OP_CODE_SELECT_INTERRUPT_MODE);
  if (mode) {
    attachInterrupt(0, intSvcRoutine0, mode);
  }
  mode = sreadcmd8(OP_CODE_SELECT_INTERRUPT_MODE);
  if (mode) {
    attachInterrupt(1, intSvcRoutine1, mode);
  }
  ANALOG_INPUT_PIN = sreadcmd8(OP_CODE_SELECT_ANALOG_PIN);
}


/*
  setup() implementation.

  We basically handshake with the PC and then attach the interrupt service
  routines if needed.
 */
void setup() {
  setupBoard();
  handshake(SKETCH_ID, SKETCH_VERSION);
  _config();
}


/*
  loop() implementation.
  
  Here we just listen to the serial port for the RunControl to be started or
  stopped.
 */
void loop() {
  RUN_CONTROL.listen();
}


/*
  Interrupt service routine on interrupt 0.

  If the RunControl is running, latch the time and the state of INTERRUPT_PIN_0
  and write the stuff to the serial port.
 */
void intSvcRoutine0() {
  if (RUN_CONTROL.running()) {
    _timestamp = RUN_CONTROL.microsSinceStart();
    _state = digitalRead(INTERRUPT_PIN_0);
    swriteDigitalTransition(INTERRUPT_PIN_0, _state, _timestamp);
    if (ANALOG_INPUT_PIN != NO_OP_PIN_NUMBER) {
      _adc = analogRead(ANALOG_INPUT_PIN);
      _timestamp = RUN_CONTROL.millisSinceStart();
      swriteAnalogReadout(ANALOG_INPUT_PIN, _adc, _timestamp);
    }
  }
}


/*
  Interrupt service routine on interrupt 1.

  If the RunControl is running, latch the time and the state of INTERRUPT_PIN_1
  and write the stuff to the serial port.
 */
void intSvcRoutine1() {
  if (RUN_CONTROL.running()) {
    _timestamp = RUN_CONTROL.microsSinceStart();
    _state = digitalRead(INTERRUPT_PIN_1);
    swriteDigitalTransition(INTERRUPT_PIN_1, _state, _timestamp);
    if (ANALOG_INPUT_PIN != NO_OP_PIN_NUMBER) {
      _adc = analogRead(ANALOG_INPUT_PIN);
      _timestamp = RUN_CONTROL.millisSinceStart();
      swriteAnalogReadout(ANALOG_INPUT_PIN, _adc, _timestamp);
    }
  }
}
