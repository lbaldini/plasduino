//-*- Mode:c -*-
// *********************************************************************
// * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "plasduino.h"
#include "shield_lab1.h"


#define SKETCH_ID SKETCH_ID_TEST_LED
#define SKETCH_VERSION 2


int _payload = 0;
int _pin = 0;
int _status = 0;


void setup() {
  setupBoard();
  handshake(SKETCH_ID, SKETCH_VERSION);
}


void loop() {
  if (Serial.available()) {
    _payload = sreadcmd8(OP_CODE_TOGGLE_LED);
    // We encode the command as pin + status << 4 on the control side.
    _pin = _payload & 0xF;
    _status = (_payload >> 4) & 0x1;
    if (_pin == PIN_GREEN_LED | _pin == PIN_YELLOW_LED){
      digitalWrite(_pin, _status);
    }
  }
}

