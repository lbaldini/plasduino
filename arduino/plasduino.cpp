// *********************************************************************
// * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "plasduino.h"


/*
  Initialize the serial connection and write the sketch ID and version to
  the serial port.
 */
void handshake(uint8_t sketchId, uint8_t sketchVersion)
{
  Serial.begin(BAUD_RATE);
  swrite(sketchId);
  swrite(sketchVersion);
}


/*
  Read an 8-bit unsigned integer from the serial port (mind there is no check
  whatsoever on whether there is any data available to be read).
 */
uint8_t _sreaduint8()
{
  uint8_t payload = Serial.read();
  return payload;
}


/*
  Read a 16-bit unsigned integer from the serial port (mind there is no
  check whatsoever on whether there is any data available to be read).
 */
uint16_t _sreaduint16() {
  uint16_t payload = Serial.read();
  payload |= Serial.read() << 8;
  return payload;
}


/*
  Read a 32-bit unsigned integer from the serial port (mind there is no
  check whatsoever on whether there is any data available to be read).
  
  The type cast in the last two bitshifts is there in order to prevent the
  compiler from complaining about:
  warning: left shift count >= width of type
 */
uint32_t _sreaduint32() {
  uint32_t payload = Serial.read();
  payload |= Serial.read() << 8;
  payload |= (uint32_t)Serial.read() << 16;
  payload |= (uint32_t)Serial.read() << 24;
  return payload;
}


/*
  Write an 8-bit unsigned integer to the serial port.
  Return the number of bytes actually written to the serial port.
*/
uint8_t swrite(uint8_t value)
{
  return Serial.write(value);
}


/*
  Write a 16-bit unsigned integer to the serial port.
  Return the number of bytes actually written to the serial port.
*/
uint8_t swrite(uint16_t value)
{
  uint8_t payload[2] = {
    (uint8_t) ((value >> 8) & 0xFF),
    (uint8_t) (value & 0xFF)
  };
  return Serial.write(payload, sizeof(payload));
}


/*
  Write a 32-bit unsigned integer to the serial port.
  Return the number of bytes actually written to the serial port.
*/
uint8_t swrite(uint32_t value)
{
  uint8_t payload[4] = {
    (uint8_t) ((value >> 24) & 0xFF),
    (uint8_t) ((value >> 16) & 0xFF),
    (uint8_t) ((value >> 8 ) & 0xFF),
    (uint8_t) (value & 0xFF)
  };
  return Serial.write(payload, sizeof(payload));
}


/*
  Write a digital transition to the serial port.
*/
void swriteDigitalTransition(uint8_t pin, uint8_t state, uint32_t timestamp)
{
  uint8_t info = ((state & 0x1) << 7) | (pin & 0x7F);
  swrite(uint8_t(DIGITAL_TRANSITION_HEADER));
  swrite(info);
  swrite(timestamp);
}


/*
  Write an analog readout to the serial port.
*/
void swriteAnalogReadout(uint8_t pin, uint16_t adc, uint32_t timestamp)
{
  swrite(uint8_t(ANALOG_READOUT_HEADER));
  swrite(pin);
  swrite(timestamp);
  swrite(adc);
}



/*
  Read an 8-bit opcode followed by an 8-bit payload from the serial port (mind
  this is a blocking read). Write the protocol data to the serial port and
  return the payload.
*/
uint8_t sreadcmd8(uint8_t targetOpcode)
{
  while (Serial.available() < 2)
    ;
  uint8_t opcode = _sreaduint8();
  uint8_t payload = _sreaduint8();
  swrite(targetOpcode);
  swrite(opcode);
  swrite(payload);
  return payload;
}


/*
  Read an 8-bit opcode followed by a 16-bit payload from the serial port (mind
  this is a blocking read). Write the protocol data to the serial port and
  return the payload.
*/
uint16_t sreadcmd16(uint8_t targetOpcode)
{
  while (Serial.available() < 3)
    ;
  uint8_t opcode = _sreaduint8();
  uint16_t payload = _sreaduint16();
  swrite(targetOpcode);
  swrite(opcode);
  swrite(payload);
  return payload;
}


/*
  Read an 8-bit opcode followed by a 32-bit payload from the serial port (mind
  this is a blocking read). Write the protocol data to the serial port and
  return the payload.
*/
uint32_t sreadcmd32(uint8_t targetOpcode)
{
  while (Serial.available() < 5)
    ;
  uint8_t opcode = _sreaduint8();
  uint32_t payload = _sreaduint32();
  swrite(targetOpcode);
  swrite(opcode);
  swrite(payload);
  return payload;
}

