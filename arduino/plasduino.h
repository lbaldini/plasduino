// *********************************************************************
// * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#ifndef plasduino_h
#define plasduino_h

// Conditional compilation for supporting both the post-1.0 and the pre-1.0
// arduino releases.
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif


#include "protocol.h"
#include "sketches.h"


/*
  Definition of the interrupt pins.
  
  Most Arduino boards have two external interrupts: numbers 0 (on digital pin 2)
  and 1 (on digital pin 3); this is all we are supporting at this time.
*/
#define INTERRUPT_PIN_0 2
#define INTERRUPT_PIN_1 3
#define INTERRUPT_MODE_DISABLE 0
#define INTERRUPT_MODE_ENABLE_CHANGE 1
#define INTERRUPT_MODE_ENABLE_FALLING 2
#define INTERRUPT_MODE_ENABLE_RISING 3

/* Constants for counting operation in polling mode.
 */
#define POLLING_MODE_ENABLE_CHANGE 0
#define POLLING_MODE_ENABLE_FALLING -1
#define POLLING_MODE_ENABLE_RISING 1

#define BAUD_RATE 115200
#define EOL_CHAR "\n"

#define NO_OP_PIN_NUMBER 255


void handshake(uint8_t sketchId, uint8_t sketchVersion);

uint8_t _sreaduint8();

uint16_t _sreaduint16();

uint32_t _sreaduint32();

uint8_t swrite(uint8_t value);

uint8_t swrite(uint16_t value);

uint8_t swrite(uint32_t value);

void swriteDigitalTransition(uint8_t pin, uint8_t state, uint32_t timestamp);

void swriteAnalogReadout(uint8_t pin, uint16_t adc, uint32_t timestamp);

uint8_t sreadcmd8(uint8_t targetOpcode);

uint16_t sreadcmd16(uint8_t targetOpcode);

uint32_t sreadcmd32(uint8_t targetOpcode);

#endif
