#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import urllib2
import time

from plasduino.__plasduino__ import PLASDUINO_ARDUINO
from plasduino.__logging__ import logger, abort
from plasduino.arduino.gArduinoBoard import gArduinoBoard


""" This is a small utility module intended to parse all the information
in the arduino boards.txt files and make it available in a python-friendly
version.

Since ide version 1.5 the arduino developers switched to a new scheme where:
1\ there are multiple files supporting multiple architectures.
2\ there are cpu submenus supposting boards produced in multiple flavors.

Having multiple boards.txt files makes the old approach (where we were
parsing the txt files on the fly through regular expressions) more cumbersome,
so we decided to add this small module. And, since we're at that, I guess it
makes sense to stick to the format the arduino folks came up with.
"""


BASE_ARDUINO_URL = 'https://raw.github.com/arduino/Arduino'
ARCH_LIST = ['avr', 'sam']

PY_PREAMBLE = \
"""# coding=UTF-8
# (The former is necessary as the arduino folks use non-ASCII characters in
# their boards.txt files.)

# Created by boards2py.convert() on %s.
#
# Do not edit by hand!


BOARD_DICT = {}

"""


PY_HACK = \
"""
# Beginning of hacks.

# A hack for the diecimila. Not sure why the new boards.txt file
# does not contain the vid, pid pair.
SERIAL_ID_DICT[('0x0403', '0x6001')] = 'atmega328diecimila168'

# End of hacks :-)
"""


PY_TRAILER = \
"""
BOARD_LIST = BOARD_DICT.keys()
BOARD_LIST.sort()


def getBoardByModel(model):
    try:
        return BOARD_DICT[model]
    except KeyError:
        return None 


def getBoardBySerialId(vid, pid):
    try:
        return SERIAL_ID_DICT[(vid, pid)]
    except KeyError:
        return None



if __name__ == '__main__':
    print(SERIAL_ID_DICT)
    print(getBoardByModel('uno'))
    print(getBoardByModel('tre'))
    print(getBoardBySerialId('0x2341', '0x0042'))
    print(getBoardBySerialId(None, None))
"""


def getLocalFilePath(arch):
    """ Return the file path to the local txt file containing the
    specific information for a given architecture.

    This is encapsulated in a function as it needs to be used both
    when downloading and when converting to python.
    """
    return os.path.join(PLASDUINO_ARDUINO, 'boards_arch-%s.txt' % arch)


def download(branch):
    """ Download the necessary board.txt files from the arduino github
    repository.
    """
    for arch in ARCH_LIST:
        url = '%s/%s/hardware/arduino/%s/boards.txt' %\
              (BASE_ARDUINO_URL, branch, arch)
        logger.info('Parsing remote URL %s...' % url)
        content = urllib2.urlopen(url)
        filePath = getLocalFilePath(arch)
        logger.info('Opening local file %s...' % filePath)
        localFile = open(filePath, 'w')
        localFile.write('# Retrieved from %s on %s.\n\n' %\
                        (url, time.asctime()))
        logger.info('Saving content to local file...')
        localFile.write(content.read())
        localFile.close()
        logger.info('Local file closed.')


def __parseTextLine(line):
    """ Parse a single line of text from a arduino boards.txt files.
    """
    line = line.strip('\n').strip()
    if line.startswith('#') or line.startswith('menu') or not len(line):
        return None
    model, pair = line.split('.', 1)
    key, value = pair.split('=')
    return model, key, value


def convert():
    """ Convert the .txt files into a python module containing all the
    information (to be imported downstream).
    """
    
    def addBoard(board):
        """ Convenience nested function.
        """
        for variant in board.getVariantList():
            boards.append(variant)

    # Write the board info to file.
    prevModel = None
    boards = []
    for arch in ARCH_LIST:
        filePath = getLocalFilePath(arch)
        logger.info('Parsing input file %s...' % filePath)
        for line in open(filePath):
            line = line.strip('\n').strip()
            if not line.startswith('#') and not line.startswith('menu') and \
               len(line):
                model, key, value = __parseTextLine(line)
                if prevModel is None or model != prevModel:
                    if prevModel is not None:
                        addBoard(board)
                    board = gArduinoBoard(model, arch)
                board.set(key, value)
                prevModel = model
        addBoard(board)
    filePath = os.path.join(PLASDUINO_ARDUINO, '__boards__.py')
    logger.info('Opening output file %s...' % filePath)
    outputFile = open(filePath, 'w')
    outputFile.write(PY_PREAMBLE % time.asctime())
    for board in boards:
        model = board['model']
        logger.info('Writing full information for model %s...' % model)
        outputFile.write('BOARD_DICT[\'%s\'] = %s\n\n' % (model, board))
    # Create a dictionary of (vid, pid) for convenience.
    serialDict = {}
    for board in boards:
        for vid, pid in board.getSerialIdList():
            key = (vid, pid)
            if not serialDict.has_key(key):
                serialDict[key] = board.get('model')
    logger.info('Writing serial interface data: %s...' % serialDict)
    outputFile.write('SERIAL_ID_DICT = %s\n\n' % serialDict)
    # Finalize the file.
    outputFile.write(PY_HACK)
    outputFile.write(PY_TRAILER)
    outputFile.close()
    logger.info('Output file closed.')

 


if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-d', action = 'store_true', dest = 'download',
                      help = 'Download a new version of the boards.txt files.')
    parser.add_option('-b', type = str, default = 'ide-1.5.x', dest = 'branch',
                      help = 'The git branch for the boards.txt files')
    (opts, args) = parser.parse_args()
    if opts.download:
        download(opts.branch)
    convert()
