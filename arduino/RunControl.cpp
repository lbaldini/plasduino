// *********************************************************************
// * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "RunControl.h"


/* Constructor.
  
   Nothing really fancy, or even interesting, happening here.
 */
RunControl::RunControl() :
  m_running(false),
  m_startMicros(0),
  m_startMillis(0)
{
  
}


/* Start the run.

   We latch the values returned by the micros() and millis() functions from the
   standard library for later use and set the m_running class member to true.
 */
void RunControl::start()
{
  m_startMicros = micros();
  m_startMillis = millis();
  m_running = true;
}


/* Stop the run.

   We set the m_running class member to false and write the run end marker
   to the serial port, signaling that no more event data will be written
   out.
 */
void RunControl::stop()
{
  m_running = false;
  swrite(uint8_t(RUN_END_MARKER));
}


/* Return the microseconds since the start run.

   Note that, this being the difference between two unsigned integers,
   the rollover always happens at the maximum value.
 */
uint32_t RunControl::microsSinceStart()
{
  return micros() - m_startMicros;
}


/* Return the milliseconds since the start run.

   Note that, this being the difference between two unsigned integers,
   the rollover always happens at the maximum value.

 */
uint32_t RunControl::millisSinceStart()
{
  return millis() - m_startMillis;
}


/* Listen to the serial port for commands.

 */
void RunControl::listen()
{
if (Serial.available()) {
    byte opcode = Serial.read();
    if (opcode == OP_CODE_START_RUN) {
      start();
    }
    else if (opcode == OP_CODE_STOP_RUN) {
      stop();
    }
  }
}


/* Listen to the serial port for commands and echo out the opcode.

 */
uint8_t RunControl::echo()
{
  if (Serial.available()) {
    byte opcode = Serial.read();
    if (opcode == OP_CODE_START_RUN) {
      start();
    }
    else if (opcode == OP_CODE_STOP_RUN) {
      stop();
    }
    return opcode;
  }
  return OP_CODE_NO_OP;
}
