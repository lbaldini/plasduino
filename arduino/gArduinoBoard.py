#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import re


class gArduinoBoard(dict):

    """ Small utility class encapsulating all the information related to
    an arduino board.
    """

    VARIANT_STRING = 'menu.cpu.'

    def __init__(self, model, arch):
        """ Constructor.

        This is essentially a dictionary but we do keep track of the insertion
        order and use it in the __str__() method.
        """
        self.__KeyList = []
        self.__CpuVariants = []
        self.set('model', model)
        self.set('arch', arch)

    def set(self, key, value):
        """ Set a key.

        Note that here is where we do keep track of the insertion order
        and of the cpu variants (i.e., do not use [] to set properties).
        """
        self.__KeyList.append(key)
        self[key] = value
        if self.VARIANT_STRING in key:
            variant = key.replace(self.VARIANT_STRING, '').strip()
            variant = variant.split('=')[0].split('.')[0]
            if variant not in self.__CpuVariants:
                self.__CpuVariants.append(variant)

    def get(self, key, default = None):
        """ Access properties by key.
        """
        try:
            return self[key]
        except KeyError:
            return default

    def getVariantList(self):
        """ This is the main part of the class: we return a list of separate
        gArduinoBoard objects, i.e., one per variant.

        The information about board variants is stored in the arduino
        boards.txt files in the form of sub-menus. Here we switch back
        to a plain list in which part of the information is duplicated
        across variants of the same boards. This provides a simpler
        interaface to find out the necessary parameters at compile and upload
        time.
        """
        # If there are no variants, there is nothing to do, here.
        if not len(self.__CpuVariants):
            return [self]
        boards = []
        for variant in self.__CpuVariants:
            # Create the suffix to be appended to the model.
            suffix = re.findall('\d+', variant)[0]
            model = '%s%s' % (self['model'], suffix)
            arch = self['arch']
            # Create a new gArduinoBoard object.
            board = gArduinoBoard(model, arch)
            # Now loop over the keys of the parent board to set the
            # properties for each variant (mind that 'model' and 'arch' are
            # set, already).
            for key in self.__KeyList:
                if key not in ['model', 'arch'] and \
                   (self.VARIANT_STRING not in key or variant in key):
                    value = self[key]
                    # Strip the variant part of the key, if necessary.
                    if variant in key:
                        key = key.split(variant)[-1].strip('.')
                    if key == '':
                        key = 'nickname'
                    board.set(key, value)
            boards.append(board)
        return boards

    def getSerialIdList(self, maxNum = 4):
        """ Return a list of (vid, pid) for the serial interfaces onboard.

        This is handy for the autodetection of the boards.
        """
        ids = []
        for i in range(maxNum):
            vid, pid = self.get('vid.%d' % i), self.get('pid.%d' % i)
            if vid is not None and pid is not None:
                ids.append((vid, pid))
        return ids
    
    def __str__(self):
        """ String formatting.
        """
        text = '{'
        for key in self.__KeyList:
            text += '\'%s\': \'%s\', ' % (key, self[key])
        text = '%s}' % (text.strip(', '))
        return text
       


if __name__ == '__main__':
    data = \
"""
##############################################################

mega.name=Arduino Mega or Mega 2560

mega.vid.0=0x2341
mega.pid.0=0x0010
mega.vid.1=0x2341
mega.pid.1=0x0042

mega.upload.tool=avrdude
mega.upload.maximum_data_size=8192

mega.bootloader.tool=avrdude
mega.bootloader.low_fuses=0xFF
mega.bootloader.unlock_bits=0x3F
mega.bootloader.lock_bits=0x0F

mega.build.f_cpu=16000000L
mega.build.core=arduino
mega.build.variant=mega
# default board  may be overridden by the cpu menu
mega.build.board=AVR_MEGA2560

## Arduino Mega w/ ATmega2560
## -------------------------
mega.menu.cpu.atmega2560=ATmega2560 (Mega 2560)

mega.menu.cpu.atmega2560.upload.protocol=wiring
mega.menu.cpu.atmega2560.upload.maximum_size=258048
mega.menu.cpu.atmega2560.upload.speed=115200

mega.menu.cpu.atmega2560.bootloader.high_fuses=0xD8
mega.menu.cpu.atmega2560.bootloader.extended_fuses=0xFD
mega.menu.cpu.atmega2560.bootloader.file=stk500v2/stk500boot_v2_mega2560.hex

mega.menu.cpu.atmega2560.build.mcu=atmega2560
mega.menu.cpu.atmega2560.build.board=AVR_MEGA2560

## Arduino Mega w/ ATmega1280
## -------------------------
mega.menu.cpu.atmega1280=ATmega1280

mega.menu.cpu.atmega1280.upload.protocol=arduino
mega.menu.cpu.atmega1280.upload.maximum_size=126976
mega.menu.cpu.atmega1280.upload.speed=57600

mega.menu.cpu.atmega1280.bootloader.high_fuses=0xDA
mega.menu.cpu.atmega1280.bootloader.extended_fuses=0xF5
mega.menu.cpu.atmega1280.bootloader.file=atmega/ATmegaBOOT_168_atmega1280.hex

mega.menu.cpu.atmega1280.build.mcu=atmega1280
mega.menu.cpu.atmega1280.build.board=AVR_MEGA

##############################################################
"""

    mega = gArduinoBoard('mega', 'avr')
    for line in data.split('\n'):
        line = line.strip('\n').strip()
        if not line.startswith('#') and len(line):
            name, pair = line.split('.', 1)
            key, value = pair.split('=')
            mega.set(key, value)
    print(data)
    print(mega)
    print(mega.getSerialIdList())
    for board in mega.getVariantList():
        print(board)
        print(board.getSerialIdList())
