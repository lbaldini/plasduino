//-*- Mode:c -*-
// *********************************************************************
// * Copyright (C) 2012 Carmelo Sgro (carmelo.sgro@pi.infn.it)         *
// * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "RunControl.h"
#include "shield_lab1.h"

/*
  Configurable sketch for a synchronous analog readout on multiple pins.

  The basic idea is that of a round robin scheme where the selected pins are
  sensed one at a time, with a full loop accomplished in a fixed time.
  This sketch is *not* meant to be used when operations must be streamlined
  for speed, nor when different pines need to be sensed as close as possible
  in time. In fact the readout are equally spaced in time.
  
  Up to 6 pins (i.e., as many as available on the uno board) are supported.
*/
#define SKETCH_ID SKETCH_ID_ANALOG_SAMPLING
#define SKETCH_VERSION 3

#define MAX_NUM_INPUT_PINS 6
#define MIN_SAMPLING_INTERVAL 10


/*
  Initialize global variables.
*/ 
RunControl RUN_CONTROL;
uint8_t NUM_INPUT_PINS = 0;
uint8_t INPUT_PINS[MAX_NUM_INPUT_PINS];
uint32_t SAMPLING_INTERVAL = 0;
uint32_t POLL_DELAY = 0;
uint8_t _pin;
uint16_t _adc;
uint32_t _timestamp;



/*
  Basic sketch configuration.
 */
void _config() {
  NUM_INPUT_PINS = sreadcmd8(OP_CODE_SELECT_NUM_ANALOG_PINS);
  if (NUM_INPUT_PINS) {
    for (uint8_t i = 0; i < NUM_INPUT_PINS; i++) {
      INPUT_PINS[i] = sreadcmd8(OP_CODE_SELECT_ANALOG_PIN);
    }
    SAMPLING_INTERVAL = sreadcmd32(OP_CODE_SELECT_SAMPLING_INTERVAL);
    if (SAMPLING_INTERVAL < MIN_SAMPLING_INTERVAL) {
      SAMPLING_INTERVAL = MIN_SAMPLING_INTERVAL;
    }
    POLL_DELAY = SAMPLING_INTERVAL/NUM_INPUT_PINS;
  }
}


/*
  setup() implementation.

  Here we essentially read the parameters to be configured from the
  python application and setup things for the polling cycle.
  
  Note we enforce a minimum sampling interval (again, this is *not* streamlined
  for speed) and we divide the interval itself by the number of pins to
  be readout---the last figure being the actual delay to be used in the
  loop() call. 
*/
void setup() {
  setupBoard();
  handshake(SKETCH_ID, SKETCH_VERSION);
  _config();
}


/*
  loop() implementation.

  In order to minimize possible issues with the input impedence to the
  arduino ADC being too large (which implies that the time necessary to
  charge up the sample/hold capacitor might be too large), we do a first
  readout and then wait for POLL_DELAY ms before we do the actual analog
  readout which is written to the serial port. By doing this we ensure 
  that the ADC is connected to the desired channel of the multiplexer for
  the maximum possible amount of time before we issue the analogRead to be
  written out.
 */
void loop() {
  RUN_CONTROL.listen();
  if (RUN_CONTROL.running()) {
    for (uint8_t i = 0; i < NUM_INPUT_PINS; i++) {
      _pin = INPUT_PINS[i];
      _adc = analogRead(_pin);
      delay(POLL_DELAY);
      _adc = analogRead(_pin);
      _timestamp = RUN_CONTROL.millisSinceStart();
      swriteAnalogReadout(_pin, _adc, _timestamp);
    }
  }
}
