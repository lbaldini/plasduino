//-*- Mode:c -*-
// *********************************************************************
// * Copyright (C) 2012 Carmelo Sgro (carmelo.sgro@pi.infn.it)         *
// * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
// *                                                                   *
// * For the license terms see the file LICENCE, distributed           *
// * along with this software.                                         *
// *********************************************************************
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#include "RunControl.h"


#define SKETCH_ID SKETCH_ID_TEST_PROTOCOL
#define SKETCH_VERSION 3


uint8_t TEST_DIGITAL_PIN = NO_OP_PIN_NUMBER;
uint8_t TEST_ANALOG_PIN = NO_OP_PIN_NUMBER;
RunControl RUN_CONTROL;


/*
  Basic sketch configuration.
 */
void _config() {
  TEST_DIGITAL_PIN = sreadcmd8(OP_CODE_SELECT_DIGITAL_PIN);
  TEST_ANALOG_PIN = sreadcmd8(OP_CODE_SELECT_ANALOG_PIN);
}


/*
  setup() implementation.
 */
void setup() {
  handshake(SKETCH_ID, SKETCH_VERSION);
  _config();
}


/*
  loop() implementation.
 */
void loop() {
  RUN_CONTROL.listen();
  if (RUN_CONTROL.running()) {
    uint32_t timestamp = RUN_CONTROL.microsSinceStart();
    swriteDigitalTransition(TEST_DIGITAL_PIN, HIGH, timestamp);
  }
  delay(1000);
}
