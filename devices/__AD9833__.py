#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from plasduino.devices.gDevice import gDevice
from plasduino.__logging__ import logger


class AD9833(gDevice):
    
    """ Class describing an AD9833 waveform generator.

    The AD9833 comes with a 16-bits control register, 2 28-bits frequency
    registers and 2 12-bits phase registers. The data sheet is available at:
    http://www.analog.com/static/imported-files/data_sheets/AD9833.pdf

    D15 and D14 have a special meaning:
    (*) D15 = 0, D14 = 0 -> writing the control register.
    (*) D15 = 0, D14 = 1 -> writing FREQ0.
    (*) D15 = 1, D14 = 0 -> writing FREQ1.
    (*) D15 = 1, D14 = 1 -> writing PHASE0/1 (D13 identifies which).

    The layout or the control register is:
    D15  -         see above.
    D14  -         see above.
    D13  B28       1: write all 28 bits in two passes; 0: write only 14 bits.
    D12  HLB       1: write the 14 MSB; 0: write the 14 LSB (need B28 = 0).
    D11  FSELECT   select FREQ0/1 register.
    D10  PSELECT   select PHASE0/1 register.
    D9   Reserved  should be 0.
    D8   Reset     reset.
    D7   SLEEP1    enable (0) or disable (1) internal MCLK.
    D6   SLEEP12   power-up (0) or power down (1) the internal DAC.
    D5   OPBITEN   DAC to ouptut (0) or square wave (1).
    D4   Reserved  shuold be 0.
    D3   DIV2      with OPBITEN, affects the amplitude of the square wave.
    D2   Reserved  should be 0.
    D1   Mode      0 for sinusoidal, 1 for triangular (if OPBITEN = 0).
    D0   Reserved  should be 0.

    The output frequency is f_MCLK * FREQREG / 2**28.

    The output phase is 2*pi * PHASEREG / 2**12.

    (2**28 = 268,435,456, 2**12 = 4,096)
    """

    OUTPUT_MODE_SQUARE = 0x0
    OUTPUT_MODE_TRIANGULAR = 0x1
    OUTPUT_MODE_SINUSOIDAL = 0x2
    MAX_FREQ_VALUE = 2**28 - 1 
    MAX_PHASE_VALUE = 2**12 - 1

    def __init__(self):
        """ Constructor.
        """
        gDevice.__init__(self, 'AD9833', 'ANALOG DEVICES')

    def payloadPowerUp(self, mode, freqRegister = 0, phaseRegister = 0):
        """ Power up the DAC.

        D11  FSELECT   select FREQ0/1 register.
        D10  PSELECT   select PHASE0/1 register.

        Payload for the square wave (FSELECT and PSELECT = 0):
        D15 D14 D13 D12 | D11 D10 D9  D8 |  D7  D6  D5  D4 |  D3  D2  D1  D0
        0   0   1   0   | 0   0   0*  0  |  0   0   1   0* |  0   0*  0   0*

        Payload for the triangular wave (FSELECT and PSELECT = 0):
        D15 D14 D13 D12 | D11 D10 D9  D8 |  D7  D6  D5  D4 |  D3  D2  D1  D0
        0   0   1   0   | 0   0   0*  0  |  0   0   0   0* |  0   0*  1   0*

        Payload for the sinusoidal wave (FSELECT and PSELECT = 0):
        D15 D14 D13 D12 | D11 D10 D9  D8 |  D7  D6  D5  D4 |  D3  D2  D1  D0
        0   0   1   0   | 0   0   0*  0  |  0   0   0   0* |  0   0*  0   0*
        """
        if mode == self.OUTPUT_MODE_SQUARE:
            content = 0x20
        elif mode == self.OUTPUT_MODE_TRIANGULAR:
            content = 0x2
        elif mode == self.OUTPUT_MODE_SINUSOIDAL:
            content = 0x0
        else:
            logger.error('Unknown output mode (%s) for AD9833.' % mode)
            return None
        content |= ((freqRegister & 0x1) << 11)
        content |= ((phaseRegister & 0x1) << 10)
        # Is this really necessary?
        #content |= 0x1 << 13
        return content

    def payloadPowerDown(self):
        """ Return the payload to power down the chip.

        D7   SLEEP1    enable (0) or disable (1) internal MCLK.
        D6   SLEEP12   power-up (0) or power down (1) the internal DAC.
        Here we set both D7 and D6 up.
        """
        content = 0xc0
        return content

    def payloadSetFrequency(self, frequency, freqRegister = 0,
                            mclk = 20000000.0):
        """ Return the payload(s) to write one of the frequency registers.

        All frequencies are in Hz.

        Note we need three SDATA outputs, here, namely:
        (*) a control word with B13 = 1 to signal the two 16-bits numbers.
        (*) the 14 LSBs.
        (*) the 14 MSBs.

        Remember:
        (*) D15 = 0, D14 = 1 -> writing FREQ0.
        (*) D15 = 1, D14 = 0 -> writing FREQ1.
        """
        control = 0x20c0
        content = long(frequency/mclk*self.MAX_FREQ_VALUE)
        if content > self.MAX_FREQ_VALUE:
            content = self.MAX_FREQ_VALUE
        elif content < 1:
            content = 1
        lsbs = content & 0x3fff
        msbs = (content & 0xfffc000) >> 14
        if freqRegister == 0:
            lsbs |= 0x4000
            msbs |= 0x4000
        elif freqRegister == 1:
            lsbs |= 0x8000
            msbs |= 0x8000
        else:
            logger.error('Unknown FREQ register (%s) for AD9833.' %\
                             freqRegister)
            return None
        return (control, lsbs, msbs)

    def payloadSetPhase(self, phase, phaseRegister = 0):
        """ Return the payload to write one of the phase registers.

        Remember:
        D15 and D14 are set to one, D13 identifies the phaseRegister, D12 does
        not matter and the rest is the actual 12 bits.
        (*) D15 = 1, D14 = 1 -> writing PHASE0/1 (D13 identifies which).
        """
        content = int(phase/(2*math.pi)*self.MAX_PHASE_VALUE)
        if content > self.MAX_PHASE_VALUE:
            content = self.MAX_PHASE_VALUE
        elif content < 0:
            content = 0
        content = (content & 0xfff) | (0x3 << 14)
        content |= ((phaseRegister & 0x1) << 13)
        return content
            


if __name__ == '__main__':
    wg = AD9833()
    print(wg)
    for mode in [AD9833.OUTPUT_MODE_SQUARE, AD9833.OUTPUT_MODE_TRIANGULAR,
                 AD9833.OUTPUT_MODE_SINUSOIDAL]:
        print('Payload for power up in mode %d:' % mode)
        print('0x%x' % wg.payloadPowerUp(mode))

    print('Payload for power down:')
    print('0x%x' % wg.payloadPowerDown())

    freq = 2400.
    phase = math.pi
    clock = 10000000.
    for reg in [0, 1]:
        print('Payload for loading %.2f on FREQ%d (mclk = %.2f):' %\
                  (freq, reg, clock))
        for item in wg.payloadSetFrequency(freq, reg, clock):
            print('0b{0:016b}'.format(item), '0x%x' % item)
        print('Payload for loading %.2f on PHASE%d:' % (phase, reg))
        print('0b{0:016b}'.format(wg.payloadSetPhase(phase, reg)),
              '0x%x' % item)
