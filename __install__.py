#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import plasduino.__utils__ as __utils__


""" Basic installation file.

We might consider moving all this stuff back into the __plasduino__module.
"""

""" Folder for the plasduino configuration files.

This gets automatically created if it does not exist.
"""
PLASDUINO_CONFIG = os.path.expanduser(os.path.join('~', 'plasduino'))
if not os.path.exists(PLASDUINO_CONFIG):
    __utils__.createFolder(PLASDUINO_CONFIG)

""" Path to the system-wide configuration file.
"""
CONFIG_FILE_PATH = os.path.join(PLASDUINO_CONFIG, 'plasduino.cfg')


""" Path to the run id file.
"""
RUN_ID_FILE_PATH_DEFAULT = os.path.join(PLASDUINO_CONFIG, 'runId.cfg')


""" Folder for the plasduino data files.
"""
PLASDUINO_DATA_DEFAULT = os.path.join(PLASDUINO_CONFIG, 'data')


""" Folder for the plasduino log files.
"""
PLASDUINO_LOG_DEFAULT = os.path.join(PLASDUINO_CONFIG, 'log')


""" Folder for the plasduino temporary files.
"""
PLASDUINO_TMP_DEFAULT = os.path.join(PLASDUINO_CONFIG, 'tmp')


""" Folder for custom modules.

The user will install here extra modules that are not within the official
plasduino distribution. This folder should not be created until there is
something to put in it.
"""
PLASDUINO_CUSTOM_MODULES = os.path.join(PLASDUINO_CONFIG, 'modules')


""" Path to moduleinfo pickle files containing the relevant information for
the user-defined modules.
"""
PLASDUINO_CUSTOM_MODULEINFO_FILE_PATH = os.path.join(PLASDUINO_CONFIG,
                                                     'custom_modules.pickle')




if __name__ == '__main__':
    for item in dir():
        if item.isupper():
            print('%s = %s' % (item, eval(item)))
