#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



class gTeamMember:

    """
    """

    def __init__(self, name, email, webpage = None, startYear = 2012,
                 endYear = None):
        """Constructor.
        """
        self.Name = name
        self.Email = email
        self.WebPage = webpage
        self.StartYear = startYear
        self.EndYear = endYear

    def __str__(self):
        """ String formatting.
        """
        return '%s <%s>' % (self.Name, self.Email)



DEVELOPERS = [
    gTeamMember('Luca Baldini', 'luca.baldini@pi.infn.it',
                'http://www.df.unipi.it/~baldini'),
    gTeamMember('Johan Enoc Bregeon', 'johan.bregeon@pi.infn.it'),
    gTeamMember('Jacopo Nespolo', 'j.nespolo@gmail.com'),
    gTeamMember('Massimiliano Razzano', 'massimiliano.razzano@pi.infn.it'),
    gTeamMember('Melissa Pesce-Rollins', 'melissa.pesce.rollins@pi.infn.it'),
    gTeamMember('Carmelo Sgro', 'carmelo.sgro@pi.infn.it')
    ]


def mailtoDevelopersAnchor(text):
    """ Return an html anchor to send an email to all developers.
    """
    mailingList = str([item.Email for item in DEVELOPERS])
    mailingList = mailingList.strip('[] ').replace('\'', '').replace(' ', '')
    return '<a href="mailto:%s">%s</a>' % (mailingList, text)

GRAPHICS_DESIGNERS = [
    gTeamMember('Rosalia Nunziante', 'rosalia.nunziante@libero.it')
]

ELX_GURUS = [
    gTeamMember('Massimo Minuti', 'massimo.minuti@pi.infn.it'),
    gTeamMember('Fabio Morsani', 'fabio.morsani@pi.infn.it')
    ]

TECHNICIANS = [
    gTeamMember('Enrico Andreoni', 'enrico.andreoni@df.unipi.it'),
    gTeamMember('Andrea Bianchi', 'andrea.bianchi@df.unipi.it'),
    gTeamMember('Virginio Merlin', 'virginio.merlin@df.unipi.it'),
    gTeamMember('Stefano Orselli', 'orselli@df.unipi.it')
    ]

CREDITS = ['Franco Angelini',
           'Bruno Barsella',
           'Francesco Fidecaro',
           'Claudio Luperini',
           'Marco Maria Massai'
           ]

CREDITS_TXT = ('%s' % CREDITS).strip('[]').replace('\'', '')


if __name__ == '__main__':
    print('Developers:')
    for developer in DEVELOPERS:
        print(developer)
    print('')
    print(mailtoDevelopersAnchor('hello!'))
    print('')
    print('Technical support:')
    for technician in TECHNICIANS:
        print(technician)
