
from __root__ import *
from numpy import array
from math import acos, sqrt

MARKER_STYLE  = 7
T_START   = 40
T_STOP    = 200
T_START_1 = 5
T_STOP_1  = 90
T1_FINAL  = 23.4
DT        = 0#0.2
DT1       = 0.2
#OUTPUT_FOLDER = "../"

# in case you want to change Fitter
#ROOT.TVirtualFitter.SetDefaultFitter("Minuit2");
#ROOT.TVirtualFitter.SetDefaultFitter("Fumili2");


###############
def getResiduals(x,y,f):
    r  = []
    rr = []
    sc = 0
    for i in xrange(len(x)):
        r.append(y[i] -f.Eval(x[i]))
        c = (y[i] -f.Eval(x[i]))**2 
        rr.append(c)
        sc+= c
    return  (array(r), array(rr), sc)

###############


f  = open("data/temperature_000191_processed.txt")

# read data file
t = [] # time
T = [] # Temperature
for l in f:
    if l[0]!="#":
        [a0, a1, a2] = l.strip('\n').split('\t\t')
        t.append(float(a0))
        T.append(float(a1))
        
n = len(t)
t = array(t)
T = array(T)
dt = array(n*[0.])
dT = array(n*[DT])

#f1 = open("data/temperature_000192_processed.txt")
f1 = open("data/temperature_000145_processed.txt")

# read data file
t1 = [] # time
T1 = [] # Temperature
for l in f1:
    if l[0]!="#":
        [a0, a1, a2] = l.strip('\n').split('\t\t')
        t1.append(float(a0))
        #T1.append(float(a1))
        T1.append(float(a2))
        
n1 = len(t1)
t1 = array(t1)
T1 = array(T1)
dt1 = array(n1*[0.])
dT1 = array(n1*[DT1])

# ================ plot bar
canvas = TCanvas("temperature_bar", grids=True)

gb = ROOT.TGraphErrors(n, t, T, dt, dT)
gb.SetTitle("Temperature")
gb.GetYaxis().SetTitle("Temperature [#circC]")
gb.GetXaxis().SetTitle("Elapsed time [s]")
gb.SetMarkerStyle(MARKER_STYLE)
gb.GetXaxis().SetRangeUser(T_START, T_STOP)
gb.GetYaxis().SetRangeUser(20, 50)
#gb.GetYaxis().SetNdivisions(505)
gb.GetYaxis().CenterTitle(True)
gb.Draw("AP")

canvas.annotate(45,  23, "position x in cm, #Deltax = 0.2 cm" , ndc =False)
canvas.annotate(50,  46, "x = 0 (reference)" , ndc =False)
canvas.annotate(80,  40, "x = 10" ,            ndc =False)
canvas.annotate(120, 32, "x = 22.5" ,          ndc =False)
canvas.annotate(155, 24, "x = 35" ,            ndc =False)
canvas.annotate(190, 27, "Air" ,               ndc =False)
canvas.Update()
#canvas.save()

# ================ plot bar
canvas1 = TCanvas("temperature_tau", grids=True, logy=True)

#T1 = T1-T1_FINAL
g1 = ROOT.TGraphErrors(n1, t1, T1, dt1, dT1)
g1.SetTitle("Temperature")
g1.GetYaxis().SetTitle("Temperature [#circC]")
g1.GetXaxis().SetTitle("Elapsed time [s]")
g1.SetMarkerStyle(MARKER_STYLE)
g1.GetXaxis().SetRangeUser(T_START_1, T_STOP_1)
g1.GetYaxis().CenterTitle(True)
g1.Draw("AP")


e1 = ROOT.TF1("e1", "exp([0]-x/[1])", 20, 90)
e1.SetParameters(10, 4)
e1.SetLineColor(ROOT.kRed)
g1.Fit("e1", "R")


canvas1.Update()
#canvas1.save()
