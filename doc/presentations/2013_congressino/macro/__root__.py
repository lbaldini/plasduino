
import os
import sys
import ROOT

def setGrayscalePalette():
    """ Load the black and white palette.
    """
    print 'Loading the grayscale palette...'
    Palette_Gray.CreateGradientColorTable(ROOT.gStyle)
    print 'Done.'

def revertPalette():
    """ Revert the palette to the default value (to be used in case we
    temporarely switch to black and white).
    """
    print 'Reverting the ROOT palette to the original settings...'
    Palette_Default.CreateGradientColorTable(ROOT.gStyle)
    print 'Done.'

# Top-level settings.
STYLE_NAME  = 'congressino'
STYLE_TITLE = 'Style for the plasduino poster'
PALETTE = 'Default'
DEBUG = False
TEXT_FONT = 43
TEXT_SIZE = 31
LABEL_TEXT_SIZE = 0.9*TEXT_SIZE
LEGEND_TEXT_SIZE = 0.9*TEXT_SIZE
SMALL_TEXT_SIZE = 0.8*TEXT_SIZE
SMALLER_TEXT_SIZE = 0.7*TEXT_SIZE
SMALLEST_TEXT_SIZE = 0.6*TEXT_SIZE
CANVAS_DEF_WIDTH  = 840
CANVAS_DEF_HEIGHT = 600

ROOT.gROOT.SetStyle('Plain')
STYLE = ROOT.gStyle
STYLE.SetOptTitle(0)
STYLE.SetOptStat(0000)

# More setting
STYLE.SetCanvasDefW(CANVAS_DEF_WIDTH)
STYLE.SetCanvasDefH(CANVAS_DEF_HEIGHT)
# Text Font and Precision
# The text font code is combination of the font number and the precision.
#
#   Text font code = 10*fontnumber + precision
#
# Font numbers must be between 1 and 14.
#
# The precision can be:
# 0 fast hardware fonts (steps in the size)
# 1 scalable and rotatable hardware fonts (see below)
# 2 scalable and rotatable hardware fonts
# 3 scalable and rotatable hardware fonts. Text size is given in pixels. 
STYLE.SetTextFont(TEXT_FONT)
STYLE.SetTextSize(TEXT_SIZE)
STYLE.SetTitleFont(TEXT_FONT, 'XYZ')
STYLE.SetTitleSize(TEXT_SIZE, 'XYZ')
STYLE.SetLabelFont(TEXT_FONT, 'XYZ')
STYLE.SetLabelSize(LABEL_TEXT_SIZE, 'XYZ')
STYLE.SetTitleYOffset(1.16)
STYLE.SetTitleXOffset(1.08)
STYLE.SetTitleOffset(1.0, 'Z')
STYLE.SetLegendBorderSize(0)
CANVAS_RIGHT_MARGIN = 0.03
STYLE.SetPadRightMargin(CANVAS_RIGHT_MARGIN)
CANVAS_TOP_MARGIN = 0.05
STYLE.SetPadTopMargin(CANVAS_TOP_MARGIN)
CANVAS_LEFT_MARGIN = 0.125*TEXT_SIZE/31.
STYLE.SetPadLeftMargin(CANVAS_LEFT_MARGIN)
CANVAS_BOTTOM_MARGIN = 0.130*TEXT_SIZE/31.
STYLE.SetPadBottomMargin(CANVAS_BOTTOM_MARGIN)
STYLE.SetStatBorderSize(0)
STYLE.SetStatFont(TEXT_FONT)
STYLE.SetStatFontSize(SMALLEST_TEXT_SIZE)
STYLE.SetGridColor(ROOT.kGray + 1)
STYLE.SetGridStyle(2)
STYLE.SetStatStyle(0)
STYLE.SetMarkerStyle(7)
STYLE.SetLineWidth(2)
STYLE.SetHistLineWidth(2)

# Apply the style
#ROOT.gROOT.SetStyle(STYLE_NAME)
#ROOT.gROOT.ForceStyle()

ROOT_OBJECT_POOL = []

def store(rootObject):
    ROOT_OBJECT_POOL.append(rootObject)



class TCanvas(ROOT.TCanvas):
    """ Small wrapper around the TCanvas object.
    """

    def __init__(self, name, title = None, **kwargs):
        """ Overloaded constructor.
        """
        ROOT.TCanvas.__init__(self, name, title or name)
        if kwargs.get('logx', False):
            self.SetLogx(True)
        if kwargs.get('logy', False):
            self.SetLogy(True)
        if kwargs.get('logz', False):
            self.SetLogz(True)
        if kwargs.get('gridx', False) or kwargs.get('grids', False):
            self.SetGridx(True)
        if kwargs.get('gridy', False) or kwargs.get('grids', False):
            self.SetGridy(True)
        if kwargs.get('colz', False):
            if kwargs.get('ztitle', False):
                self.SetRightMargin(0.16)
            else:
                self.SetRightMargin(0.12)

    def setThreePerPage(self):
        """ Set the width of the canvas in such a way three figures next to
        each other will fit in one page.
        """
        width = int(CANVAS_DEF_WIDTH/1.5 + 0.5)
        self.SetWindowSize(width, CANVAS_DEF_HEIGHT)
        self.SetLeftMargin(1.12*self.GetLeftMargin())

    def setOnePerPage(self):
        """
        """
        width = 2*CANVAS_DEF_WIDTH
        self.SetWindowSize(width, CANVAS_DEF_HEIGHT)
        self.SetLeftMargin(0.5*self.GetLeftMargin())

    def setAitoff(self, colz = False):
        if colz:
            height = CANVAS_DEF_WIDTH/2 - 40
            rightMargin = 0.17
        else:
            height = CANVAS_DEF_WIDTH/2 + 20
            rightMargin = 0.01
        self.SetWindowSize(CANVAS_DEF_WIDTH, height)
        self.SetLeftMargin(0.01)
        self.SetTopMargin(0.01)
        self.SetRightMargin(rightMargin)
        self.SetBottomMargin(0.01)

    def Update(self,noDebug=False):
        """ Overloaded method to update the canvas and write the style
        version, if in debug mode.
        """
        if DEBUG and not noDebug:
            text = '"%s" style. %s' % (STYLE_NAME, __revision__)
            text = text.replace('$', '').strip()
            self.annotate(0.99, 0.99, text, ndc = True, align = 33,
                          size = SMALLEST_TEXT_SIZE)
            return
        ROOT.TCanvas.Update(self)

            
    def annotate(self, x, y, text, ndc = True, align = 11, size = TEXT_SIZE,
                 color = ROOT.kBlack, angle = 0):
        """ Draw some text on the canvas.
        """
        self.cd()
        label = ROOT.TLatex(x, y, text)
        if ndc:
            label.SetNDC(True)
        label.SetTextAlign(align)
        label.SetTextSize(size)
        label.SetTextColor(color)
        label.SetTextAngle(angle)
        label.Draw()
        store(label)
        ROOT.TCanvas.Update(self)
        
    def label(self, text, x = 0.155, y = 0.875):
        """ Put a label such (a), (b) for the composed panels.
        """
        self.annotate(x, y, '(%s)' % text)

    def save(self, outputFolder = None, formats = ['pdf', 'png']):
        """ Save the canvas in different formats using the canvas name to
        define the file name.
        """
        for format in formats:
            filePath = '%s.%s' % (self.GetName(), format)
            if outputFolder is not None:
                filePath = os.path.join(outputFolder, filePath)
            self.SaveAs(filePath)


class TStackCanvas(TCanvas):

    """
    """
    VSPACE = 0.034

    def __init__(self, name, numPads, title = None, **kwargs):
        """ Constructor.
        """
        self.Height = kwargs.get('height', 300*numPads)
        ROOT.TCanvas.__init__(self, name, title or name, CANVAS_DEF_WIDTH,
                              self.Height)
        # Scale the top and bottom margins to the current canvas height.
        topMargin = float(CANVAS_TOP_MARGIN)/self.Height*CANVAS_DEF_HEIGHT
        botMargin = float(CANVAS_BOTTOM_MARGIN)/self.Height*CANVAS_DEF_HEIGHT
        # Vertical sectioning in canvas units.
        dymid = (1 - topMargin - botMargin)/numPads
        dytop = dymid + topMargin
        dybot = dymid + botMargin
        self.TopPadHeight = int(dytop*self.Height + 0.5)
        self.MidPadHeight = int(dymid*self.Height + 0.5)
        self.BotPadHeight = int(dybot*self.Height + 0.5)
        # Create the pads.
        self.PadList = []
        for i in xrange(numPads):
            if i == 0:
                y1 = 1. - dytop
                y2 = 1.
                pad = ROOT.TPad('pad%d' % i, 'Pad %d' % i, 0.0, y1, 1.0, y2)
                topm = CANVAS_TOP_MARGIN/self.TopPadHeight*CANVAS_DEF_HEIGHT
                botm = self.VSPACE/self.TopPadHeight*CANVAS_DEF_HEIGHT
                pad.SetBottomMargin(botm)
                pad.SetTopMargin(topm)
            elif i == numPads - 1:
                y2 = y1
                y1 = 0.
                pad = ROOT.TPad('pad%d' % i, 'Pad %d' % i, 0.0, 0.0, 1.0, y2)
                topm = self.VSPACE/self.BotPadHeight*CANVAS_DEF_HEIGHT
                botm = CANVAS_BOTTOM_MARGIN/self.BotPadHeight*CANVAS_DEF_HEIGHT
                pad.SetBottomMargin(botm)
                pad.SetTopMargin(topm)
            else:
                y2 = y1
                y1 = y2 - dymid
                pad = ROOT.TPad('pad%d' % i, 'Pad %d' % i, 0.0, y1, 1.0, y2)
                botm = 0.5*self.VSPACE/self.MidPadHeight*CANVAS_DEF_HEIGHT
                topm = 0.5*self.VSPACE/self.MidPadHeight*CANVAS_DEF_HEIGHT
                pad.SetBottomMargin(botm)
                pad.SetTopMargin(topm)
            self.PadList.append(pad)
            pad.Draw()
        if kwargs.get('gridx', False) or kwargs.get('grids', False):
            self.SetGridx(True)
        if kwargs.get('gridy', False) or kwargs.get('grids', False):
            self.SetGridy(True)
        self.cd(1)

    def annotate(self, x, y, text, ndc = True, align = 11, size = TEXT_SIZE,
                 color = ROOT.kBlack, angle = 0, pad = 1):
        """ Draw some text on the canvas.
        """
        self.cd(pad)
        label = ROOT.TLatex(x, y, text)
        if ndc:
            label.SetNDC(True)
        label.SetTextAlign(align)
        label.SetTextSize(size)
        label.SetTextColor(color)
        label.SetTextAngle(angle)
        label.Draw()
        store(label)
        self.cd()
        ROOT.TCanvas.Update(self)

    def SetGridx(self, gridx):
        """
        """
        for pad in self.PadList:
            pad.SetGridx(gridx)

    def SetGridy(self, gridy):
        """
        """
        for pad in self.PadList:
            pad.SetGridy(gridy)

    def cd(self, pad = None):
        """
        """
        if pad is None:
            TCanvas.cd(self)
        else:
            self.PadList[pad - 1].cd() 
        
    def setupPlots(self, *plots):
        """
        """
        for plot in plots[:-1]:
            plot.GetXaxis().SetLabelOffset(3)
            plot.GetXaxis().SetTitleOffset(3)
            yoffset = plot.GetYaxis().GetTitleOffset()*self.Height/\
                CANVAS_DEF_HEIGHT
            plot.GetYaxis().SetTitleOffset(yoffset)
        plot = plots[-1]
        xoffset = plot.GetXaxis().GetTitleOffset()/\
            self.BotPadHeight*self.Height
        plot.GetXaxis().SetTitleOffset(xoffset)
        plot.GetYaxis().SetTitleOffset(yoffset)



class TResidualCanvas(TCanvas):

    """
    """

    def __init__(self, name, title = None, **kwargs):
        """ Constructor.
        """
        ROOT.TCanvas.__init__(self, name, title or name)
        split = kwargs.get('vsplit', 0.4)
        self.MainPad = ROOT.TPad('main_pad', 'Main pad', 0.0, split, 1.0, 1.0)
        self.MainPad.SetBottomMargin(0.032)
        self.MainPad.SetTopMargin(0.08)
        self.ResPad = ROOT.TPad('res_pad', 'Res pad', 0.0, 0.0, 1.0, split)
        self.ResPad.SetBottomMargin(0.32)
        self.ResPad.SetTopMargin(0.01)
        if kwargs.get('logx', False):
            self.SetLogx(True)
        if kwargs.get('logy', False):
            self.SetLogy(True)
        if kwargs.get('gridx', False) or kwargs.get('grids', False):
            self.SetGridx(True)
        if kwargs.get('gridy', False) or kwargs.get('grids', False):
            self.SetGridy(True)
        self.MainPad.Draw()
        self.ResPad.Draw()
        self.cd(1)

    def setupPlots(self, mainPlot, resPlot):
        """
        """
        try:
            mainPlot.SetLabelOffset(2)
            resPlot.SetTitleOffset(2.65)
        except AttributeError:
            mainPlot.GetXaxis().SetLabelOffset(2)
            resPlot.GetXaxis().SetTitleOffset(2.65)

    def SetLogx(self, logx):
        """
        """
        self.MainPad.SetLogx(logx)
        self.ResPad.SetLogx(logx)

    def SetLogy(self, logy):
        """
        """
        self.MainPad.SetLogy(logy)
        self.ResPad.SetLogy(logy)

    def SetGridx(self, gridx):
        """
        """
        self.MainPad.SetGridx(gridx)
        self.ResPad.SetGridx(gridx)

    def SetGridy(self, gridy):
        """
        """
        self.MainPad.SetGridy(gridy)
        self.ResPad.SetGridy(gridy)

    def annotate(self, x, y, text, ndc = True, align = 11, size = TEXT_SIZE,
                 color = ROOT.kBlack, angle = 0):
        """ Draw some text on the canvas.
        """
        self.cd(1)
        label = ROOT.TLatex(x, y, text)
        if ndc:
            label.SetNDC(True)
        label.SetTextAlign(align)
        label.SetTextSize(size)
        label.SetTextColor(color)
        label.SetTextAngle(angle)
        label.Draw()
        store(label)
        self.cd()
        ROOT.TCanvas.Update(self)

    def cd(self, pad = None):
        """ cd into one of the two pads.
        """
        if pad is None:
            TCanvas.cd(self)
        elif pad == 1:
            self.MainPad.cd()
        elif pad == 2:
            self.ResPad.cd()   




class TLegend(ROOT.TLegend):
    """ Small wrapper around the ROOT.TLegend class.
    """
    
    LINE_LENGTH = 0.250
    ROW_SPACING = 0.055

    def __init__(self, x = 0.16, y = 0.9, entries = []):
        """ Constructor. (x, y) are the coordinates of the top-left corner.
        """
        ROOT.TLegend.__init__(self, x, y, x + self.LINE_LENGTH, y)
        self.SetTextFont(TEXT_FONT)
        self.SetTextSize(LEGEND_TEXT_SIZE)
        self.SetFillStyle(0)
        for entry in entries:
            self.AddEntry(entry, entry.GetTitle(), 'lpf')

    def vstretch(self, factor):
        """ Stretch the legend vertically.
        """
        self.SetY1(self.GetY2() - factor*(self.GetY2() - self.GetY1()))

    def AddEntry(self, plot, label = None, options = 'lpf'):
        """ Overload method for constant spacing between rows.
        """
        if plot is None:
            return
        if label is None:
            label = plot.GetTitle()
        ROOT.TLegend.AddEntry(self, plot, label, options)
        self.SetY1(self.GetY1() - self.ROW_SPACING)

    def AddEntries(self, plots, options = 'lpf'):
        """ Add a list of plots to the captions.
        """
        for plot in plots:
            self.AddEntry(plot, None, options)



def setupBox(box, x, y, numRows, width = 0.25):
    """ Setup a stat box or a fit box with a given number of rows.
    (x, y) is the top-left corner of the box.
    """
    box.SetX1NDC(x)
    box.SetX2NDC(x + width)
    box.SetY1NDC(y - numRows*TLegend.ROW_SPACING)
    box.SetY2NDC(y)
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
 


if __name__ == '__main__':
    c = TCanvas('test')
    h1 = ROOT.TH1F('h1', 'h1', 100, -5, 5)
    h1.FillRandom('gaus', 1000)
    h1.SetXTitle('x-axis [a. u.]')
    h1.SetYTitle('y-axis [a. u.]')
    h1.SetLineColor(ROOT.kBlue)
    h2 = ROOT.TH1F('h2', 'h2', 100, -5, 5)
    h2.FillRandom('gaus', 800)
    h2.SetLineColor(ROOT.kRed)
    l = TLegend(0.15, 0.9)
    l.AddEntry(h1, 'First hist')
    l.AddEntry(h2, 'Second hist')
    l.AddEntry(h1, 'First hist (twice)', 'l')
    h1.Draw()
    h2.Draw('same')
    l.Draw()
    c.Update()
    c.save()
