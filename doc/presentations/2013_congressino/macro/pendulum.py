
from __root__ import *
from numpy import array
from math import acos, sqrt

MARKER_STYLE  = 7
F_ORDER_1     = ROOT.kBlue
F_ORDER_2     = ROOT.kRed
#OUTPUT_FOLDER = "../"

# in case you want to change Fitter
#ROOT.TVirtualFitter.SetDefaultFitter("Minuit2");
#ROOT.TVirtualFitter.SetDefaultFitter("Fumili2");


###############
def getResiduals(x,y,f):
    r  = []
    rr = []
    sc = 0
    for i in xrange(len(x)):
        r.append(y[i] -f.Eval(x[i]))
        c = (y[i] -f.Eval(x[i]))**2 
        rr.append(c)
        sc+= c
    return  (array(r), array(rr), sc)

###############


f = open("data/pendulum.txt")
L  = 116.
s  = 2.4
Dp = 1.e-5

# read data file
t = []
p = []
x = []
v = []
ev = [] 
for l in f:
    if l[0]!="#":
        [a0, a1, a2] = l.strip('\n').split('\t')
        t.append(float(a0))
        p.append(float(a1))
        x.append(float(a2))
        v.append(acos(1-((s/float(a2))**2)/(2*981*L)))
        
        # let's try to put a error on v
        z = ((s/float(a2))**2)/(2*981*L)
        k = 1./sqrt(1 + ( 1-z )**2 )
        ds = 2.*k*z*(0.01/s)
        da = 2.*k*z*(Dp/float(a2))
        dL =    k*z*(1./L)
        ev.append(sqrt(ds**2 + da**2 + dL**2) )
        #print v[-1], "-", ds, da, dL,"-", ev[-1], 100*ev[-1]/v[-1]         

n = len(t)
t = array(t)
p = array(p)
x = array(x)
v = array(v)
ep = array(n*[Dp])
ex = array(n*[0.])
ev = array(ev)

# ================ plot T tv A
#http://en.wikipedia.org/wiki/Pendulum_%28mathematics%29
canvas = TCanvas("pendulum_TvsA", grids=True)

g2 = ROOT.TGraphErrors(n, v, p, ex, ep)
g2.SetTitle("Pendulum {#DeltaP = %.0e s}" %Dp)
g2.GetYaxis().SetTitle("Period T [s]")
g2.GetXaxis().SetTitle("Amplitude #theta [rad]")
g2.SetMarkerStyle(MARKER_STYLE)
g2.GetYaxis().SetRangeUser(2.135, 2.165)
#g2.GetYaxis().SetNdivisions(505)
g2.GetYaxis().CenterTitle(True)
g2.Draw("AP")

# Check 1st order correction
f1 =ROOT.TF1("f1", "[0]*(1 + (x**2)/[1])", 0.1, 0.5)
f1.SetNpx(500);
# WARNING: you need to careully set DeltaP to a reasonable value to make the fit converge
f1.SetParameters(2.13372991, 16.)
f1.SetParName(0,"T_{0}");
f1.SetParName(1,"C_{0}");
f1.SetLineColor(F_ORDER_1)
f1.FixParameter(1, 16.)
g2.Fit("f1", "N")

# Check 2nd order correction
f2 =ROOT.TF1("f2", "[0]*(1 + (x**2)/[1] + (x**4)/[2] )", 0.1, 0.5)
f2.SetNpx(500);
f2.SetParameters(2.13372991, 16., 279.182)
f2.SetParName(0,"T_{0}");
f2.SetParName(1,"C_{0}");
f2.SetParName(2,"C_{1}");
f2.SetLineColor(F_ORDER_2)
f2.FixParameter(1, 16.)
f2.FixParameter(2, 3071./11)
g2.Fit("f2")

canvas.annotate(0.17, 0.85, "#DeltaT set to %.0f #mus" %(Dp*1e6))
canvas.Update()
canvas.save()

# residual for 1nd order
canvas1st = TCanvas("pendulum_TvsA_res1st", grids=True)
(r1, d, chi) = getResiduals(v, p, f1)
r1 = 1000*r1 # from s fo ms
gr1 = ROOT.TGraph(n, v, r1)
gr1.SetMarkerStyle(MARKER_STYLE)
gr1.SetTitle("Fit residuals -- 1st order")
gr1.GetYaxis().SetTitle("Period T, Data-Fit [ms] ")
gr1.GetXaxis().SetTitle("Amplitude #theta [rad]")
gr1.GetYaxis().SetRangeUser(-2.e-1, 3.e-1)
gr1.GetYaxis().SetNdivisions(512)
gr1.GetYaxis().CenterTitle(True)
gr1.Draw("AP")
l1 =ROOT.TF1("l1", "0", 0.1, 0.5)
l1.SetLineColor(F_ORDER_1)
l1.Draw("sames")
canvas1st.annotate(0.17, 0.85, "T(#theta)=T_{0}(1 + #frac{1}{16}#theta^{2})", color = F_ORDER_1)
canvas1st.Update()
canvas1st.save()

# residual for 2nd order
canvas2nd = TCanvas("pendulum_TvsA_res2nd", grids=True)
(r2, d, chi) = getResiduals(v, p, f2)
r2 = 1000*r2 # from s fo ms
gr2 = ROOT.TGraph(n, v, r2)
gr2.SetMarkerStyle(MARKER_STYLE)
gr2.SetTitle("Fit residualss -- 2nd order")
gr2.GetYaxis().SetTitle("Period T, Data-Fit [ms] ")
gr2.GetXaxis().SetTitle("Amplitude #theta [rad]")
#gr2.GetYaxis().SetRangeUser(-1.5e-1, 1.5e-1)
gr2.GetYaxis().SetRangeUser(-2.e-1, 3.e-1)
gr2.GetYaxis().SetNdivisions(512)
gr2.GetYaxis().CenterTitle(True)
gr2.Draw("AP")
l2 =ROOT.TF1("l2", "0", 0.1, 0.5)
l2.SetLineColor(F_ORDER_2)
l2.Draw("sames")
canvas2nd.annotate(0.17, 0.85, "T(#theta)=T_{0}(1 + #frac{1}{16}#theta^{2} + #frac{3072}{11}#theta^{4})", color = F_ORDER_2)
canvas2nd.Update()
canvas2nd.save()

# residual 1D histogram
canvasHist = TCanvas("pendulum_TvsA_reHist", grids=True)
ROOT.gStyle.SetOptStat(2210)
#ROOT.gStyle.SetStatFormat("5.2g")
#ROOT.gStyle.SetFitFormat("5.2g")
ROOT.gStyle.SetOptFit(1)

h2 = ROOT.TH1F("h2", "Fit residuals histogram;Period T, Data-Fit [ms];Entries/bin", 50, -0.1, 0.1)
h2.SetLineColor(F_ORDER_2)
for i in r2:
    h2.Fill(i)
h2.Draw()

gg = ROOT.TF1("gg", "gaus", -0.1, 0.1)
h2.Fit("gg")
canvasHist.Update()
# need a better formatting of statbox
statBox = h2.GetListOfFunctions().FindObject("stats")
statBox.SetX1NDC(0.67)
statBox.SetX2NDC(0.97)
statBox.SetY2NDC(0.95)
h2.Draw()

canvasHist.Update()
canvasHist.save()

print "my stat:"
print h2.GetEntries()
print h2.GetMean(), h2.GetMeanError()
print h2.GetRMS(), h2.GetRMSError()
print "fit:"
print gg.GetChisquare(), gg.GetNDF()
print gg.GetParameter(0), gg.GetParError(0)
print gg.GetParameter(1), gg.GetParError(1)
print gg.GetParameter(2), gg.GetParError(2)

