from __root__ import *
from array import array
ROOT.gStyle.SetOptStat(112210)
HIST_TIME_MAX = 1600 # 3000

# read data
f = file('data/gps.txt')

x = []
for l in f:
  if l[0] == 'T':
      x.append(float(l.strip('\n').split('TT')[1]))

print "reading", len(x), 'times'

y = []
t0 = 0
for i in x:
    y.append(i-t0 - 1000000)
    t0 =i
    
y = y[1:]
n = len(y)
print "reading", n, 'differences'

y = array('f',y)
t = array('f', range(n))

c = TCanvas("clockDeviationHist", grids=True, logy=True)
h = ROOT.TH1F("h", ";Deviation #DeltaT-1e6 [#mus];Entries/bin", 500, -1000, 1000)
for (id, i) in enumerate(y):
  if id <= HIST_TIME_MAX: 
    h.Fill(i)
    
h.Draw()
h.GetXaxis().SetRangeUser(-200, -1)
h.GetXaxis().SetNdivisions(515)
c.Update()
# need a better formatting of statbox
statBox = h.GetListOfFunctions().FindObject("stats")
statBox.SetX1NDC(0.67)
statBox.SetX2NDC(0.97)
statBox.SetY2NDC(0.95)
h.Draw()
h.GetXaxis().SetRangeUser(-200, 0)
c.annotate(0.69, 0.65, "First %d s" % HIST_TIME_MAX)
c.Update()
c.save()

c1 = TCanvas("clockDeviationVsT", grids=True)
g = ROOT.TGraph(n, t, y)
g.SetMarkerStyle(7)
g.Draw("AP")
g.SetTitle("")
g.GetXaxis().SetTitle("Elapsed time [s]")
g.GetYaxis().SetTitle("Deviation #DeltaT-1e6 [#mus]")
c1.Update()

c1.annotate(490, -650, "heating the board #rightarrow", ndc =False)
#size = SMALL_TEXT_SIZE, color = ROOT.kBlack, angle = 0)
c1.save()
