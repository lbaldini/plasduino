\documentclass[portrait,final]{poster}

\graphicspath{{figures/}}

\usepackage{wrapfig}
\usepackage{eurosym}

\renewcommand\refname{\vspace*{-0.8em}}

\title{plasduino: an inexpensive, general purpose data acquisition framework
  for didactic experiments}
\author[1,2]{L.~Baldini}
\author[2]{J.~Bregeon}
\author[2]{M.~Pesce-Rollins}
\author[2]{C.~Sgr\`o}
\author[1]{E.~Andreoni}
\author[1]{A.~Bianchi}
\author[1]{C.~Luperini}
\author[1]{V.~Merlin}
\author[1,2]{J.~Nespolo}
\author[1]{S.~Orselli}
\affil[1]{Universit\`a di Pisa}
\affil[2]{INFN-Sezione di Pisa}

\begin{document}

\begin{poster}
  { % Poster Options---change stuff at your leisure.
    grid            = no             , % Visualize a debug grid.
    headerheight    = 0.15\textheight, % The height of the title/author block.
    titlefont       = \bf\huge       , % Font for the title.
    authorfont      = \large         , % Font for the authors/affiliations.
    abstractfont    = \small         , % Font for the abstract box.
    headerfont      = \bf\Large      , % Font for the box headers.
    textfont        = \small         , % Font for the body text.
    textmargin      = 0.5em          , % Left/right margins for the body text.
    headerColorOne  = MidnightBlue   , %
    textcolor       = MidnightBlue   , % Default text color.
    twocolsabstract = no             , % Use two columns for the abstract.
    linewidth       = 1pt            , % Width of the lines.
    borderColor     = MidnightBlue
  }
  { % Left logo
    %\includegraphics[width=0.15\textwidth]{fermi_logo_large}
    \rule{0pt}{100pt}
  }
  { % Right logo
    %\includegraphics[width=0.15\textwidth]{infn_logo}
  }

  \abstract{}{%
    Abstract: plasduino is an open hardware/software project aimed at providing
    all the necessary tools to assemble a flexible, easy-to-use,
    general-purpose data acquisition system suitable for didactic physics 
    experiments for under 50~\euro. We make available a comprehensive software
    framework for controlling the data acquisition, the schematics for the
    hardware shields and a detailed description of some real-life applications
    that can be replicated with minimal effort.
  }


  \headerbox{The arduino platform}{name=arduinobox,%
    column=0,span=3,below=abstract}{
    \begin{wrapfigure}[7]{r}{0.32\textwidth}
      \vspace*{-10pt}\includegraphics[width=0.32\textwidth]{arduino}
    \end{wrapfigure}
    Arduino~\cite{arduino} is an open-source electronics prototyping platform
    based on flexible, easy-to-use hardware. The microcontroller on the board is
    programmed using a dedicated high-level language that comes with an
    extensive collection of libraries implementing a huge variety of
    functionalities.
    
    \smallskip

    Pretty much any arduino board comes with:
    \begin{itemize}
    \item a microcontroller and everything needed to support it;
    \item 14 or more digital I/O pins (with PWM and/or interrupt capabilities);
    \item 6 or more analog inputs equipped with a 10 or 12 bit ADC;
    \item a USB interface to/from a host computer (for uploading the firmware
      to the microcontroller and transferring data).
    \end{itemize}
  }


  \headerbox{The software framework}{name=softwarebox,%
    column=3,span=3,below=abstract}{
    \begin{wrapfigure}{r}{0.32\textwidth}
     \vspace*{0pt}\includegraphics[width=0.32\textwidth]{plasduino_dice}
    \end{wrapfigure}
    We implemented a cross-platform software and framework~\cite{plasduino,plasduino_rep}
    based on the {\sc Python} programming language~\cite{python}. 
    It provides, among other things:
    %The software framework~\cite{plasduino,plasduino_rep} we have implemented
    %is based on the {\sc python} programming language~\cite{python}.
    %It is intrinsically cross-platform and provides, among other things:
    \begin{itemize}
    \item a set of modular custom widgets for the GUI;
    \item a multi-threaded run control for the communication with
      the microcontroller (featuring auto-detection of the USB port and
      auto-upload of the firmware) and for data processing and archiving;
    \item a configurable logging facility;
    \item an easy-to-use graphical configuration editor;
    \item a collection of well documented modules for specific 
      didactic experiments.
    \end{itemize}
    The code is released under the GPL license. Packages are available for 
    RedHat and Debian/Ubuntu GNU-Linux distributions.
  }

  \headerbox{An example: a custom shield}{name=shieldbox,%
    column=0,span=3,below=arduinobox}{
    \begin{wrapfigure}[5]{r}{0.32\textwidth}
      \vspace*{-15pt}\includegraphics[width=0.32\textwidth]{shield_lab1}
    \end{wrapfigure}
    Shields are PCBs to be plugged directly on the arduino board.
    They constitute the interface between the microcontroller and the
    outside world.
    
    \smallskip

    Our first test shield is designed to interface arduino to simple
    sensors such as optical gates and thermistors. It features:
    \begin{itemize}
    \item two stereo mini-jack connectors to acquire signals (and provide
      bias to) a pair of optical gates;
    \item one stereo mini-jack connector to interface a pair of thermistors;
    \item a timer circuit for internal testing and calibration.
    \end{itemize}
  }


  \headerbox{Timing performance}{name=timecalibbox,%
    column=3,span=3,below=softwarebox}{%, bottomaligned=shieldbox}{
    \begin{center}
    \includegraphics[width=0.45\textwidth]{clockDeviationHist}\hfill%
    \includegraphics[width=0.45\textwidth]{clockDeviationVsT}
    \end{center}

    Arduino's libraries include the function \texttt{micros()} to latch a
    timer incremented by the $16$~MHz clock prescaled $\times 64$
    ($4~\mu$s nominal resolution). We tested the timing with:
    \begin{itemize}
    \item the built-in 555 timer circuit from a few~Hz to several kHz;
    \item the PPS signal from a GPS for absolute time calibration:
      \begin{itemize}
      \item $127~\mu$s average deviation for a test board at room temperature,
        corresponding to a $\sim 10^{-4}$ systematic error on the absolute time;
      \item $\sim 2~\mu$s RMS (i.e. the $4~\mu$s nominal step size is real).
      \end{itemize}
    \end{itemize}    

  }


  \headerbox{An example: the pendulum}{name=pendulumbox,%
    column=0,span=3,below=shieldbox}{
    \begin{center}
    \includegraphics[width=0.4\textwidth]{pendulum_TvsA}\hfill%
    \includegraphics[width=0.4\textwidth]{pendulum_TvsA_reHist}\\
    \includegraphics[width=0.4\textwidth]{pendulum_TvsA_res1st}\hfill%
    \includegraphics[width=0.4\textwidth]{pendulum_TvsA_res2nd}
    \end{center}

    Setup: a pendulum with 4 suspension strings and an optical gate reading out
    the transit times of a small tag attached to it. The amplitude of the oscillation 
    can be estimated from the velocity at the bottom:
    \begin{itemize}
    \item clearly see the $\theta^4$ terms;
    \item time residuals of $\sim 20~\mu$s (dominated by the jitter of the input
      signal).
    \end{itemize}
  }


  \headerbox{An example: a temperature monitor}{name=tempmonbox,%
    column=3,span=3,below=timecalibbox}{%, bottomaligned=pendulumbox}{
    \begin{wrapfigure}{r}{0.45\textwidth}
     \vspace*{-10pt}\includegraphics[width=0.45\textwidth]{temperature_bar}
    \end{wrapfigure}
    Arduino provides a 10~bit ADC that can be latched up to
    $\sim 10$~kHz rate.
    
    \smallskip
    One of our software modules synchronously polls the reading of
    two thermistors (connected to our shields) at $2$~Hz rate. We
    studied the propagation of heat through a metal bar:
    \begin{itemize}
    \item measure the temperature in different positions;
    \item measure the response time of the sensor (Newton's law).
    \end{itemize}
  }


  \headerbox{Future developments}{name=conclusionbox,%
    column=0,span=3,below=pendulumbox}{
    The project has just started and we surely welcome new participants.
    A few projects currently in the works:
    \begin{itemize}
    \item a shield driving a motor for studying the mechanical resonance;
    \item a shield to read out signals from photomultiplier tubes
      (4 discriminator channels and four-fold configurable coincidence logic,
      with the possibility of adding a GPS for the absolute timestamps);
    \item a programmable waveform generator;
    \item a fully-fledged oscilloscope.
    \end{itemize}
  }


  \headerbox{For more information\ldots}{name=refbox,%
    column=3,span=3,below=tempmonbox, bottomaligned=conclusionbox}{
    \begin{thebibliography}{9}
    \bibitem{arduino} \texttt{http://arduino.cc/}
    \bibitem{plasduino} \texttt{http://pythonhosted.org/plasduino/}
    \bibitem{plasduino_rep}
      \texttt{https://bitbucket.org/lbaldini/plasduino/wiki/Home}
    \bibitem{python} \texttt{http://www.python.org/}
    \end{thebibliography}

    \smallskip

    We gratefully acknowledge the support of F.~Angelini, B.~Barsella,
    F.~Fidecaro, M.~M.~Massai, M.~Minuti, F.~Morsani.
  }
 

\end{poster}

\end{document}
