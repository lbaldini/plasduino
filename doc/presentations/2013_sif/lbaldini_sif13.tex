% Author: Luca Baldini (luca.baldini@pi.infn.it)

\documentclass[10pt, table, xcolor=dvipsnames]{beamer}
\usetheme[navbar=false, bkgimage=true, shadow=true]{plasduino}
\usetikzlibrary{snakes}
\usepackage{eurosym}

\title{plasduino}
\author{Luca Baldini}
\institute[UNIPI and INFN]{Universit\`a and INFN--Pisa}
\email{luca.baldini@pi.infn.it}
%\collaboration{Fermi LAT}
\date[SIF 2013]{Congresso Nazionale della SIF\\
  Trieste, 23--27 settembre 2013}


\begin{document}

{
  \setbeamertemplate{headline}{}
  \setbeamertemplate{footline}{}
  \begin{frame}
    \manual{
      \node[anchor=west,color=MidnightBlue] at (0., 0.8)%
           {\fontsize{30}{30}\selectfont plasduino};
      \node[anchor=west,color=MidnightBlue] at (0.03, 0.73)%
           {\small Open source data acquisition framework};
      \draw[color=gray] (0,0.89)--(1,0.89);
      \draw[color=gray] (0,0.685)--(1,0.685);
      \minipagenode{}{0.5,0.5}{north}{\textwidth}{
        Luca Baldini$^{1,2}$,
        Carmelo Sgr\`o$^{2}$,
        Enrico Andreoni$^{1}$,
        Franco Angelini$^{1}$,
        Andrea Bianchi$^{1}$,
        Johan Bregeon$^{3}$,
        Francesco Fidecaro$^{1}$,
        Marco Maria Massai$^{1}$,
        Virginio Merlin$^{1}$,
        Jacopo Nespolo$^{1}$,
        Melissa Pesce-Rollins$^{2}$,
        Stefano Orselli$^{1}$
        
        \bigskip
        
        {\scriptsize
          $^1$ Dipartimento di Fisica E. Fermi, Universit\`a di Pisa\\
          $^2$ INFN--Sezione di Pisa\\
          $^3$ CNRS IN2P3/INSU, Montpellier
        }

        \bigskip

        Congresso Nazionale della SIF\\
        Trieste, 23--27 settembre 2013
      }
    }
  \end{frame}
}

\begin{frame}
  \frametitle{Introduzione}
  Plasduino \`e un progetto per un ambiente \emph{hardware} e \emph{software}
  di acquisizione dati concepito per esperienze didattiche. Mira ad essere:
  \begin{itemize}
  \item Semplice ed estendibile:
    \begin{itemize}
    \item Semplice da installare;
    \item Semplice da utilizzare;
    \item Adattabile con poco sforzo alle proprie esigenze.
    \end{itemize}
  \item Economico ($\sim 50$~\euro):
    \begin{itemize}
    \item Replicabile senza investimenti proibitivi sulla scala di un
      laboratorio didattico.
    \end{itemize}
  \item Completamente aperto (\emph{Free and Open Source}):
    \begin{itemize}
    \item Utilizza solo componenti (\emph{hardware} e \emph{software}) aperti;
    \item Il codice sorgente \`e liberamente accessibile sotto licenza GPL;
    \item Gli schemi elettronici e la documentazione sono disponibili sulla
      pagina web del progetto.
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Arduino: il cuore del sistema}
  \framesubtitle{\url{http://www.arduino.cc/}}
  \manual{
    \imagenode{}{0.5,0.98}{north}{0.45\textwidth}{arduino}
    \node at (0.8,0.97) {I/O digitali};
    \node at (0.82,0.56) {I/O analogici};
    \node at (0.15,0.85) {Connettore USB};
    \node at (0.15,0.61) {Alimentazione esterna};
    \node at (0.84,0.68) {Microcontrollore};
    \minipagenode{}{0.5,0.45}{north}{\textwidth}{%
      \begin{itemize}
      \item Arduino: una piattaforma \emph{open} di prototipizzazione
        elettronica:
        \begin{itemize}
        \item Flessibile, facile da utilizzare, realizzata in Italia;
        \item Programmabile ad alto livello (C/C++), ampia libreria di funzioni;
        \item Utilizzato e supportato da una comunit\`a numerosa ed attiva.
        \end{itemize}
      \item Una tipica scheda di Arduino (per 23~\euro) offre:
        \begin{itemize}
        \item Un microcontrollore;
        \item 14+ ingressi/uscite digitali (con supporto per PWM e
          \emph{interrupt});
        \item 6+ ingressi analogici con ADC a 10/12 bit;
        \item Un'interfaccia seriale via USB.
        \end{itemize}
      \end{itemize}
    }
  }
\end{frame}


\begin{frame}
  \frametitle{Gli ``shield''}
  \manual{
    \imagenode{}{0.5,0.98}{north}{0.45\textwidth}{shield_lab1}
    \node at (0.1,0.65) {Connettori per i sensori};
    \minipagenode{}{0.5,0.47}{north}{\textwidth}{%
      \begin{itemize}
      \item Uno \emph{shield} \`e un circuito stampato da connettere
        \emph{sopra} la scheda di Arduino.
        \begin{itemize}
        \item Agisce da interfaccia con il mondo esterno: connettori per i
          sensori, condizionamento dei segnali, led, calibrazione interna.
        \end{itemize}
      \item Uno shield supporta in genere pi\`u di un'esperienza didattica.
        \begin{itemize}
        \item e.g., abbiamo uno shield unico per un pacchetto di 7 esperienze di
          meccanica e termodinamica (replicabile a $\sim 25$~\euro).
        \end{itemize}
      \item Disegnati ``in casa'' ed assemblati dai tecnici di laboratorio: 
        \begin{itemize}
        \item Descrizione, schemi elettronici, maschere e lista dei componenti
          disponibili sulla pagina web del progetto.
        \end{itemize}
      \end{itemize}
    }
  }
\end{frame}


\begin{frame}
  \frametitle{Il software di acquisizione}
  \begin{itemize}
  \item plasduino offre un ambiente completo di acquisizione dati
    da eseguire su un PC connesso ad Arduino.
    \begin{itemize}
    \item Implementato nel linguaggio di programmazione Python%
      \footnote{\`E il linguaggio con la crescita di popolarit\`a pi\`u
      grande nello scorso decennio.};
    \item Multipiattaforma (GNU/Linux, Windows, Mac OS);
    \end{itemize}
  \item Un'applicazione \emph{multi-thread} per il controllo dell'acquisizione:
    \begin{itemize}
    \item Riconoscimento automatico della porta USB cui \`e connesso Arduino;
    \item Semplice protocollo di comunicazione per facilitare il \emph{debug};
    \item Caricamento automatico del \emph{firmware} sul microcontrollore;
    \item Raccolta, elaborazione ed archiviazione dei dati.
    \end{itemize}
  \item Un sistema completo di logging (su terminale e \emph{file}).
  \item Una vasta gamma di \emph{widget} per l'interfaccia grafica.
    \begin{itemize}
    \item Organizzati in moduli da assemblare a piacimento
      (vedi la prossima diapositiva).
    \end{itemize}
  \item Librerie per gestire sensori e dispositivi.
  \item Un insieme di moduli per esperienze specifiche.
  \item $\sim10$k linee di codice nel complesso.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{L'interfaccia grafica$^1$}
  \manual{
    \only<1>{
    %650
    \imagenode{}{-0.08,0.99}{north west}{0.585\textwidth}{plasduino_launcher}
    %447
    \imagenode{}{0.43,0.55}{north}{0.402\textwidth}{plasduino_pendulum}
    %510
    \imagenode{}{1.09,0.96}{north east}{0.459\textwidth}{plasduino_config}
    \draw (0.64,0.05)--(1.03,0.05); 
    \node at (0.83,0.0) {\scriptsize $^1$Logo disegnato da Rosalia Nunziante};
    }
    \only<2>{
    %932
      \imagenode{}{0.5,0.98}{north}{0.95\textwidth}{plasduino_tempmonitor}
      %\node at (0.15,0.86) {Logo di Rosalia Nunziante};
    }
  }
\end{frame}


\begin{frame}
  \frametitle{Calibrazione delle misure di tempo}
  \includegraphics[width=0.5\textwidth]{clockDeviationVsT}%
  \includegraphics[width=0.5\textwidth]{clockDeviationHist}

  \begin{itemize}
  \item La libreria di Arduino fornisce un \emph{timer} incrementato dal
    clock a 16~MHz prescalato di un fattore 64:
    \begin{itemize}
    \item i.e., con una granularit\`a nominale di 4~$\mu$s.
    \end{itemize}
  \item Testato in laboratorio il 1PPS di un GPS (e con un impulsatore):
    \begin{itemize}
    \item RMS dell'intervallo misurato tra due 1PPS successivi di
      $1.8~\mu$s, non lontano da $4/\sqrt{12}~\mu$s.
    \item Deviazione media dal valore nominale di $\sim 100~\mu$s (su 1~s)
      a temperatura ambiente.
    \end{itemize}
  \item La granularit\`a di 4~$\mu$s \`e \emph{vera}.
  \item Errore sistematico di $10^{-4}$ sulle misure di tempo.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Il pendolo ``digitale''}
  \manual{
    \minipagenode{}{-0.1,0.98}{north west}{0.6\textwidth}{%
      \includegraphics[width=\textwidth,clip=true,trim=40 100 40 0]%
                      {pendulum_digital}%
    }
    \node at (0.3,0.45) {Traguardo ottico};
    \only<1>{%
      \imagenode{}{1.1,0.98}{north east}{0.6\textwidth}{pendulum_TvsA}%
    }
    \only<2>{%
      \imagenode{}{1.1,0.98}{north east}{0.6\textwidth}{pendulum_TvsA_res1st}%
    }
    \only<3>{%
      \imagenode{}{1.1,0.98}{north east}{0.6\textwidth}{pendulum_TvsA_res2nd}%
    }
    \minipagenode{}{0.5,0.3}{north}{\textwidth}{
      \begin{itemize}
      \item Misura del periodo $T$ e del tempo di transito di una bandierina nel
        punto pi\`u basso.
        \begin{itemize}
        \item Misura dello smorzamento esponenziale (?)
        \end{itemize}
      \item Trascurando le perdite di energia in una oscillazione
        possiamo stimare l'ampiezza $\theta_{\rm max}$.
        \begin{itemize}
        \item Misura dell'anarmonicit\`a del pendolo.
        \item Si apprezza chiaramente il termine in $\theta^4$!
        \end{itemize}
      \end{itemize}
    }
  }
\end{frame}


\begin{frame}
  \frametitle{Il pendolo ``analogico''}
  \manual{
    \minipagenode{}{-0.1,0.98}{north west}{0.6\textwidth}{%
      \includegraphics[width=\textwidth,clip=true,trim=100 40 100 80]%
                      {pendulum_analog}%
    }
    \node at (0.4,0.77) {$+ 5$~V};
    \node at (0.,0.58) {0~V};
    \node at (0.07,0.95) {All'ingresso analogico};
    \imagenode{}{1.1,0.98}{north east}{0.6\textwidth}{pendulumView}
    \minipagenode{}{0.5,0.3}{north}{\textwidth}{
      \begin{itemize}
      \item Utilizziamo una punta immersa in acqua come partitore
        resistivo.
        \begin{itemize}
        \item Direttamente ad uno degli ingressi analogici.
        \end{itemize}
      \item 10 bit ($2^{10} = 1024$) di ADC su $\sim 20$~cm corrisponde
        ad una risoluzione spaziale di $\sim 200/\sqrt{12} \sim 60~\mu$m.
        \begin{itemize}
        \item Il $\chi^2$ ed i residui indicano che, almeno vicino al centro,
          non siamo lontani dalla risoluzione ``teorica''.
        \item Non-linearit\`a residua al livello di qualche \% ai bordi.
        \end{itemize}
      \end{itemize}
    }
  }
\end{frame}


\begin{frame}
  \frametitle{Un generatore di forme d'onda a buon mercato}
  \includegraphics[height=0.6\textheight]{wave_gui}\hfill%
  \includegraphics[trim=0 100 100 200, clip=true, height=0.6\textheight]%
                  {wave_scope}

  \begin{itemize}
  \item Basato sul circuito integrato AD9833.
    \begin{itemize}
    \item Disponibile a $\sim 5$~\euro~dal vostro rivenditore di fiducia.
    \item Montato su uno shield ed interfacciato tramite protocollo SPI.
    \item Onda quadra, triangolare e sinusoidale, frequenza variabile fino
      a qualche centinaio di kHz.
    \end{itemize}
  \item Sostituto economico per il vostro generatore da banco?
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Stato dell'arte}
  \begin{itemize}
  \item Primo ``pacchetto'' di 7 esperienze didattiche complete:
    \begin{itemize}
    \item ``Digitali'': piano inclinato, pendolo;
    \item ``Analogiche'': oscillazioni smorzate, oscillazioni
      forzate, misure di temperatura;
    \item Bonus: tavolo ad aria, simulazione del lancio
      di dadi.
    \end{itemize}
  \item Utilizzate con successo lo scorso anno nel corso di Laboratorio
    di Fisica per gli studenti del primo anno all'Universit\`a di Pisa.
    \begin{itemize}
    \item Numerose segnalazioni di \emph{bug} e richieste di funzionalit\`a
      da parte degli studenti.
    \item Molti studenti hanno scaricato ed installato il pacchetto ``a casa''.
    \item Uno studente ha creato un pacchetto di installazione per la sua
      distribuzione favorita (archlinux) di GNU/Linux!
    \end{itemize}
  \item Prima versione ``stabile'' in fase di rilascio.
    \begin{itemize}
    \item Verr\`a utilizzata nell'anno accademico appena iniziato.
    \end{itemize}
  \item Speriamo di estendere l'esperienza ai laboratori degli anni successivi.%
    \begin{itemize}
    \item Sono in fase di studio un semplice oscilloscopio a due canali
      ed un DAQ per leggere fototubi.
    \end{itemize}
  \item E---perch\'e no?---alle scuole superiori.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Conclusioni}
  \centering\includegraphics[width=0.8\textwidth]{webpage}

  \begin{itemize}
  \item Alcuni link utili:
    \begin{itemize}
    \item Pagina web del progetto:
      {\scriptsize\url{http://pythonhosted.org/plasduino/}}
    \item Download:
      {\scriptsize\url{https://bitbucket.org/lbaldini/plasduino/downloads}}
    \item Repositorio:
      {\scriptsize\url{https://bitbucket.org/lbaldini/plasduino/src}}
    \item Issue tracker:
      {\scriptsize\url{https://bitbucket.org/lbaldini/plasduino/issues}}
    \end{itemize}
  \item Ogni manifestazione di interesse \`e benvenuta.
  \end{itemize}
\end{frame}


\end{document}


