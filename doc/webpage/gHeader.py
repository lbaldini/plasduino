#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __format__ import indent


class gHeader:
    
    """ Class representing the header portion of an html web page.
    """

    def __init__(self, title, keywords, description, author, stylesheet):
        """ Constructor.
        """
        self.Title = title
        self.Keywords = keywords
        self.Description = description
        self.Author = author
        self.Stylesheet = stylesheet

    def html(self):
        """ html representation.
        """
        text = """<!DOCTYPE html>\n\n"""
        text += '<html lang="en">\n\n'
        text += indent('<head>\n', 1)
        text += indent('<meta http-equiv="content-type" content="text/html; ' \
                           'charset=utf-8"/>\n', 2)
        text += indent('<title>%s</title>\n' % self.Title, 2)
        text += indent('<meta name="keywords" content="%s"/>\n' %\
                           self.Keywords, 2)
        text += indent('<meta name="description" content="%s"/>\n' %\
                           self.Description, 2)
        text += indent('<meta name="author" content="%s"/>\n' %\
                           self.Author, 2)
        text += indent('<link rel="stylesheet" href="%s" '\
                           'type="text/css" media="all"/>\n' % self.Stylesheet,\
                           2)
        text += indent('</head>\n', 1)
        return text
        
    def __str__(self):
        """ String formatting.
        """
        return self.html()


if __name__ == '__main__':
    header = gHeader('title', 'keywords', 'description', 'author', 'stylesheet')
    print(header)
