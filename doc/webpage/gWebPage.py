#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import re

from plasduino.__logging__ import logger
from plasduino.__team__ import DEVELOPERS, GRAPHICS_DESIGNERS, ELX_GURUS,\
    TECHNICIANS, CREDITS_TXT, mailtoDevelopersAnchor
from plasduino.__plasduino__ import PLASDUINO_WEB_HTML, PLASDUINO_WEB_PAGES,\
    PLASDUINO_MODULES

# These imports are local as we didn't want to add useless __init__.py
# files scattered around.
from __bitbucket__ import getHtmlAnchorToTip, getLinkToTip
from __format__ import indent
from __layout__ import MODULE_DICT
from gHeader import gHeader
from gTopBar import gTopBar
from gFooter import gFooter


class gWebPage:

    """ Class describing a web page.
    """

    def __init__(self, name, target, menuBar):
        """ Constructor.
        """
        self.Name = name
        self.Target = target
        self.MenuBar = menuBar
        filePath = os.path.join(PLASDUINO_WEB_PAGES, self.Target)
        if not os.path.exists(filePath):
            logger.info('Could not find %s, creating default blank page...' %\
                            filePath)
            outputFile = open(filePath, 'w')
            text  = '<!--keywords>%s</keywords-->\n' % 'plasduino'
            text += '<!--description>%s</description-->\n' % 'plasduino'
            text += indent('\n<p>\nPage under construction.\n</p>\n', 1)
            outputFile.writelines(text)
            outputFile.close()
            logger.info('Done.')

    def pre(self):
        """ Do nothing method to be overloaded/set at runtime.
        """
        pass

    def post(self):
        """ Do nothing method to be overloaded/set at runtime.
        """
        pass

    def __getMetaData(self, name, data, default = 'plasduino'):
        """ Use regular expressions to parse the meta data in the input
        html files.
        """
        pattern = '(?<=\<!--%s\>).*(?=\</%s--\>)' % (name, name)
        metaData = re.search(pattern, data)
        if metaData is None:
            return default
        return metaData.group(0)

    def write(self, dryRun = False):
        """ Write the page to file.
        """
        filePath = os.path.join(PLASDUINO_WEB_PAGES, self.Target)
        # Parse the metadata
        inputFile = open(filePath)
        fileContent = inputFile.read()
        keywords = self.__getMetaData('keywords', fileContent, 'plasduino')
        description = self.__getMetaData('description', fileContent, 'ciao')
        author = self.__getMetaData('author', fileContent, 'the plasduino team')
        inputFile.close()
        # Done.
        title = 'plasduino &ndash; open source data acquisition &ndash; %s' %\
            self.Name
        stylesheetUrl = 'css/stylesheet.css'
        header = gHeader(title, keywords, description, author, stylesheetUrl)
        self.Content  = '%s\n' % header.html()
        self.Content += '%s\n' % indent('<body>', 1)
        self.Content += '%s\n' % indent(gTopBar().html(), 2)
        self.Content += '%s\n' % indent(self.MenuBar.html(), 2)
        self.Content += indent('<div id="content">\n', 2)
        self.Content += indent('\n<h2>%s</h2>\n' % self.Name, 3)
        self.pre()
        for line in open(filePath):
            if not line.startswith('<!--'):
                self.Content += indent(line, 3)
        self.post()
        self.Content += indent('</div>\n', 2)
        self.Content += '\n%s\n' % indent(gFooter().html(), 2)
        self.Content += indent('\n</body>', 1)
        self.Content += '</html>\n'
        filePath = os.path.join(PLASDUINO_WEB_HTML, self.Target)
        if not dryRun:
            logger.info('Writing %s...' % filePath)
            open(filePath, 'w').writelines(self.Content)

    def __str__(self):
        """
        """
        return self.Content



class gConfigurationWebPage(gWebPage):

    """ Specialized web page.
    """

    def post(self):
        import subprocess
        executable = os.path.join(PLASDUINO_MODULES, 'plasduino')
        text = subprocess.check_output([executable, '-h'])
        text = text.replace('\n', '</br>\n').replace(' ', '&nbsp;')
        self.Content += '<div class="code">\n'
        self.Content += indent(text, 3)
        self.Content += '</div>\n'



class gTeamWebPage(gWebPage):

    """ Specialized web page.
    """

    def pre(self):
        text = '<h3>Core developers:</h3>\n'
        text += '<p><ul>\n'
        for developer in DEVELOPERS:
            text += '<li>'
            if developer.WebPage:
                text += '<a href="%s">%s</a>' %\
                    (developer.WebPage, developer.Name)
            else:
                text += '%s' % developer.Name
            text += ' (<a href="mailto:%s">%s</a>)' %\
                (developer.Email, developer.Email)
            text += '</li>\n' % developer
        text += '</ul></p>'
        text += '<p>'
        text += 'Want to send an e-mail to all the developers? Click %s!' %\
            mailtoDevelopersAnchor('here')
        text += '</p>'

        text += '<h3>Graphics design:</h3>\n'
        text += '<p><ul>\n'
        for designer in GRAPHICS_DESIGNERS:
            text += '<li>'
            if designer.WebPage:
                text += '<a href="%s">%s</a>' %\
                    (designer.WebPage, designer.Name)
            else:
                text += '%s' % designer.Name
            text += ' (<a href="mailto:%s">%s</a>)' %\
                (designer.Email, designer.Email)
            text += '</li>\n' % designer
        text += '</ul></p>'

        text += '<h3>Electronics support:</h3>\n'
        text += '<p><ul>\n'
        for guru in ELX_GURUS:
            text += '<li>'
            if guru.WebPage:
                text += '<a href="%s">%s</a>' %\
                    (guru.WebPage, guru.Name)
            else:
                text += '%s' % guru.Name
            text += ' (<a href="mailto:%s">%s</a>)' %\
                (guru.Email, guru.Email)
            text += '</li>\n' % guru
        text += '</ul></p>'

        text += '<h3>Technical support:</h3>\n'
        text += '<p><ul>\n'
        for technician in TECHNICIANS:
            text += '<li>'
            if technician.WebPage:
                text += '<a href="%s">%s</a>' %\
                    (technician.WebPage, technician.Name)
            else:
                text += '%s' % technician.Name
                text += ' (<a href="mailto:%s">%s</a>)' %\
                    (technician.Email, technician.Email)
            text += '</li>\n' % technician
        text += '</ul></p>\n'
        text += '<h3>Credits:</h3>\n'
        text += '<p>\n'
        text += '%s.\n' % CREDITS_TXT
        text += '</p>\n'
        self.Content += indent(text, 3)



class gModuleWebPage(gWebPage):

    """ Specialized web page.
    """
    
    def pre(self):
        """
        Overloaded class method.
        """
        moduleInfo = MODULE_DICT[self.Name]
        text = '<p>\n'
        text += indent('One-line summary: %s\n' % moduleInfo.shortAbstract(), 1)
        text += '</p>\n'
        text += '<p>\n'
        text += indent('Abstract: %s\n' % moduleInfo.longAbstract(), 1)
        text += '</p>\n'
        text += '<p>\n'
        author = moduleInfo['author']
        contact = moduleInfo['contact']
        if contact:
            text += indent('Author: <a href="mailto:%s">%s</a>' %\
                               (contact, author), 1)
        else:
            text += indent('Author: %s.\n' % author, 1)
        text += '</p>\n'
        text += '<p>\n'
        executable = os.path.basename(moduleInfo['executable'])
        executablePath = 'modules/%s' % executable
        anchor = getHtmlAnchorToTip(executablePath, executable)
        text += indent('Executable: %s\n' % anchor, 1)
        text += '</p>\n'
        text += '<p>\n'
        deps = 'Dependencies: '
        for dependency in moduleInfo['dependencies']:
            deps += '%s, ' % dependency.htmlAnchor()
        deps = deps.strip(' ,')
        deps += '.'
        text += indent(deps, 1)
        text += '</p>\n'
        if moduleInfo['shield'].Name:
            text += '<p>\n'
            text += indent(('Shield: %s\n' %\
                                moduleInfo['shield'].htmlAnchor()), 1)
            text += '</p>\n'
        filePath = moduleInfo['executable']
        content = open(filePath).read()
        try:
            sketchName = re.search('(?<=SKETCH_NAME).+', content).group(0)
            sketchName = sketchName.strip(' \'=')
            sketchPath = 'arduino/%s/%s.ino' % (sketchName, sketchName)
            text += '<p>\n'
            text += indent(('Sketch: %s\n' %\
                               getHtmlAnchorToTip(sketchPath, sketchName)), 1)
            text += '</p>\n'
        except:
            pass
        self.Content += indent(text, 3)



class gReleaseNotesWebPage(gWebPage):

    """ Specialized web page.
    """

    def post(self):
        """ Overloaded class method.
        """
        from plasduino.__plasduino__ import RELEASE_NOTES_PATH
        tagFound = False
        text = ''
        notes = open(RELEASE_NOTES_PATH)
        for line in notes:
            line = line.strip()
            if line.startswith('-----'):
                tagFound = True
                line = notes.next()
                if len(text):
                    text += indent('</li>', 2)
                    text += indent('</ul>', 1)
                text += '<h4>&mdash;&nbsp;%s</h4>\n' % (line.strip('\n'))
                text += indent('<ul>', 1)
                line = notes.next()
                line = notes.next().strip()
            if line.startswith('*'):
                if not text.endswith('>\n') and len(text):
                    text += indent('</li>', 2)
                line = line.strip('*').strip()
                if not tagFound:
                    text += indent('<ul>', 1)
                    tagFound = True
                text += indent('<li>', 2)
            text += indent(line, 2)
        text += indent('</li>', 2)
        text += indent('</ul>', 1)
        self.Content += indent(text, 3)



if __name__ == '__main__':
    from gMenuBar import gMenuBar
    bar = gMenuBar()
    menu = bar.addMenu('Home', 'index.html')
    menu = bar.addMenu('Installation')
    menu.addMenu('Pre-requisites', 'prerequisites.html')
    menu.addMenu('Downloads', 'downloads.html')
    menu.addMenu('GNU/Linux', 'installation_linux.html')
    menu.addMenu('Windows', 'installation_windows.html')
    menu.addMenu('Mac', 'installation_mac.html')
    menu = bar.addMenu('Overview')
    menu.addMenu('Features', 'features.html')
    menu.addMenu('Quick start', 'quickstart.html')
    menu.addMenu('Configuration', 'configuration.html')
    menu.addMenu('Architecture', 'architecture.html')
    page = gWebPage('Test page', 'test.html', bar)
    page.write(True)
    print(page)
