#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


BASE_SRC_TIP_URL = 'https://bitbucket.org/lbaldini/plasduino/src/tip/'


def getLinkToTip(filePath):
    """ Get the link to the tip version of a given file (to be browsed
    online) in the mercurial repository.
    """
    return '%s%s?at=default' % (BASE_SRC_TIP_URL, filePath)

def getHtmlAnchorToTip(filePath, name = None):
    """ Get an html amchor to the tip version of a given file (to be browsed
    online) in the mercurial repository.
    """
    link = getLinkToTip(filePath)
    name = name or filePath
    return '<a href="%s">%s</a>' % (link, name) 




if __name__ == '__main__':
    filePath = 'arduino/sktchAnalogSampling/sktchAnalogSampling.ino'
    print(getLinkToTip(filePath))
    print(getHtmlAnchorToTip(filePath))
    print(getHtmlAnchorToTip(filePath, 'sktchAnalogSampling'))
    
