#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import time

from __format__ import indent
from plasduino.__plasduino__ import getTagInfo


class gFooter:
    
    """ Class describing a footer for an html web page.
    """

    def html(self):
        """
        """
        tag, buildDate = getTagInfo()
        text  = '<div id="footer">\n'
        text += indent('Copyright &copy; 2013&ndash;2014 the plasduino team<br>\n', 1)
        text += indent(
            """We try and make sure that this page validates as\n
<a href="http://validator.w3.org/check?uri=referer">HTML 5</a>\n
and\n
<a href="http://jigsaw.w3.org/css-validator/check/referer">css level 3</a>\n
(feel free to bug us if it doesn't).<br>\n""", 1)
        text += indent('Compiled from plasduino %s, last update %s.' %\
            (tag, time.strftime('%A, %B %d %Y at %H:%M (%z)')), 1)
        text += '</div>'
        return text

    def __str__(self):
        """ String formatting.
        """
        return self.html()



if __name__ == '__main__':
    footer = gFooter()
    print(footer)
