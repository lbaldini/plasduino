#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __format__ import indent


class gMenuBase(list):

    """ Base class for a menu entry.
    """

    NULL_URL = '#'

    def addMenu(self, name, url = NULL_URL):
        """ Add a menu item.
        """
        menu = gMenu(name, url)
        self.append(menu)
        return menu

    def html(self):
        """ html representation.
        """
        return ''

    def __str__(self):
        """ String formatting.
        """
        return self.html()



class gMenu(gMenuBase):
    
    """ Utility class representing a menu item for a web page.
    """

    def __init__(self, name, url = gMenuBase.NULL_URL):
        """ Constructor.
        """
        self.Name = name
        self.Url = url
        gMenuBase.__init__(self)

    def html(self):
        """ Get the html snippet representing the menu bar.
        """
        text = '<li><a href="%s">%s</a>' % (self.Url, self.Name)
        if len(self):
            text += indent('\n<ul>\n', 1)
            for menu in self:
                text += indent('%s\n' % menu, 2)
            text += indent('</ul>\n', 1)
        text += '</li>'
        return text



class gMenuBar(gMenuBase):

    """ Utility class representing a menu bar for a web page.
    """

    def html(self):
        """
        """
        text = '<div id="menu">\n'
        text += indent('<nav>\n', 1)
        text += indent('<ul>\n', 2)
        for menu in self:
            text += indent('%s\n' % menu, 3)
        text += indent('</ul>', 2)
        text += indent('</nav>', 1)
        text += '</div>'
        return text

    def getPageList(self):
        """
        """
        pageList = []
        for menu in self:
            if menu.Url != gMenuBase.NULL_URL:
                pageList.append((menu.Name, menu.Url))
            for submenu in menu:
                if submenu.Url != gMenuBase.NULL_URL:
                    pageList.append((submenu.Name, submenu.Url))
        return pageList



if __name__ == '__main__':
    bar = gMenuBar()
    menu = bar.addMenu('Home', 'index.html')
    menu = bar.addMenu('Installation')
    menu.addMenu('Pre-requisites', 'prerequisites.html')
    menu.addMenu('Downloads', 'downloads.html')
    menu.addMenu('GNU/Linux', 'installation_linux.html')
    menu.addMenu('Windows', 'installation_windows.html')
    menu.addMenu('Mac', 'installation_mac.html')
    menu = bar.addMenu('Overview')
    menu.addMenu('Features', 'features.html')
    menu.addMenu('Quick start', 'quickstart.html')
    menu.addMenu('Configuration', 'configuration.html')
    menu.addMenu('Architecture', 'architecture.html')
    print(bar)
