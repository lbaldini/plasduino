#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.modulemanager.__modulemanager__ import loadModuleInfo
from plasduino.__plasduino__ import PLASDUINO_DIST_MODULEINFO_FILE_PATH

from gMenuBar import gMenuBar


""" Retrieve module information for later use.
"""
MODULE_DICT = loadModuleInfo(PLASDUINO_DIST_MODULEINFO_FILE_PATH)
MODULE_NAMES = MODULE_DICT.keys()
MODULE_NAMES.sort()
MODULE_LIST = [MODULE_DICT[name] for name in MODULE_NAMES]


""" Setup the menu bar.
"""
MENU_BAR = gMenuBar()
menu = MENU_BAR.addMenu('Home', 'index.html')
menu = MENU_BAR.addMenu('Installation')
menu.addMenu('Pre-requisites', 'prerequisites.html')
menu.addMenu('Downloads', 'downloads.html')
menu.addMenu('GNU/Linux', 'installation_linux.html')
menu.addMenu('Windows', 'installation_windows.html')
menu.addMenu('Mac', 'installation_mac.html')
menu = MENU_BAR.addMenu('Overview')
menu.addMenu('Screenshots', 'screenshots.html')
menu.addMenu('Features', 'features.html')
menu.addMenu('Quick start', 'quickstart.html')
menu.addMenu('Configuration', 'configuration.html')
menu.addMenu('Architecture', 'architecture.html')
menu = MENU_BAR.addMenu('Modules')
for module in MODULE_LIST:
    menu.addMenu(module['name'], '%s.html' % module.getFileName())
menu = MENU_BAR.addMenu('Shields')
menu.addMenu('Lab 1', 'shield_lab1.html')
menu.addMenu('Scaler', 'shield_scaler.html')
menu = MENU_BAR.addMenu('Core development')
menu.addMenu('Team', 'team.html')
menu.addMenu('Source code', 'source.html')
menu.addMenu('Release howto', 'release_howto.html')
menu.addMenu('Release notes', 'release_notes.html')



if __name__ == '__main__':
    print(MENU_BAR)


