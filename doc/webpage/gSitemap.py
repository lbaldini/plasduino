#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import time
import re

from __layout__ import MENU_BAR
from plasduino.__logging__ import logger
from plasduino.__plasduino__ import PLASDUINO_WEB_HTML, PLASDUINO_WEB_PAGES


# From http://www.sitemaps.org/protocol.html:
# 
# <lastmod>
#    date of last modification in W3C format.
#
# <changefreq> 
#    always
#    hourly
#    daily
#    weekly
#    monthly
#    yearly
#    never
#
# <priority>
#    0 to 1, default 0.5
#

def w3timestamp():
    """ Return the current time in W3C format, see
    http://www.w3.org/TR/NOTE-datetime

    YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)
    """
    t = time.strftime('%Y-%m-%dT%H:%M:%S%z')
    t = t[:-2] + ':' + t[-2:]
    return t


CHANGEFREQ_DICT = {'': ''
                   }

def changfreq(target):
    """
    """
    target = target.replace('.html', '')
    try:
        return CHANGEFREQ_DICT[target]
    except KeyError:
        return 'monthly'


PRIORITY_DICT = {'index' : 1.0
                 }

def priority(target):
    """
    """
    target = target.replace('.html', '')
    try:
        return PRIORITY_DICT[target]
    except KeyError:
        return 0.5


class gImage:
    
    """
    """

    def __init__(self, location, caption):
        """
        """
        self.Location = location
        self.Caption = caption

    def html(self):
        return '    <image:image>\n' +\
               '      <image:loc>%s</image:loc>\n' % self.Location +\
               '      <image:caption>%s</image:caption>\n' % self.Caption +\
               '    </image:image>\n'



class gSitemap:
    
    """
    """

    HEADER = """<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"\n
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"\n
        xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">\n"""

    TRAILER = """</urlset>\n"""

    def __init__(self, rootUrl):
        """
        """
        self.Timestamp = w3timestamp()
        self.RootUrl = rootUrl
        self.Xml = self.HEADER
        for (name, url) in MENU_BAR.getPageList():
            if url.endswith('.html'):
                self.Xml += '  <url>\n'
                self.Xml += '    <loc>%s/%s</loc>\n' % (self.RootUrl, url)
                self.Xml += '    <lastmod>%s</lastmod>\n' % self.Timestamp
                self.Xml += '    <changefreq>%s</changefreq>\n' %\
                    changfreq(url)
                self.Xml += '    <priority>%s</priority>\n' %\
                    priority(url)
                for img in self.findImages(url):
                    self.Xml += img.html()
                self.Xml += '  </url>\n'
        self.Xml += self.TRAILER
        
    def findImages(self, url):
        """
        """
        images = []
        filePath = os.path.join(PLASDUINO_WEB_PAGES, url)
        for item in re.findall('\<img .*\>', file(filePath).read()):
            src = re.search('(?<=src=").*(?=" )', item).group(0)
            src = '%s/%s' % (self.RootUrl, src)
            alt = re.search('(?<=alt=").*(?="/\>)', item).group(0)
            images.append(gImage(src, alt))
        return images

    def write(self, filePath):
        """
        """
        filePath = os.path.join(PLASDUINO_WEB_HTML, filePath)
        logger.info('Writing output file %s...' % filePath)
        file(filePath, 'w').writelines(self.Xml)
        logger.info('Done.')




if __name__ == '__main__':
    gSitemap('http://pythonhosted.org/plasduino').write('sitemap_plasduino.xml')
    
