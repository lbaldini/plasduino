#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import glob
import os


import plasduino.__utils__ as __utils__


from plasduino.__plasduino__ import PLASDUINO_WEB, PLASDUINO_WEB_HTML,\
    PLASDUINO_WEB_IMAGES, PLASDUINO_WEB_CSS, PLASDUINO_MODULES

from __layout__ import MENU_BAR, MODULE_DICT
from gWebPage import gWebPage, gConfigurationWebPage, gTeamWebPage,\
    gModuleWebPage, gReleaseNotesWebPage
from plasduino.__logging__ import logger


def cleanup():
    """ Initial cleanup.
    """
    __utils__.createFolder(PLASDUINO_WEB_HTML)
    __utils__.cleanup(PLASDUINO_WEB_HTML)

def copyFiles():
    """ Copy over to the html folder the necessary files.
    """
    googleFilePath = os.path.join(PLASDUINO_WEB, 'google',
                                  'google8a84d96068999eab.html')
    __utils__.cp(googleFilePath, PLASDUINO_WEB_HTML)
    __utils__.cp(PLASDUINO_WEB_IMAGES,
                 os.path.join(PLASDUINO_WEB_HTML, 'images'))
    __utils__.cp(PLASDUINO_WEB_CSS,
                 os.path.join(PLASDUINO_WEB_HTML, 'css'))

def generatePages():
    """ Generate the web pages.
    """
    for (name, url) in MENU_BAR.getPageList():
        if name in MODULE_DICT.keys() and url.startswith('plasduino_'):
            page = gModuleWebPage(name, url, MENU_BAR)
        elif url == 'team.html':
            page = gTeamWebPage(name, url, MENU_BAR)
        elif url == 'configuration.html':
            page = gConfigurationWebPage(name, url, MENU_BAR)
        elif url == 'release_notes.html':
            page = gReleaseNotesWebPage(name, url, MENU_BAR)
        else:
            page = gWebPage(name, url, MENU_BAR)
        page.write()

def createSitemap():
    from gSitemap import gSitemap
    gSitemap('http://pythonhosted.org/plasduino').write('sitemap_plasduino.xml')

def dumpModuleInfo():
    """ Run the code dumping the module info.
    """
    logger.info('Dumping module info...')
    from plasduino.modulemanager.__modulemanager__ import dumpModuleInfo
    from plasduino.__plasduino__ import PLASDUINO_DIST_MODULEINFO_FILE_PATH,\
        PLASDUINO_MODULES
    dumpModuleInfo([PLASDUINO_MODULES], PLASDUINO_DIST_MODULEINFO_FILE_PATH)

def deploy():
    """
    """
    cleanup()
    dumpModuleInfo()
    copyFiles()
    generatePages()
    createSitemap()


if __name__ == '__main__':
    deploy()
    
