
from plasduino.analysis.__ROOT__ import *



class Bowl(ROOT.TGeoVolume):

    """ Class describing the bowl for the water.
    """
    
    LENGTH = 250.
    WIDTH = 30.
    HEIGHT = 40

    def __init__(self, color = ROOT.kGray):
        """ Constructor.
        """
        self.Shape = ROOT.TGeoBBox('Bowl shape', self.LENGTH/2.,
                                   self.WIDTH/2., self.HEIGHT/2.)
        ROOT.TGeoVolume.__init__(self, 'Bowl volume', self.Shape)
        self.SetLineColor(color)


class Water(ROOT.TGeoVolume):

    """ Class describing the bowl for the water.
    """

    def __init__(self, color = ROOT.kBlack):
        """ Constructor.
        """
        self.Shape = ROOT.TGeoBBox('Water shape', Bowl.LENGTH/2.,
                                   Bowl.WIDTH/2., Bowl.HEIGHT/4.)
        ROOT.TGeoVolume.__init__(self, 'Water volume', self.Shape)
        self.SetLineColor(color)



class Contact(ROOT.TGeoVolume):

    """ Class describing the bowl for the water.
    """
    
    LENGTH = 1.
    WIDTH = 20.
    HEIGHT = 50

    def __init__(self, color = ROOT.kGray):
        """ Constructor.
        """
        self.Shape = ROOT.TGeoBBox('Contact shape', self.LENGTH/2.,
                                   self.WIDTH/2., self.HEIGHT/2.)
        ROOT.TGeoVolume.__init__(self, 'Contact volume', self.Shape)
        self.SetLineColor(color)



class Mass(ROOT.TGeoVolume):

    """ Class describing the mass for the pendulum.
    """

    RADIUS = 25
    THICKNESS = 5

    def __init__(self, color = ROOT.kBlack):
        """ Constructor.
        """
        self.Shape = ROOT.TGeoTube('Mass shape', 0, self.RADIUS, self.THICKNESS)
        ROOT.TGeoVolume.__init__(self, 'Mass volume', self.Shape)
        self.SetLineColor(color)



class Stem(ROOT.TGeoVolume):

    """ Class describing the stem for the pendulum.
    """
    
    LENGTH = 5.
    WIDTH = 5.
    HEIGHT = 100.

    def __init__(self, color = ROOT.kGray):
        """ Constructor.
        """
        self.Shape = ROOT.TGeoBBox('Stem shape', self.LENGTH/2.,
                                   self.WIDTH/2., self.HEIGHT/2.)
        ROOT.TGeoVolume.__init__(self, 'Stem volume', self.Shape)
        self.SetLineColor(color)



class Tip(ROOT.TGeoVolume):

    """ Class describing the tip for the pendulum.
    """
    
    LENGTH = 1.
    WIDTH = 1.
    HEIGHT = 30.

    def __init__(self, color = ROOT.kGray):
        """ Constructor.
        """
        self.Shape = ROOT.TGeoBBox('Tip shape', self.LENGTH/2.,
                                   self.WIDTH/2., self.HEIGHT/2.)
        ROOT.TGeoVolume.__init__(self, 'Tip volume', self.Shape)
        self.SetLineColor(color)




class ExperimentalSetup(ROOT.TGeoVolume):
    
    """ Class describing the experimental setup.
    """

    SIDE = 1000
    HEIGHT = 1000

    def __init__(self):
        """
        """
        self.Shape = ROOT.TGeoBBox('Experiment shape', self.SIDE/2.,
                                   self.SIDE/2., self.HEIGHT/2.)
        ROOT.TGeoVolume.__init__(self, 'Experiment volume', self.Shape)
        self.WaterVol = Water(ROOT.kGray)
        dz = -50
        trans = ROOT.TGeoTranslation(0.0, 0.0, dz)
        self.AddNode(self.WaterVol, 1, trans)
        self.BowlVol = Bowl(ROOT.kBlack)
        dz += Bowl.HEIGHT/4.
        trans = ROOT.TGeoTranslation(0.0, 0.0, dz)
        self.AddNode(self.BowlVol, 2, trans)
        self.MassVol = Mass(ROOT.kBlack)       
        dz = 0.
        shift = ROOT.TGeoTranslation(0.0, 0.0, dz)
        rot =  ROOT.TGeoRotation('rotation', 0., 90., 0.)
        trans = ROOT.TGeoCombiTrans(shift, rot)
        self.AddNode(self.MassVol, 3, trans)
        self.StemVol = Stem(ROOT.kBlack)
        dz = Stem.HEIGHT - Mass.RADIUS
        trans = ROOT.TGeoTranslation(0.0, 0.0, dz)
        self.AddNode(self.StemVol, 4, trans)
        self.TipVol = Tip(ROOT.kBlack)
        dz = -Tip.HEIGHT/2. - Mass.RADIUS
        trans = ROOT.TGeoTranslation(0.0, 0.0, dz)
        self.AddNode(self.TipVol, 5, trans)

        self.ContactVol = Contact(ROOT.kBlack)
        dz = -38.5
        trans = ROOT.TGeoTranslation(-Bowl.LENGTH/2., 0.0, dz)
        self.AddNode(self.ContactVol, 6, trans)
        trans = ROOT.TGeoTranslation(Bowl.LENGTH/2., 0.0, dz)
        self.AddNode(self.ContactVol, 7, trans)





def draw():
    geo = ROOT.TGeoManager('Geometry manager', 'Geometry manager')
    setup = ExperimentalSetup()
    geo.SetTopVolume(setup)
    geo.CloseGeometry()
    geo.SetVisLevel(4)
    geo.SetVisOption(0)
    setup.Draw()
    t1 = ROOT.TLatex(-0.65, -0.35, 'Ground')
    t1.SetTextSize(SMALL_TEXT_SIZE)
    t1.Draw()
    t2 = ROOT.TLatex(0.3, 0.05, '+V_{cc}')
    t2.SetTextSize(SMALL_TEXT_SIZE)
    t2.Draw()
    t3 = ROOT.TLatex(-0.2, 0.65, 'To analog input')
    t3.SetTextSize(SMALL_TEXT_SIZE)
    t3.Draw()
    raw_input('Press enter to exit...')



if __name__ == '__main__':
    draw()
