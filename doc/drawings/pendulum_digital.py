
from plasduino.analysis.__ROOT__ import *



class Mass(ROOT.TGeoVolume):

    """ Class describing the mass for the pendulum.
    """
    
    LENGTH = 250.
    WIDTH = 40.
    HEIGHT = 40.

    def __init__(self, color = ROOT.kBlack):
        """ Constructor.
        """
        self.Shape = ROOT.TGeoBBox('Mass shape', self.LENGTH/2.,
                                   self.WIDTH/2., self.HEIGHT/2.)
        ROOT.TGeoVolume.__init__(self, 'Mass volume', self.Shape)
        self.SetLineColor(color)



class Flag(ROOT.TGeoVolume):

    """ Class describing the flag for the pendulum.
    """
    
    LENGTH = 30.
    WIDTH = 2.
    HEIGHT = 80.

    def __init__(self, color = ROOT.kBlack):
        """ Constructor.
        """
        self.Shape = ROOT.TGeoBBox('Flag shape', self.LENGTH/2.,
                                   self.WIDTH/2., self.HEIGHT/2.)
        ROOT.TGeoVolume.__init__(self, 'Flag volume', self.Shape)
        self.SetLineColor(color)



class Gate(ROOT.TGeoVolume):
    
    """
    """

    def __init__(self, color = ROOT.kBlack):
        """ COnstructor.
        """

        ROOT.TGeoVolume.__init__(self, 'Gate volume', self.Shape)
        self.SetLineColor(color)



class Rope(ROOT.TPolyLine3D):
    
    """
    """

    def __init__(self, x0, y0, z0, x1, y1, z1):
        """
        """
        ROOT.TPolyLine3D.__init__(self)
        self.SetPoint(0, x0, y0, z0)
        self.SetPoint(1, x1, y1, z1)
        self.SetLineColor(ROOT.kGray)
        



class ExperimentalSetup(ROOT.TGeoVolume):
    
    """ Class describing the experimental setup.
    """

    SIDE = 1000
    HEIGHT = 1000

    def __init__(self):
        """
        """
        self.Shape = ROOT.TGeoBBox('Experiment shape', self.SIDE/2.,
                                   self.SIDE/2., self.HEIGHT/2.)
        ROOT.TGeoVolume.__init__(self, 'Experiment volume', self.Shape)
        self.MassVol = Mass(ROOT.kBlack)
        dz = 0
        trans = ROOT.TGeoTranslation(0.0, 0.0, dz)
        self.AddNode(self.MassVol, 1, trans)
        self.FlagVol = Flag(ROOT.kBlack)
        dz = -30
        trans = ROOT.TGeoTranslation(0.0, 0.0, dz)
        self.AddNode(self.FlagVol, 2, trans)
        #self.GateVol = Gate(ROOT.kBlack)
        #dz = -50
        #trans = ROOT.TGeoTranslation(0.0, 0.0, dz)
        #self.AddNode(self.GateVol, 2, trans)



def draw():
    geo = ROOT.TGeoManager('Geometry manager', 'Geometry manager')
    setup = ExperimentalSetup()
    geo.SetTopVolume(setup)
    geo.CloseGeometry()
    geo.SetVisLevel(4)
    geo.SetVisOption(0)
    setup.Draw()
    l1 = Rope(-Mass.LENGTH/2., Mass.WIDTH/2., Mass.HEIGHT/2., 0, 0, 500)
    l1.Draw('same')
    l2 = Rope(Mass.LENGTH/2., Mass.WIDTH/2., Mass.HEIGHT/2., 0, 0, 500)
    l2.Draw('same')
    l3 = Rope(-Mass.LENGTH/2., -Mass.WIDTH/2., Mass.HEIGHT/2., 0, 0, 500)
    l3.Draw('same')
    l4 = Rope(Mass.LENGTH/2., -Mass.WIDTH/2., Mass.HEIGHT/2., 0, 0, 500)
    l4.Draw('same')
    l = Rope(0, -2*Mass.WIDTH, -50, 0, 2*Mass.WIDTH, -50)
    l.SetLineStyle(7)
    l.SetLineColor(ROOT.kBlack)
    l.Draw()
    t1 = ROOT.TLatex(0.1, -0.55, 'Photo-gate')
    t1.Draw()
    t2 = ROOT.TLatex(0.1, -0.2, 'Flag')
    t2.Draw()
    m = ROOT.TPolyMarker3D(1, 20)
    m.SetPoint(0, 0, 0, -50)
    m.Draw('same')
    ROOT.gPad.Update()
    raw_input('Press enter to exit...')



if __name__ == '__main__':
    draw()
