\documentclass{plasduino-datasheet}

\usetikzlibrary{patterns}

\title{Shield lab1 version 2}

\begin{document}

\begin{article}

\maketitle

\secsummary

This shield is designed for the lab course for the freshmen at the physics
department of the University of Pisa. We currently use it mainly for a set
of mechanics and thermodynamics didactic experiments.

All the relevant files (Fritzing files, etchable pdf files, gerber files
and part lists) can be found on \url{https://bitbucket.org/lbaldini/plasduino/src/tip/shields/lab1/lab1_v2}.


\secfeatures

The main purpose of the the shield is to provide a convenient interface
between the arduino I/O ports and the external world. In fact the shield
provides mini-jack connectors for two optical gates and a pair of $10$~k$\Omega$
thermistors, along with generic in-line female pins providing access to four
arduino analog inputs, distributing the $+5$~V and providing a PWM power output
(through an external power supply) to drive a motor.

A IC-555 timer provides a square wave, whose frequency is adjustable
(roughly between $~1$~Hz and a few kHz) through the R1 potentiometer, that
can be used to test the functionality of the assembly and to measure the
time jitter of the system. The IC-555 output can be fed to the D2 arduino
digital input by switching the jumper on SV1 (by default the D2 arduino pin
is connected to the proper pin of the D0 female mini-jack on the shield).

The shield provides all the circuitry to modulate the voltage from an external
power supply (provided on the Pwr input connector) with the PWM from the
arduino D9 digital output. This allows to drive an external motor (changing
the duty cycle of the PWM on the arduino D9 controls the speed of the motor
itself).

Finally, the shield provides a red (PWR) led signaling that the assembly is
powered up and a general-purpose yellow (LY) led connected to the arduino
D13 output pin that can be used in the sketches. A reset button completes the
board.


\secconnections

Here is a more detailed description of the I/O connectors (refer to the
following section for the pinout).

The D0 and D1 mini-jacks provide the interface for two optical gates.
For both connectors two of the three pins are connected to ground and $+5$~V,
(the power for the photo-diodes) while the third goes to the arduino digital
inputs D2 and D3, respectively.

The A01 mini-jack provides the interface for two $10$~k$\Omega$ thermistors on
the arduino analog inputs A0 and A1, respectively. Note that the shield also
features two 10~kOhm ($1\%$) resistors from the analog inputs A0 and A1 to
ground, acting as voltage dividers for the thermistors, so that these inputs
cannot be used as general-purpose analog inputs.

The Sgn1 in-line 6-pins generic female header provides connections to
ground, $+5$~V and the arduino analog inputs A2--A5 (with the A2 and A3
lines passing through a dual operational amplifier acting as a buffer).
These inputs \emph{can} be used as generic analog inputs.

The motor 2-pin female header provides the output for driving a motor
(with the external power being supplied on the Pwr 2-pin female header
next to it).


\secpinout

Here are the detailed pinouts for the male minicam connectors to be plugged
into the shield inputs.

\begin{figure}[htb!]
  \begin{tikzpicture}[scale=1]
    \node at (-4.5, 1) {};
    \stereominijack{Ground}{Signal}{$V_{\rm cc}$}
  \end{tikzpicture}
  \caption{Pinout for the male mini-jacks to D0 and D1 input on the shield.}
  \label{fig:digital_pinout}
\end{figure}


\begin{figure}[htb!]
  \begin{tikzpicture}[scale=1]
    \node at (-4.5, 1) {};
    \stereominijack{A1}{A0}{$V_{\rm cc}$}
  \end{tikzpicture}
  \caption{Pinout for the male mini-jacks to A01 input on the shield.}
  \label{fig:analog_pinout}
\end{figure}



\onecolumn

\secassembly

\begin{table}[hb!]
  \begin{center}
    \begin{tabular}{p{0.09\textwidth}@{}p{0.34\textwidth}@{}p{0.55\textwidth}}
      \hline
      Label & Description & Purpose\\
      \hline
      \hline
      A01    & Mini-jack stereo 3.5mm (3 pins) &
      Connector for two thermistors to analog inputs A0 and A1 ($10$~k$\Omega$
      resistors on board completing the voltage divider).\\
      C1     & $1~\mu$F capacitor &
      Part of the passing network for the 555 IC generating the calibration
      square wave.\\
      C2     & $100$~nF capacitor &
      Capacitor on the power line of the 555 IC generating the calibration
      square wave.\\
      C3     & $100$~nF capacitor &
      Capacitor on the power line of the (optional) operational amplifier acting 
      as a buffer on the A2 and A3 arduino analog inputs.\\
      C4     & $100$~nF capacitor &
      Filtering capacitor for the $+5$~V output voltage to the analog
      pendulum (through Sgn1).\\
      C5     & $1~\mu$F capacitor &
      Filtering capacitor for the $+5$~V output voltage to the analog
      pendulum (through Sgn1). \\
      D0     & Mini-jack stereo 3.5mm (3 pins) &
      Connector for a signal to the arduino digital input D2 (includes $+5$~V
      to be fed to the optical gate).\\
      D1     & Mini-jack stereo 3.5mm (3 pins) &
      Connector for a signal to the arduino digital input D3 (includes $+5$~V
      to be fed to the optical gate).\\
      D3     & Zener diode &
      Part of the circuitry to drive an external motor via PWM (through the
      Motor connector).\\
      IC-555 & 555 Timer (package DIP8) &
      Integrated circuit to generate a square wave that can be fed to
      digital input D2 for calibration and test.\\
      J2     & Generic male header (8 pins) &
      Pin strip to connect the shield on top of the arduino board.\\
      J3     & Generic male header (6 pins) &
      Pin strip to connect the shield on top of the arduino board.\\
      J4     & Generic male header (10 pins) &
      Pin strip to connect the shield on top of the arduino board.\\
      J5     & Generic male header (8 pins) &
      Pin strip to connect the shield on top of the arduino board.\\
      LY     & Yellow LED (5mm) &
      General purpose led connected to the arduino digital pin D13 (to be used
      in the sketches).\\
      Motor  & Screw terminal (2 pins) &
      Two-pin output connector to provide a PWM-modulated voltage to
      an external motor.\\
      Pwr    & Screw terminal (2 pins) &
      Two-pin input connector for the external power supply providing the
      power to drive the external motor.\\
      PWR    & Red LED (5mm) &
      Red led signaling that the shield (and, even more importantly, the
      arduino) is powered up.\\
      Q2     & NPN-Transistor (package TO92) &
      Part of the circuitry to drive an external motor via PWM (through the
      Motor connector).\\
      R1     & $100$~k$\Omega$ rotary potentiometer ($9$~mm) &
      Part of the passing network for the 555 IC generating the calibration
      square wave (controls the frequency).\\
      R2     & $220~\Omega$ resistor ($5\%$) &
      Part of the passing network for the 555 IC generating the calibration
      square wave.\\
      R3     & $10$~k$\Omega$ resistor ($1\%$) &
      Voltage divider for a $10$~k$\Omega$ thermistor to be connected to
      arduino analog input A0 through the A01 connector.\\
      R4     & $10$~k$\Omega$ resistor ($1\%$) &
      Voltage divider for a $10$~k$\Omega$ thermistor to be connected to
      arduino analog input A1 through the A01 connector.\\
      R6     & $1$~k$\Omega$ resistor ($5\%$) &
      Polarizing resistor for the red PWR led.\\ 
      R7     & $1$~k$\Omega$ resistor ($5\%$) &
      Polarizing resistor for the yellow LY led.\\
      R10    & $1$~k$\Omega$ resistor ($5\%$) &
      Part of the circuitry to drive an external motor via PWM (through the
      Motor connector).\\
      RESET  & Push button &
      Push button to reset the arduino board.\\
      Sgn1   & Screw terminal (6 pins) &
      Connector for the analog pendulum.\\
      SV1    & Generic male header (2 pins) &
      Jumper connecting to the arduino digital input D2 either the D0 connector
      or the IC-555 output.\\
      SVG    & Generic female header (2 pins) &
      Jumper to put the Q2 emitter to ground.\\
      SVP    & Generic female header (3 pins) &
      Jumper to switch between the arduino 5~V and an external power supply
      for driving the motor.\\
      U1     & Dual Op-Amp (package DIP8) &
      Optional op amp acting as a buffer for the signal to be fed to the
      A2 and A3 arduino analog inputs.\\
      \hline
    \end{tabular}
  \end{center}
\end{table}


\clearpage


%\secshopping


%\clearpage


\secschematics

\begin{figure}[htp!]
  \begin{center}
    \includegraphics[width=0.52\textwidth,clip=true,trim=23 23 23 23]%
                    {figures/lab1_v2_etch_silk_top}\\\medskip
    \includegraphics[width=0.52\textwidth,clip=true,trim=23 23 23 23]%
                    {figures/lab1_v2_etch_copper_top}\\\medskip
    \includegraphics[width=0.52\textwidth,clip=true,trim=23 23 23 23]%
                    {figures/lab1_v2_etch_copper_bottom}
    \caption{Etchable board layouts (from top to bottom: silk top, copper
      top, copper bottom). Mind that they're not to scale, so this page
      is not suitable for the actual board etching (use the separate pdf
      files linked on the web page instead.)}
  \end{center}
\end{figure}



\end{article}

\end{document}
