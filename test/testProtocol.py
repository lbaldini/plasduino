#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
import time
import struct

import plasduino.arduino.protocol_h as ptcl

from plasduino.daq.gRunControl import gRunControl
from plasduino.daq.gArduinoRunControl import gArduinoRunControl
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.__logging__ import logger



class testprotocolRunControl(gArduinoRunControl):

    """ Minimal RunControl class for testing purposes.
    """

    SKETCH_NAME = 'sktchTestProtocol'

    def _start(self):
        """ Start the RunControl (STOPPED -> RUNNING).
        """
        gRunControl._start(self)
        self.ArduinoManager.swriteStartRun()

    def _stop(self):
        """ Stop the RunControl (RUNNING -> STOPPED).
        """
        self.ArduinoManager.swriteStopRun()
        self.flushInput()
        gRunControl._stop(self)

    def poll(self):
        """ Overloaded poll() method.
        """
        transition = self.ArduinoManager.sreadDigitalTransition()
        if transition is not None:
            print(transition)




if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    m = testprotocolRunControl(gEventBuffer())
    if m.connectToArduino(timeout = 0.2):
        sys.exit()
    # Since we're probably debugging, force a fresh upload of the sketch.
    # We might want to add a command-line switch, here.
    #m.ArduinoManager.loadSketch(m.SKETCH_NAME)
    m.ArduinoManager.swritecmd(ptcl.OP_CODE_SELECT_DIGITAL_PIN, 'B', 3)
    m.ArduinoManager.swritecmd(ptcl.OP_CODE_SELECT_ANALOG_PIN, 'B', 2)
    m.setRunning()
    time.sleep(5)
    m.setStopped()

