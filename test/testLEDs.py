#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys

import plasduino.arduino.protocol_h as ptcl

from PyQt4 import QtCore, QtGui
from plasduino.gui.gMainWindow import gMainWindow
from plasduino.gui.gPushButton import gPushButton
from plasduino.gui.gQt import connect
from plasduino.__logging__ import logger
from plasduino.daq.gArduinoManager import gArduinoManager
from plasduino.arduino.shield_lab1_h import PIN_GREEN_LED, PIN_YELLOW_LED



class ledsWindow(gMainWindow):

    """ Main window for a simple application to turn a LED on/off.
    """
    
    SKETCH_NAME = 'sktchTestLEDs'
    WINDOW_TITLE = 'LED test'

    def __init__(self, **kwargs):
        gMainWindow.__init__(self, **kwargs)
        self.GreenButton = QtGui.QPushButton('Green')
        self.GreenButton.setCheckable(True)
        self.YellowButton = QtGui.QPushButton('Yellow')
        self.YellowButton.setCheckable(True)
        self.QuitButton = QtGui.QPushButton('Quit')
        self.addWidget(self.GreenButton)
        self.addWidget(self.YellowButton)
        self.addWidget(self.QuitButton)
        self.__setupConnections()
        self.ArduinoManager = gArduinoManager()
        self.ArduinoManager.connect(self.SKETCH_NAME)
        if not self.ArduinoManager.connected():
            self.GreenButton.setEnabled(False)
            self.YellowButton.setEnabled(False)
        
    def __setupConnections(self):
        """ Setup the connections.
        """
        connect(self.QuitButton, 'clicked()', self.close)
        connect(self.GreenButton, 'clicked()', self.changeGreen)
        connect(self.YellowButton, 'clicked()', self.changeYellow)

    def close(self):
        """ Turn off the leds and close the window.
        """
        if self.ArduinoManager.connected():
            self.setLedStatus(PIN_YELLOW_LED, 0)
            self.setLedStatus(PIN_GREEN_LED, 0)
        gMainWindow.close(self)

    def setLedStatus(self, led, status):
        """ Turn on/off a given led.
        """
        payload = led + (status << 4)
        self.ArduinoManager.swritecmd(ptcl.OP_CODE_TOGGLE_LED, 'B', payload)

    def changeGreen(self):
        """ Toggle the green led. 
        """
        btnStatus = self.GreenButton.isChecked()
        self.setLedStatus(PIN_GREEN_LED, btnStatus)

    def changeYellow(self):
        """ Toggle the yellow led.
        """
        btnStatus = self.YellowButton.isChecked()
        self.setLedStatus(PIN_YELLOW_LED, btnStatus)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = ledsWindow()
    w.show()
    application.exec_() 
