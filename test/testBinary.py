#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
import struct

import plasduino.arduino.protocol_h as ptcl

from plasduino.__logging__ import logger, abort
from plasduino.daq.gDigitalTransition import gDigitalTransition
from plasduino.daq.gAnalogReadout import gAnalogReadout



def playback(filePath):
    """ Play back a binary data file.
    """
    logger.info('Opening input file %s...' % filePath)
    if not filePath.endswith('.dat'):
        abort('Skipping file (please provide a .dat file).')
    inputFile = open(filePath, 'rb')
    logger.info('Reading header...')
    producerId = struct.unpack('>B', inputFile.read(1))[0]
    producerVersion = struct.unpack('>B', inputFile.read(1))[0]
    logger.info('Found producer id %d version %d.' %\
                (producerId, producerVersion))
    logger.info('Starting playback (Enter to advance, q+Enter to exit)...')
    while(1):
        # Read one byte and find out the data type.
        header = inputFile.read(1)
        if len(header) == 0:
            break
        header = struct.unpack('>B', header)[0]
        # Parse the actual data...
        if header == ptcl.DIGITAL_TRANSITION_HEADER:
            payload = inputFile.read(gDigitalTransition.LENGTH)
            data = gDigitalTransition(payload)
        elif header == ptcl.ANALOG_READOUT_HEADER:
            payload = inputFile.read(gAnalogReadout.LENGTH)
            data = gAnalogReadout(payload)
        else:
            abort('Unknown header %s' % header)
        # ...and print it
        print data,
        if raw_input() == 'q':
            break
    logger.info('Bye bye...')



if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    (opts, args) = parser.parse_args()
    for filePath in args:
        playback(filePath)
