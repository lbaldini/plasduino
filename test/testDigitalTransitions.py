#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import struct

import plasduino.arduino.protocol_h as ptcl

from plasduino.__logging__ import logger, abort

print ptcl.DIGITAL_TRANSITION_HEADER


def playbackOld(filePath):
    """
    """
    logger.info('Opening input file %s...' % filePath)
    if not filePath.endswith('.dat'):
        abort('Skipping file (please provide a .dat file).')
    inputFile = open(filePath, 'rb')
    outputFilePath = filePath.replace('.dat', '.txt')
    logger.info('Opening output file %s...' % outputFilePath)
    outputFile = open(outputFilePath, 'w')
    while(1):
        try: 
            header = struct.unpack('>B', inputFile.read(1))[0]
            pinNumber = header & 0x7
            edge = (header >> 3) & 0x1
            micros = struct.unpack('>L', inputFile.read(4))[0]
            outputFile.write('%d\t%d\t%d\n' % (pinNumber, edge, micros))
        except struct.error:
            break
    inputFile.close()
    outputFile.close()
    logger.info('Done, bye.')


def playbackNew(filePath):
    """
    """
    logger.info('Opening input file %s...' % filePath)
    if not filePath.endswith('.dat'):
        abort('Skipping file (please provide a .dat file).')
    inputFile = open(filePath, 'rb')
    outputFilePath = filePath.replace('.dat', '.txt')
    logger.info('Opening output file %s...' % outputFilePath)
    outputFile = open(outputFilePath, 'w')
    producerId = struct.unpack('>B', inputFile.read(1))[0]
    producerVersion = struct.unpack('>B', inputFile.read(1))[0]
    logger.info('Found producer id %d version %d.' %\
                (producerId, producerVersion))
    while(1):
        try: 
            header = struct.unpack('>B', inputFile.read(1))[0]
            info = struct.unpack('>B', inputFile.read(1))[0]
            pinNumber = info & 0x7F
            edge = (info >> 7) & 0x1
            micros = struct.unpack('>L', inputFile.read(4))[0]
            outputFile.write('%d\t%d\t%d\n' % (pinNumber, edge, micros))
        except struct.error:
            break
    inputFile.close()
    outputFile.close()
    logger.info('Done, bye.')
        




if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    (opts, args) = parser.parse_args()
    for filePath in args:
        playbackNew(filePath)
