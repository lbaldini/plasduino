#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
from plasduino.modulemanager.gModuleInfo import gModuleInfo

#-- PLASDUINO_MODULEINFO_BEGIN
__modulename__   = 'No op'
__executable__   = os.path.basename(__file__)
__dependencies__ = ['python', 'PyQt4', 'pyserial', 'avrdude', 'arduino']
__shield__       = None
__author__       = 'Luca Baldini'
__contact__      = 'luca.baldini@pi.infn.it'
__abstract__     = """No-operation module.

This is just to test the add/remove module functionality in the plasduino
launcher.
"""

__moduleinfo__ = gModuleInfo(__modulename__, __abstract__, __executable__,
                             __dependencies__, __shield__, author = __author__,
                             contact = __contact__)
#-- PLASDUINO_MODULEINFO_END


from PyQt4 import QtGui
from plasduino.gui.gMainWindow import gMainWindow


class noopWindow(gMainWindow):

    """ Main window for the noop test module.
    """

    WINDOW_TITLE = __modulename__
    MODULE_OPTIONS = ['gui.style', 'gui.skin']

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gMainWindow.__init__(self, **kwargs)
        self.addWidget(QtGui.QLabel('Howdy, partner?'))


def launch():
    """ Launch the main application.
    """
    mainWindow = noopWindow()
    mainWindow.show()
    return mainWindow


if __name__ == '__main__':
    TOP_LEVEL_MODULE_OPTIONS = noopWindow.MODULE_OPTIONS
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    mainWindow = launch()
    application.exec_() 
