# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


clean:
	rm -f *.pyc *.pyo *~ */*.pyc */*.pyo */*~ */*/*.pyc */*/*.pyo \
	       	*/*/*~ arduino/*/.sconsign.dblite src.log *_rpm.log *_deb.log
	rm -rf __pycache__ */__pycache__

cleanall:
	python setup.py clean
	rm -rf dist build MANIFEST
	make clean;
	cd analysis; make clean
	cd doc/webpage; make clean
	cd doc/datasheets; make cleanall
	cd doc/presentations/2013_congressino/; make cleanall
	cd doc/presentations/2013_sif/; make cleanall	
	cd doc/papers/2013_ncc/; make cleanall
