#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import ROOT
ROOT.gROOT.SetStyle('Plain')


from plasduino.daq.gDataTable import readDataTable
from plasduino.__logging__ import logger, abort



def plot(filePath, resolution = 4e-6):
    """ Open a text file and plot the corresponding data.
    """
    dataTable = readDataTable(filePath)
    if dataTable.Creator != 'timeresolutionEventBuffer':
        abort('The input file does not seem properly formatted.')
    values = [row[0] for row in dataTable]
    xmin  = min(values) - 1.5*resolution
    xmax  = max(values) + 1.5*resolution
    xbins = int((xmax - xmin)/resolution + 0.5)
    print xmin, xmax, xbins
    h = ROOT.TH1F('h', 'h', xbins, xmin, xmax)
    for val in values:
        h.Fill(val)
    return h



if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    (opts, args) = parser.parse_args()
    if len(args) == 0:
        abort('Please provide an input file.')
    filePath = args[0]
    h = plot(filePath)
    h.Draw()

