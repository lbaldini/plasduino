#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys

from PyQt4 import QtCore, QtGui
from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.daq.gArduinoRunControl import gArduinoRunControl
from plasduino.__logging__ import logger
from plasduino.arduino.plasduino_h import INTERRUPT_MODE_DISABLE,\
    INTERRUPT_MODE_ENABLE_CHANGE



class timeresolutionEventBuffer(gEventBuffer):
    
    """ Specialized data buffer object to study the time resolution of the
    data acquisition system.
    """

    PROCESSED_DATA_FIELDS = ['Time']
    PROCESSED_DATA_UNITS = ['s']
    PROCESSED_DATA_FORMAT = ['%.6f']

    def fillDataTable(self):
        """ Reimplemented from the base class.
        
        This simply take the differences between every two timestamps.
        This factors out the fact that the duty cycle of the calibration
        signal is not exactly 50%.
        """
        for i in xrange(2, len(self)):
            dt = self[i]['seconds'] - self[i-2]['seconds']
            self.DataTable.addRow([dt])



class timeresolutionRunControl(gArduinoRunControl):

    SKETCH_NAME = 'sktchDigitalTimer'
    INTERRUPT_MODE_0 = INTERRUPT_MODE_ENABLE_CHANGE
    INTERRUPT_MODE_1 = INTERRUPT_MODE_DISABLE

    """ Specialized RunControl class for measuring the time resolution.
    """

    def __init__(self, maxSeconds = None):
        """ Constructor.
        """
        eventBuffer = timeresolutionEventBuffer()
        gArduinoRunControl.__init__(self, eventBuffer, maxSeconds)

    def connectToArduino(self, timeout = None, autoUpload = True):
        """ Overloaded method.
        """
        errCode = gArduinoRunControl.connectToArduino(self, timeout, autoUpload)
        if not errCode:
            self.ArduinoManager.setupDigitalTimerSketch(self.INTERRUPT_MODE_0,
                                                        self.INTERRUPT_MODE_1)
        return errCode

    def poll(self):
        """ Implementation of the polling cycle on the PC side.
        """
        gArduinoRunControl.pollAlternateTransitions(self)



class timeresolutionWindow(gAcquisitionWindow):

    """ Small application window to test the time measurement with
    arduino interrupts.
    """

    WINDOW_TITLE = 'Time resolution'

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gAcquisitionWindow.__init__(self, **kwargs)
        self.RunControl = timeresolutionRunControl()
        self.connectRunControl()
        self.RunControl.connectToArduino(timeout = 0.2)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = timeresolutionWindow()
    w.show()
    application.exec_() 
