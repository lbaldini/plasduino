import ROOT
ROOT.gROOT.SetStyle('Plain')

WINDOWS = [
    (120, 150, 4.),
    (320, 350, 4.),
    (860, 890, 4.2),
    (1020, 1040, 4.2),
    (1100, 1140, 4.5),
    (1290, 1320, 4.7),
    (1420, 1445, 4.7),
    (1530, 1555, 4.7),
    (1620, 1650, 4.7),
    (1820, 1845, 4.7),
    (1890, 1920, 4.7),
    (1960, 1990, 4.7),
    (2040, 2070, 4.7)
]

g = ROOT.TGraph()
for line in open('000777_processed.txt'):
    if not line.startswith('#'):
        t, s = [float(item) for item in line.strip('\n').split()]
        g.SetPoint(g.GetN(), t, s)
g.Draw('alp')

gres = ROOT.TGraph()

f = ROOT.TF1('f', '[0] + [1]*sin([2]*x + [3])', 0, 1000)
f.SetNpx(1000)
f.SetParLimits(1, 12, 1000)
f.SetParameters(490, 20, 4., 1)

for smin, smax, p2 in WINDOWS:
    print smin, smax
    f.SetRange(smin, smax)
    f.SetParameter(2, p2)
    g.Fit('f', 'R')
    g.GetXaxis().SetRangeUser(smin, smax)
    g.Draw()
    ROOT.gPad.Update()
    #raw_input()
    omega = f.GetParameter(2)
    amplitude = f.GetParameter(1)
    gres.SetPoint(gres.GetN(), omega, amplitude)

gres.SetMarkerStyle(24)
gres.SetLineStyle(7)
gres.Draw('alp')
ROOT.gPad.Update()
