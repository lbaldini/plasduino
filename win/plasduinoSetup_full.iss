; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{63967066-59F7-4A3B-B8EA-4DF3884116F4}
AppName=Plasduino Videoclick
AppVersion=1.0
;AppVerName=Plasduino Videoclick 1.0
AppPublisher=Plasduino Group
AppPublisherURL=http://pythonhosted.org/plasduino/
AppSupportURL=http://pythonhosted.org/plasduino/
AppUpdatesURL=http://pythonhosted.org/plasduino/
DefaultDirName={pf}\PlasduinoVideoclick
DefaultGroupName=Plasduino Videoclick
OutputBaseFilename=setupPlasduinoVideoclick
SetupIconFile=C:\Users\IEUser\Downloads\PlasduinoVideoclick\plasduino\artwork\skins\cold\plasduino_icon.ico
Compression=lzma
SolidCompression=yes
ChangesEnvironment = yes
;AlwaysRestart = yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\Users\IEUser\Downloads\PlasduinoVideoclick\plasduino_videoclic.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\IEUser\Downloads\PlasduinoVideoclick\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\Plasduino Videoclick"; Filename: "{app}\plasduino_videoclic.bat"; IconFilename: "{app}\plasduino\artwork\skins\cold\plasduino_icon.ico"
Name: "{commondesktop}\Plasduino Videoclick"; Filename: "{app}\plasduino_videoclic.bat"; Tasks: desktopicon; IconFilename: "{app}\plasduino\artwork\skins\cold\plasduino_icon.ico"
Name: "{group}\Plasduino Dice"; Filename: "{app}\plasduino_dice.bat"; IconFilename: "{app}\plasduino\artwork\skins\cold\plasduino_icon.ico"

[Run]
Filename: "{app}\plasduino_videoclic.bat"; Description: "{cm:LaunchProgram,Plasduino Videoclick}"; Flags: shellexec skipifsilent

[Registry]
;Root: "HKCU"; Subkey: "Environment"; ValueType: string; ValueName: "PLASDUINODIR"; ValueData: "{app}"
Root: "HKLM"; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType: string; ValueName: "PLASDUINODIR"; ValueData: "{app}"
