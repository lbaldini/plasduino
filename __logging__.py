#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import logging
import os
import glob
import sys
import time


""" See http://docs.python.org/library/logging.html for anything you might
want to know about the logging module.

Particularly the table with the formatting strings.
"""

START_CONSOLE_MARKER = '>>>'


def abort(msg = None):
    """ Abort the execution of the program.
    """
    if msg is None:
        msg = '%s [FATAL ERROR] Abort.' % START_CONSOLE_MARKER
    else:
        msg = '%s [FATAL ERROR] %s. Abort.' % (START_CONSOLE_MARKER, msg)
    sys.exit(msg)



class gConsoleFormatter(logging.Formatter):

    """ Console formatter class.
    """

    def format(self, record):
        """ Overload format method.
        """
        if record.levelno <= logging.INFO:
            return '%s %s' % (START_CONSOLE_MARKER, record.msg)
        else:
            return '%s [%s] %s' % (START_CONSOLE_MARKER, record.levelname,
                                   record.msg)
            #return '%s [%s from %s, line %d] %s' %\
            #    (START_CONSOLE_MARKER, record.levelname, record.module,
            #     record.lineno, record.msg)



class gConsoleHandler(logging.StreamHandler):

    """ Console handler class.
    """

    def __init__(self):
        """ Constructor.
        """        
        logging.StreamHandler.__init__(self)
        self.setLevel(logging.DEBUG)
        self.setFormatter(gConsoleFormatter())



class gFileFormatter(logging.Formatter):
    
    """ File formatter class.
    """

    def format(self, record):
        """ Overloaded format method.
        """
        timestamp = time.strftime('%d %b %Y %H:%M:%S',
                                  time.localtime(record.created))
        ms = 1000*(record.created - int(record.created))
        timestamp = '%s.%d' % (timestamp, ms)
        return '[from %s, function %s, line %d @ %s]\n[%s] %s\n' %\
            (record.module, record.funcName, record.lineno, timestamp,
             record.levelname, record.msg)



class gFileHandler(logging.FileHandler):
    
    """
    """

    def __init__(self, filePath, mode = 'a', encoding = None, delay = False):
        """ Constructor.
        """
        if os.path.exists(filePath):
            filePath = '%s.0' % filePath
        logger.info('Opening output log file %s...' % filePath)
        logging.FileHandler.__init__(self, filePath, mode, encoding, delay)
        self.setLevel(logging.DEBUG)
        self.setFormatter(gFileFormatter())
        logger.addHandler(self)

    def close(self):
        """
        """
        if self in logger.handlers:
            logger.removeHandler(self)
            logging.FileHandler.close(self)
            logger.info('Output log file %s closed.' % self.baseFilename)



logger = logging.getLogger('plasduino')
logger.setLevel(logging.DEBUG)

# Terminal setting.
consoleHandler = gConsoleHandler()
logger.addHandler(consoleHandler)

""" This took me a long time to figure out, but due to some interaction with
QThread at some point I was getting all the log messages print twice.

http://stackoverflow.com/questions/4566327/python-logger-logging-things-twice-to-console
"""
logger.propagate = 0



if __name__ == '__main__':
    logger.debug('This is a debug message.')
    logger.info('This is an info message.')
    logger.warn('This is a warning message.')
    logger.error('This is an error message.')
    logger.critical('This is a critical message.')
