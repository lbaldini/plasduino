#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.daq.gDataTable import readDataTable
from plasduino.analysis.gRootTreeWriter import gRootTreeWriter
from plasduino.__logging__ import logger, abort


def text2root(inputFilePath, outputFilePath = None, treeName = 'Tuple'):
    """ Convert a text file to ROOT.
    """
    if not inputFilePath.endswith('.txt'):
        abort('Please provide an input txt file.')
    if outputFilePath is None:
        outputFilePath = inputFilePath.replace('.txt', '.root')
    table = readDataTable(inputFilePath)
    if table is None:
        abort('Could not read data table.')
    writer = gRootTreeWriter(outputFilePath, treeName)
    branches = []
    for item in table.Header:
        branches.append(item.replace(' ', '_'))
    for branchName in branches:
        writer.addBranch(branchName, 'F')
    for row in table:
        for (i, branchName) in enumerate(branches):
            writer.setValue(branchName, row[i])
        writer.fill()
    writer.close()
 


if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    (opts, args) = parser.parse_args()
    if not len(args):
        parser.error('Please provide at least an input file.')
    for filePath in args:
        text2root(filePath)
