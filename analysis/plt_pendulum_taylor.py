#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gCanvas import gCanvas
from plasduino.analysis.gF1 import gF1
from plasduino.__logging__ import logger

import gPendulumLib


fList = []
c = gCanvas('pendulum_taylor', Logy = True)
for i in range(7):
    order = 2*i
    f = gF1('f%d' % order, '%.15f*(x/%f)**%d' %\
    (gPendulumLib.PERIOD_TAYLOR_COEFFICIENTS[i],
     gPendulumLib.RAD_TO_DEG, order), 0, 90)
    if order == 0:
        f.Draw()
        f.GetYaxis().SetRangeUser(1e-6, 10)
        f.GetXaxis().SetTitle('#theta_{0} [#circ]')
        f.GetYaxis().SetTitle('Taylor expansion term')
    else:
        f.Draw('same')
    x = 25 + i/6.*50.
    y = 0.5*f.Eval(x)
    c.annotate(x, y, 'n = %d' % order, ndc = False, size = SMALL_TEXT_SIZE)
    fList.append(f)   
c.Update()


for theta in [40, 50, 60, 70]:
    print('Relative weight at theta = %.2f...' % theta)
    for i in range(7):
        order = 2*i
        print('Order %d: %e' % (order, fList[i].Eval(theta)))
