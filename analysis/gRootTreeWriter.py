#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import ROOT
import array

from plasduino.__logging__ import logger


class gRootTreeWriter:

    """ Small utility class to write a flat ntuple into an output ROOT file.
    """

    def __init__(self, filePath, treeName):
        """ Constructor.
        """
        logger.info('Opening output file %s...' % filePath)
        self.__OutputFile = ROOT.TFile(filePath, 'RECREATE')
        logger.info('Creating tree "%s"...' % treeName)
        self.__OutputTree = ROOT.TTree(treeName, treeName)
        self.__ArrayDict = {}

    def addBranch(self, branchName, branchType = 'D'):
        """ Add a branch to the output tree.
        """
        branchTitle = '%s/%s' % (branchName, branchType)
        logger.info('Adding branch %s...' % branchTitle)
        a = array.array(branchType.lower(), [0])
        self.__ArrayDict[branchName] = a
        self.__OutputTree.Branch(branchName, a, branchTitle)

    def setValue(self, branchName, value):
        """ Set the value of a specific array.
        """
        self.__ArrayDict[branchName][0] = value

    def fill(self):
        """ Fill the output tree.
        """
        self.__OutputTree.Fill()

    def close(self):
        """ Write the tree and close the output file.
        """
        logger.info('Writing tree and saving output file...')
        self.__OutputTree.Write()
        self.__OutputFile.Close()
        logger.info('Done.')
        



def main():
    w = gRootTreeWriter('test.root', 'Tuple')
    w.addBranch('RunId', 'I')
    w.addBranch('EventId', 'I')
    for i in range(100):
        w.setValue('RunId', 0)
        w.setValue('EventId', i)
        w.fill()
    w.close()


if __name__ == '__main__':
    main()
