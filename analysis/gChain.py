#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gGraphErrors import gGraphErrors
from plasduino.__logging__ import logger


class gChain(ROOT.TChain):

    """ Small wrapper around the ROOT.TChain class.
    """

    def __init__(self, fileList, treeName = 'Tuple'):
        """ Constructor.
        """
        logger.info('Initializing ROOT chain %s...' % treeName)
        ROOT.TChain.__init__(self, treeName)
        if isinstance(fileList, str):
            fileList = [fileList]
        for filePath in fileList:
            logger.info('Adding %s to the chain...' % filePath)
            self.Add(filePath)
        logger.info('Done, %d entrie(s) in the chain.' % (self.GetEntries()))

    def getGraph(self, yexpr, xexpr = 'Time', xmin = None, xmax = None,
                 xtitle = None, ytitle = None, yerr = 0., cut = None, **kwargs):
        """ Create a graph from the chart.
        """
        if cut is None:
            cut = '1'
            if xmin is not None:
                cut += ' && %s > %.f' % (xexpr, xmin)
            if xmax is not None:
                cut += ' && %s < %.f' % (xexpr, xmax)
        self.Draw('%s:%s' % (yexpr, xexpr), cut)
        graph = ROOT.gPad.GetPrimitive('Graph')
        g = gGraphErrors('graph', **kwargs)
        g.Fill(graph)
        g.GetXaxis().SetTitle(xtitle or xexpr)
        g.GetYaxis().SetTitle(ytitle or yexpr)
        if yerr > 0:
            for i in range(g.GetN()):
                g.SetPointError(i, 0, yerr)
        return g



if __name__ == '__main__':
    from plasduino.analysis.gCanvas import gCanvas
    c = gCanvas('ctest')
    t = gChain('data/tempmonitor_bar.root')
    g = t.getGraph('Temp_A1', 'Time_1', 200, 300)
    g.Draw('ap')
    c.Update()
