#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gChain import gChain
from plasduino.analysis.gCanvas import gCanvas
from plasduino.analysis.gResidualCanvas import gResidualCanvas
from plasduino.analysis.gGraphErrors import gGraphErrors
from plasduino.analysis.gF1 import gF1
from plasduino.__logging__ import logger


T_MIN = 55
T_MAX = 70
#Y_ERR = 1./math.sqrt(12)
Y_ERR = 0
CHAIN = gChain('data/pendulumdrive_dump.root')

c = gResidualCanvas('pendulum_dump')
g = CHAIN.getGraph('Pos', 'Time', T_MIN, T_MAX, 'Time [s]',
                   'Position [ADC cnts]', yerr = Y_ERR)
f = gF1('f', '[0] + [1]*exp(-2*[2]*x)*sin([3]*x + [4])', T_MIN, T_MAX)
f.SetParameters(g.GetMean(2), g.GetRMS(2), 0.01, 4., 1.)
g.Fit(f, 'R')
gRes = g.GetResiduals(f)
gRes.SetYRange(-1.9, 1.9, 507)
c.drawPlots(g, gRes)
c.Update()
logger.info('Fit chisquare = %.3f/%d' % (f.GetChisquare(), f.GetNDF()))
logger.info('RMS of residuals: %.2f (expected %.2f)' %\
            (gRes.GetRMS(2), 1./math.sqrt(12)))
c.save()
