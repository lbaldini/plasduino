#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



import math

from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gCanvas import gCanvas
from plasduino.analysis.gGraphErrors import gGraphErrors
from plasduino.analysis.gF1 import gF1
from plasduino.__logging__ import logger


RAD_TO_DEG = 180./math.pi
GRAV_ACC = 9.80665


""" The Taylor coefficients for the series expansion of the pendulum period
up to the 12th order.

From http://en.wikipedia.org/wiki/Pendulum_%28mathematics%29
"""
PERIOD_TAYLOR_COEFFICIENTS = [1.,
                              1./16.,
                              11./3072.,
                              173./737280.,
                              22931./1321205760.,
                              1319183./951268147200.,
                              233526463./2009078326886400.
                          ]


def getPeriodFitFunction(order = 2, fixParameters = False, xmin = 0, xmax = 90):
    """ Return a gTF1 object representing the Taylor series of the pendulum
    period up to a given order.
    """
    name = 'f%d' % (order)
    expr = '[0]*(1'
    numPars = 1
    for i in range(2, order + 1, 2):
        expr += ' + [%d]*(x/%e)**%d' % (numPars, RAD_TO_DEG, i)
        numPars += 1
    expr += ')'
    f = gF1(name, expr, xmin, xmax)
    f.SetParameter(0, 1.)
    for i in range(1, numPars):
        f.SetParameter(i, PERIOD_TAYLOR_COEFFICIENTS[i])
        if fixParameters:
            f.FixParameter(i, PERIOD_TAYLOR_COEFFICIENTS[i])
    return f




class gTetraPendulum:
    
    """ Small utility class representing a pendulum, in the flavor used
    in our lab :-)
    """
    
    def __init__(self, length, flagDist, flagWidth):
        """ Constructor.
        """
        self.Length = length
        self.FlagDist = flagDist
        self.FlagWidth = flagWidth

    def getAmplitudeConst(self):
        """
        """
        return self.FlagWidth*math.sqrt(self.Length/GRAV_ACC)/(2*self.FlagDist)

    def setupChainAliases(self, chain, const = None):
        """
        """
        logger.info('Setting up chain aliases for the pendulum...')
        if const is None:
            const = self.getAmplitudeConst()
        expr = '%e*2*asin(%e/Transit_time)' % (RAD_TO_DEG, const)
        chain.SetAlias('Theta', expr)
        logger.info('Theta -> %s' % expr)
        expr = '%e/Transit_time' % self.FlagWidth
        chain.SetAlias('Speed', expr)
        logger.info('Speed -> %s' % expr)



if __name__ == '__main__':
    c = gCanvas('c')
    f = getPeriodFitFunction()
    f.Draw()
    c.Update()
