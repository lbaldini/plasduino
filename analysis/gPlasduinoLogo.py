#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gLatex import gLatex


class gPlasduinoLogo:

    """ plasduino logo to be put on a ROOT canvas.

    We haven't made a huge effort, yet, to make the logo scalable or
    anything like that. It is made of two pieces and we rely on the
    TLatex alignmnent for the relative positioning.
    """

    COLOR = ROOT.kGray + 1

    def __init__(self, vscale = 1.0):
        """
        """
        x = 0.65
        y = 1 - 0.01*vscale
        text = 'plasduino'
        self.BigPrint = gLatex(x, y, text, NDC = True, TextAlign = 13,
                               TextSize = 1.0*TEXT_SIZE, TextColor = self.COLOR)
        x = 0.66
        y = 1 - 0.05*vscale
        text = 'Open source data acquisition framework'
        self.SmallPrint = gLatex(x, y, text, NDC = True, TextAlign = 13,
                                 TextSize = 0.5*TEXT_SIZE,
                                 TextColor = self.COLOR)

    def Draw(self, opts = ''):
        """ Draw the logo.
        """
        self.BigPrint.Draw(opts)
        self.SmallPrint.Draw(opts)


def test():
    logo = gPlasduinoLogo()
    logo.Draw()
    


if __name__ == '__main__':
    test()
