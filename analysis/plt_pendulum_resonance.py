#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gChain import gChain
from plasduino.analysis.gCanvas import gCanvas
from plasduino.analysis.gF1 import gF1
from plasduino.analysis.gResidualCanvas import gResidualCanvas
from plasduino.analysis.gGraphErrors import gGraphErrors
from plasduino.__logging__ import logger


TIME_INTERVALS = [
    (75, 5320, 5340),
    (80, 5260, 5280),
    (85, 5180, 5200),
    (90, 5100, 5120),
    (93, 5010, 5030),
    (95, 210, 230),
    (97, 410, 430),
    (99, 660, 680), 
    (100, 720, 750),
    (101, 830, 850),
    (102, 870, 890),
    (103, 970, 990),
    (104, 1100, 1120),
    (105, 1180, 1200),
    (106, 1230, 1250),
    (112, 2450, 2470),
    (113, 2740, 2760),
    (114, 2900, 2920),
    (115, 3115, 3125),
    (117, 3530, 3550),
    (120, 3920, 3940),
    (122, 4010, 4030),
    (128, 4380, 4400),
    (135, 4480, 4500),
    (140, 4560, 4580),
    (150, 4620, 4640),
    (175, 4678, 4685),
    (200, 4730, 4750)
]

TIME_INTERVALS_2 = [
    (1275, 1295),
    (1298, 1316),
    (1318, 1334),
    (1336, 1352),
    (1520, 1560),
    (1645, 1660)
    ]


CHAIN = gChain('data/pendulumdrive_resonance.root')
FIT_FUNC = gF1('fit_func', '[0] + [1]*sin([2]*x + [3])')
FIT_FUNC_2 = gF1('fit_func2', '[0] + [1]*sin([2]*x + [3])*sin([4]*x + [5])')


gall = CHAIN.getGraph('Pos', 'Time')


def findOmega(g, zero, debug = False, minDelta = 0.2):
    """ Guess the omega of a TGraph repesenting a sinusoid.

    We use this to initialize the fit parameters in the following.
    The algorithm is fairly stupid and slow, in that we look at how
    much successive zero crossing are spaced.
    """
    x0 = ROOT.Double()
    y0 = ROOT.Double()
    x1 = ROOT.Double()
    y1 = ROOT.Double()
    crossings = []
    for i in range(1, g.GetN()):
        g.GetPoint(i - 1, x0, y0)
        g.GetPoint(i, x1, y1)
        if (y0 < zero and y1 > zero) or (y0 > zero and y1 < zero):
            x = 0.5*(x0 + x1)
            crossings.append(x)
    deltas = []
    for i in range(1, len(crossings)):
        delta = crossings[i] - crossings[i-1]
        if delta > minDelta:
            deltas.append(delta)
    if debug:
        logger.debug('Deltas: %s' % deltas)
    period = 2*sum(deltas)/len(deltas)
    omega = 2*math.pi/period
    return omega

def fit(tmin, tmax, interactive = False):
    """ Fit a portion of strip chart to a sinusoid.
    """
    logger.info('Fitting strip chart in time interval %.2f--%.2f...' %\
                    (tmin, tmax))
    g = CHAIN.getGraph('Pos', 'Time', tmin, tmax)
    FIT_FUNC.SetRange(tmin, tmax)
    p0 = g.GetMean(2)
    p1 = 1.4*g.GetRMS(2)
    p2 = findOmega(g, p0)
    p3 = 1.
    pars = ['%.2f' % item for item in [p0, p1, p2, p3]]
    logger.info('Initial parameter values: %s.' % pars)
    FIT_FUNC.SetParameters(p0, p1, p2, p3)
    FIT_FUNC.SetParLimits(0, p0*0.8, p0*1.2)
    FIT_FUNC.SetParLimits(1, p1*0.8, p1*1.2)
    g.Fit(FIT_FUNC, 'R')
    if interactive:
        g.Draw('ap')
        ROOT.gPad.Update()
        raw_input('Press enter to continue')

def fit2(tmin, tmax, interactive = False):
    """
    """
    logger.info('Fitting strip chart in time interval %.2f--%.2f...' %\
                    (tmin, tmax))
    g = CHAIN.getGraph('Pos', 'Time', tmin, tmax)
    FIT_FUNC_2.SetRange(tmin, tmax)
    p0 = g.GetMean(2)
    p1 = 2.*g.GetRMS(2)
    p2 = findOmega(g, p0)
    p3 = 1.
    p4 = 0.175
    p5 = 1.
    pars = ['%.2f' % item for item in [p0, p1, p2, p3, p4, p5]]
    logger.info('Initial parameter values: %s.' % pars)
    FIT_FUNC_2.SetParameters(p0, p1, p2, p3, p4, p5)
    FIT_FUNC_2.SetParLimits(0, p0*0.8, p0*1.2)
    FIT_FUNC_2.SetParLimits(1, p1*0.8, p1*1.2)
    g.Fit(FIT_FUNC_2, 'R')
    if interactive:
        g.Draw('ap')
        ROOT.gPad.Update()
        raw_input('Press enter to continue')

def resonanceCurve(interactive = False):
    """
    """
    g = gGraphErrors('gresonance')
    for pwm, tmin, tmax in TIME_INTERVALS:
        fit(tmin, tmax, interactive)
        omega = FIT_FUNC.GetParameter(2)
        amplitude = FIT_FUNC.GetParameter(1)
        g.SetNextPoint(omega, amplitude)
    for tmin, tmax in TIME_INTERVALS_2:
        fit2(tmin, tmax, interactive)
        omega = FIT_FUNC_2.GetParameter(2)
        amplitude = FIT_FUNC_2.GetParameter(1)
        g.SetNextPoint(omega, amplitude)
    return g



if __name__ == '__main__':
    g1 = resonanceCurve(False)
    c1 = gCanvas('pendulumdrive_resonance')
    g1.Draw()
    g1.GetXaxis().SetTitle('#omega [s^{-1}]')
    g1.GetYaxis().SetTitle('Amplitude [ADC cnts]')
    f = gF1('fres', '[0]/sqrt(([1]**2 - x**2)**2 + 4*[2]**2*x**2)')
    f.SetParameters(100, 4.2, 0.1)
    g1.Fit(f)
    c1.Update()
    c1.save()
      
    c2 = gResidualCanvas('pendulumdrive_resonance_res')
    g2 = gGraphErrors('gresonance2')
    g2.Fill(g1)
    g2.Fit(f)
    gres = g1.GetResiduals(f)
    gres.SetYRange(-12, 12, 509)
    c2.drawPlots(g2, gres)
    c2.Update()

