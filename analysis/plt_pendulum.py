#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gChain import gChain
from plasduino.analysis.gCanvas import gCanvas
from plasduino.analysis.gResidualCanvas import gResidualCanvas
from plasduino.analysis.gGraphErrors import gGraphErrors
from plasduino.analysis.gF1 import gF1
from plasduino.__logging__ import logger

import gPendulumLib


T_MIN = 70
T_MAX = 600
FIX_PARAMS = True

CHAIN = gChain('data/pendulum.root')
PENDULUM = gPendulumLib.gTetraPendulum(1.135, 1.190, 0.020728)
PENDULUM.setupChainAliases(CHAIN)

cs = gCanvas('cs')
gs = CHAIN.getGraph('Speed', 'Time', xtitle = 'Time [s]',
                    ytitle = 'Speed [m s^{-1}]')
gs.Draw()
cs.Update()

cp = gCanvas('cp')
gp = CHAIN.getGraph('Period', 'Time', xtitle = 'Time [s]',
                    ytitle = 'Period [s]')
gp.Draw()
cp.Update()

ct = gCanvas('ct')
gt = CHAIN.getGraph('Theta', 'Time', xtitle = 'Time [s]',
                    ytitle = '#theta [#circ]')
gt.Draw()
ct.Update()

cq = gCanvas('cq')
gq = gGraphErrors('gq')
CHAIN.GetEntry(0)
prev = (PENDULUM.FlagWidth/CHAIN.Transit_time)**2
for i in range(2, CHAIN.GetEntries(), 2):
    CHAIN.GetEntry(i)
    cur = (PENDULUM.FlagWidth/CHAIN.Transit_time)**2
    q = 2*math.pi*cur/(prev - cur)
    gq.SetNextPoint(CHAIN.Time, q)
    prev = cur
gq.Draw()
cq.Update()

c = gCanvas('c')
g = CHAIN.getGraph('Period', 'Theta', xtitle = '#theta [#circ]',
                   ytitle = 'Period [s]', yerr = 1e-5,
                   cut = 'Time > %f && Time < %f' % (T_MIN, T_MAX))
g.Draw()
c.Update()

c2 = gResidualCanvas('pendulum_order2')
g2 = g.Clone('pendulum2')
f2 = gPendulumLib.getPeriodFitFunction(2, FIX_PARAMS)
g2.Fit(f2)
gres2 = g2.GetResiduals(f2, 'gres2')
gres2.Scale(1000.)
gres2.SetYRange(-5.1, 5.1, 507)
gres2.GetYaxis().SetTitle('Residuals [ms]      ')
c2.drawPlots(g2, gres2)
c2.cd(1)
text = 'Second order'
c2.annotate(0.225, 0.675, text)
logger.info('Sigma of the residuals: %.1f ms' % gres2.GetRMS(2))
c2.Update()
c2.save()

c4 = gResidualCanvas('pendulum_order4')
g4 = g.Clone('pendulum4')
g4.Fill(g)
f4 = gPendulumLib.getPeriodFitFunction(4, FIX_PARAMS)
g4.Fit(f4)
gres4 = g4.GetResiduals(f4, 'gres4')
gres4.Scale(1000000.)
gres4.SetYRange(-499, 499, 507)
gres4.GetYaxis().SetTitle('Residuals [#mus]      ')
c4.drawPlots(g4, gres4)
c4.cd(1)
text = 'Fourth order'
c4.annotate(0.225, 0.675, text)
logger.info('Sigma of the residuals: %.1f us' % gres4.GetRMS(2))
c4.Update()
c4.save()


c6 = gResidualCanvas('pendulum_order6')
g6 = g.Clone('pendulum6')
g6.Fill(g)
f6 = gPendulumLib.getPeriodFitFunction(6, FIX_PARAMS)
g6.Fit(f6)
gres6 = g6.GetResiduals(f6, 'gres4')
gres6.Scale(1000000.)
gres6.SetYRange(-499, 499, 507)
gres6.GetYaxis().SetTitle('Residuals [#mus]      ')
c6.drawPlots(g6, gres6)
c6.cd(1)
text = 'Sixth order'
c6.annotate(0.225, 0.675, text)
logger.info('Sigma of the residuals: %.1f us' % gres6.GetRMS(2))
c6.Update()
c6.save()
