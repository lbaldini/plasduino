#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gCanvas import gCanvas
from plasduino.analysis.gF1 import gF1
from plasduino.analysis.gGraphErrors import gGraphErrors
from plasduino.__logging__ import logger



# External power supply providing 5 V.
V0 = 5

c1 = gCanvas('pendulum_resistance')
f1 = gF1('line', '[0]*x')
g1 = gGraphErrors('gresistance')
for line in open('data/resistance_distilled_water.txt'):
    if not line.startswith('#'):
        m, I = [float(item) for item in line.strip('\n').split()]
        R = V0/I
        g1.SetNextPoint(1000./m, R)
g1.GetXaxis().SetTitle('1/m [kg^{-1}]')
g1.GetYaxis().SetTitle('Resistance [M#Omega]')
g1.Fit(f1)
g1.Draw('ap')
c1.Update()


c2 = gCanvas('pendulum_resistance_trend')
f2 = gF1('f2', '[0] + [1]*exp(-[2]*x)')
f2.SetParameters(0.3, 0.01, 2e-8)
g2 = gGraphErrors('gresistance_trend')
for line in open('data/resistance_distilled_water_trend.txt'):
    if not line.startswith('#'):
        t, I = [float(item) for item in line.strip('\n').split()]
        t /= 1000.
        R = V0/I
        g2.SetNextPoint(t, R, 0, 0.1/I*R)
g2.GetXaxis().SetTitle('Time [ks]')
g2.GetYaxis().SetTitle('Resistance [M#Omega]')
g2.Fit(f2)
g2.Draw('ap')
c2.Update()
