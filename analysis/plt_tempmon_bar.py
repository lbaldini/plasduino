#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gChain import gChain
from plasduino.analysis.gCanvas import gCanvas
from plasduino.analysis.gResidualCanvas import gResidualCanvas
from plasduino.analysis.gGraphErrors import gGraphErrors
from plasduino.analysis.gF1 import gF1
from plasduino.sensors.__NXFT15XH103FA2B__ import NXFT15XH103FA2B
from plasduino.__logging__ import logger


V0 = 5.16  # Voltage from the power supply [V]
I0 = 0.40  # Current from the power supply [A]
D0 = 2.500 # Bar diameter [cm]

A0 = math.pi*(0.5*D0)**2 # Cross section of the bar [cm^2]

DX = 0.2/math.sqrt(12.)

TIME_INTERVALS = [
    (100, 112),
    (125, 145),
    (160, 180),
    (195, 225),
    (235, 255),
    (270, 305),
    (315, 340),
    (380, 400),
    (410, 455),
    (470, 505),
    (520, 565),
    (580, 620),
    (635, 685),
    (700, 740),
    (760, 805)
]

CHAIN = gChain('data/tempmonitor_bar.root')

c1 = gCanvas('c1', 'Whole strip chart')
g1 = CHAIN.getGraph('Temp_A1', 'Time_1')
g1.Draw('ap')
c1.Update()


c2 = gCanvas('tempmon_resolution', 'Measurement error', Logy = True)
thermistor = NXFT15XH103FA2B()
R0 = 10000
DELTA_R_1 = R0*0.01
DELTA_R_2 = R0*0.001
gErrADC = gGraphErrors('gerr_ADC')
gErrRes1 = gGraphErrors('gerr_res1', LineStyle = 7)
gErrRes2 = gGraphErrors('gerr_res2', LineStyle = 7)
gErrTherm = gGraphErrors('gerr_therm', LineStyle = 7)
for adc in range(1, 1022):
    # Calculate the temperature.
    T = thermistor.adc2celsius(adc, R0)
    # Error due to the ADC.
    dT = 0.5*abs(thermistor.adc2celsius(adc + 1, R0) - \
                     thermistor.adc2celsius(adc - 1, R0))
    dT /= math.sqrt(12.)
    gErrADC.SetNextPoint(T, dT)
    # Error due to the resistor.
    dT = 0.5*abs(thermistor.adc2celsius(adc, R0 + DELTA_R_1) - \
                     thermistor.adc2celsius(adc, R0 - DELTA_R_1))
    gErrRes1.SetNextPoint(T, dT)
    dT = 0.5*abs(thermistor.adc2celsius(adc, R0 + DELTA_R_2) - \
                     thermistor.adc2celsius(adc, R0 - DELTA_R_2))
    gErrRes2.SetNextPoint(T, dT)
    # Error due to the thermistor.
    dT = thermistor.getMeasurementError(adc, R0)
    gErrTherm.SetNextPoint(T, dT)
gErrADC.GetXaxis().SetRangeUser(-40, 125)
gErrADC.GetXaxis().SetTitle('Temperature [#circC]')
gErrADC.GetYaxis().SetRangeUser(0.01, 5)
gErrADC.GetYaxis().SetTitle('#Delta T [#circC]')
gErrADC.Draw('al')
gErrRes1.Draw('l,same')
gErrRes2.Draw('l,same')
gErrTherm.Draw('l, same')
x = 80.
y = 1.1*gErrTherm.Eval(x)
c2.annotate(x, y, 'NXFT15XH103FA2B tolerance', ndc = False,
            size = SMALL_TEXT_SIZE, align = 31)
x = 20.
y = 1.2*gErrADC.Eval(x)
c2.annotate(x, y, 'ADC resolution', ndc = False, size = SMALL_TEXT_SIZE,
            align = 21)
x = 100.
y = 1.1*gErrRes1.Eval(x)
c2.annotate(x, y, 'R tolerance (1%)', ndc = False, size = SMALL_TEXT_SIZE,
            align = 31)
x = 120.
y = 1.1*gErrRes2.Eval(x)
c2.annotate(x, y, 'R tolerance (0.1%)', ndc = False, size = SMALL_TEXT_SIZE,
            align = 31)
c2.Update()
c2.save()


gTemp = gGraphErrors('gtemp')
const = gF1('const', '[0]')
for i, (tmin, tmax) in enumerate(TIME_INTERVALS):
    logger.info('Fitting in %f--%f...' % (tmin, tmax))
    x = 2.5*(i + 1)
    const.SetRange(tmin, tmax)
    g1.Fit(const, 'RQ')
    T = const.GetParameter(0)
    dT = gErrADC.Eval(T)
    gTemp.SetNextPoint(x, T, DX, dT)
c3 = gResidualCanvas('tempmonitor_bar', 'Temperature profile')
gTemp.GetXaxis().SetTitle('Position along the bar [cm]')
gTemp.GetYaxis().SetTitle('Temperature [#circC]')
gTemp.GetYaxis().SetRangeUser(22.1, 30.7)
f = gF1('f', 'pol1')
f.SetParameters(30, -0.2)
gTemp.Fit(f, 'M')
gRes = gTemp.GetResiduals(f)
x = ROOT.Double()
y = ROOT.Double()
for i in range(gRes.GetN()):
    gRes.GetPoint(i, x, y)
    dT = gErrADC.Eval(x)
    gRes.SetPointError(i, DX, dT)
c3.drawPlots(gTemp, gRes)
gRes.SetYRange(-0.25, 0.25, 509)
zero = gF1('zero', '0', 0, 100)
c3.cd(2)
zero.Draw('same')
c3.cd(1)
logger.info('dT/dx = %.4f +- %.4f deg cm^-1' %\
            (f.GetParameter(1), f.GetParError(1)))
logger.info('chi2/ndof = %.1f/%d' % (f.GetChisquare(), f.GetNDF()))
cond = -(V0*I0)/(f.GetParameter(1)*A0)
logger.info('Estimated conductivity: %.2f W cm^-1 deg C^-1' % cond)
c3.Update()
c3.save()
