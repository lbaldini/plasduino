#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gCanvas import gCanvas
from plasduino.analysis.gPlasduinoLogo import gPlasduinoLogo
from plasduino.__plasduino__ import PLASDUINO_WEB_PAGE


class gResidualCanvas(gCanvas):

    """ Multi-pad canvas for residual plots.
    """

    DEFAULT_VERT_SPLIT = 0.4

    def __init__(self, name, title = None, timestamp = False, **kwargs):
        """ Constructor.
        """
        self.__VertSplit = kwargs.get('vsplit', self.DEFAULT_VERT_SPLIT)
        self.__VertScale = 1./(1 - self.__VertSplit)
        gCanvas.__init__(self, name, title or name, **kwargs)
        self.MainPad.Draw()
        self.ResPad.Draw()
        self.Logo = gPlasduinoLogo(self.__VertScale)
        self.cd(1)

    def setup(self, **kwargs):
        """ Overloaded method.
        """
        self.MainPad = ROOT.TPad('main_pad', 'Main pad', 0.0, self.__VertSplit,
                                 1.0, 1.0)
        self.ResPad = ROOT.TPad('res_pad', 'Res pad', 0.0, 0.0, 1.0,
                                self.__VertSplit)
        self.MainPad.SetBottomMargin(0.032)
        self.MainPad.SetTopMargin(CANVAS_TOP_MARGIN*self.__VertScale)
        self.ResPad.SetBottomMargin(CANVAS_BOTTOM_MARGIN/self.__VertSplit)
        self.ResPad.SetTopMargin(0.01)

    def Update(self):
        """ Overloaded update method.
        """
        self.cd(1)
        self.Logo.Draw()
        if self.WebPage:
            self.cd(2)
            self.annotate(0.05, 0.042*self.__VertScale, PLASDUINO_WEB_PAGE,
                          size = SMALLEST_TEXT_SIZE, align = 13,
                          color = gPlasduinoLogo.COLOR)
        ROOT.TCanvas.Update(self)

    def drawPlots(self, *plots):
        """ Draw the plots on the canvas.
        """
        for i, plot in enumerate(plots):
            self.cd(i + 1)
            plot.Draw()
            if i == 0:
                plot.GetXaxis().SetLabelOffset(2)
            else:
                plot.GetXaxis().SetTitleOffset(2.65)
                plot.GetYaxis().CenterTitle()

    def SetLogx(self, logx = True):
        """ Set the log scale on the x-axis for all pads.
        """
        self.MainPad.SetLogx(logx)
        self.ResPad.SetLogx(logx)

    def SetLogy(self, logy = True):
        """  Set the log scale on the y-axis for all pads.
        """
        self.MainPad.SetLogy(logy)
        self.ResPad.SetLogy(logy)

    def SetGridx(self, gridx = True):
        """ Set the grids on the x-axis for all pads.
        """
        self.MainPad.SetGridx(gridx)
        self.ResPad.SetGridx(gridx)

    def SetGridy(self, gridy = True):
        """ Set the grids on the y-axis for all pads.
        """
        self.MainPad.SetGridy(gridy)
        self.ResPad.SetGridy(gridy)

    def annotate(self, x, y, text, ndc = True, align = 11, size = TEXT_SIZE,
                 color = ROOT.kBlack, angle = 0):
        """ Draw some text on the canvas.
        """
        label = ROOT.TLatex(x, y, text)
        if ndc:
            label.SetNDC(True)
        label.SetTextAlign(align)
        label.SetTextSize(size)
        label.SetTextColor(color)
        label.SetTextAngle(angle)
        label.Draw()
        store(label)
        ROOT.TCanvas.Update(self)

    def cd(self, pad = None):
        """ cd into one of the two pads.
        """
        if pad is None:
            gCanvas.cd(self)
        elif pad == 1:
            self.MainPad.cd()
        elif pad == 2:
            self.ResPad.cd()   



if __name__ == '__main__':
    from plasduino.analysis.gH1F import gH1F
    c = gResidualCanvas('cres', Grids = True)
    hmain = gH1F('htest', 'htest', 100, 0, 1, XTitle = 'Something [a. u.]',
                 YTitle = 'Something else [a. u.]')
    hres = hmain.Clone()
    hnorm = hmain.Clone()
    hres.SetYTitle('Residuals')
    c.drawPlots(hmain, hres)
    c.Update()
    c1 = gCanvas('cnorm')
    hnorm.Draw()
    c1.Update()
