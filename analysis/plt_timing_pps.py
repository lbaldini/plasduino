#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import glob

from plasduino.analysis.__ROOT__ import *
from plasduino.analysis.gChain import gChain
from plasduino.analysis.gCanvas import gCanvas
from plasduino.analysis.gResidualCanvas import gResidualCanvas
from plasduino.analysis.gGraphErrors import gGraphErrors
from plasduino.analysis.gF1 import gF1
from plasduino.analysis.gH1F import gH1F
from plasduino.__logging__ import logger
from plasduino.daq.gDataTable import gDataTable
from plasduino.analysis.gRootTreeWriter import gRootTreeWriter



def text2root(inputFilePath, outputFilePath = None, treeName = 'Tuple'):
    """
    """
    from plasduino.sensors.__NXFT15XH103FA2B__ import NXFT15XH103FA2B
    if not inputFilePath.endswith('.txt'):
        abort('Please provide an input txt file.')
    if outputFilePath is None:
        outputFilePath = inputFilePath.replace('.txt', '.root')
    writer = gRootTreeWriter(outputFilePath, treeName)
    writer.addBranch('DeltaTime', 'F')
    writer.addBranch('Temperature', 'F')
    prev = None
    thermistor = NXFT15XH103FA2B()
    for line in open(inputFilePath):
        if not line.startswith('#'):
            line = line.strip('\n').strip('T')
            micros, adc = [int(value) for value in line.split('-')]
            temp = thermistor.adc2celsius(adc)
            if prev is not None:
                delta = micros - prev - 1000000
                writer.setValue('DeltaTime', delta)
                writer.setValue('Temperature', temp)
                writer.fill()
            prev = micros
    writer.close()


def convert():
    """ Convert all files to ROOT.
    """
    for filePath in glob.glob('data/timing_pps*.txt'):
        text2root(filePath)


def analyze(filePath, label = '', padding = 8):
    """ Analyze a ROOT file.
    """
    chain = gChain(filePath)
    htemp = gH1F('htemp_%s' % label, 'htemp', 100, 20, 50)
    chain.Project('htemp_%s' % label, 'Temperature')
    cut = 'abs(Temperature - %f) < 0.15' % htemp.GetMean()
    hdelta = gH1F('hdelta_%s' % label, 'hdelta', 500, -1000, 1000)
    chain.Project('hdelta_%s' % label, 'DeltaTime')
    m = hdelta.GetMean()
    m -= (m % 4)
    m += 2
    hdeltacut = gH1F('hdeltacut_%s' % label, 'hdeltacut',
                     2*padding, m - 4*padding, m + 4*padding)
    chain.Project('hdeltacut_%s' % label, 'DeltaTime', cut)
    hdeltacut.SetXTitle('PPS residuals [#mus]')
    hdeltacut.SetYTitle('Entries/bin')
    return htemp, hdelta, hdeltacut



#convert()


fileList = glob.glob('data/timing_pps*.root')
fileList.remove('data/timing_pps_rampdown.root')
fileList.sort()

c = gCanvas('time_scale_drift')
g = gGraphErrors('g')
f = gF1('f', 'pol1')
for i, filePath in enumerate(fileList):
    htemp, hdelta, hdeltacut = analyze(filePath, i)
    g.SetNextPoint(htemp.GetMean(), hdelta.GetMean(), htemp.GetRMS(),
                   hdelta.GetRMS())
g.GetXaxis().SetTitle('Temperature [#circC]')
g.GetYaxis().SetTitle('PPS residuals [#mus]')
g.Draw()
g.Fit(f, 'M')
logger.info('Absolute time scale drift: %.1f us' % f.GetParameter(1))
c.Update()
c.save()


c1 = gCanvas('time_jitter')
htemp, hdelta, hdeltacut = analyze('data/timing_pps_2.root')
hdeltacut.Draw()
logger.info('RMS @ %.1f deg: %.2f us' % (htemp.GetMean(), hdeltacut.GetRMS()))
c1.Update()
c1.save()
