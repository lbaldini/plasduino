#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.__plasduino__ import PLASDUINO_ROOT
from plasduino.__logging__ import logger
from plasduino import __utils__

import os


def red(text):
    """ Turn terminal text in red.
    """
    return '\033[91m%s\033[0m' % text

def yellow(text):
    """ Turn terminal text in yellow.
    """
    return '\033[93m%s\033[0m' % text

def grep(expr, maxNesting = 3, fileTypes = ['*.py', '*.h', '*.cpp']):
    """ Search for a piece of text in the code.
    """
    logger.info('Searching for expression "%s"...' % expr)
    target = ''
    for level in range(maxNesting):
        for suffix in fileTypes:
            base = '*/' * level
            target += ' %s/%s%s' % (PLASDUINO_ROOT, base, suffix)
    cmd = 'grep -n "%s" %s' % (expr, target)
    output = __utils__.getcmdoutput(cmd, True)
    if not len(output):
        logger.info('No occurrence(s) found.')
        return
    for (i, line) in enumerate(output.split('\n')):
        if os.path.basename(__file__) in line:
            pass
        else:
            filePath, lineNumber, pattern = line.split(':', 2)
            lineNumber = yellow(lineNumber)
            pattern = pattern.replace(expr, red(expr)).strip()
            logger.info('%s at line %s\n%s' % (filePath, lineNumber, pattern))



if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    (opts, args) = parser.parse_args()
    for expr in args:
        grep(expr)
