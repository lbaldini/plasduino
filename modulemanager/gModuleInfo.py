#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os


class gModuleInfo(dict):

    """ Small utility class encapsulating the relevant information
    about a given module.

    name: should be capitalized;
    abstract: should contain a short (one line or so) heading, optionally
        followed by a blank line and more extensive description.
        The description should be on a single line, and only include returns
        when needed, so that it properly word wraps when displayed.
    executable: should be the shell command used to launch a module,
        complete with extension. By convention modules' executables  are 
        prefixed by 'plasduino_' , e.g. plasduino_mymodule.py
    """
    
    OPTIONAL_KEYS = ['author', 'contact', 'webpage']

    def __init__(self, name, abstract, executable, dependencies,
                 shield = None, **kwargs):
        """ Constructor.
        """
        dict.__init__(self)
        self['name'] = name.title()
        self['abstract'] = self.formatText(abstract)
        if executable.endswith('.pyc'):
            executable = executable.rstrip('c')
        self['executable'] = executable
        self.setDependencies(dependencies)
        self.setShield(shield)
        for key in self.OPTIONAL_KEYS:
            self[key] = kwargs.get(key, None)

    def setDependencies(self, dependencies):
        """ Setup the module dependencies.
        """
        from plasduino.modulemanager.__dependencies__ import getDependency
        dependencies = [getDependency(dep) for dep in dependencies]
        self['dependencies'] = dependencies

    def setShield(self, shield):
        """ Setup the module shield.
        """
        from plasduino.modulemanager.__shields__ import getShield
        self['shield'] = getShield(shield)

    def getFileName(self):
        """ Return the executable file name (with no extenstion).
        """
        return os.path.basename(self['executable']).split('.')[0]

    def shortAbstract(self):
        """ Return the first line of the abstract.
        """
        try:
            return self['abstract'].split('\n\n')[0]
        except:
            return None

    def longAbstract(self):
        """ Return the first line of the abstract.
        """
        try:
            return self['abstract'].split('\n\n')[1]
        except:
            return None

    def formatText(self, text):
        """ Return a formatted version of a piece of text.
        """
        text = text.replace('\n\n', '\\')
        text = text.replace('\n', ' ')
        text = text.replace('   ', ' ')
        text = text.replace('  ', ' ')
        text = text.strip('\n')
        text = text.replace('\\', '\n\n')
        return text

    def __str__(self):
        """ Returns all info in a nice format
        """
        text = ""
        text += '%s\n\n'  % self.get('abstract')
        author = self.get('author')
        if author is not None:
            text += 'Author: %s' % self['author']
            contact = self.get('contact')
            if contact is not None:
                text += ' (%s)' % contact
        if not text.endswith('\n'):
            text += '\n\n'
        webpage = self.get('webpage')
        if webpage is not None:
            text += webpage
        return text



if __name__ == '__main__':
    abstract = """A short abstract.

And then, after an empty line, some more detailed explanation."""

    info = gModuleInfo('test', abstract, 'plasduino_test.py', ['python'])
    info['author'] = "Nikola Tesla"
    info['contact'] = "nikola.tesla@aldi.la"
    info['webpage'] = "awesomemodule@plasduino.org"
    print(info)
    print()
    print(info['abstract'])
    print()
    print(info.shortAbstract())
    print(info['dependencies'])
