#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.modulemanager.gArduinoShield import gArduinoShield
from plasduino.__logging__ import logger


SHIELD_DICT = {}


def addShield(name, url):
    """ Add a shield to the underlying dictionary.
    """
    SHIELD_DICT[name] = gArduinoShield(name, url)


addShield(None, None)
addShield('lab1', 'http://pythonhosted.org/plasduino/shield_lab1.html')


def getShield(name):
    """ Access dependencies by name.
    """
    try:
        return SHIELD_DICT[name]
    except KeyError:
        logger.error('Unknown shield %s.' % name)
        return None


if __name__ == '__main__':
    for key, value in SHIELD_DICT.items():
        print(key, value.htmlAnchor())
        
