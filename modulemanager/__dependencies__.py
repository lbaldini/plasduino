#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.modulemanager.gDependency import gDependency, gDependencyGroup
from plasduino.__logging__ import logger


DEPENDENCY_DICT = {}


def addDependency(name, url):
    """ Add a dependency to the underlying dictionary.
    """
    DEPENDENCY_DICT[name] = gDependency(name, url)

def addDependencyGroup(name, deps):
    """ Add a dependency to the underlying dictionary.
    """
    DEPENDENCY_DICT[name] = gDependencyGroup(name, deps)


addDependency('python', 'http://www.python.org/')
addDependency('PyQt4', 'http://www.riverbankcomputing.co.uk/software/pyqt/')
addDependency('PyQwt', 'http://pyqwt.sourceforge.net/')
addDependency('pyserial', 'http://pyserial.sourceforge.net/')
addDependency('avrdude', 'http://savannah.nongnu.org/projects/avrdude')
addDependency('arduino', 'http://arduino.cc/en/Main/Software')
addDependency('pyaudio', 'http://people.csail.mit.edu/hubert/pyaudio/')

ffmpeg = gDependency('ffmpeg', 'http://ffmpeg.org/')
avconv = gDependency('avconv', 'http://libav.org/avconv.html')
addDependencyGroup('videodec', [ffmpeg, avconv])


def getDependency(name):
    """ Access dependencies by name.
    """
    try:
        return DEPENDENCY_DICT[name]
    except KeyError:
        logger.error('Unknown dependency %s.' % name)
        return None


if __name__ == '__main__':
    for key, value in DEPENDENCY_DICT.items():
        print(key, value.htmlAnchor())
