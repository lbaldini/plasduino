#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import glob
import os
import pickle
import imp
import sys

from plasduino.__logging__ import logger, abort


def retrieveModuleInfo(modulePath):
    """ Retrieve module information.
   
    Note that, before we do anything, we set sys.dont_write_bytecode to True,
    in order to avoid creating unnecessary pyc files (remember, this will 
    be run as a post-installation script, and we don't want to create .pyc
    files in random locations that the package manager doesn't know about).
    """
    prevState = sys.dont_write_bytecode
    sys.dont_write_bytecode = True
    folderPath, fileName = os.path.split(modulePath)
    moduleName = fileName.replace('.py', '')
    try:
        module = imp.load_source(moduleName, modulePath)
        moduleInfo = module.__moduleinfo__
        # Why is this necessary? Why don't we use the abspath
        # in the modules to start with?
        moduleInfo['executable'] =\
            os.path.join(folderPath, moduleInfo['executable'])
        sys.dont_write_bytecode = prevState
        return moduleInfo
    except Exception as exception:
        logger.info(exception)
        sys.dont_write_bytecode = prevState
        return None

def dumpModuleInfo(folderList, outputFilePath, pattern = 'plasduino_*.py'):
    """ Loop over the module folder and pickle all the __moduleinfo__
    variables into a single file.

    The basic rule for inclusion is that we loop over all the folders in the
    folderList argument and search for files in there that match the given
    pattern (default plasduino_*.py).

    The only way of enforcing a minimum amount on information embedded in the
    modules is being not forgiving: we're exiting with no mercy, here, if we
    can't import the __moduleinfo__ from a module.
    """    
    if not isinstance(folderList, list):
        folderList = [folderList]
    infoDict = {}
    for folderPath in folderList:
        logger.info('Collecting module information in %s...' % folderPath)
        for modulePath in glob.glob(os.path.join(folderPath, pattern)):
            logger.info('Processing %s...' % modulePath)
            moduleName = os.path.basename(modulePath).replace('.py', '')
            moduleInfo = retrieveModuleInfo(modulePath)
            if moduleInfo is not None:
                infoDict[moduleInfo['name']] = moduleInfo
    logger.info('Dumping module information to %s...' % outputFilePath)
    pickle.dump(infoDict, open(outputFilePath, 'w'))
    logger.info('Done.')

def loadModuleInfo(filePath):
    """ Load back the information dictionary from the pickle file.

    If the file does not exist, silently return an empty dictionary.
    """
    if not os.path.exists(filePath):
        return {}
    logger.info('Loading module information from %s...' % filePath)
    infoDict = pickle.load(open(filePath, 'r'))
    logger.info('Done.')
    return infoDict



if __name__ == '__main__':
    from plasduino.__plasduino__ import PLASDUINO_MODULES,\
        PLASDUINO_DIST_MODULEINFO_FILE_PATH
    dumpModuleInfo(PLASDUINO_MODULES, PLASDUINO_DIST_MODULEINFO_FILE_PATH)
    print(loadModuleInfo(PLASDUINO_DIST_MODULEINFO_FILE_PATH))
