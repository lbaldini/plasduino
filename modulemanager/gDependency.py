#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


class gDependency:

    """ Small utility class representing a dependency for the package.
    """

    def __init__(self, name, url):
        """ Constructor.
        """
        self.Name = name
        self.Url = url

    def htmlAnchor(self):
        """ Return a html anchor to the dependency url.
        """
        return '<a href="%s">%s</a>' % (self.Url, self.Name)

    def __str__(self):
        """ String formatting.
        """
        return self.Name



class gDependencyGroup:

    """ Another utility class representing a dependency group, i.e., a set
    of packages the user can choose from.
    """

    def __init__(self, name, dependencies):
        """ Constructor.
        """
        self.Name = name
        self.Dependencies = dependencies

    def htmlAnchor(self):
        """ Return a html anchor to the dependency url.
        """
        anchor = ''
        for dep in self.Dependencies:
            anchor += '%s | ' % dep.htmlAnchor()
        return anchor.strip(' |')

    def __str__(self):
        """ String formatting.
        """
        return self.Name




if __name__ == '__main__':
    dep = gDependency('python', 'http://www.python.org')
    print(dep)
    print(dep.htmlAnchor())

    ffmpeg = gDependency('ffmpeg', 'http://ffmpeg.org/')
    avconv = gDependency('avconv', 'http://libav.org/avconv.html')
    depGroup = gDependencyGroup('videodec', [ffmpeg, avconv])
    print(depGroup)
    print(depGroup.htmlAnchor())
