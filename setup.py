#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from distutils.core import setup
import glob

from __tag__ import TAG


_NAME = 'plasduino'

_DESCRIPTION = 'Didactic data acquisition framework'

_AUTHOR = 'Luca Baldini'

_AUTHOR_EMAIL = 'luca.baldini@pi.infn.it'

_LICENCE = 'GNU General Public License v3 or later'

_URL = 'http://pythonhosted.org/plasduino/'

_PACKAGES = [
    'plasduino', 'plasduino.arduino', 'plasduino.artwork',
    'plasduino.calibration', 'plasduino.daq', 'plasduino.devices',
    'plasduino.doc', 'plasduino.gui', 'plasduino.modulemanager',
    'plasduino.sensors', 'plasduino.test'
    ]

_PACKAGE_DIR = {
    'plasduino': '.'
    }

_PACKAGE_DATA = {
    'plasduino'              : ['LICENSE', 'README', 'runId.cfg.sample',
                                'plasduino.cfg.sample', 'plasduino.desktop'],
    'plasduino.arduino'      : ['*.h', '*.cpp', 'boards*.txt', 'SConstruct',
                                '*/*.ino', '*/*.hex'],
    'plasduino.artwork'      : ['skins/*/*.svg', 'skins/*/*.png'],
    'plasduino.calibration'  : [],
    'plasduino.daq'          : [],
    'plasduino.devices'      : [],
    'plasduino.doc'          : ['*.1', 'release.notes'],
    'plasduino.gui'          : [],
    'plasduino.modulemanager': [],
    'plasduino.sensors'      : ['*.dat'],
    'plasduino.test'         : []
    }

_SCRIPTS = glob.glob('modules/*.py') + ['modules/plasduino']

_CLASSIFIERS = [
    'Development Status :: 5 - Production/Stable',
    'Environment :: X11 Applications :: Qt',
    'Environment :: Win32 (MS Windows)',
    'Intended Audience :: Education',
    'Intended Audience :: Science/Research',
    'License :: OSI Approved :: '
    'GNU General Public License v3 or later (GPLv3+)',
    'Operating System :: POSIX :: Linux',
    'Operating System :: Microsoft :: Windows',
    'Programming Language :: Python',
    'Topic :: Education',
    'Topic :: Scientific/Engineering :: Physics'
    ]


if __name__ == '__main__':

    setup(name = _NAME,
          description = _DESCRIPTION,
          version = TAG,
          author = _AUTHOR,
          author_email = _AUTHOR_EMAIL,
          license = _LICENCE,
          url = _URL,
          packages = _PACKAGES,
          package_dir = _PACKAGE_DIR,
          package_data = _PACKAGE_DATA,
          scripts = _SCRIPTS,
          classifiers = _CLASSIFIERS)
