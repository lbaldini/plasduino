%define name plasduino
%define version VERSION
%define unmangled_version VERSION
%define release fc
%define pyversion 2.7

Summary: Hardware/software framework for lab data acquisition
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
License: GNU General Public License v3 or later
Group: Science
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Vendor: The plasduino team
Packager: The plasduino team
Provides: plasduino
Requires: python PyQt4 PyQwt pyserial avrdude pyaudio
Url: http://pythonhosted.org/plasduino/
BuildRequires:desktop-file-utils

%description
Plasduino is a data acquisition framework for the physics lab. It builds on top
of an old project for data acquisition through the parallel port (plas) and is
based on the arduino board (hence the name plasduino).

Plasduino is an open hardware/software project. The codebase is released 
under the GPL licence along with the relevant arduino shield schematics. 
The main goal is to provide all the necessary tools to assemble a flexible, 
easy-to-use, general purpose data acquisition system, suitable for physics 
didactic experiments, for under $50.

%prep
%setup -n %{name}-%{unmangled_version}

%build
python setup_rpm.py build

%install
python setup_rpm.py install -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
desktop-file-install %{buildroot}/%{_datadir}/applications/plasduino.desktop

%clean
rm -rf $RPM_BUILD_ROOT

%post
plasduino_updatemoduelinfo.py

%files -f INSTALLED_FILES
%defattr(-,root,root)
%{_libdir}/python%{pyversion}/site-packages/%{name}/
