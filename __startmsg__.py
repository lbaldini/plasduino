# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.__plasduino__ import PACKAGE_NAME, PLASDUINO_WEB_PAGE
from plasduino.__tag__ import TAG, BUILD_DATE
from plasduino.__copyright__ import COPYRIGHT


MSG = """
    This is %s version %s (built on %s).

    %s

    %s comes with ABSOLUTELY NO WARRANTY.
    This is free software (and hardware), and you are welcome to redistribute
    it under certain conditions. See the LICENSE file for details.

    Visit %s for more information.
""" % (PACKAGE_NAME, TAG, BUILD_DATE, COPYRIGHT.replace('\n', '\n    '),
       PACKAGE_NAME, PLASDUINO_WEB_PAGE)


if __name__ == '__main__':
    print(MSG)

