#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from copy import deepcopy

from distutils.core import setup
from plasduino.__tag__ import TAG

import plasduino.setup as base_setup


""" Add the debian stuff to the package data and get rid of the licence.
"""
_PACKAGE_DATA = deepcopy(base_setup._PACKAGE_DATA)
_PACKAGE_DATA['plasduino'] += ['debian/control', 'debian/changelog',
                               'debian/compat', 'debian/copyright',
                               'debian/rules', 'debian/postinst',
                               'debian/menu', 'debian/postrm',
                               'debian/source/format',
                               'debian/plasduino.desktop',
                               'debian/plasduino.install',
                               'debian/plasduino.manpages']
_PACKAGE_DATA['plasduino'].remove('LICENSE')


""" And do the standard setup.
"""
setup(name = base_setup._NAME,
      description = base_setup._DESCRIPTION,
      version = base_setup.TAG,
      author = base_setup._AUTHOR,
      author_email = base_setup._AUTHOR_EMAIL,
      license = base_setup._LICENCE,
      url = base_setup._URL,
      packages = base_setup._PACKAGES,
      package_dir = base_setup._PACKAGE_DIR,
      package_data = _PACKAGE_DATA,
      scripts = base_setup._SCRIPTS,
      classifiers = base_setup._CLASSIFIERS)
