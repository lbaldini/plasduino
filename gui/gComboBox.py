#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtGui


class gComboBox(QtGui.QComboBox):

    """ Small wrapper around the QComboBox class.
    """

    def __init__(self, parent = None, entries = [], default = None):
        """ Overloaded constructor.
        """
        QtGui.QComboBox.__init__(self, parent)
        for (i, key) in enumerate(entries):
            self.addItem(key)
            if key == default:
                self.setCurrentIndex(i)



class gBinaryComboBox(gComboBox):

    """ Small combo box with two entries (True and False).
    """

    def __init__(self, parent = None, default = 'True'):
        """ Overloaded constructor.
        """
        gComboBox.__init__(self, parent, ['True', 'False'], str(default))



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    box = gBinaryComboBox(default = False)
    box.show()
    application.exec_() 
