#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtGui

from plasduino.__logging__ import logger
from plasduino.gui.gQt import connect, disconnect
from plasduino.gui.gVideoclickBaseDialog import gVideoclickBaseDialog
from plasduino.daq.gVideoclickPoint import gVideoclickPoint



class gVideoclickScaleDialog(gVideoclickBaseDialog):

    """ Small dialog window to set the origin of the coordinate system.
    """

    def __init__(self, parent = None):
        """ Constructor.
        """
        windowTitle = 'Set the scale factor for the coordinate system'
        text = 'Use the left and right mouse buttons to mark the two ends ' \
            'of a known lenght on the frame in the main window, fill in the ' \
            'value of the lenght in the text box, then push the save button ' \
            'to save your choice, cancel to ignore it.'
        gVideoclickBaseDialog.__init__(self, parent, windowTitle, text)
        self.Layout.addWidget(QtGui.QLabel('Known length [cm]', self), 1, 0)
        self.LineEdit = QtGui.QLineEdit(self)
        self.Layout.addWidget(self.LineEdit, 1, 1)
        self.Layout.setColumnMinimumWidth(0, self.width()/2 - 4)
        connect(self.LineEdit, 'textChanged(QString)', self.checkData)
        self.Point1 = None
        self.Point2 = None
        self.ScaleFactor = None

    def checkData(self):
        """ Check whether we have all the pieces in place to set the scale
        factor (and set the status of the save button accordingly).
        """
        try:
            scale = float(self.LineEdit.text())
        except ValueError:
            self.SaveButton.setEnabled(False)
            return
        if self.Point1 and self.Point2:
            d = self.Point1.distToPoint(self.Point2)
            if d > 0 and scale > 0:
                self.SaveButton.setEnabled(True)
                self.ScaleFactor = scale/d
                return
            else:
                self.SaveButton.setEnabled(False)
        else:
            self.SaveButton.setEnabled(False)

    def leftClick(self, x, y):
        """ Slot for the left mouse click. 
        """
        self.Point1 = gVideoclickPoint(x, y)
        self.parent().repaintFrame()
        self.parent().drawPoint(self.Point1, 'p1')
        if self.Point2 is not None:
            self.parent().drawPoint(self.Point2, 'p2')
            self.parent().drawLine(self.Point1, self.Point2)
            self.checkData()

    def rightClick(self, x, y):
        """ Slot for the right mouse click. 
        """
        self.Point2 = gVideoclickPoint(x, y)
        self.parent().repaintFrame()
        self.parent().drawPoint(self.Point2, 'p2')
        if self.Point1 is not None:
            self.parent().drawPoint(self.Point1, 'p1')
            self.parent().drawLine(self.Point1, self.Point2)
            self.checkData()

    def accept(self):
        """ Overloaded accept() method.
        """
        logger.info('Setting the reference system scale factor to %s.' %\
                        self.ScaleFactor)
        self.ignoreMouseEvents()
        self.parent().EventBuffer.RefSystemScale = self.ScaleFactor
        self.parent().repaintFrame()
        self.parent().updateSystemDisplay()
        QtGui.QDialog.accept(self)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    dialog = gVideoclickScaleDialog()
    dialog.exec_()
