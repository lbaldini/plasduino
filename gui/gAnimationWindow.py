#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import time

from PyQt4 import QtCore, QtGui
from plasduino.__logging__ import logger
from plasduino.gui.gQt import connect, disconnect
from plasduino.gui.gPushButton import gTextPushButton



class gAnimationWindow(QtGui.QDialog):

    """ Small window used to display simple animations.
    """
    
    TITLE = 'Animation window'
    WIDTH = 800
    HEIGHT = 80

    def __init__(self, parent = None, period = 2.0):
        """ Constructor.
        """
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(self.TITLE)
        self.Layout = QtGui.QGridLayout(self)
        self.Canvas = QtGui.QLabel(self)
        self.Layout.addWidget(self.Canvas, 0, 0, 1, 4)
        self.StartChronoButton = gTextPushButton('Start', self,
                                                 self.startChronoPushed)
        self.DiscardChronoButton = gTextPushButton('Discard', self,
                                                   self.discardChronoPushed)
        self.Layout.addWidget(self.StartChronoButton, 1, 0)
        self.Layout.addWidget(self.DiscardChronoButton, 1, 1)
        self.DiscardChronoButton.setEnabled(False)
        self.ChronoLabel = QtGui.QLabel(self)
        self.LastChronoValue = None
        self.Layout.addWidget(self.ChronoLabel, 1, 2)
        self.MonitorLabel = QtGui.QLabel(self)
        self.MonitorLabel.setAlignment(QtCore.Qt.AlignRight | \
                                           QtCore.Qt.AlignCenter)
        self.Layout.addWidget(self.MonitorLabel, 1, 3)
        self.resetChronoLabel()
        self.FrameList = []
        self.Timer = QtCore.QTimer(self)
        connect(self.Timer, 'timeout()', self.advance)
        self.setupFrames(period)

    def startChronoPushed(self):
        """
        """
        if self.StartChronoButton.text() == 'Start':
            self.ChronoStart = time.time()
            connect(self.Timer, 'timeout()', self.updateChronoLabel)
            self.StartChronoButton.setText('Stop')
            self.DiscardChronoButton.setEnabled(False)
        else:
            disconnect(self.Timer, 'timeout()', self.updateChronoLabel)
            self.emit(QtCore.SIGNAL('chronoStopped(double)'),
                      self.LastChronoValue)
            self.StartChronoButton.setText('Start')
            self.DiscardChronoButton.setEnabled(True)

    def discardChronoPushed(self):
        """
        """
        self.resetChronoLabel()
        self.DiscardChronoButton.setEnabled(False)
        self.emit(QtCore.SIGNAL('chronoDiscarded()'))

    def setChronoLabel(self, value):
        """
        """
        self.ChronoLabel.setText('%.3f s' % value)

    def resetChronoLabel(self):
        """
        """
        self.setChronoLabel(0)

    def updateChronoLabel(self):
        """
        """
        self.LastChronoValue = time.time() - self.ChronoStart
        self.setChronoLabel(self.LastChronoValue)

    def setupFrames(self, period, refreshInterval = 20,
                    ballRadius = 50,
                    backgroundColor = QtCore.Qt.white,
                    ballColor = QtCore.Qt.blue,
                    lineColor = QtCore.Qt.red):
        """ Setup the animation frames.
        """
        logger.info('Setting up animation frames...')
        self.NumFrames = int(period*1000/refreshInterval + 0.5)
        self.__CurrentFrame = 0
        x0 = self.WIDTH/2
        dx = 0.4*self.WIDTH
        y0 = self.HEIGHT/2
        for step in range(self.NumFrames):
            theta = 2*math.pi*float(step)/self.NumFrames
            x = x0 + dx*math.sin(theta)
            pixmap = QtGui.QPixmap(self.WIDTH, self.HEIGHT)
            pixmap.fill(backgroundColor)
            painter = QtGui.QPainter(pixmap)
            pen = QtGui.QPen(lineColor)
            pen.setWidth(1)
            painter.setPen(pen)
            painter.setBrush(ballColor)
            painter.drawLine(x0 - dx, y0, x0 + dx, y0)
            painter.drawLine(x0, y0 - 20, x0, y0 + 20)
            painter.drawLine(x0 - dx, y0 - 10, x0 - dx, y0 + 10)
            painter.drawLine(x0 + dx, y0 - 10, x0 + dx, y0 + 10)
            painter.drawEllipse(x - ballRadius/2, y0 - ballRadius/2,
                                ballRadius, ballRadius)
            del painter
            self.FrameList.append(pixmap)
        logger.info('Done.')
        self.Timer.setInterval(refreshInterval)
        self.paintCurrentFrame()

    def setFrame(self, index):
        """ Set the current frame.
        """
        self.Canvas.setPixmap(self.FrameList[index])
        self.Canvas.repaint()

    def paintCurrentFrame(self):
        """ Paint the current frame.
        """
        self.setFrame(self.__CurrentFrame)

    def advance(self):
        """ Go to the next frame.
        """
        self.__CurrentFrame += 1
        if self.__CurrentFrame >= self.NumFrames:
            t = time.time()
            self.MonitorLabel.setText('(Last period: %.6f s)' %\
                                          (t - self.LastRollover))
            self.LastRollover = t
            self.__CurrentFrame = 0
        self.paintCurrentFrame()

    def start(self):
        """ Start the animation.
        """
        self.LastRollover = time.time()
        self.Timer.start()

    def stop(self):
        """ Stop the animation.
        """
        self.Timer.stop()




if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    window = gAnimationWindow()
    window.show()
    window.start()
    application.exec_() 
