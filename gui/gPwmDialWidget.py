#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtCore, QtGui
from plasduino.gui.gDialWidget import gDialWidget
from plasduino.gui.gPushButton import gTextPushButton
from plasduino.gui.gQt import connect


class gPwmDialWidget(QtGui.QGroupBox):

    """ Specialized dial widget to control the PWM duty cycle on an
    arduino digital output.

    This is essentially a dial widget assuming values between 0 and 255,
    along with a push button to enable/diable the PWM output.
    """

    def __init__(self, parent = None, title = 'PWM dial', minDac = 0,
                 maxDac = 255, startDac = 100, dialTitle = 'Duty cycle'):
        """ Constructor.
        """
        QtGui.QGroupBox.__init__(self, title, parent)
        layout = QtGui.QVBoxLayout(self)
        self.setLayout(layout)
        numSteps = maxDac - minDac + 1
        self.PwmDial = gDialWidget(self, title = dialTitle, minimum = minDac,
                                   maximum = maxDac, dialSteps = numSteps,
                                   textLabelFmt = '%d')
        self.PwmDial.setMajorTicks([i for i in range(minDac, maxDac + 1, 25)])
        self.PwmDial.setMinorTicks([i for i in range(minDac, maxDac + 1, 5)])
        self.PwmDial.setValue(startDac)
        layout.addWidget(self.PwmDial)
        self.ToggleButton = gTextPushButton('Toggle', self)
        self.ToggleButton.setFixedWidth(self.PwmDial.Label.width())
        layout.addWidget(self.ToggleButton, 2, QtCore.Qt.AlignCenter)
        self.setOutputDisabled()
        self.setupConnections()

    def setEnabled(self, enabled = True):
        """ Enable/disable the widget.
        """
        self.PwmDial.setEnabled(enabled)
        self.ToggleButton.setEnabled(enabled)

    def value(self):
        """ Return the value (an integer between 0 and 255) of the dial widget.
        """
        return int(self.PwmDial.value())

    def signalUpdate(self):
        """ Emit the signal to notify the consumers that the widget has been
        updated.
        """
        self.emit(QtCore.SIGNAL('updated(int)'), self.value())

    def setupConnections(self):
        """ Setup the connections.
        """
        connect(self.ToggleButton, 'clicked()', self.toggle)
        connect(self.PwmDial, 'updated(float)', self.signalUpdate)
        
    def setOutputEnabled(self):
        """ Set the widget in output-enabled mode.
        """
        self.PwmDial.setEnabled(True)
        self.ToggleButton.setText('Disable PWM')
        self.__OutputEnabled = True
        self.emit(QtCore.SIGNAL('updated(int)'), self.value())

    def setOutputDisabled(self):
        """ Set the widget in output-disabled mode.
        """
        self.PwmDial.setEnabled(False)
        self.ToggleButton.setText('Enable PWM')
        self.__OutputEnabled = False
        self.emit(QtCore.SIGNAL('updated(int)'), 0)

    def toggle(self):
        """ Toggle between the two states of the widget.
        """
        if self.__OutputEnabled:
            self.setOutputDisabled()
        else:
            self.setOutputEnabled()



if __name__ == '__main__':
    def testSlot(value):
        """ Test slot.
        """
        print(value)

    from plasduino.__cfgparse__ import getapp
    application = getapp()
    d = gPwmDialWidget()
    connect(d, 'updated(int)', testSlot)
    d.show()
    application.exec_() 

