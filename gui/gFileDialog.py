#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2103 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import re
import os

from PyQt4 import QtCore, QtGui

from plasduino.gui.gQt import connect



class gFileType:

    """ Small utility class representing a file type.
    """
    
    def __init__(self, extension, description):
        """ Constructor.
        """
        self.Extension = extension
        self.Description = description

    def getFilter(self):
        """ Return the filter to be used in a file dialog.
        """
        return '%s (*.%s)' % (self.Description, self.Extension)


""" The Qt separator for multiple file types in a dialog.
"""
FILTER_SEPARATOR = ';;'

def getFilter(fileTypes):
    """ Return the filter for a list of file types.
    """
    filterExpr = ''
    for fileType in fileTypes:
        filterExpr += '%s;;' % fileType.getFilter()
    return filterExpr.rstrip(';')


""" Define some useful file types.
"""
FILE_TYPE_ANY = gFileType('*'  , 'All files')
FILE_TYPE_TXT = gFileType('txt', 'Plain text')
FILE_TYPE_CSV = gFileType('csv', 'Comma-separated values')
FILE_TYPE_DAT = gFileType('dat', 'plasduino binary data')
FILE_TYPE_AVI = gFileType('avi', 'Audio Video Interleave')
FILE_TYPE_FLV = gFileType('flv', 'Flash Video File')
FILE_TYPE_MP4 = gFileType('mp4', 'MPEG-4 Part 14')
FILE_TYPE_3GP = gFileType('3gp', '3GPP media container format')
FILE_TYPE_PY  = gFileType('py' , 'python source')



def getFileExtension(filePath):
    """ Return the file extension for a given file path.
    """
    return filePath.split('.')[-1]



def isFileType(filePath, fileType):
    """
    """
    return (getFileExtension(filePath) == fileType.Extension)


class gFileDialog(QtGui.QFileDialog):

    """ Small wrapper class around a QFileDialog object.
    """

    pass



class gFileOpenDialog(QtGui.QFileDialog):

    """ Small wrapper class around a QFileDialog object.
    """

    def __init__(self, fileTypes = [FILE_TYPE_ANY], parent = None,
                 caption = '', directory = '',
                 fileMode = QtGui.QFileDialog.ExistingFile):
        """ Constructor.
        """
        filterExpr = getFilter(fileTypes)
        QtGui.QFileDialog.__init__(self, parent, caption, directory, filterExpr)
        self.setAcceptMode(QtGui.QFileDialog.AcceptOpen)
        self.setFileMode(fileMode)


def getOpenFilePath(fileTypes = [FILE_TYPE_ANY], parent = None,
                    caption = '', directory = ''):
    """ Convenience function for retrieving a single existing file
    to open.
    
    On posix we do escape spaces in file names.
    """
    dialog = gFileOpenDialog(fileTypes, parent, caption, directory,
                             QtGui.QFileDialog.ExistingFile)
    if dialog.exec_():
        filePath = str(dialog.selectedFiles()[0])
        if os.name == 'posix':
            filePath = filePath.replace(' ', '\ ')
        return filePath
    else:
        return None



def getOpenFileList(fileTypes = [FILE_TYPE_ANY], parent = None,
                    caption = '', directory = ''):
    """ Convenience function for retrieving one or more existing files
    to open.

    On posix we do escape spaces in file names.
    """
    dialog = gFileOpenDialog(fileTypes, parent, caption, directory,
                             QtGui.QFileDialog.ExistingFiles)
    if dialog.exec_():
        fileList = map(str, dialog.selectedFiles())
        if os.name == 'posix':
            fileList = [filePath.replace(' ', '\ ') for filePath in fileList]
        return fileList
    else:
        return []



class gFileSaveDialog(QtGui.QFileDialog):

    """ Small wrapper class around a QFileDialog object.

    Among the other things, here we prevent the user from inserting spaces in
    the file names (sorry for the Windows users).
    """

    def __init__(self, fileTypes, parent = None, caption = '', directory = ''):
        """ Constructor.
        """
        filterExpr = getFilter(fileTypes)
        QtGui.QFileDialog.__init__(self, parent, caption, directory, filterExpr)
        self.setAcceptMode(QtGui.QFileDialog.AcceptSave)
        self.LineEdit = self.findChild(QtGui.QLineEdit)
        connect(self.LineEdit, 'textChanged(QString)', self.checkFileName)

    def checkFileName(self, text):
        """ Make sure the user does not put any space in the file name.
        """
        if ' ' in text:
            self.LineEdit.setText(text.replace(' ', ''))



def getSaveFilePath(fileTypes, parent = None, caption = '', directory = ''):
    """ Convenience function for retrieving a save file path.

    Note that we enforce here that the file name ends with the file
    extension.
    """
    dialog = gFileSaveDialog(fileTypes, parent, caption, directory)
    if dialog.exec_():
        filePath = str(dialog.selectedFiles()[0])
        selectedFilter = dialog.selectedNameFilter()
        regexp = '(?<=\(\*.)[a-zA-Z0-9]*(?=\))'
        extension = re.search(regexp, selectedFilter).group(0)
        if not filePath.endswith('.%s' % extension):
            filePath = '%s.%s' % (filePath, extension)
        return filePath
    else:
        return None



class gFileDialogWidget(QtGui.QLineEdit):

    """ A line edit prompting a file dialog when you click on.
    """

    def __init__(self, parent = None, text = ''):
        """
        """
        QtGui.QLineEdit.__init__(self, text, parent)
        
    def mouseReleaseEvent(self, e):
        """
        """
        fileDialog = gFileDialog(self)
        if fileDialog.exec_():
            self.setText(fileDialog.selectedFiles()[0])



class gFolderDialogWidget(gFileDialogWidget):
    
    """ A line edit prompting a folder dialog when you click on.
    """

   
    def mouseReleaseEvent(self, e):
        """
        """
        fileDialog = gFileDialog(self)
        fileDialog.setFileMode(QtGui.QFileDialog.Directory)
        if fileDialog.exec_():
            self.setText(fileDialog.selectedFiles()[0])


if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    print(getSaveFilePath([FILE_TYPE_TXT, FILE_TYPE_CSV]))
    print(getOpenFilePath([FILE_TYPE_ANY]))
    print(getOpenFileList([FILE_TYPE_ANY]))
