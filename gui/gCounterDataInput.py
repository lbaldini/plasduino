#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtGui

from plasduino.gui.gDataInput import gDataInput
from plasduino.gui.gComboBox import gComboBox



class gCounterDataInput(gDataInput):
    
    """ Specialized data input class for counting applications.
    """

    MODE_FIELD_NAME = 'mode'
    INPUT_PIN_FIELD_NAME = 'input_pin_%d'

    def __init__(self, parent = None, inputPins = [2, 3],**kwargs):
        """ Constructor.
        """
        gDataInput.__init__(self, **kwargs)
        widget = gComboBox(self, ['Rising', 'Falling', 'Change'])
        self.addField(self.MODE_FIELD_NAME, 'Digital counting mode', widget,
                      checkBox = False)
        for inputPin in inputPins:
            widget = QtGui.QCheckBox(self)
            widget.setChecked(True)
            self.addField(self.INPUT_PIN_FIELD_NAME % inputPin,
                          'Digital input D%s' % inputPin, widget,
                          checkBox = False)
        


if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    d = gCounterDataInput()
    d.show()
    application.exec_() 
