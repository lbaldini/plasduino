#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from PyQt4 import QtCore, QtGui
from plasduino.gui.gQt import connect, disconnect



class gDialWidget(QtGui.QWidget):

    """ Widget including a dial and a label connected to each other.

    This is actually a major rework of the QDial class, reimplementing the
    paintEvent() method for the widget in order to be able to have full
    control over the tick marks and the associated labels.

    Both linear and logarithmic dials are supported. The basic idea is that,
    since a granularity finer than ~1/1000 is practically impossible with
    a dial (or any other widget), we do divide the range of the underlying
    QDial in 1000 (adjustable) steps (the range goes from 0 to steps) and
    we reimplement the logic for retrieving the dial value passing
    the minimum and maximum as keyword arguments to the widget constructor
    (i.e., the actual QDial minimum and maximum are only used internally
    in a way that should be totally transparent to the user). The dial is
    connected to a QLineEdit whose text can be edited by hand and therefore
    has potentially arbitrary precision (much more than the 1000 steps of the
    QDial).

    The gDialWidget provides a updated(float) signal to notify that
    the value has changes---triggered either by a sliderReleased() of the
    QDial or by a editingFinished() of the QLineEdit.
    """

    HOR_SCALE_FACTOR = 1.75
    VER_SCALE_FACTOR = 2.1
    THETA_MIN = math.pi/6.
    THETA_MAX = 11.*math.pi/6.
    R_INNER = 0.46
    R_MINOR_TICKS = 0.50
    R_MAJOR_TICKS = 0.54
    R_LABELS = 0.75
    R_LABEL_MOD = 0.5
    
    def __init__(self, parent = None, **kwargs):
        """ Constructor.
        """
        self.__Diameter = kwargs.get('diameter', 100)
        self.__LabelFmt = kwargs.get('textLabelFmt', '%.3f')
        self.__TickLabelFmt = kwargs.get('tickLabelFmt', '%s')
        self.__Minimum = float(kwargs.get('minimum', 0))
        self.__Maximum = float(kwargs.get('maximum', 1))
        self.__Logarithmic = kwargs.get('logarithmic', False)
        if self.__Logarithmic:
            self.__Range = math.log10(self.__Maximum/self.__Minimum)
        else:
            self.__Range = self.__Maximum - self.__Minimum
        self.__NumSteps = kwargs.get('dialSteps', 1000)
        self.__Title = kwargs.get('title', None)
        if self.__Title is not None:
            self.TOP_PADDING = 0.15
        else:
            self.TOP_PADDING = 0
        QtGui.QWidget.__init__(self, parent)
        self.setFixedSize(self.HOR_SCALE_FACTOR*self.__Diameter,
                          self.VER_SCALE_FACTOR*self.__Diameter)
        self.TitleLabel = QtGui.QLabel(self)
        if self.__Title is not None:
            self.TitleLabel.setText(self.__Title)
        self.TitleLabel.setFixedWidth(self.HOR_SCALE_FACTOR*self.__Diameter)
        self.TitleLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.Dial = QtGui.QDial(self)
        self.Dial.setFixedSize(self.__Diameter, self.__Diameter)
        self.Dial.setMinimum(0)
        self.Dial.setMaximum(self.__NumSteps)
        self.Dial.setWrapping(False)
        dx = 0.5*(self.HOR_SCALE_FACTOR - 1)*self.__Diameter
        dy = 0.5*(self.HOR_SCALE_FACTOR - 1.)*self.__Diameter +\
            self.TOP_PADDING*self.__Diameter
        self.Dial.move(dx, dy)
        self.Label = QtGui.QLineEdit(self)
        dy = (0.5*self.HOR_SCALE_FACTOR + 0.75)*self.__Diameter +\
            self.TOP_PADDING*self.__Diameter
        self.Label.setFixedWidth(self.__Diameter)
        self.Label.move(dx, dy)
        self.updateLabelText()
        self.setupConnections()
        self.MajorTicks = []
        self.MinorTicks = []
        self.TickLabels = []

    def setupConnections(self):
        """ Setup the connection between the dial and the label.
        """
        connect(self.Dial, 'valueChanged(int)', self.updateLabelText)
        connect(self.Dial, 'sliderReleased()', self.signalUpdate)
        connect(self.Label, 'editingFinished()', self.updateDialValue)
        connect(self.Label, 'editingFinished()', self.signalUpdate)

    def __getTheta(self, val):
        """ Return the dial rotation corresponding to a given value.
        
        This is used to draw the ticks and labels in the reimplemented
        paintEvent() method.
        """
        if self.__Logarithmic:
            frac = (math.log10(val/self.__Minimum))/(self.__Range)
        else:
            frac = (val - self.__Minimum)/(self.__Range)
        return self.THETA_MIN + frac*(self.THETA_MAX - self.THETA_MIN)

    def setMajorTicks(self, ticks, labels = None, labelFmt = '%.2f'):
        """ Set the major ticks (ans associated labels) for the dial.
        """
        self.MajorTicks = []
        self.TickLabels = []
        x0 = 0.5*self.HOR_SCALE_FACTOR*self.__Diameter
        y0 = (0.44 + 0.25*self.HOR_SCALE_FACTOR)*self.__Diameter  +\
            self.TOP_PADDING*self.__Diameter
        for i, val in enumerate(ticks):
            theta = self.__getTheta(val)
            x1 = x0 - self.R_INNER*self.__Diameter*math.sin(theta)
            y1 = y0 + self.R_INNER*self.__Diameter*math.cos(theta)
            x2 = x0 - self.R_MAJOR_TICKS*self.__Diameter*math.sin(theta)
            y2 = y0 + self.R_MAJOR_TICKS*self.__Diameter*math.cos(theta)
            self.MajorTicks.append(QtCore.QLine(x1, y1, x2, y2))
            r = self.R_LABELS - self.R_LABEL_MOD*\
                (self.R_LABELS - self.R_MAJOR_TICKS)*abs(math.cos(theta))
            x = x0 - r*self.__Diameter*math.sin(theta)
            y = y0 + r*self.__Diameter*math.cos(theta)
            try:
                text = labels[i]
            except:
                text = self.__TickLabelFmt % val
            self.TickLabels.append((x, y, text))

    def setMinorTicks(self, ticks):
        """ Set the minor ticks for the dial.
        """
        self.MinorTicks = []
        x0 = 0.5*self.HOR_SCALE_FACTOR*self.__Diameter
        y0 = (0.44 + 0.25*self.HOR_SCALE_FACTOR)*self.__Diameter +\
            self.TOP_PADDING*self.__Diameter
        for i, val in enumerate(ticks):
            theta = self.__getTheta(val)
            x1 = x0 - self.R_INNER*self.__Diameter*math.sin(theta)
            y1 = y0 + self.R_INNER*self.__Diameter*math.cos(theta)
            x2 = x0 - self.R_MINOR_TICKS*self.__Diameter*math.sin(theta)
            y2 = y0 + self.R_MINOR_TICKS*self.__Diameter*math.cos(theta)
            self.MinorTicks.append(QtCore.QLine(x1, y1, x2, y2))

    def paintEvent(self, event):
        """ Reimplemented paintEvent() method.
        """
        QtGui.QWidget.paintEvent(self, event)
        painter = QtGui.QPainter(self)
        pen = QtGui.QPen(QtCore.Qt.gray)
        pen.setWidth(1)
        painter.setPen(pen)
        for line in self.MinorTicks:
            painter.drawLine(line)
        for line in self.MajorTicks:
            painter.drawLine(line)
        if self.isEnabled():
            pen = QtGui.QPen(QtCore.Qt.black)
            painter.setPen(pen)
        for x, y, text in self.TickLabels:
            painter.drawText(x-50, y-10, 100, 20, QtCore.Qt.AlignCenter, text)
        del painter

    def getDialValue(self):
        """ Return the dial value.
        """
        val = float(self.Dial.value())/self.__NumSteps
        if self.__Logarithmic:
            return 10**(math.log10(self.__Minimum) + val*self.__Range)
        else:
            return self.__Minimum + val*self.__Range

    def setDialValue(self, value):
        """ Set the dial value.
        """
        if self.__Logarithmic:
            frac = (math.log10(value/self.__Minimum))/(self.__Range)
        else:
            frac = (value - self.__Minimum)/(self.__Range)
        value = self.Dial.minimum() +\
            frac*(self.Dial.maximum() - self.Dial.minimum())
        self.Dial.setValue(int(value))

    def getLabelValue(self):
        """ Get the label value.
        """
        return float(self.Label.text())

    def setLabelValue(self, value):
        """ Set the labet text.
        """
        self.Label.setText(self.__LabelFmt % value)

    def value(self):
        """ Convenience function.

        Note this returns the value of the text label (not that of the dial),
        as this has in principle arbitrary precision.
        """
        return self.getLabelValue()

    def setValue(self, value):
        """ Set the dial and label values.
        """
        self.setDialValue(value)
        self.setLabelValue(value)

    def updateLabelText(self):
        """ Set the label text.
        """
        self.setLabelValue(self.getDialValue())

    def updateDialValue(self):
        """ Set the dial value.

        Note we wrap the setDialValue() call with a signal/slot
        disconnect/connect in order to prevent the call itself to invoke yet
        another call to updateLabelText()---in which case the final value of
        the text would be forced to snap to the dial grid.
        """
        disconnect(self.Dial, 'valueChanged(int)', self.updateLabelText)
        self.setDialValue(self.getLabelValue())
        connect(self.Dial, 'valueChanged(int)', self.updateLabelText)

    def signalUpdate(self):
        """ Emit the signal to notify the consumers that the widget has been
        updated.
        """
        self.emit(QtCore.SIGNAL('updated(float)'), self.value())



if __name__ == '__main__':

    def testUpdate(value):
        """ Test function for the updated(float) signal.
        """
        print(value)

    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = QtGui.QWidget()
    layout = QtGui.QGridLayout(w)
    d1 = gDialWidget(w, title = None)
    d1.setMinorTicks([i*0.025 for i in range(40)])
    d1.setMajorTicks([0.0, 0.25, 0.5, 0.75, 1])
    layout.addWidget(d1, 0, 0)
    d2 = gDialWidget(w, minimum = 1, maximum = 1e4, logarithmic = True,
                     tickLabelFmt = '%d', title = 'Log dial')
    d2.setMajorTicks([10**i for i in range(5)])
    d2.setMinorTicks([i*10**j for j in range(4) for i in range(1, 10)])
    layout.addWidget(d2, 0, 1)
    connect(d1, 'updated(float)', testUpdate)
    w.show()
    application.exec_() 
