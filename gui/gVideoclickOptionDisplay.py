#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtGui, QtCore

from plasduino.gui.gVideoclickDataDisplay import gVideoclickDataDisplay
from plasduino.gui.gVideoclickZoomPixmapWidget import\
    gVideoclickZoomPixmapWidget



class gVideoclickOptionDisplay(QtGui.QGroupBox):
   
    """ Custom display with check boxes to set the visualization options.
    """

    TOTAL_WIDTH = gVideoclickDataDisplay.LABEL_WIDTH + \
        gVideoclickDataDisplay.DATA_WIDTH
    CHECK_WIDTH = 20
    LABEL_WIDTH = TOTAL_WIDTH - CHECK_WIDTH
    PHYSICAL_UNITS_FIELD_NAME = 'physical_units'
    ALL_POINTS_FIELD_NAME = 'all_points'

    def __init__(self, parent = None, title = '', **kwargs):
        """ Constructor.
        """
        QtGui.QGroupBox.__init__(self, title, parent)
        self.Layout = QtGui.QGridLayout(self)
        self.Layout.setColumnMinimumWidth(0, self.LABEL_WIDTH)
        self.Layout.setColumnMinimumWidth(1, self.CHECK_WIDTH)
        self.Layout.setColumnStretch(0, 1)
        self.Layout.setColumnStretch(1, 0)
        self.LabelWidgetDict = {}
        self.CheckWidgetDict = {}
        self.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding,
                           QtGui.QSizePolicy.Fixed)
        self.addCheckBox(self.PHYSICAL_UNITS_FIELD_NAME,
                         'Coordinates in physical units', False)
        self.addCheckBox(self.ALL_POINTS_FIELD_NAME,
                         'Show all data points', True)

    def addCheckBox(self, key, label, enabled = False):
        """ Add a checl box next to a text label.
        """
        row = self.Layout.rowCount()
        labelWidget = QtGui.QLabel(label, self)
        checkWidget = QtGui.QCheckBox(self)
        self.LabelWidgetDict[key] = labelWidget
        self.CheckWidgetDict[key] = checkWidget
        self.Layout.addWidget(labelWidget, row, 0)
        self.Layout.addWidget(checkWidget, row, 1, QtCore.Qt.AlignRight)
        self.__setEnabled(key, enabled)

    def getPhysicalUnitsCheckBox(self):
        """ Return the check box for the physical units.
        """
        return self.CheckWidgetDict[self.PHYSICAL_UNITS_FIELD_NAME]

    def getAllPointsCheckBox(self):
        """ Return the check box for showing all the data points.
        """
        return self.CheckWidgetDict[self.ALL_POINTS_FIELD_NAME]

    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(gVideoclickZoomPixmapWidget.DEFAULT_SIZE, 10)

    def __enabled(self, key):
        """ Return whether the widgets corresponding to a given key are
        enabled.
        """
        return self.CheckWidgetDict[key].isEnabled()

    def __setEnabled(self, key, enabled = True):
        """ Enable/disable the two widgets corresponding to a given key.
        """
        self.LabelWidgetDict[key].setEnabled(enabled)
        self.CheckWidgetDict[key].setEnabled(enabled)

    def __checked(self, key):
        """ Return whether the check box corresponding to a given key is
        checked.
        """
        return self.CheckWidgetDict[key].isChecked()

    def __setChecked(self, key, checked = True):
        """ Check/uncheck corresponding to a given key.
        """
        self.CheckWidgetDict[key].setChecked(checked)

    def isPhysicalUnitsEnabled(self):
        """ Return whether the physical units widgets are enabled.
        """
        return self.__enabled(self.PHYSICAL_UNITS_FIELD_NAME)

    def setPhysicalUnitsEnabled(self, enabled = True):
        """ Set the physical units widgets enabled.
        """
        self.__setEnabled(self.PHYSICAL_UNITS_FIELD_NAME, enabled)

    def isPhysicalUnitsChecked(self):
        """ Return whether the physical units check box is checked.
        """
        return self.__checked(self.PHYSICAL_UNITS_FIELD_NAME)

    def setPhysicalUnitsChecked(self, checked = True):
        """ Set the physical units check box checked.
        """
        self.__setChecked(self.PHYSICAL_UNITS_FIELD_NAME, checked)
        
    def isAllPointsChecked(self):
        """ Return whether the check box to show all data is checked.
        """
        return self.__checked(self.ALL_POINTS_FIELD_NAME)

    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(gVideoclickZoomPixmapWidget.DEFAULT_SIZE, 10)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    display = gVideoclickOptionDisplay()
    display.show()
    application.exec_() 
