#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

from PyQt4 import QtCore, QtGui
from plasduino.gui.gPushButton import gOkButton
from plasduino.__plasduino__ import PLASDUINO_ROOT, PACKAGE_NAME,\
    PLASDUINO_WEB_PAGE
from plasduino.__tag__ import TAG, BUILD_DATE


class gMessageBox(QtGui.QMessageBox):

    """ Small utility class wrapping a QMessageBox.

    The main thing that we're doing here is being able to set the width.
    The solution for doing it comes from
    http://www.qtcentre.org/threads/22298-QMessageBox-Controlling-the-width?p=113348#post113348
    """

    def __init__(self, parent = None, **kwargs):
        """ Constructor.
        """
        icon = kwargs.get('icon', QtGui.QMessageBox.NoIcon)
        title = kwargs.get('title', 'Message')
        text = kwargs.get('text', '')
        width = kwargs.get('width', 400)
        richText = kwargs.get('richText', False)
        flags = kwargs.get('flags', QtCore.Qt.Dialog)
        buttons = QtGui.QMessageBox.Ok
        QtGui.QMessageBox.__init__(self, icon, title, text, buttons, parent,
                                   flags)
        if richText:
            self.setTextFormat(1)
        if width is not None:
            spacer = QtGui.QSpacerItem(width, 0, QtGui.QSizePolicy.Minimum,
                                       QtGui.QSizePolicy.Expanding)
            layout = self.layout()
            layout.addItem(spacer, layout.rowCount(), 0, 1,
                           layout.columnCount())



class gInfoBox(gMessageBox):

    def __init__(self, parent = None, **kwargs):
        kwargs['title'] = 'Information'
        gMessageBox.__init__(self, parent, **kwargs)



class gWarningBox(gMessageBox):

    def __init__(self, parent = None, **kwargs):
        kwargs['title'] = 'Warning'
        gMessageBox.__init__(self, parent, **kwargs)



class gErrorBox(gMessageBox):

    def __init__(self, parent = None, **kwargs):
        kwargs['title'] = 'Error'
        gMessageBox.__init__(self, parent, **kwargs)



class gFatalErrorBox(gMessageBox):

    def __init__(self, parent = None, **kwargs):
        kwargs['title'] = 'Fatal error'
        gMessageBox.__init__(self, parent, **kwargs)



""" Specific message box for the Help->About drop-down menu.
"""

from plasduino.__copyright__ import COPYRIGHT_HTML

ABOUT_PLASDUINO_TEXT = \
"""<p>This is %s version %s (built on %s).</p>

<p>%s</p>

<p>%s comes with ABSOLUTELY NO WARRANTY. This is free software (and hardware), and you are welcome to redistribute it under certain conditions. See the LICENSE file for details.</p>

<p>Visit <a href=%s>%s</a> for more information.</p>
""" % (PACKAGE_NAME, TAG, BUILD_DATE, COPYRIGHT_HTML, PACKAGE_NAME,
       PLASDUINO_WEB_PAGE, PLASDUINO_WEB_PAGE)

def aboutPlasduinoMessageBox(parent = None):
    """ Plasduino about box.
    """
    return gMessageBox(parent, title = 'About %s' % PACKAGE_NAME,
                       text = ABOUT_PLASDUINO_TEXT, width = 550,
                       richText = True)

def licenseMessageBox(parent = None):
    """ Plasduino licence box.
    """
    box = gMessageBox(parent, title = '%s license' % PACKAGE_NAME)
    filePath = os.path.join(PLASDUINO_ROOT, 'LICENSE')
    licenseText = open(filePath).read()
    textEdit = QtGui.QTextEdit(box)
    textEdit.setMinimumWidth(720)
    textEdit.setMinimumHeight(600)
    textEdit.setStyleSheet("font: 11pt \"Courier\";");
    textEdit.setPlainText(licenseText)
    layout = box.layout()
    layout.addWidget(textEdit, 0, 0, 1, 0)
    return box


def releaseNotesMessageBox(parent = None):
    """ Plasduino licence box.
    """
    from plasduino.__plasduino__ import RELEASE_NOTES_PATH
    box = gMessageBox(parent, title = '%s release notes' % PACKAGE_NAME)
    filePath = os.path.join(RELEASE_NOTES_PATH)
    licenseText = open(filePath).read()
    textEdit = QtGui.QTextEdit(box)
    textEdit.setMinimumWidth(730)
    textEdit.setMinimumHeight(600)
    textEdit.setStyleSheet("font: 11pt \"Courier\";");
    textEdit.setPlainText(licenseText)
    layout = box.layout()
    layout.addWidget(textEdit, 0, 0, 1, 0)
    return box



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    b = aboutPlasduinoMessageBox()
    b.exec_()
    b = gFatalErrorBox(text = 'What happened????')
    b.exec_()
