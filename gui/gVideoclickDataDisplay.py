#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtCore

from plasduino.gui.gDataDisplay import gDataDisplay
from plasduino.gui.gVideoclickZoomPixmapWidget import\
    gVideoclickZoomPixmapWidget



class gVideoclickDataDisplay(gDataDisplay):
    
    """ Custom data display for the videoclick module.
    """

    LABEL_WIDTH = 125
    DATA_WIDTH = 110
    OBJECT_POSITION_FIELD_NAME = 'object_pos'
    FRAME_NUMBER_FIELD_NAME = 'frame_num'
 
    def __init__(self, parent = None, numObjects = 2):
        """ Constructor.
        """
        gDataDisplay.__init__(self, parent, minLabelWidth = self.LABEL_WIDTH,
                              minDataWidth = self.DATA_WIDTH)
        self.__NumObjects = numObjects
        self.addFrameNumberField()
        self.addElapsedTimeField()
        for objectId in range(1, self.__NumObjects + 1):
            self.addObjectPositionField(objectId)
        self.setFieldDataFormat(gDataDisplay.ELAPSED_TIME_FIELD_NAME, '%.2f')
        self.__PhysicalUnitsEnabled = False

    def addFrameNumberField(self, fmt = '%d'):
        """ Add the data field for the frame number.
        """
        self.addField(self.FRAME_NUMBER_FIELD_NAME, 'Frame number', fmt)
        
    def setFrameNumber(self, value):
        """ Set the frame number.
        """
        self.setFieldValue(self.FRAME_NUMBER_FIELD_NAME, value)

    def addObjectPositionField(self, objectId, fmt = '%s', enabled = True):
        """ Add the data field for the position of a generic object.
        """
        fieldName = '%s_%d' % (self.OBJECT_POSITION_FIELD_NAME, objectId)
        self.addField(fieldName, 'Position %d [px]' % objectId, fmt)
        if not enabled:
            self.DataWidgetDict[fieldName].setEnabled(False)
            self.LabelWidgetDict[fieldName].setEnabled(False)

    def setPhysicalUnitsEnabled(self, enabled = True):
        """ Change the units in the text label for the position entries
        according on whether we are displaying the coordinates in raw or
        physical values.
        """
        if enabled != self.__PhysicalUnitsEnabled:
            if enabled:
                units = 'cm'
            else:
                units = 'px'
            for objectId in range(1, self.__NumObjects + 1):
                key = '%s_%d' % (self.OBJECT_POSITION_FIELD_NAME, objectId)
                widget = self.LabelWidgetDict[key]
                widget.setText('Position %d [%s]' % (objectId, units))
            self.__PhysicalUnitsEnabled = enabled
        
    def setObjectPosition(self, objectId, position):
        """ Set the position for a generic object.
        """
        fieldName = '%s_%d' % (self.OBJECT_POSITION_FIELD_NAME, objectId)
        self.setFieldValue(fieldName, position)

    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(gVideoclickZoomPixmapWidget.DEFAULT_SIZE, 10)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    display = gVideoclickDataDisplay()
    display.show()
    application.exec_() 
