#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtCore, QtGui
from plasduino.gui.gPushButton import gPushButton
from plasduino.gui.gQt import connect
from plasduino.gui.__gui__ import WINDOW_MIN_WIDTH


class gTransportBar(QtGui.QGroupBox):

    """ This is a transport bar widget that can be customized (in terms of
    the buttons it features) by passing a suitabls mask to the constructor.

    Each button in the transport bar corresponds to a specific action and
    emits a specific signal when clicked. It is responsibility of the
    downstream code to connect those signals to suitable slots.
    """
    
    ACTION_LIST  = ['begin', 'previous','stop', 'pause', 'start', 'next', 'end',
                    'quit']

    MASK_QUIT    = 0b00000001
    MASK_MINIMAL = 0b00101001
    MASK_NORMAL  = 0b00111001
    MASK_FULL    = 0b11111111

    STATUS_QUIT     = 0b00000001
    STATUS_STOPPED  = 0b11001111
    STATUS_RUNNING  = 0b00110000
    STATUS_PAUSED   = 0b00101000
    STATUS_DISABLED = 0b00000000

    def __init__(self, parent = None, **kwargs):
        """ Constructor.
        """
        title = kwargs.get('title', 'Transport bar')
        mask = kwargs.get('mask', self.MASK_MINIMAL)
        tooltips = kwargs.get('tooltips', {})
        QtGui.QGroupBox.__init__(self, title, parent)
        self.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding,
                           QtGui.QSizePolicy.Fixed)
        separation = kwargs.get('separation', 15)
        layout = QtGui.QHBoxLayout(self)
        self.__ButtonDict = {}
        if mask == self.MASK_QUIT:
            spacer = QtGui.QSpacerItem(separation, 0)
            layout.addItem(spacer)
            layout.setStretch(layout.count() - 1, 1)
        for (i, action) in enumerate(self.ACTION_LIST):
            if (mask >> (len(self.ACTION_LIST) - i - 1)) & 0b1:
                button = gPushButton(self, action = action)
                self.__ButtonDict[action] = button
                func = eval('self.%s' % action)
                connect(button, 'clicked()', func)
                layout.addWidget(button)
                layout.setStretch(layout.count() - 1, 0)
                if action in ['previous', 'start', 'end']:
                    spacer = QtGui.QSpacerItem(separation, 0)
                    layout.addItem(spacer)
                    layout.setStretch(layout.count() - 1, 1)
        self.setTooltip('quit', 'Quit the application')
        self.setTooltips(tooltips)
        self.setLayout(layout)
        self.setWindowTitle(title)
        self.setStatus(self.STATUS_STOPPED)

    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(WINDOW_MIN_WIDTH, 10)
        
    def getButton(self, action):
        """ Retrieve a button by action.
        """
        try:
            return self.__ButtonDict[action]
        except:
            return None

    def setButtonEnabled(self, action, enabled = True):
        """ Enable/disable a single button.
        """
        button = self.getButton(action)
        if button is not None:
            button.setEnabled(enabled)

    def setTooltip(self, action, tooltip):
        """ Set the tooltip for a specific button.
        """
        button = self.getButton(action)
        if button is not None:
            button.setToolTip(tooltip)

    def setTooltips(self, tooltipDict):
        """ Set the tooltips based on a dictionary.
        """
        for (action, tooltip) in tooltipDict.items():
            self.setTooltip(action, tooltip)

    def setStatus(self, status):
        """ Enable/disable buttons based on a bitmap.
        """
        for (i, action) in enumerate(self.ACTION_LIST):
            enabled = (status >> (len(self.ACTION_LIST) - i - 1)) & 0b1
            self.setButtonEnabled(action, enabled)

    def begin(self):
        """ Emit the "begin" signal.
        """
        self.emit(QtCore.SIGNAL('begin()'))

    def previous(self):
        """ Emit the "previous" signal'.
        """
        self.emit(QtCore.SIGNAL('previous()'))

    def stop(self):
        """ Emit the "stop" signal and put the bar in "stopped" mode.
        """
        self.emit(QtCore.SIGNAL('stop()'))
        self.setStatus(self.STATUS_STOPPED)

    def pause(self):
        """ Emit the "pause" signal and put the bar in "paused" mode.
        """
        self.emit(QtCore.SIGNAL('pause()'))
        self.setStatus(self.STATUS_PAUSED)

    def start(self):
        """ Emit the "start" signal and put the bar in "running" mode.
        """
        self.emit(QtCore.SIGNAL('start()'))
        self.setStatus(self.STATUS_RUNNING)

    def next(self):
        """ Emit the "next" signal.
        """
        self.emit(QtCore.SIGNAL('next()'))

    def end(self):
        """ Emit the "end" signal.
        """
        self.emit(QtCore.SIGNAL('end()'))

    def quit(self):
        """ Emit the "quit" signal.
        """
        self.emit(QtCore.SIGNAL('quit()'))
    


if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    bar = gTransportBar(mask = gTransportBar.MASK_FULL)
    connect(bar, 'quit()', bar.close)
    bar.show()
    application.exec_() 
