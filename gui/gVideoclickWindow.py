#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import time

from PyQt4 import QtGui, QtCore
from plasduino.gui.gMainWindow import gMainWindow
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gQt import connect, disconnect
from plasduino.gui.gVideoclickDataDisplay import gVideoclickDataDisplay
from plasduino.gui.gVideoclickSystemDisplay import gVideoclickSystemDisplay
from plasduino.gui.gVideoclickOptionDisplay import gVideoclickOptionDisplay
from plasduino.gui.gVideoclickPixmapWidget import gVideoclickPixmapWidget
from plasduino.gui.gVideoclickZoomPixmapWidget import \
    gVideoclickZoomPixmapWidget
from plasduino.daq.__video__ import getDecoder
from plasduino.__logging__ import logger



class gVideoclickWindow(gMainWindow):
    
    """ Main window for the videoclick application.

    Note that this class encapsulates much more functionalities than the
    gAcquisitionWindow base class used in the arduino application (particularly
    it includes the functionalities that are deferred to the RunControl in
    the arduino application). We might want to revisit this, but the basic
    reason for this design choice is that the video point operation is
    largely tied to the graphical user interface.
    """

    WINDOW_TITLE = 'Video point'
    TOOLTIP_DICT = {'begin'   : 'Go to the beginning of the clip',
                    'previous': 'Go to the previous frame',
                    'stop'    : 'Stop the clip',
                    'pause'   : 'Pause the clip',
                    'start'   : 'Start reproducing the clip',
                    'next'    : 'Go to the next frame',
                    'end'     : 'Go to the end of the clip'
                    }

    def __init__(self, **kwargs):
        """ Constructor.
        """
        kwargs['numLayoutColumns'] = 3
        gMainWindow.__init__(self, **kwargs)
        self.setWindowTitle(self.WINDOW_TITLE)
        self.MenuBar.setMinimumWidth(3*self.width())
        self.insertMenuAction('File', 'Close current clip', self.closeClip)
        self.insertMenuAction('File', 'Open video clip', self.openClip)
        self.PixmapWidget = gVideoclickPixmapWidget(self)
        self.addWidget(self.PixmapWidget, 3, 0, rowSpan = 4,
                       align = QtCore.Qt.AlignTop)
        self.TransportBar = gTransportBar(self, mask = gTransportBar.MASK_FULL)
        self.addWidget(self.TransportBar, 7, 0, align = QtCore.Qt.AlignLeft)
        self.TransportBar.setTooltips(self.TOOLTIP_DICT)
        self.MainGridLayout.setColumnMinimumWidth(1, 5)
        msg = 'Extracting frames (this might take a few seconds)...'
        self.ExtractProgressDialog = QtGui.QProgressDialog(msg, 'Cancel', 0,
                                                           100, self)
        self.ExtractProgressDialog.setWindowTitle(self.WINDOW_TITLE)
        self.setupConnections()
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        decoderName = TOP_LEVEL_CONFIGURATION['video.video-decoder']
        frameFormat = TOP_LEVEL_CONFIGURATION['video.frame-format']
        self.otherFilePath = None
        self.VideoDecoder = getDecoder(decoderName, frameFormat)
        if self.VideoDecoder is None:
            msg = 'Unknown video decoder "%s". Please choose a valid decoder ' \
                'from the Configuration menu.' % decoderName
            self.popupFatalError(msg)
        elif not self.VideoDecoder.installed():
            msg = 'Video decoder %s does not seem to be installed. Please ' \
                'choose a valid decoder from the Configuration menu.' %\
                decoderName
            self.popupFatalError(msg)
        else:
            self.setupVideoConnections()
            self.reset()
        self.repaint()
        self.NumObjects = TOP_LEVEL_CONFIGURATION['video.num-objects']
        self.DataDisplay = gVideoclickDataDisplay(self, self.NumObjects)
        self.addWidget(self.DataDisplay, 3, 2)
        self.SystemDisplay = gVideoclickSystemDisplay(self)
        self.addWidget(self.SystemDisplay, 4, 2)
        self.OptionDisplay = gVideoclickOptionDisplay(self)
        self.addWidget(self.OptionDisplay, 5, 2)
        self.ZoomPixmapWidget = gVideoclickZoomPixmapWidget()
        self.addWidget(self.ZoomPixmapWidget, 6, 2, rowSpan = 2)
        self.PixmapWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def closeEvent(self, event):
        """ Overloaded closeEvent slot.
        """
        try:
            self.VideoDecoder.cleanup(self.otherFilePath)
        except AttributeError:
            pass
        event.accept()

    def disableControls(self):
        """ Disable all the controls on the GUI.
        """
        self.TransportBar.setStatus(gTransportBar.STATUS_QUIT)
        gMainWindow.disableControls(self)

    def reset(self):
        """ Reset the operation.
        """
        self.VideoDecoder.clearInfo()
        self.VideoDecoder.cleanup()
        self.CurrentFrame = None
        self.TransportBar.setStatus(gTransportBar.STATUS_QUIT)
        self.getMenuAction('File', 'Open video clip').setEnabled(True)
        self.getMenuAction('File', 'Close current clip').setEnabled(False)

    def setupConnections(self):
        """ Set up the signal/slot connections.
        """
        connect(self.TransportBar, 'quit()', self.close)
        connect(self.TransportBar, 'start()', self.play)
        connect(self.TransportBar, 'pause()', self.pause)
        connect(self.TransportBar, 'stop()', self.stop)
        connect(self.TransportBar, 'next()', self.next)
        connect(self.TransportBar, 'previous()', self.previous)
        connect(self.TransportBar, 'begin()', self.begin)
        connect(self.TransportBar, 'end()', self.end)

    def setupVideoConnections(self):
        """
        """
        connect(self.VideoDecoder, 'done()', self.openClipFinalize)
        connect(self.VideoDecoder, 'progress(int)',
                self.ExtractProgressDialog.setValue)
        connect(self.ExtractProgressDialog, 'canceled()',
                self.VideoDecoder.terminate)

    def openClip(self):
        """ Open a clip and extract the frames for subsequent anlysis.
        
        Note that the frame extraction is executed on a seprate thread, with
        a progress dialog showing the status of the operation.
        When the frames are ready, the first one is displayed on its
        widget.

        TODO: We don't seem to be able to click on the Cancel button and
        stop the frame extraction. We should fix this.
        """
        from plasduino.gui.gFileDialog import getOpenFilePath, \
            FILE_TYPE_AVI, FILE_TYPE_FLV, FILE_TYPE_MP4, FILE_TYPE_3GP
        filePath = getOpenFilePath([FILE_TYPE_AVI, \
                                    FILE_TYPE_FLV, \
                                    FILE_TYPE_MP4, \
                                    FILE_TYPE_3GP], self, 'Open clip')
        self.otherFilePath = None
        if filePath is None:
            return
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        frameRate = TOP_LEVEL_CONFIGURATION['video.frame-rate']
        self.VideoDecoder.open(filePath)

        if self.VideoDecoder.FrameRate == None:
            msg = 'Cannot find video frame rate.\n' \
                  'Do you want to try reformatting the video '\
                  'forcing the frame rate to 25 fps?\n'\
                  'This may take some time...'
            reply = QtGui.QMessageBox.warning(self, 'Warning', msg, 
                                              QtGui.QMessageBox.Yes, 
                                              QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                self.otherFilePath = self.VideoDecoder.formatFrameRate()
                self.VideoDecoder.open(self.otherFilePath)

        if not self.VideoDecoder.hasInfo():
            msg = 'Cannot retrieve video clip information.\n\n' \
                'Make sure you have one of the supported video decoders ' \
                'correctly intalled and properly selected in the ' \
                'configuration editor (also, make sure that the input file ' \
                'is not corrupted and contains a valid media stream).'
            self.popupFatalError(msg)
            return
        if frameRate is not None:
            self.VideoDecoder.forceFrameRate(frameRate)
        self.ExtractProgressDialog.setValue(0)
        self.ExtractProgressDialog.show()
        self.repaint()
        self.VideoDecoder.start()
            
    def openClipFinalize(self):
        """ Finalize the open clip process.
        """
        self.ExtractProgressDialog.setValue(100)
        self.ExtractProgressDialog.hide()
        gVideoclickWindow.setFrame(self, 1)
        self.PixmapWidget.setZoomPixmapLabel(self.ZoomPixmapWidget)
        self.ZoomPixmapWidget.setEnabled(True)
        self.TransportBar.setStatus(0b00000111)
        self.getMenuAction('File', 'Close current clip').setEnabled(True)
        self.getMenuAction('File', 'Open video clip').setEnabled(False)

    def closeClip(self):
        """ Close the current clip.
        """
        self.reset()
        self.PixmapWidget.reset()
        self.ZoomPixmapWidget.reset()

    def setPixmap(self, filePath):
        """ Set the image to be displayed in the pixmap widget.
        """
        if not os.path.exists(filePath):
            logger.error('Could not find %s.' % filePath)
            return
        self.PixmapWidget.setPixmap(filePath)

    def repaintFrame(self):
        """ Repaint the current frame.
        """
        self.setPixmap(self.VideoDecoder.getFrameFilePath(self.CurrentFrame))

    def setFrame(self, frameNumber):
        """ Set the frame to be displayed on the main pixmap.

        Note that this assumes we have a valid gVideoDecoder object available.
        If that's not the case we print an error message and exit.
        
        We also take care, here, of enabling/disabling the transport bar
        buttons. And yes, there's a lot of boring logics to pay attention to
        ensure we don't violate the clip bounds...
        """
        if self.VideoDecoder is None:
            logger.error('Cannot set frame with no valid video clip available.')
            return
        if frameNumber < 1 or frameNumber > self.VideoDecoder.getNumFrames():
            logger.error('Invalid frame number (%s).' % frameNumber)
            return
        self.CurrentFrame = frameNumber
        self.setPixmap(self.VideoDecoder.getFrameFilePath(frameNumber))
        # Are we at the begin?
        if self.CurrentFrame == 1:
            self.TransportBar.setButtonEnabled('previous', False)
            self.TransportBar.setButtonEnabled('begin', False)
            self.TransportBar.setButtonEnabled('next', True)
            self.TransportBar.setButtonEnabled('end', True)
        # Or at the end?
        elif self.CurrentFrame == self.VideoDecoder.getNumFrames():
            self.TransportBar.setButtonEnabled('previous', True)
            self.TransportBar.setButtonEnabled('begin', True)
            self.TransportBar.setButtonEnabled('next', False)
            self.TransportBar.setButtonEnabled('end', False)
        # Or in the middle?
        else:
            self.TransportBar.setButtonEnabled('previous', True)
            self.TransportBar.setButtonEnabled('begin', True)
            self.TransportBar.setButtonEnabled('next', True)
            self.TransportBar.setButtonEnabled('end', True)

    def play(self):
        """
        """
        pass

    def pause(self):
        """
        """
        pass

    def stop(self):
        """
        """
        pass

    def next(self):
        """ Go the next frame.
        """
        self.setFrame(self.CurrentFrame + 1)

    def previous(self):
        """ Go to the previous frame.
        """
        self.setFrame(self.CurrentFrame - 1)

    def begin(self):
        """ Go the first frame.
        """
        self.setFrame(1)

    def end(self):
        """ Go to the last frame.
        """
        self.setFrame(self.VideoDecoder.getNumFrames())



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gVideoclickWindow()
    w.show()
    application.exec_()
