#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

from PyQt4 import QtCore, QtGui
from plasduino.__plasduino__ import PLASDUINO_ARTWORK
from plasduino.gui.gQt import connect


class gPushButton(QtGui.QPushButton):

    """ Small convenience class wrapping a QtGui.QPushButton.
    """
    
    def __init__(self, parent = None, **kwargs):
        """ Constructor.
        """
        QtGui.QPushButton.__init__(self, parent)
        action = kwargs.get('action', None)
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        skin = TOP_LEVEL_CONFIGURATION.get('gui.skin')
        if action is not None:
            self.setupIcon(action, skin)

    def setupIcon(self, action, skin):
        """ Setup the icon set for the button.
        """
        folderPath = os.path.join(PLASDUINO_ARTWORK, 'skins', skin)
        filePath = os.path.join(folderPath, '%s_enabled.svg' % action)
        pixmap = QtGui.QPixmap(filePath)
        icon = QtGui.QIcon(pixmap)
        filePath = os.path.join(folderPath, '%s_disabled.svg' % action)
        pixmap = QtGui.QPixmap(filePath)
        icon.addPixmap(pixmap, QtGui.QIcon.Disabled)
        self.setIcon(icon)
        self.setIconSize(QtCore.QSize(pixmap.size()))
        self.setFixedSize(QtCore.QSize(pixmap.size()))



class gTextPushButton(QtGui.QPushButton):
    
    """ Small convenience class for a button with fixed size, some text,
    and an optional slot to be attached to.
    """

    WIDTH = 70
    HEIGHT = 35

    def __init__(self, text, parent = None, slot = None):
        """ Constructor.
        """
        QtGui.QPushButton.__init__(self, text, parent)
        self.setFixedSize(self.WIDTH, self.HEIGHT)
        if slot is not None:
            connect(self, 'clicked()', slot)

    

class gOkButton(gTextPushButton):

    def __init__(self, parent = None, slot = None):
        """ Constructor.
        """
        gTextPushButton.__init__(self, 'Ok', parent, slot)



class gSaveButton(gTextPushButton):

    def __init__(self, parent = None, slot = None):
        """ Constructor.
        """
        gTextPushButton.__init__(self, 'Save', parent, slot)



class gCancelButton(gTextPushButton):

    def __init__(self, parent = None, slot = None):
        """ Constructor.
        """
        gTextPushButton.__init__(self, 'Cancel', parent, slot)
