#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.gui.gDataDisplay import gDataDisplay
from plasduino.gui.__gui__ import  DATA_WIDGET_DATA_WIDTH
from PyQt4 import QtCore
SYNC_LABEL_WIDTH = 130

class gAcquisitionDataDisplay(gDataDisplay):
    
    """ Standard data display for data acquisition.
    """

    def __init__(self, parent = None):
        gDataDisplay.__init__(self, parent)
        self.addRunIdField()
        self.addNumEventsField()
        self.addElapsedTimeField()
        self.addAverageRateField()

class gSynchAcquisitionDataDisplay(gDataDisplay):
    
    """ Data display for synchronous data acquisition.
        In this care readout rate is constant, 
        so we don't need fields like Average Rate
    """

    def __init__(self, parent = None, minLabelW = SYNC_LABEL_WIDTH):
        gDataDisplay.__init__(self, parent, minLabelWidth = minLabelW)
        self.minLabelWidth = minLabelW
        self.addRunIdField()
        self.addNumEventsField()
        self.addElapsedTimeField()

    def sizeHint(self):
        """ Overloaded function for managing the size policy.
            Needs to be adapted since we may want this diplay on a side
        """
        return QtCore.QSize(self.minLabelWidth +DATA_WIDGET_DATA_WIDTH, 10)

if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    d = gAcquisitionDataDisplay()
    d.setRunId(313)
    d.show()
    application.exec_() 
