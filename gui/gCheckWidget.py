#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtCore, QtGui
from plasduino.gui.gQt import connect
from plasduino.__logging__ import abort


def getWidgetValue(widget):
    """ Return the value hold by a generic widget.

    Depending on the widget type this might be really mean returning the
    value() or the text() or the currentText() or whatever is appropriate.

    Note that the function references to itself in case we are passing over
    a composite gCheckWidget object.
    """
    if isinstance(widget, gCheckWidget):
        return getWidgetValue(widget.Widget)
    if isinstance(widget, QtGui.QSpinBox):
        return widget.value()
    elif isinstance(widget, QtGui.QLineEdit):
        return widget.text()
    elif isinstance(widget, QtGui.QComboBox):
        return widget.currentText()
    else:
        abort('Could not retrieve value for %s. Abort' % widget)


class gCheckWidget(QtGui.QWidget):

    """ Small class describing a generic widget with a check box attached.

    The check box is used to enable/disable the widget.
    """

    def __init__(self, widget, parent = None, checked = True):
        """ Constructor.
        """
        QtGui.QWidget.__init__(self, parent)
        self.Layout = QtGui.QHBoxLayout(self)
        self.Layout.setContentsMargins(0, 0, 15, 0)
        self.setLayout(self.Layout)
        self.Widget = widget
        self.CheckBox = QtGui.QCheckBox(parent)
        self.CheckBox.setFixedWidth(18)
        self.Layout.addWidget(self.Widget, 1)
        self.Layout.addWidget(self.CheckBox, 0)
        self.setChecked(checked)
        connect(self.CheckBox, 'stateChanged(int)', self.Widget.setEnabled)
                
    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(125, 10)

    def setChecked(self, checked = True):
        """
        """
        self.CheckBox.setChecked(checked)
        self.Widget.setEnabled(checked)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gCheckWidget(QtGui.QLineEdit())
    w.show()
    application.exec_() 
        
