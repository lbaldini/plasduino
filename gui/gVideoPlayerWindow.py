#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import sys
import time

from PyQt4 import QtCore, QtGui, phonon
from plasduino.gui.gMainWindow import gMainWindow
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gQt import connect


class gVideoPlayerWindow(gMainWindow):
    
    """ 
    """

    WINDOW_TITLE = 'Video player'

    def __init__(self, **kwargs):
        """ Constructor.
        """
        gMainWindow.__init__(self, pixmap = None, **kwargs)
        self.setWindowTitle(self.WINDOW_TITLE)
        self.Player = phonon.Phonon.VideoPlayer(phonon.Phonon.VideoCategory,
                                                self)
        self.addWidget(self.Player)
        self.TransportBar = gTransportBar(self, mask = gTransportBar.MASK_FULL)
        self.addWidget(self.TransportBar)
        self.setupConnections()
        self.CurrentFrame = None

    def setupConnections(self):
        """
        """
        connect(self.TransportBar, 'quit()', self.close)
        connect(self.TransportBar, 'start()', self.play)
        connect(self.TransportBar, 'pause()', self.pause)
        connect(self.TransportBar, 'stop()', self.stop)
        connect(self.TransportBar, 'next()', self.next)
        connect(self.TransportBar, 'previous()', self.previous)

    def load(self, filePath):
        """
        """
        self.Player.load(phonon.Phonon.MediaSource(filePath))
        #self.Player.mediaObject().stateChanged.connect(self.test)
        self.Player.pause()
        self.CurrentFrame = 0

    def play(self):
        """
        """
        self.Player.play()

    def pause(self):
        """
        """
        self.Player.pause()

    def stop(self):
        """
        """
        self.Player.stop()

    def next(self):
        """
        """
        print(self.Player.mediaObject().totalTime())
        self.Player.seek(9002)
        time.sleep(0.5)
        print(self.Player.currentTime())

    def previous(self):
        """
        """
        self.Player.seek(0)
        time.sleep(0.5)
        print(self.Player.currentTime())



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    application.setApplicationName('PlasduinoVideoPlayer')
    w = gVideoPlayerWindow()
    w.load('/data/luca/work/presentations/2010_highschool/video/LAT.avi')
    w.show()
    application.exec_()
