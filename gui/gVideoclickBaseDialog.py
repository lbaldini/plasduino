#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtGui

from plasduino.gui.gQt import connect, disconnect


class gVideoclickBaseDialog(QtGui.QDialog):

    """ Base class for the dialogs to set the origin and the scale factor
    for the reference system.
    """

    def __init__(self, parent = None, windowTitle = 'plasduino',
                 text = 'Howdy?'):
        """ Constructor.

        This is usually called with a videoclick main window as parent,
        and we do provide do-nothing slots in this base class to be
        connected with the right/left mouse clicks on the pixmap widget of
        the main window itself.
        """
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(windowTitle)
        self.setFixedWidth(400)
        self.Layout = QtGui.QGridLayout(self)
        self.Label = QtGui.QLabel()
        self.Label.setWordWrap(True)
        self.Label.setText(text)
        self.Layout.addWidget(self.Label, 0, 0, 1, 2)
        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Save |
                                           QtGui.QDialogButtonBox.Cancel)
        self.SaveButton = buttonBox.button(QtGui.QDialogButtonBox.Save)
        self.SaveButton.setEnabled(False)
        connect(buttonBox, 'accepted()', self.accept)
        connect(buttonBox, 'rejected()', self.reject)
        self.Layout.addWidget(buttonBox, 2, 0, 1, 2)
        try:
            self.__PixmapWidget = self.parent().PixmapWidget
        except AttributeError:
            self.__PixmapWidget = None
        self.acceptMouseEvents()

    def acceptMouseEvents(self):
        """ Connect the appropriate slot to react to the mouse clicks on the
        parent pixmap.
        """
        if self.__PixmapWidget is not None:
            connect(self.__PixmapWidget, 'frameLeftClicked(int, int)',
                    self.leftClick)
            connect(self.__PixmapWidget, 'frameRightClicked(int, int)',
                    self.rightClick)

    def ignoreMouseEvents(self):
        """ Disconnect from the parent pixmap.

        This should be called when the dialog is closed.
        """
        if self.__PixmapWidget is not None:
            disconnect(self.__PixmapWidget, 'frameLeftClicked(int, int)',
                       self.leftClick)
            disconnect(self.__PixmapWidget, 'frameRightClicked(int, int)',
                       self.rightClick)

    def leftClick(self):
        """ Do-nothing slot.
        """
        pass

    def rightClick(self):
        """ Do-nothing slot.
        """
        pass

    def reject(self):
        """ Overloade reject() method
        """
        self.ignoreMouseEvents()
        if self.parent() is not None:
            self.parent().repaintFrame()
        QtGui.QDialog.reject(self)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    dialog = gVideoclickBaseDialog()
    dialog.exec_()
