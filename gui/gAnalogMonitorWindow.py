#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import PyQt4.Qwt5 as Qwt

from PyQt4 import QtCore, QtGui, Qt
from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.gui.gPlotWidget import gPlotWidget
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gAcquisitionDataDisplay import gSynchAcquisitionDataDisplay
from plasduino.gui.gQt import connect
from plasduino.gui.gStripChart import gStripChart
from plasduino.gui.gStripChartGroup import gStripChartGroup
from plasduino.gui.gFileDialog import getOpenFilePath, FILE_TYPE_TXT,\
    FILE_TYPE_CSV
from plasduino.__logging__ import logger


class gAnalogMonitorWindow(gAcquisitionWindow):
    
    """
    """

    WINDOW_TITLE = 'Analog monitor'

    def __init__(self, **kwargs):
        """ Constructor.
        """
        kwargs['numLayoutColumns'] = kwargs.get('numLayoutColumns', 3)
        self.__VerticalLayout = kwargs.get('verticalLayout', True)
        gAcquisitionWindow.__init__(self, **kwargs)
        self.insertMenuAction('File', 'Load data from file', self.loadFile, 0)
        self.StripChartGroup = gStripChartGroup(self.PlotWidget)
        self.Zoomer = Qwt.QwtPlotZoomer(Qwt.QwtPlot.xBottom,
                                        Qwt.QwtPlot.yLeft,
                                        Qwt.QwtPicker.DragSelection,
                                        Qwt.QwtPicker.AlwaysOff,
                                        self.PlotWidget.canvas())
        self.Zoomer.setRubberBandPen(Qt.QPen(Qt.Qt.black))
        self.disableZooming()

    def loadFile(self):
        """ Load data from a txt/csv file.
        """
        caption = 'Load data from file'
        filePath = getOpenFilePath([FILE_TYPE_TXT, FILE_TYPE_CSV], self,
                                   caption)
        if filePath is not None:
            from plasduino.daq.gDataTable import readDataTable
            table = readDataTable(filePath)
            if table is None:
                msg = 'This does not look like a properly formatted table.'\
                    '\n\nUnable to load data.'
                logger.error(msg.replace('\n\n', ' '))
                self.popupError(msg)
                return
            if not hasattr(self, 'RunControl'):
                msg = 'The main window has no RunControl member ' \
                    '(no actual data will be loaded).'
                self.popupError(msg)
                return
            if table.Creator != self.RunControl.EventBuffer.__class__.__name__:
                msg = 'This doesn\'t look like a suitable table '\
                    '(the table creator is %s while it should really '\
                    'be %s).\n\nUnable to load data.' %\
                    (table.Creator,
                     self.RunControl.EventBuffer.__class__.__name__)
                logger.error(msg.replace('\n\n', ' '))
                self.popupError(msg)
                return
            self.loadData(table)

    def loadData(self, table):
        """ Do nothing loadData() method.
        """
        logger.error('loadData() method not implemented, yet.')

    def setup(self):
        """ Setup the widgets on the gui.
        """
        self.PlotWidget = gPlotWidget(self, xTitle = 'Time [s]')
        self.DataDisplay = gSynchAcquisitionDataDisplay(self)
        self.TransportBar = gTransportBar(self, tooltips = \
                                              gAcquisitionWindow.TOOLTIP_DICT)
        connect(self.TransportBar, 'quit()', self.close)
        if self.__VerticalLayout:
            self.addWidget(self.PlotWidget, 3, 0, 1, 3)
            self.addWidget(self.TransportBar, 4, 0, align = QtCore.Qt.AlignTop)
            self.TransportBar.setMaximumWidth(350)
            self.addItem(QtGui.QSpacerItem (150, 0), 4, 1)
            self.addWidget(self.DataDisplay, 4, 2, align = QtCore.Qt.AlignTop)
        else:
            self.TransportBar.setMaximumWidth(350)
            self.addWidget(self.PlotWidget, 3, 0, 3, 1)
            self.addItem(QtGui.QSpacerItem (50, 0), 3, 1)
            self.addWidget(self.DataDisplay, 3, 2, align = QtCore.Qt.AlignTop)
            self.addWidget(self.TransportBar, 5, 2, align = QtCore.Qt.AlignTop)

    def synchronousUpdate(self, *args):
        """ Standard synchronous update slot.

        This is automatically connected to a QTimer in the RunControl. It
        assumes that the main window owns a gAcquisitionDataDisplay object.
        If you replace it with a custom data display you'll have to overload
        this one too.
        """
        self.DataDisplay.setNumEvents(len(self.RunControl.getEventBuffer()))
        self.DataDisplay.setElapsedTime(self.RunControl.getElapsedTime())

    def enableZooming(self):
        """ Enable zooming.
        """
        self.Zoomer.setEnabled(True)
        self.Zoomer.setZoomBase()

    def disableZooming(self):
        """ Disable zooming.
        
        Need to explicitely set the autoscale flag on the y axis, here,
        in case the zoom has been used in the previous run.

        Note that plot axes are defined as
        enum  	Axis {
        yLeft,
        yRight,
        xBottom,
        xTop,
        axisCnt
        }
        """
        self.Zoomer.setEnabled(False)
        self.PlotWidget.setAxisAutoScale(0)

    def addChart(self, name, bufferSize = 1000, **kwargs):
        """ Convenience function.
        """
        self.StripChartGroup.add(name, bufferSize, **kwargs)

    def clearCharts(self):
        """ Clear all the plots on the main plot widget.
        """
        self.StripChartGroup.clear()

    def updateCharts(self):
        """ Do-nothing method to be reimplemented in derived classes.
        """
        pass

 



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gAnalogMonitorWindow()
    w.show()
    application.exec_() 
