#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

from PyQt4 import QtCore, QtGui
from plasduino.gui.gMainWindow import gMainWindow
from plasduino.gui.gAcquisitionDataDisplay import gAcquisitionDataDisplay
from plasduino.gui.gAcquisitionDataInput import gAcquisitionDataInput
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gQt import connect
from plasduino.__logging__ import logger


class gAcquisitionWindow(gMainWindow):
    
    """ Specialized main window for data acquisition, featuring a
    general-purpose data display and a transport bar.
    """

    WINDOW_TITLE = 'Acquisition window'
    TOOLTIP_DICT = {'start': 'Start the data acquisition',
                    'stop' : 'Stop the data acquisition',
                    'pause': 'Pause the data acquisition'
                    }

    def __init__(self, **kwargs):
        """ Constructor.

        The class relies on havinf certain members (that might be set to the
        initial value None). Do not overload the constructor, overload
        the setup() method instead.
        """
        gMainWindow.__init__(self, **kwargs)
        self.DataDisplay = None
        self.DataInput = None
        self.TransportBar = None
        self.setup()

    def setup(self):
        """ Set up the necessary widgets in the window.

        Derived classes should overload this function to achieve the
        derived behavior.
        """
        self.DataDisplay = gAcquisitionDataDisplay(self)
        self.addWidget(self.DataDisplay)
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        if TOP_LEVEL_CONFIGURATION.get('gui.run-control-params'):
            self.DataInput = gAcquisitionDataInput(self)
            self.addWidget(self.DataInput)
        self.TransportBar = gTransportBar(self, tooltips = self.TOOLTIP_DICT)
        self.TransportBar.setTooltips(self.TOOLTIP_DICT)
        self.addWidget(self.TransportBar)
        connect(self.TransportBar, 'quit()', self.close)

    def disableControls(self):
        """ Disable all the controls on the GUI.
        """
        if self.DataDisplay is not None:
            self.DataDisplay.setEnabled(False)
        if self.DataInput is not None:
            self.DataInput.setEnabled(False)
        if self.TransportBar is not None:
            self.TransportBar.setStatus(gTransportBar.STATUS_QUIT)
        gMainWindow.disableControls(self)

    def setFieldValue(self, key, value):
        """ Set the value of a specific data field.
        """
        self.DataDisplay.setFieldValue(key, value)

    def resetFieldValue(self, key):
        """ Reset the value of a specific field.
        """
        self.DataDisplay.resetFieldValue(key)

    def setRunId(self, runId):
        """ Set the run number.
        """
        self.DataDisplay.setRunId(runId)

    def update(self, *args):
        """ Do nothing function.

        This can be overloaded in the derived classes and connected to a signal
        in order to get the window updated on an event by event basis.
        """
        pass

    def synchronousUpdate(self, *args):
        """ Standard synchronous update slot.

        This is automatically connected to a QTimer in the RunControl. It
        assumes that the main window owns a gAcquisitionDataDisplay object.
        If you replace it with a custom data display you'll have to overload
        this one too.
        """
        self.DataDisplay.setNumEvents(len(self.RunControl.getEventBuffer()))
        self.DataDisplay.setElapsedTime(self.RunControl.getElapsedTime())
        self.DataDisplay.setAverageRate(self.RunControl.getAverageRate())
        


if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gAcquisitionWindow()
    w.setRunId(313)
    w.show()
    application.exec_() 

