#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Jacopo Nespolo <j.nespolo@gmail.com>           *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



import sys
from PyQt4 import QtGui, QtCore
from plasduino.gui.__gui__ import getWindowIcon, getWindowLogo
from plasduino.gui.gQt import connect
from plasduino.gui.gMainWindow import gMainWindow
from plasduino.gui.gScrollableLabel import gScrollableLabel
from plasduino.__logging__ import logger, abort



class gChooseModuleDialog(QtGui.QDialog):
    """ QtWidget subclass to list the available modules and launch,
    show some info and lauch them.

    Note that at the beginning all the items in the QListWidget are
    deselected and the open button is disabled (it will be enabled as soon as
    an item is selected).
    """
    
    def __init__(self, **kwargs):
        """ Constructor.
        """
        QtGui.QDialog.__init__(self)
        self.MainGridLayout = QtGui.QGridLayout(self)
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        skin = TOP_LEVEL_CONFIGURATION.get('gui.skin')
        self.setWindowIcon(getWindowIcon(skin))
        self.MainGridLayout.addWidget(getWindowLogo(skin), 0, 0,
                                      QtCore.Qt.AlignCenter)
        self.Splitter = QtGui.QSplitter(self)
        self.Splitter.setOrientation(QtCore.Qt.Horizontal)
        self.Splitter.setSizePolicy(QtGui.QSizePolicy.Expanding,
                                    QtGui.QSizePolicy.Expanding)
        self.MainGridLayout.setRowMinimumHeight(1, 20)
        self.MainGridLayout.addWidget(self.Splitter, 2, 0)
        self.LeftBox = QtGui.QGroupBox(self.Splitter)
        self.LeftBox.setTitle('Available modules')
        self.LeftBox.setMinimumWidth(200)
        self.LeftLayout = QtGui.QGridLayout(self.LeftBox)
        self.ListWidget = QtGui.QListWidget(self.LeftBox)
        self.LeftLayout.addWidget(self.ListWidget, 0, 0, 1, 3)
        self.AddButton = QtGui.QPushButton(self.LeftBox)
        self.AddButton.setText('Add...')
        self.LeftLayout.addWidget(self.AddButton, 1, 0)
        self.RemoveButton = QtGui.QPushButton(self.LeftBox)
        self.RemoveButton.setText('Remove...')
        self.RemoveButton.setEnabled(False)
        self.LeftLayout.addWidget(self.RemoveButton, 1, 2)
        self.RightBox = QtGui.QGroupBox(self.Splitter)
        self.RightBox.setTitle('Description')
        self.RightBox.setMinimumWidth(420)
        self.RightLayout = QtGui.QGridLayout(self.RightBox)
        self.DescriptionLabel = gScrollableLabel(self.RightBox)
        self.RightLayout.addWidget(self.DescriptionLabel)
        self.RightButtonBox = QtGui.QDialogButtonBox(self.RightBox)
        self.RightButtonBox.setStandardButtons(QtGui.QDialogButtonBox.Open | 
                                               QtGui.QDialogButtonBox.Close)
        self.setOpenButtonEnabled(False)
        self.adjustSize()
        self.setSizePolicy(QtGui.QSizePolicy.Preferred,
                           QtGui.QSizePolicy.Preferred)
        connect(self.RightButtonBox, 'accepted()', self.accept)
        connect(self.RightButtonBox, 'rejected()', self.reject)
        self.RightLayout.addWidget(self.RightButtonBox)
        connect(self.ListWidget, 'itemSelectionChanged()', self.showDescription)

    def reset(self):
        """ Clear all selection and display a welcome message on the
        launcher.
        """
        msg = 'Welcome to the main plasduino launcher!\n\nSelect an ' \
            'application from the list on the left and click the "Open" ' \
            'button to launch it.'
        self.ListWidget.clearSelection()
        self.DescriptionLabel.setText(msg)
        self.setOpenButtonEnabled(False)

    def setOpenButtonEnabled(self, enabled = True):
        """ Enable/disable the open button.
        """
        button = self.RightButtonBox.button(QtGui.QDialogButtonBox.Open)
        button.setEnabled(enabled)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    dialog = gChooseModuleDialog()
    dialog.Dictionary = {'key1': 'value1', 'key2': 'value2'}
    dialog.populateListWidget()
    dialog.DescriptionLabel.setText("Like many programmers of his generation, Torvalds had cut his teeth not on mainframe computers like the IBM 7094, but on a motley assortment of home-built computer systems. As university student, Torvalds had made the step up from C programming to Unix, using the university's MicroVAX. This ladder-like progression had given Torvalds a different perspective on the barriers to machine access. For Stallman, the chief barriers were bureaucracy and privilege. For Torvalds, the chief barriers were geography and the harsh Helsinki winter. Forced to trek across the University of Helsinki just to log in to his Unix account, Torvalds quickly began looking for a way to log in from the warm confines of his off-campus apartment")
    dialog.exec_()
    print(dialog.getCurrentModuleInfo())
