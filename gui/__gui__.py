#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

from PyQt4 import QtGui
from plasduino.__plasduino__ import PLASDUINO_SKINS, PLASDUINO_ARTWORK


DATA_WIDGET_LABEL_WIDTH = 250
DATA_WIDGET_DATA_WIDTH = 125
DATA_WIDGET_CHECK_BOX_WIDTH = 20
LAYOUT_MARGINS = 20
WINDOW_MIN_WIDTH = DATA_WIDGET_LABEL_WIDTH + DATA_WIDGET_DATA_WIDTH + 40

def listSkins():
    """ List the available skins.
    """
    return os.listdir(PLASDUINO_SKINS)


def getWindowIcon(skin):
    """ Return the plasduino window icon for a given skin.
    """
    skinFolderPath = os.path.join(PLASDUINO_ARTWORK, 'skins', skin)
    iconFilePath = os.path.join(skinFolderPath, 'plasduino_icon.png')
    return QtGui.QIcon(iconFilePath)

def getWindowLogo(skin):
    """ Return the plasduino window icon for a given skin.
    """
    skinFolderPath = os.path.join(PLASDUINO_ARTWORK, 'skins', skin)
    logoFilePath = os.path.join(skinFolderPath, 'plasduino_logo.png')
    pixmapLabel = QtGui.QLabel()
    pixmap = QtGui.QPixmap(logoFilePath)
    pixmapLabel.setPixmap(pixmap)
    return pixmapLabel

