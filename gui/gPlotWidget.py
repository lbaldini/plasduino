#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import PyQt4.Qwt5 as Qwt

from PyQt4 import Qt, QtGui, QtCore
from plasduino.gui.gQt import connect


class gLegend(Qwt.QwtLegend):

    """ Small convenience class wrapping Qwt.QwtLegend.
    """

    def __init__(self):
        """ Constructor.
        """
        Qwt.QwtLegend.__init__(self)
        self.setFrameStyle(Qt.QFrame.NoFrame)
        self.setItemMode(Qwt.QwtLegend.ClickableItem)

    def insert(self, plotItem, legendItem):
        """
        """
        Qwt.QwtLegend.insert(self, plotItem, legendItem)



class gPlotWidget(Qwt.QwtPlot):

    """ Small wrapper around the Qwt.QwtPlot class (this is essentially a
    widget to embed plots in a gui).

    We provide interfaces to some of the basic configuration settings through
    keyword arguments.
    """

    def __init__(self, parent = None, **kwargs):
        """ Constructor.
        """
        Qwt.QwtPlot.__init__(self, parent)
        self.__MinimumWidth = kwargs.get('minimumWidth', 900)
        self.__MinimumHeight = kwargs.get('minimumHeight', 320)
        backgroundColor = kwargs.get('backgroundColor', Qt.Qt.white)
        legend = kwargs.get('legend', True)
        xTitle = kwargs.get('xTitle', None)
        yTitle = kwargs.get('yTitle', None)
        grids = kwargs.get('grids', True)
        mouseTracking = kwargs.get('mouseTracking', True)
        self.setMinimumSize(self.__MinimumWidth, self.__MinimumHeight)
        self.setCanvasBackground(backgroundColor)
        self.alignScales()
        if xTitle is not None:
            self.setXTitle(xTitle)
        if yTitle is not None:
            self.setYTitle(yTitle)
        if grids:
            self.addGrids()
        if legend:
            self.Legend = gLegend()
            self.insertLegend(self.Legend, Qwt.QwtPlot.TopLegend)
        if mouseTracking:
            self.Picker = Qwt.QwtPlotPicker(self.canvas())
            self.Picker.setTrackerMode(Qwt.QwtPlotPicker.AlwaysOn)
        self.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding,
                           QtGui.QSizePolicy.MinimumExpanding)

    def sizeHint(self):
        """ Overload method for the size policy,
        """
        return QtCore.QSize(self.__MinimumWidth, self.__MinimumHeight)

    def setXTitle(self, title):
        """ Set the title for the x axis.
        """
        self.setAxisTitle(Qwt.QwtPlot.xBottom, title)

    def setYTitle(self, title):
        """ Set the title for the y axis.
        """
        self.setAxisTitle(Qwt.QwtPlot.yLeft, title)

    def addGrids(self):
        """ Add grids on the canvas.
        """
        self.Grid = Qwt.QwtPlotGrid()
        self.Grid.attach(self)
        self.Grid.setPen(Qt.QPen(Qt.Qt.black, 0, Qt.Qt.DotLine))

    def alignScales(self):
        self.canvas().setFrameStyle(Qt.QFrame.Box | Qt.QFrame.Plain)
        self.canvas().setLineWidth(1)
        for i in range(Qwt.QwtPlot.axisCnt):
            scaleWidget = self.axisWidget(i)
            if scaleWidget:
                scaleWidget.setMargin(0)
            scaleDraw = self.axisScaleDraw(i)
            if scaleDraw:
                scaleDraw.enableComponent(Qwt.QwtAbstractScaleDraw.Backbone,
                                          False)


if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    widget = gPlotWidget(xTitle = 'Elapsed time [s]', yTitle = 'ADC counts')
    widget.show()
    application.exec_() 
    
