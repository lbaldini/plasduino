#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

import plasduino.daq.__audio__ as __audio__
import plasduino.arduino.__boards__ as __boards__

from PyQt4 import QtCore, QtGui
from plasduino.__install__ import CONFIG_FILE_PATH
from plasduino.gui.gQt import connect
from plasduino.__logging__ import logger
from plasduino.gui.gDataInput import gDataInput
from plasduino.gui.gPushButton import gSaveButton, gCancelButton
from plasduino.gui.gComboBox import gComboBox, gBinaryComboBox
from plasduino.gui.__gui__ import listSkins, getWindowIcon
from plasduino.gui.gSpinBox import gSpinBox, gNoneSpinBox
from plasduino.gui.gFileDialog import gFolderDialogWidget, gFileDialogWidget
from plasduino.sensors.__sensors__ import listSensors
from plasduino.daq.__video__ import FRAME_FORMATS, VIDEO_DECODERS


""" List of the configuration settings that are not implemented yet
(the corresponding widgets will be therefore disabled).

The entries should be removed from the list as we implement the remaining
features.
"""
PHONY_SETTINGS = [('gui', 'language'),
                  ('daq', 'max-acquisition-time'),
                  ('daq', 'max-num-events'),
                  ('daq', 'disable-log-file'),
                  ('arduino', 'arduino-port'),
                  ('arduino', 'arduino-board'),
                  ('arduino', 'vendor-id'),
                  ('arduino', 'product-id')
                  ]


class gConfigurationEditor(QtGui.QDialog):
    
    """ Window widget to edit the plasduino configuration file.
    """

    def __init__(self, parent = None, filePath = CONFIG_FILE_PATH):
        """ Constructor.
        """
        logger.info('Starting configuration editor...')
        QtGui.QDialog.__init__(self)
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION,\
            groupRequired, argumentRequired, gConfigParser
        skin = TOP_LEVEL_CONFIGURATION.get('gui.skin')
        self.setWindowIcon(getWindowIcon(skin))
        self.setWindowTitle('plasduino configuration editor')
        self.Layout = QtGui.QGridLayout(self)
        self.TabWidget = QtGui.QTabWidget(self)
        self.Layout.addWidget(self.TabWidget, 0, 0)
        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Save |
                                           QtGui.QDialogButtonBox.Cancel)
        connect(buttonBox, 'accepted()', self.save)
        connect(buttonBox, 'rejected()', self.cancel)
        self.Layout.addWidget(buttonBox, 1, 0)
        configParser = gConfigParser(filePath)
        self.DataInputDict = {}
        for section in configParser.sections():
            dataInput = gDataInput(self, title = '', flat = True,
                                   minLabelWidth = 150, minDataWidth = 300)
            self.DataInputDict[section] = dataInput
            index = self.TabWidget.addTab(dataInput, section)
            if not groupRequired(section):
                self.TabWidget.removeTab(index)
            for (field, value) in configParser.items(section):
                dataWidget = self.getWidget(section, field, value)
                dataInput.addField(field, field, dataWidget)
                if (section, field) in PHONY_SETTINGS:
                    dataInput.setFieldEnabled(field, False)
                if not argumentRequired(section, field):
                    dataInput.setFieldVisible(field, False)
            dataInput.Layout.setRowStretch(dataInput.Layout.rowCount(), 1)
    
    def getValue(self, section, field):
        """ Retrieve the value of a specific setting from the corresponding
        widget.
        """
        return self.DataInputDict[section].getFieldValue(field)

    def getWidget(self, section, field, value):
        """ Get the appropriate widget to display/edit a specific option.

        The long series of if/elif/else is ugly, but I guess there's no really
        way around here.

        A simple QLineEdit is the fallback widget, if not otherwise specified.
        """
        from plasduino.__cfgparse__ import AUTOCONFIG
        if value in ['True', 'False']:
            widget = gBinaryComboBox(self, value)
        elif section == 'gui':
            if field == 'style':
                keys = [AUTOCONFIG] + list(QtGui.QStyleFactory.keys())
                widget = gComboBox(self, keys, value)
            elif field == 'language':
                widget = gComboBox(self, ['en'], value)
            elif field == 'skin':
                widget = gComboBox(self, listSkins(), value)
        elif section == 'daq':
            if field in ['max-acquisition-time', 'max-num-events']:
                widget = gNoneSpinBox(self, value = value, step = 10,
                                      minimum = 0, maximum = 1000000)
            elif field in ['data-file-dir', 'log-file-dir', 'tmp-file-dir']:
                widget = gFolderDialogWidget(self, value)
            elif field == 'run-id-file-path':
                widget = gFileDialogWidget(self, value)
        elif section == 'arduino':
            if field == 'arduino-board':
                keys = [AUTOCONFIG] + __boards__.BOARD_LIST
                widget = gComboBox(self,  keys, value)
        elif section == 'sensors':
            keys = ['None'] + listSensors()
            widget = gComboBox(self,  keys, value)
        elif section == 'video':
            if field == 'video-decoder':
                widget = gComboBox(self, VIDEO_DECODERS, value)
            elif field == 'frame-rate':
                widget = gNoneSpinBox(self, value = value, step = 5,
                                      minimum = 0, maximum = 100)
            elif field == 'frame-format':
                widget = gComboBox(self, FRAME_FORMATS, value)
            elif field == 'num-objects':
                widget = gSpinBox(self, minimum = 1, maximum = 6,
                                  value = int(value))
        elif section == 'audio':
            if field == 'sample-rate':
                values = [str(item) for item in __audio__.SAMPLE_RATE_VALUES]
                widget = gComboBox(self, values, value)
            elif field == 'buffer-size':
                values = [str(item) for item in __audio__.BUFFER_SIZE_VALUES]
                widget = gComboBox(self, values, value)
            elif field == 'prescale-factor':
                widget = gSpinBox(self, minimum = 1, maximum = 1024,
                                  value = int(value))
        try:
            return widget
        except UnboundLocalError:
            return QtGui.QLineEdit(value, self)

    def write(self, filePath):
        """ Write the configuration to a file.
        """
        outputFile = open(filePath, 'w')
        for line in open(CONFIG_FILE_PATH):
            if line.startswith('#') or line.startswith(';') or line.isspace():
                outputFile.writelines(line)
            elif line.startswith('['):
                section = line.strip('\n[]')
                outputFile.writelines(line)
            else:
                field = line.split(':')[0].strip()
                value = self.getValue(section, field)
                outputFile.writelines('%s: %s\n' % (field, value))
        outputFile.close()

    def save(self):
        """ Save the configuration file.
        """
        import plasduino.__utils__ as __utils__
        filePath = '%s.new' % CONFIG_FILE_PATH
        logger.info('Saving configuration to %s' % filePath)
        self.write(filePath)
        __utils__.mv(CONFIG_FILE_PATH, '%s.old' % CONFIG_FILE_PATH)
        __utils__.mv(filePath, CONFIG_FILE_PATH)
        self.accept()

    def cancel(self):
        """
        """
        logger.info('Disregarding changes...')
        self.reject()



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    editor = gConfigurationEditor()
    editor.exec_()
