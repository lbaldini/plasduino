#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtGui, QtCore


class gVideoclickPixmapWidget(QtGui.QLabel):

    """ Base widget for the video point application.

    This is essentially a QLabel meant to host a QPixmap with additional
    facilities for handling mouse events and setting up appropriate 
    cursors.

    The default coordinate system is that native of the pixmap, i.e.
    coordinates are measured in pixel with the origin on the *top-left*
    corner of the image. Having the origin in the *bottom-left* corner
    would surely be more physically intuitive, but we can't switch altogether
    to this alternative system, as we need to use the native one in *all* the
    interactions with the pixmap (e.g., when cropping or when drawing stuff
    over it). For the time being we stick to the native system all over the
    place.
    """

    def __init__(self, parent = None, defaultWidth = 640, defaultHeight = 480):
        """ Constructor.
        """
        QtGui.QLabel.__init__(self, parent)
        self.ParentWindow = parent
        self.resize(defaultWidth, defaultHeight)
        self.DefaultHeight = defaultHeight
        self.NullPixmap = QtGui.QPixmap(defaultWidth, defaultHeight)
        self.setCursor(QtCore.Qt.CrossCursor)
        self.reset()

    def setPixmap(self, filePath):
        """ Set the image to be displayed in the widget.
        """
        if not self.isEnabled():
            self.setEnabled(True)
        QtGui.QLabel.setPixmap(self, QtGui.QPixmap(filePath))

    def reset(self):
        """ Display a null pixmap.
        """
        QtGui.QLabel.setPixmap(self, self.NullPixmap)
        self.setEnabled(False)
        self.__ZoomPixmapLabel = None
        self.setMouseTracking(False)

    def setZoomPixmapLabel(self, label):
        """
        """
        self.__ZoomPixmapLabel = label
        self.setMouseTracking(True)

    def mouseReleaseEvent(self, event):
        """ Overloaded mousePressEvent().

        Here we signal that the pixmap has been pressed with a given
        mouse button at a given position.
        """
        (x, y) = (event.x(), event.y())
        button = event.button()
        if button == QtCore.Qt.LeftButton:
            self.emit(QtCore.SIGNAL('frameLeftClicked(int, int)'), x, y)
        elif button == QtCore.Qt.RightButton:
            self.emit(QtCore.SIGNAL('frameRightClicked(int, int)'), x, y)

    def mouseMoveEvent(self, event):
        """ Overloaded mouseMoveEvent().

        The idea is to use this to create a cropped zoomed version of the
        original frame to help the user grabbing the objects on the frame
        itself.
        """
        (x, y) = (event.x(), event.y())
        if self.__ZoomPixmapLabel is not None:
            self.__ZoomPixmapLabel.setPixmap(self.pixmap(), x, y)
        if self.ParentWindow is not None:
            self.ParentWindow.showMessage('Mouse position: (%d, %d)' % (x, y))

        

if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gVideoclickPixmapWidget()
    w.setPixmap('data/tmp_frame_100.png')
    w.show()
    application.exec_() 
