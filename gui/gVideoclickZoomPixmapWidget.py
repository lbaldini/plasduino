#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtGui, QtCore


class gVideoclickZoomPixmapWidget(QtGui.QLabel):

    """ Widget for displaying a zoomed portion of an image.
    """

    DEFAULT_SIZE = 250

    def __init__(self, parent = None, zoomFactor = 3.5, size = DEFAULT_SIZE):
        """ Constructor.
        """
        QtGui.QLabel.__init__(self, parent)     
        self.ZoomFactor = zoomFactor
        self.NullPixmap = QtGui.QPixmap(size, size)
        self.setFixedSize(size, size)
        self.reset()

    def reset(self):
        """ Reset the widget.
        """
        QtGui.QLabel.setPixmap(self, self.NullPixmap)
        self.setEnabled(False)

    def setPixmap(self, pixmap, x, y):
        """ Main overloaded method.

        Here we create a copy of the relevant portion of the input pixmap,
        scale it to the right dimensions and, finally, display it. We also
        draw a couple of reference lines to help the user focus the target.

        Simple as it seems, there's some non-trivial logic involved here
        when it comes to handle the borders of the main pixmap. In those
        cases, in order to have a zoomed pixmap of fixed dimensions, we no
        longer crop the base image symmetrically around the mouse position
        and we have to move the reference cross instead.

        Note also that the "del painter" statement is required to avoid a
        "QPaintDevice: Cannot destroy paint device that is being painted"
        error, with a subsequent segmentation fault.
        """
        if not self.isEnabled():
            self.setEnabled(True)
        w = int(0.5*self.width()/self.ZoomFactor + 0.5)
        x0 = x - w
        y0 = y - w
        dx = 0
        dy = 0
        if x0 < 0:
            dx = -x0
        elif x0 > pixmap.rect().right() - 2*w:
            dx = pixmap.rect().right() - 2*w - x0
        if y0 < 0:
            dy = -y0
        elif y0 > pixmap.rect().bottom() - 2*w:
            dy = pixmap.rect().bottom() - 2*w - y0
        pixmap = pixmap.copy(x0 + dx, y0 + dy, 2*w, 2*w)
        pixmap = pixmap.scaled(self.size(), QtCore.Qt.KeepAspectRatio)
        painter = QtGui.QPainter(pixmap)
        painter.setPen(QtGui.QPen(QtCore.Qt.white, 2))
        w = self.width()
        h = self.height()
        dx *= self.ZoomFactor
        dy *= self.ZoomFactor
        gap = 6
        hline1 = QtCore.QLine(0, h/2 - dy, w/2 - dx - gap, h/2 - dy)
        hline2 = QtCore.QLine(w/2 - dx + gap, h/2 - dy, w, h/2 - dy)
        vline1 = QtCore.QLine(w/2 - dx, 0, w/2 - dx, h/2 - dy - gap)        
        vline2 = QtCore.QLine(w/2 - dx, h/2 - dy + gap, w/2 - dx, h)        
        painter.drawLine(hline1)
        painter.drawLine(hline2)
        painter.drawLine(vline1)
        painter.drawLine(vline2)
        del painter
        QtGui.QLabel.setPixmap(self, pixmap)
