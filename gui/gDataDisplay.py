#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys

from PyQt4 import QtCore, QtGui
from plasduino.gui.__gui__ import DATA_WIDGET_LABEL_WIDTH,\
    DATA_WIDGET_DATA_WIDTH, WINDOW_MIN_WIDTH


class gDataDisplay(QtGui.QGroupBox):

    """ Small utility class for displaying data in a main window.

    This is the base for one of the building blocks in the gAcquisitionWindow
    class.
    """

    DEFAULT_VALUE = '-'
    
    RUN_ID_FIELD_NAME = 'run_id'
    NUM_EVENTS_FIELD_NAME = 'num_events'
    ELAPSED_TIME_FIELD_NAME = 'elapsed_time'
    AVERAGE_RATE_FIELD_NAME = 'average_rate'
    TEMPERATURE_FIELD_NAME = 'temperature'
    LAST_TIMESTAMP_FIELD_NAME = 'last_timestamp'
    TIMESTAMP_FIELD_NAME = 'timestamp'
    PIN_NUMBER_FIELD_NAME = 'pin_number'
    ADC_VALUE_FIELD_NAME = 'adc_value'
    ENG_VALUE_FIELD_NAME = 'eng_value'

    def __init__(self, parent = None, title = 'Data display', **kwargs):
        """ Constructor.
        """
        QtGui.QGroupBox.__init__(self, title, parent)
        minLabelWidth = kwargs.get('minLabelWidth', DATA_WIDGET_LABEL_WIDTH)
        minDataWidth = kwargs.get('minDataWidth', DATA_WIDGET_DATA_WIDTH)
        self.Layout = QtGui.QGridLayout(self)
        self.Layout.setColumnMinimumWidth(0, minLabelWidth)
        self.Layout.setColumnMinimumWidth(1, minDataWidth)
        self.Layout.setColumnStretch(0, 1)
        self.Layout.setColumnStretch(1, 0)
        self.LabelWidgetDict = {}
        self.DataWidgetDict = {}
        self.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding,
                           QtGui.QSizePolicy.Fixed)

    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(WINDOW_MIN_WIDTH, 10)

    def addField(self, key, label, fmt = '%s'):
        """ Add a new field to the display.
        """
        row = self.Layout.rowCount()
        labelWidget = QtGui.QLabel(label, self)
        dataWidget = QtGui.QLabel(self)
        dataWidget.setFrameShape(QtGui.QFrame.StyledPanel)
        dataWidget.setAutoFillBackground(True)
        dataWidget.setBackgroundRole(QtGui.QPalette.BrightText)
        dataWidget.FmtString = fmt
        self.Layout.addWidget(labelWidget, row, 0)
        self.Layout.addWidget(dataWidget, row, 1)
        self.DataWidgetDict[key] = dataWidget
        self.LabelWidgetDict[key] = labelWidget
        dataWidget.setText(str(self.DEFAULT_VALUE))

    def setFieldDataFormat(self, key, fmt):
        """ Set the format string for the data label for a specific field.
        """
        try:
            self.DataWidgetDict[key].FmtString = fmt
        except KeyError:
            sys.exit('Data display has no field "%s". Abort.' % key)

    def setFieldValue(self, key, value):
        """ Set the content of the data widget for a specific field.

        We try and use the data widget's format string, and fall back to
        the generic "%s" forma string in case of TypeError.
        """
        if value is None:
            value = self.DEFAULT_VALUE
        dataWidget = self.DataWidgetDict[key]
        try:
            dataWidget.setText(dataWidget.FmtString % value)
        except TypeError:
            dataWidget.setText('%s' % value)

    def resetFieldValue(self, key):
        """ Set the field value to the default value.
        """
        self.setFieldValue(key, str(self.DEFAULT_VALUE))

    def reset(self):
        """ Reset all field.
        """
        for key in self.DataWidgetDict.keys():
            self.resetFieldValue(key)

    def addRunIdField(self, fmt = '%d'):
        """ Add a field to display the run number.
        """
        self.addField(self.RUN_ID_FIELD_NAME, 'Run number', fmt)

    def setRunId(self, value):
        """ Set the run number field value.
        """
        self.setFieldValue(self.RUN_ID_FIELD_NAME, value)

    def addNumEventsField(self, fmt = '%d'):
        """ Add a field to display the number of acquired events.
        """
        self.addField(self.NUM_EVENTS_FIELD_NAME, 'Number of events', fmt)

    def setNumEvents(self, value):
        """ Set the field value for the number of events.
        """
        self.setFieldValue(self.NUM_EVENTS_FIELD_NAME, value)

    def addElapsedTimeField(self, fmt = '%.1f'):
        """ Add a field to display the elapsed time.
        """
        self.addField(self.ELAPSED_TIME_FIELD_NAME, 'Elapsed time [s]', fmt)

    def setElapsedTime(self, value):
        """ Set the field value for the elapsed time.
        """
        self.setFieldValue(self.ELAPSED_TIME_FIELD_NAME, value)

    def addAverageRateField(self, fmt = '%.3f'):
        """ Add a field to display the average data acquisition rate.
        """
        self.addField(self.AVERAGE_RATE_FIELD_NAME,
                      'Average data acquisition rate [Hz]', fmt)

    def setAverageRate(self, value):
        """
        """
        self.setFieldValue(self.AVERAGE_RATE_FIELD_NAME, value)

    def addTemperatureField(self, fmt = '%.1f'):
        """ Add a field to display the temperature analog readout.
        """
        self.addField(self.TEMPERATURE_FIELD_NAME, 'Temperature [deg C]', fmt)

    def setTemperature(self, value):
        """
        """
        self.setFieldValue(self.TEMPERATURE_FIELD_NAME, value)

    def addLastTimestampField(self, fmt = '%.6f'):
        """ 
        """
        self.addField(self.LAST_TIMESTAMP_FIELD_NAME,
                      'Last timestamp [s]', fmt)

    def setLastTimestamp(self, value):
        """
        """
        self.setFieldValue(self.LAST_TIMESTAMP_FIELD_NAME, value)

    def addTimestampField(self, fmt = '%.3f'):
        """
        """
        self.addField(self.TIMESTAMP_FIELD_NAME, 'Timestamp [s]', fmt)
        
    def setTimestamp(self, value):
        """
        """
        self.setFieldValue(self.TIMESTAMP_FIELD_NAME, value)
 
    def addPinNumberField(self, fmt = '%d'):
        """
        """
        self.addField(self.PIN_NUMBER_FIELD_NAME, 'Pin', fmt)
        
    def setPinNumber(self, value):
        """
        """
        self.setFieldValue(self.PIN_NUMBER_FIELD_NAME, value)

    def addAdcValueField(self, fmt = '%d'):
        """
        """
        self.addField(self.ADC_VALUE_FIELD_NAME, 'ADC counts', fmt)
        
    def setAdcValue(self, value):
        """
        """
        self.setFieldValue(self.ADC_VALUE_FIELD_NAME, value) 

    def addEngValueField(self, label, fmt = '%.3f'):
        """
        """
        self.addField(self.ENG_VALUE_FIELD_NAME, label, fmt)
        
    def setEngValue(self, value):
        """
        """
        self.setFieldValue(self.ENG_VALUE_FIELD_NAME, value)





if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    d = gDataDisplay()
    d.addField('RunId', 'Run number')
    d.reset()
    d.show()
    application.exec_() 
