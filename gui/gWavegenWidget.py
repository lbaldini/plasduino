#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from PyQt4 import QtCore, QtGui
from plasduino.gui.__gui__ import DATA_WIDGET_LABEL_WIDTH,\
    DATA_WIDGET_DATA_WIDTH, WINDOW_MIN_WIDTH
from plasduino.gui.gDialWidget import gDialWidget
from plasduino.devices.__AD9833__ import AD9833


PI = QtCore.QChar(0x03c0)

def strpi(text):
    """ Concatenate a piece of text with the pi sign.
    """
    return QtCore.QString(text) + PI


class gWavegenWidget(QtGui.QGroupBox):

    """ Basic widget to control the wave generator shield.
    """

    def __init__(self, parent = None, **kwargs):
        """ Constructor.
        """
        title = kwargs.get('title', 'Waveform settings')
        QtGui.QGroupBox.__init__(self, title, parent)
        self.SquareRadioButton = QtGui.QRadioButton('Square')
        self.TriangleRadioButton = QtGui.QRadioButton('Triangular')
        self.SineRadioButton = QtGui.QRadioButton('Sinusoidal')
        self.SquareRadioButton.setChecked(True)
        self.Layout = QtGui.QGridLayout(self)
        self.RadioLayout = QtGui.QHBoxLayout()
        self.RadioLayout.addWidget(self.SquareRadioButton)
        self.RadioLayout.addWidget(self.TriangleRadioButton)
        self.RadioLayout.addWidget(self.SineRadioButton)
        self.Layout.addLayout(self.RadioLayout, 0, 0, 1, 3)
        self.FrequencyDial = gDialWidget(self, title = 'Frequency [Hz]',
                                         minimum = 0.1, maximum = 1e6,
                                         logarithmic = True,
                                         textLabelFmt = '%.2f')
        ticks = [10**i for i in range(-1, 7)]
        labels = ['0.1', '1', '10', '100', '1k', '10k', '100k', '1M']
        self.FrequencyDial.setMajorTicks(ticks, labels)
        ticks = [i*10**j for j in range(-1, 6) for i in range(1, 10)]
        self.FrequencyDial.setMinorTicks(ticks)
        self.FrequencyDial.setValue(1000)
        self.Layout.setRowMinimumHeight(1, 20)
        self.Layout.addWidget(self.FrequencyDial, 2, 0)
        self.PhaseDial = gDialWidget(self, title = 'Phase [rad]',
                                     minimum = 0, maximum = 2*math.pi,
                                     textLabelFmt = '%.4f')
        ticks = [i*0.5*math.pi for i in range(5)]
        labels = ['0', strpi('1/2'), PI, strpi('3/2'), strpi('2')]
        self.PhaseDial.setMajorTicks(ticks, labels)
        ticks = [i*0.05*math.pi for i in range(50)]
        self.PhaseDial.setMinorTicks(ticks)
        self.PhaseDial.setValue(0)
        self.Layout.addWidget(self.PhaseDial, 2, 2)
        self.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding,
                           QtGui.QSizePolicy.Fixed)
        self.Layout.setColumnStretch(1, 1)

    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(WINDOW_MIN_WIDTH, 20)

    def getMode(self):
        """ Return the output mode.
        """
        if self.SquareRadioButton.isChecked():
            return AD9833.OUTPUT_MODE_SQUARE
        elif self.TriangleRadioButton.isChecked():
            return AD9833.OUTPUT_MODE_TRIANGULAR
        elif self.SineRadioButton.isChecked():
            return AD9833.OUTPUT_MODE_SINUSOIDAL

    def getFrequency(self):
        """ Return the output frequency.
        """
        return self.FrequencyDial.value()

    def getPhase(self):
        """ Return the output phase.
        """
        return self.PhaseDial.value()

    def getSettings(self):
        """ Return the settings to be passed to the actual chip.
        """
        return (self.getMode(), self.getFrequency(), self.getPhase())





if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gWavegenWidget()
    w.PhaseDial.setEnabled(0)
    w.show()
    application.exec_() 
