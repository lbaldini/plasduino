#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
from plasduino.gui.gCounterDataDisplay import gCounterDataDisplay
from plasduino.gui.gCounterDataInput import gCounterDataInput
from plasduino.gui.gTransportBar import gTransportBar
from plasduino.gui.gQt import connect
from plasduino.gui.gAcquisitionDataInput import gAcquisitionDataInput



class gCounterWindow(gAcquisitionWindow):

    """ Specialized data acquisition window for counting applications.
    """

    def __init__(self, inputPins = [2, 3], **kwargs):
        """ Constructor.
        """
        self.__InputPins = inputPins
        self.CounterInput = None
        gAcquisitionWindow.__init__(self, **kwargs)

    def setup(self):
        """ Set up the necessary widgets in the window.

        Derived classes should overload this function to achieve the
        derived behavior.
        """
        self.DataDisplay = gCounterDataDisplay(self, self.__InputPins)
        self.addWidget(self.DataDisplay)
        self.CounterInput = gCounterDataInput(self, self.__InputPins)
        self.addWidget(self.CounterInput)
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        if TOP_LEVEL_CONFIGURATION.get('gui.run-control-params'):
            self.DataInput = gAcquisitionDataInput(self)
            self.addWidget(self.DataInput)
        self.TransportBar = gTransportBar(self, tooltips = self.TOOLTIP_DICT)
        self.TransportBar.setTooltips(self.TOOLTIP_DICT)
        self.addWidget(self.TransportBar)
        connect(self.TransportBar, 'quit()', self.close)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gCounterWindow()
    w.setRunId(313)
    w.show()
    application.exec_() 

