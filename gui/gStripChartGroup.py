#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import PyQt4.Qwt5 as Qwt
from PyQt4 import Qt

from plasduino.gui.gQt import connect
from plasduino.gui.gStripChart import gStripChart
from plasduino.__logging__ import logger



class gStripChartGroup:

    """ Class representing a group of strip charts.

    Beside providing functionalities such as adjusting the x-range based on all
    the graphs in the grouop, we essentially do some book-keeping, here.
    """

    COLORS = [Qt.Qt.red, Qt.Qt.blue, Qt.Qt.black, Qt.Qt.gray]

    def __init__(self, plotWidget):
        """ Constructor.
        """
        self.__PlotWidget = plotWidget
        self.__ChartList = []
        self.__ChartNameDict = {}
        self.__ChartPinDict = {}

    def add(self, name, pinNumber = None, bufferSize = 1000, **kwargs):
        """ Add a plot to the group.

        The optional pinNumber argument allows to keep track the analog
        pin number (if any) each plot is "attached" to, which in turn is handy
        when updating the plots during data acquisition.
        """
        if not 'color' in kwargs.keys():
            kwargs['color'] = self.COLORS[len(self)]
        chart = gStripChart(name, bufferSize, **kwargs)
        self.__ChartList.append(chart)
        self.__ChartNameDict[name] = chart
        if pinNumber is not None:
            self.__ChartPinDict[pinNumber] = chart
        chart.attach(self.__PlotWidget)
        for item in self.__PlotWidget.Legend.legendItems():
            if item.text().text() == name:
                connect(item, 'clicked()', chart.toggle)
        return chart

    def clear(self):
        """ Clear all the strip charts in the group.
        """
        for chart in self.__ChartList:
            chart.clear()

    def __len__(self):
        """ Return the number of strip charts in the group.
        """
        return len(self.__ChartList)

    def __getitem__(self, key):
        """ Support indexing.
        """
        try:
            return self.__ChartList[key]
        except Exception as e:
            logger.error('Cannot access graph by index (%s).' % e)
            return None

    def getChartByName(self, name):
        """ Retrieve a chart by name.
        """
        try:
            return self.__ChartNameDict[name]
        except Exception as e:
            logger.error('Cannot access graph by name (%s).' % e)
            return None

    def getChartByPin(self, pinNumber):
        """ Retrieve a chart by pin number.
        """
        try:
            return self.__ChartPinDict[pinNumber]
        except Exception as e:
            logger.error('Cannot access graph by pin (%s).' % e)
            return None

    def setXRange(self, xmin, xmax):
        """ Set the x-range of the parent plot widget.
        """
        self.__PlotWidget.setAxisScale(Qwt.QwtPlot.xBottom, xmin, xmax)

    def adjustXRange(self):
        """ Adjust the x-range of the parent plot widget to be just right
        to show the current set of data points for all the charts in the group.
        """
        xmin = min(chart.getXMin() for chart in self.__ChartList)
        xmax = max(chart.getXMax() for chart in self.__ChartList)
        self.setXRange(xmin, xmax)



if __name__ == '__main__':
    from plasduino.gui.gPlotWidget import gPlotWidget
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    widget = gPlotWidget(xTitle = 'Elapsed time [s]',
                         yTitle = 'Something [a.u.]')
    group = gStripChartGroup(widget)
    chart1 = group.add('Chart 1', )
    chart2 = group.add('Chart 2')
    chart1.setData([0, 1], [0, 1])
    chart2.setData([0, 1, 6], [1, 2, 3])
    widget.show()
    application.exec_()
