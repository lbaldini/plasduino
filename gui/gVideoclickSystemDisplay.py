#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtCore

from plasduino.gui.gDataDisplay import gDataDisplay
from plasduino.gui.gVideoclickDataDisplay import gVideoclickDataDisplay
from plasduino.gui.gVideoclickZoomPixmapWidget import\
    gVideoclickZoomPixmapWidget



class gVideoclickSystemDisplay(gDataDisplay):
   
    """ Custom data display for the videoclick coordinate system.

    We display here the relevant information about the origin of the
    coordinate system and the scale factor for converting between pixels
    and physical units.
    """

    LABEL_WIDTH = gVideoclickDataDisplay.LABEL_WIDTH
    DATA_WIDTH = gVideoclickDataDisplay.DATA_WIDTH
    REF_SYSTEM_SCALE_FIELD_NAME = 'ref_system_scale'
    REF_SYSTEM_ORIGIN_FIELD_NAME = 'ref_system_origin'

    def __init__(self, parent = None):
        """ Constructor.
        """
        gDataDisplay.__init__(self, parent, title = 'Reference system',
                              minLabelWidth = self.LABEL_WIDTH,
                              minDataWidth = self.DATA_WIDTH)
        self.addRefSystemScaleField()
        self.addRefSystemOriginField()

    def addRefSystemScaleField(self, fmt = '%.3f'):
        """ Add the field for the reference system scale.
        """
        self.addField(self.REF_SYSTEM_SCALE_FIELD_NAME, 'Scale factor', fmt)

    def setRefSystemScale(self, value):
        """ Set the scale value.
        """
        self.setFieldValue(self.REF_SYSTEM_SCALE_FIELD_NAME, value)

    def addRefSystemOriginField(self, fmt = '%s'):
        """ Add the field for the reference system origin.
        """
        self.addField(self.REF_SYSTEM_ORIGIN_FIELD_NAME, 'Origin', fmt)

    def setRefSystemOrigin(self, value):
        """ Set the origin.
        """
        self.setFieldValue(self.REF_SYSTEM_ORIGIN_FIELD_NAME, value)

    def update(self, eventBuffer):
        """ Update the reference system information according to the
        setting of the input event buffer.
        """
        self.setRefSystemScale(eventBuffer.RefSystemScale)
        self.setRefSystemOrigin(eventBuffer.RefSystemOrigin)

    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(gVideoclickZoomPixmapWidget.DEFAULT_SIZE, 10)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    display = gVideoclickSystemDisplay()
    display.show()
    application.exec_() 
