#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import PyQt4.Qwt5 as Qwt

from PyQt4 import Qt
from plasduino.gui.gQt import connect


class gStripChart(Qwt.QwtPlotCurve):
    
    """ Basic class represening a strip chart.

    As there's no point in displaying a strip chart with an indefinitely large
    number of points, this class also implements the basic functionality
    of a circular buffer, in that only the last n (with n adjustable) points
    are displayed at run time. To this end, the full data set is always stored
    in the self.__DataX/Y class members, while suitable calls to
    Qwt.QwtPlotCurve.setData() allow to switch from the circular buffer to
    the full visualization mode.
    """

    def __init__(self, name, bufferSize = 1000, **kwargs):
        """ Constructor.
        """
        self.__CircularBufferSize = bufferSize
        color = kwargs.get('color', Qt.Qt.blue)
        lineWidth = kwargs.get('lineWidth', 2)
        lineStyle = kwargs.get('lineStyle', Qt.Qt.SolidLine)
        markerSize = kwargs.get('markerSize', 8)
        markerBorder = kwargs.get('markerBorder', 1)
        Qwt.QwtPlotCurve.__init__(self, name)
        brush = Qt.QBrush(color)
        pen = Qt.QPen(brush, lineWidth, lineStyle)
        self.setPen(pen)
        if markerSize > 0:
            self.setSymbol(Qwt.QwtSymbol(Qwt.QwtSymbol.Ellipse, brush,
                                         Qt.QPen(Qt.Qt.black, markerBorder),
                                         Qt.QSize(markerSize, markerSize)))
        self.clear()

    def toggle(self):
        """ Change the visibility of the plot (i.e. hide it if it's visible
        and show it otherwise).
        """
        self.setVisible(not self.isVisible())
        self.plot().replot()

    def clear(self):
        """ Clear the data points.
        """
        self.__DataX = []
        self.__DataY = []
        self.setData(self.__DataX, self.__DataY)

    def addDataPoint(self, x, y, adjust = False):
        """ Add a data point to the strip chart.

        Note that we refer all the timestamps to the first one as to get
        rid of the (irrelevant) time offset in the readouts from arduino
        (where times are referred to the last reset of the board).
        """
        self.__DataX.append(x)
        self.__DataY.append(y)
        self.setData(self.__DataX[-self.__CircularBufferSize:],
                     self.__DataY[-self.__CircularBufferSize:])
        if adjust:
            self.adjustXRange()

    def getXMin(self):
        """ Return the minimum x value for the data currently displayed in the
        strip chart.
        """
        return self.data().x(0)

    def getXMax(self):
        """Return the maximum x value for the data currently displayed in the
        strip chart.
        """
        return self.data().x(self.data().size() - 1)

    def getXRange(self):
        """ Return the x-range for the data currently displayed in the
        strip chart.
        """
        return (self.getXMin(), self.getXMax())
    
    def setXRange(self, xmin, xmax):
        """ Set the x-range of the parent plot widget.
        """
        self.plot().setAxisScale(Qwt.QwtPlot.xBottom, xmin, xmax)

    def adjustXRange(self):
        """ Adjust the x-range of the parent plot widget to be just right
        to show the current set of data points.
        """
        self.setXRange(self.getXMin(), self.getXMax())

    def showAllDataPoints(self, adjust = False):
        """ Force all the data points to be shown.
        """
        self.setData(self.__DataX, self.__DataY)
        if adjust:
            self.adjustXRange



if __name__ == '__main__':
    from plasduino.gui.gPlotWidget import gPlotWidget
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    widget = gPlotWidget(xTitle = 'Elapsed time [s]', yTitle = 'ADC counts')
    chart = gStripChart('stripchart')
    chart.attach(widget)
    chart.setData([0, 1], [0, 1])
    widget.show()
    application.exec_()
