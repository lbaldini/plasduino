#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtGui

from plasduino.__logging__ import logger
from plasduino.gui.gVideoclickBaseDialog import gVideoclickBaseDialog
from plasduino.daq.gVideoclickPoint import gVideoclickPoint


class gVideoclickOriginDialog(gVideoclickBaseDialog):

    """ Small dialog window to set the origin of the coordinate system.
    """

    def __init__(self, parent = None):
        """ Constructor.
        """
        windowTitle = 'Set the origin of the coordinate system'
        text = 'Click with left button on the frame on the main window to ' \
            'mark the origin of the reference system, then push the save ' \
            'button to save your choice, cancel to ignore it.'
        gVideoclickBaseDialog.__init__(self, parent, windowTitle, text)
        self.RefSystemOrigin = None

    def leftClick(self, x, y):
        """ Slot for the left mouse click. 
        """
        self.RefSystemOrigin = gVideoclickPoint(x, y)
        self.SaveButton.setEnabled(True)
        self.parent().repaintFrame()
        self.parent().drawPoint(self.RefSystemOrigin, 'Origin')

    def accept(self):
        """ Overloaded accept() method.
        """
        logger.info('Setting the origin of the reference system to %s.' %\
                        self.RefSystemOrigin)
        self.ignoreMouseEvents()
        self.parent().EventBuffer.RefSystemOrigin = self.RefSystemOrigin
        self.parent().repaintFrame()
        self.parent().updateSystemDisplay()
        QtGui.QDialog.accept(self)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    dialog = gVideoclickOriginDialog()
    dialog.exec_()
