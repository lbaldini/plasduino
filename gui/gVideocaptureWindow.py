#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import gobject, pygst
pygst.require('0.10')
import gst
import sys

from PyQt4 import QtGui

from plasduino.__logging__ import logger


class gVideocaptureWindow(QtGui.QMainWindow):

    """ Base class for a video-capturing window.
    """

    WINDOW_TITLE = 'Video capture'
    
    def __init__(self, parent = None):
        """ Constructor.
        """
        QtGui.QMainWindow.__init__(self, parent)
        self.setCentralWidget(QtGui.QWidget())
        self.WindowId = self.centralWidget().winId()
        self.setGeometry(300, 300, 640, 480)
        self.setWindowTitle(self.WINDOW_TITLE)

    def open(self, device):
        """ Open a device.
        """
        logger.info('Opening device %s for video capture...' % device)
        self.VideoPipeline = gst.Pipeline('player')
        source = gst.element_factory_make('v4l2src', 'vsource')
        sink = gst.element_factory_make('xvimagesink', 'sink')
        fvidscale_cap = gst.element_factory_make('capsfilter', 'fvidscale_cap')
        fvidscale = gst.element_factory_make('videoscale', 'fvidscale')
        caps = gst.caps_from_string('video/x-raw-yuv')
        fvidscale_cap.set_property('caps', caps)
        source.set_property('device', device)
        self.VideoPipeline.add(source, fvidscale, fvidscale_cap, sink)
        gst.element_link_many(source, fvidscale, fvidscale_cap, sink)
        bus = self.VideoPipeline.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()
        bus.connect('message', self.onMessage)
        bus.connect('sync-message::element', self.onSyncMessage)
        logger.info('Done.')

    def onMessage(self, bus, message):
        """ Slot for the message signal.
        """
        t = message.type
        if message.type == gst.MESSAGE_EOS:
            self.VideoPipeline.set_state(gst.STATE_NULL)
        elif message.type == gst.MESSAGE_ERROR:
            err, debug = message.parse_error()
            logger.error('Error: %s' % err)
            logger.debug(debug)
            self.VideoPipeline.set_state(gst.STATE_NULL)

    def onSyncMessage(self, bus, message):
        """ Slot for the sync-message signal.
        """
        if message.structure is None:
            return
        if message.structure.get_name() == "prepare-xwindow-id":
            message.src.set_xwindow_id(self.WindowId)

    def capture(self):
        """ Start capturing the source.
        """
        self.VideoPipeline.set_state(gst.STATE_PLAYING)

    def pause(self):
        """
        """
        self.VideoPipeline.set_state(gst.STATE_NULL)



if __name__ == "__main__":
    # Note this is peculiar to the video capturing and cannot be removed
    # (at least not simply removed).
    gobject.threads_init()

    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gVideocaptureWindow()
    w.show()
    w.open('/dev/video0')
    w.capture()
    application.exec_()


