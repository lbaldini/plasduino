#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys

from PyQt4 import QtCore, QtGui
from plasduino.gui.__gui__ import DATA_WIDGET_LABEL_WIDTH,\
    DATA_WIDGET_DATA_WIDTH, WINDOW_MIN_WIDTH
from plasduino.gui.gQt import connect
from plasduino.gui.gSpinBox import gSpinBox
from plasduino.gui.gCheckWidget import gCheckWidget, getWidgetValue
from plasduino.gui.gSpinBox import gNoneSpinBox


class gDataInput(QtGui.QGroupBox):

    """ Basic widget to provide input to the data acquisition at runtime.
    """

    MAX_EVENTS_FIELD_NAME = 'max_events'
    MAX_SECONDS_FIELD_NAME = 'max_seconds'

    def __init__(self, parent = None, **kwargs):
        """ Constructor.
        """
        title = kwargs.get('title', 'Settings')
        flat = kwargs.get('flat', False)
        minLabelWidth = kwargs.get('minLabelWidth', DATA_WIDGET_LABEL_WIDTH)
        minDataWidth = kwargs.get('minDataWidth', DATA_WIDGET_DATA_WIDTH)
        QtGui.QGroupBox.__init__(self, title, parent)
        self.setFlat(flat)
        self.Layout = QtGui.QGridLayout(self)
        self.Layout.setColumnMinimumWidth(0, minLabelWidth)
        self.Layout.setColumnMinimumWidth(1, minDataWidth)
        self.Layout.setColumnStretch(0, 1)
        self.Layout.setColumnStretch(1, 0)
        self.DataWidgetDict = {}
        self.LabelWidgetDict = {}
        self.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding,
                           QtGui.QSizePolicy.Fixed)

    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(WINDOW_MIN_WIDTH, 10)

    def addField(self, key, label, dataWidget, **kwargs):
        """ Add a new field to the display.
        """
        row = self.Layout.rowCount()
        labelWidget = QtGui.QLabel(label)
        self.LabelWidgetDict[key] = labelWidget
        row = self.Layout.rowCount()
        self.Layout.addWidget(labelWidget, row, 0)
        if kwargs.get('checkBox', True):
            dataWidget = gCheckWidget(dataWidget)
            dataWidget.setChecked(False)
        self.Layout.addWidget(dataWidget, row, 1)
        self.DataWidgetDict[key] = dataWidget

    def setFieldEnabled(self, key, enabled = True):
        """
        """
        self.DataWidgetDict[key].setEnabled(enabled)
        self.LabelWidgetDict[key].setEnabled(enabled)

    def setFieldVisible(self, key, visible = True):
        """
        """
        self.DataWidgetDict[key].setVisible(visible)
        self.LabelWidgetDict[key].setVisible(visible)      

    def getFieldValue(self, key):
        """ Return the value in the data widget for a given field.
        """
        dataWidget = self.DataWidgetDict[key]
        return getWidgetValue(dataWidget)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    d = gDataInput()
    d.addField('test', 'Test field', QtGui.QLineEdit(), checkBox = False)
    d.show()
    application.exec_() 
