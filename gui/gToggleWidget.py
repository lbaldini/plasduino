#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2015 Carmelo Sgro (carmelo.sgro@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtCore, QtGui
from plasduino.gui.gPushButton import gTextPushButton
from plasduino.gui.gQt import connect


class gToggleWidget(QtGui.QGroupBox):

    """ Stupid widget with one push button
    """

    def __init__(self, parent = None, title = 'Toggle', buttontitle = 'Button'):
        """ Constructor.
        """
        QtGui.QGroupBox.__init__(self, title, parent)
        layout = QtGui.QVBoxLayout(self)
        self.setLayout(layout)
        self.ToggleButton = gTextPushButton(buttontitle, self)
        layout.addWidget(self.ToggleButton, 0, QtCore.Qt.AlignCenter)
        connect(self.ToggleButton, 'clicked()', self.toggle)

    def setButtonEnabled(self, enabled = True):
        """ Enable/disable the widget.
        """
        self.ToggleButton.setEnabled(enabled)

   
    def toggle(self):
        """ Toggle between the two states of the widget.
        """
        self.emit(QtCore.SIGNAL('toggle()'))

    def setTooltip(self, tooltip):
        """ Set the tooltip for the button
        """
        self.ToggleButton.setToolTip(tooltip)

if __name__ == '__main__':
    def testSlot():
        """ Test slot.
        """
        print("ciao")

    from plasduino.__cfgparse__ import getapp
    application = getapp()
    d = gToggleWidget()
    d.setTooltip('Tool tip')
    connect(d, 'toggle()', testSlot)
    d.show()
    application.exec_() 

