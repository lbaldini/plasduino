#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from PyQt4 import QtGui, QtCore


class gSpinBox(QtGui.QSpinBox):

    """ Small wrapper around the QSpinBox class.
    """

    def __init__(self, parent = None, **kwargs):
        """ Overloaded constructor.
        """
        QtGui.QSpinBox.__init__(self, parent)
        self.setMinimum(kwargs.get('minimum', 0))
        self.setMaximum(kwargs.get('maximum', 100))
        self.setSingleStep(kwargs.get('step', 1))
        self.setValue(kwargs.get('value', 10))



class gNoneSpinBox(gSpinBox):

    """ Custom spin box supporting the display of "None".

    Cool, isn't it? For the validator API see
    http://www.riverbankcomputing.com/pipermail/pyqt/2009-October/024786.html
    """
    
    SPECIAL_VALUE_TEXT = 'None'

    def __init__(self, parent = None, **kwargs):
        """ Overloaded constructor.
        """
        gSpinBox.__init__(self, parent, **kwargs)
        self.setSpecialValueText(self.SPECIAL_VALUE_TEXT)

    def setValue(self, value):
        """ Overloaded setValue() method.
        """
        if str(value) == self.SPECIAL_VALUE_TEXT:
            value = self.minimum()
        else:
            value = int(value)
        gSpinBox.setValue(self, value)

    def value(self):
        """ Overloaded value() method.
        """
        value = gSpinBox.value(self)
        if value == self.minimum():
            return self.SPECIAL_VALUE_TEXT
        return value

    def validate(self, text, position):
        """ Overloaded validate method.
        """
        if str(text).isdigit() or text == 'None':
            return (QtGui.QValidator.Acceptable, position)
        elif text == self.SPECIAL_VALUE_TEXT[:position]:
            return (QtGui.QValidator.Intermediate, position)
        else:
            return (QtGui.QValidator.Invalid, position)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    box = gNoneSpinBox(value = None)
    box.show()
    application.exec_() 

