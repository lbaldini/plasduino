
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QSizePolicy

class gScrollableLabel(QtGui.QWidget):

    """ Widget that wraps a QLabel in a QScrollArea, so that 
    if the word wrapped text is too long to be diplayed, the scroll
    bar appears.
    """

    def __init__(self, parent = None):
        """ Constructor.
        """
        super(gScrollableLabel, self).__init__(parent = None)
        self.initUI(self)

    def initUI(self, Widget):
        """ Setup the widgets.
        """
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.setMinimumSize(30, 20)
        self.MainLayout = QtGui.QHBoxLayout(Widget)
        self.MainLayout.setContentsMargins(1,1,1,1)
        scrollArea = QtGui.QScrollArea(Widget)
        scrollAreaContainer = QtGui.QWidget(scrollArea)
        scrollArea.setFrameShape(QtGui.QFrame.NoFrame)
        scrollArea.setWidget(scrollAreaContainer)
        scrollArea.setWidgetResizable(True)
        self.Label = QtGui.QLabel(Widget)
        self.Label.setWordWrap(True)
        scrollAreaLayout = QtGui.QVBoxLayout(scrollAreaContainer)
        scrollAreaLayout.addWidget(self.Label, 0, QtCore.Qt.AlignTop)
        self.MainLayout.addWidget(scrollArea)

    def setText(self, string):
        """ Reimplementaion of QLabel.setText method.
        """
        self.Label.setText(string)



if __name__ == "__main__":
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    widget = gScrollableLabel()
    widget.Label.setText("Like many programmers of his generation, Torvalds had cut his teeth not on mainframe computers like the IBM 7094, but on a motley assortment of home-built computer systems. As university student, Torvalds had made the step up from C programming to Unix, using the university's MicroVAX. This ladder-like progression had given Torvalds a different perspective on the barriers to machine access. For Stallman, the chief barriers were bureaucracy and privilege. For Torvalds, the chief barriers were geography and the harsh Helsinki winter. Forced to trek across the University of Helsinki just to log in to his Unix account, Torvalds quickly began looking for a way to log in from the warm confines of his off-campus apartment")
    widget.show()
    application.exec_()

