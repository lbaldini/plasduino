#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.gui.gDataDisplay import gDataDisplay


class gAnalogReadoutDisplay(gDataDisplay):
    
    """
    """

    def __init__(self, parent = None, engLabel = 'Engineering units', **kwargs):
        """
        """
        gDataDisplay.__init__(self, parent, **kwargs)
        self.addTimestampField()
        self.addPinNumberField()
        self.addAdcValueField()
        self.addEngValueField(engLabel)



class gTemperatureReadoutDisplay(gAnalogReadoutDisplay):

    """
    """

    def __init__(self, parent = None):
        """
        """
        gAnalogReadoutDisplay.__init__(self, parent, 'Temperature [deg C]',
                                       minDataWidth = 75, minLabelWidth = 100)
    def sizeHint(self):
        """ Overloaded function for managing the size policy.
        """
        return QtCore.QSize(200, 10)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    d = gTemperatureReadoutDisplay()
    d.show()
    application.exec_() 
