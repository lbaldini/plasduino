#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import atexit

import plasduino.__utils__ as __utils__

from PyQt4 import QtCore, QtGui
from plasduino.__plasduino__ import PACKAGE_NAME, PLASDUINO_ARTWORK
from plasduino.gui.__gui__ import getWindowIcon, getWindowLogo
from plasduino.gui.gQt import connect
from plasduino.gui.gMessageBox import gInfoBox, gErrorBox, gFatalErrorBox,\
    aboutPlasduinoMessageBox, licenseMessageBox, releaseNotesMessageBox
from plasduino.gui.gHtmlBrowser import gHtmlBrowser
from plasduino.gui.gFileDialog import getSaveFilePath, FILE_TYPE_TXT,\
    FILE_TYPE_CSV
from plasduino.__logging__ import logger
from plasduino.daq.gArduinoRunControl import gArduinoRunControl


class gMainWindow(QtGui.QMainWindow):

    """ Base class for a main window. 

    It provides facilities for adding widgets in a grid layout, as well
    as a status bar and a menu bar.

    Finally, it prints the glorious start message on the terminal upon
    instantiation.
    """
    
    WINDOW_TITLE = 'Main window'
    MODULE_OPTIONS = None
    TOOLTIP_DICT = {}

    def __init__(self, **kwargs):
        """ Constructor.
        
        TODO: set the size policy of the menu bar so that it expands
        properly with the main window.
        """
        QtGui.QMainWindow.__init__(self)
        self.setWindowTitle(self.WINDOW_TITLE)
        topBorder = kwargs.get('topBorder', 50)
        botBorder = kwargs.get('botBorder', 10)
        leftBorder = kwargs.get('leftBorder', 15)
        rightBorder = kwargs.get('rightBorder', 15)
        showStatusBar = kwargs.get('showStatusBar', True)
        numLayoutColumns = kwargs.get('numLayoutColumns', 1)
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        skin = TOP_LEVEL_CONFIGURATION.get('gui.skin')
        self.setWindowIcon(getWindowIcon(skin))
        self.CentralWidget = QtGui.QWidget(self)
        self.setCentralWidget(self.CentralWidget)
        self.MainGridLayout = QtGui.QGridLayout(self.CentralWidget)
        self.MainGridLayout.setContentsMargins(leftBorder, topBorder,
                                               rightBorder, botBorder)
        self.MenuBar = QtGui.QMenuBar(self)
        self.MenuDict = {}
        self.MenuActionDict = {}
        self.addMenu('File')
        self.addMenuAction('File', 'Exit', self.close)
        self.addMenu('Configuration')
        self.addMenuAction('Configuration', 'Change settings',
                           self.changeSettings)
        self.addMenuAction('Configuration', 'Restore defaults',
                           self.restoreDefaults)
        self.addMenu('Help')
        self.addMenuAction('Help', 'About', self.about)
        self.addMenuAction('Help', 'Release notes', self.releaseNotes)
        self.addMenuAction('Help', 'Read the license', self.license)
        if showStatusBar:
            self.StatusBar = QtGui.QStatusBar(self)
            self.setStatusBar(self.StatusBar)
            self.showMessage('Welcome to %s' % PACKAGE_NAME, 5)
        self.DocBrowser = None

    def closeEvent(self, event):
        """ Overloaded closeEvent slot.

        This is needed because when launching the window from the plasduino
        main launcher, the control goes back to laucher after the window
        has been closed and we don't actually exit---so that the 
        atexit.register(self.setStopped)
        line in gRunControl does not have any effect. Here we ensure that the
        RunControl is properly stopped even is we lauch the window from the
        launcher and then we close it directly. This is particularly
        relevant for the plane module (see isse #63).
        """
        try:
            self.RunControl.setStopped()
        except AttributeError:
            pass
        if self.DocBrowser is not None:
            self.DocBrowser.close()
        event.accept()

    def enableMenuBar(self):
        """ Enable the menu bar.
        """
        self.MenuBar.setEnabled(True)

    def disableMenuBar(self):
        """ Enable the menu bar.
        """
        self.MenuBar.setEnabled(False)

    def connectRunControl(self):
        """ Setup all the necessary signals/slots between the main window
        and the RunControl.

        Note that we also trigger a runIdChanged(int) signal from the
        run control right after the signal has been connected to the proper
        slot, so that the runId on the main GUI gets properly displayed.

        It is responsibility of the derived classes to call this function
        after a gRunControl object called self.RunControl has been
        instantiated and that the window has a gTransportBar class member
        called self.TransportBar.
        """
        connect(self.TransportBar, 'start()', self.RunControl.setRunning)
        connect(self.TransportBar, 'pause()', self.RunControl.setPaused)
        connect(self.TransportBar, 'stop()' , self.RunControl.setStopped)
        connect(self.RunControl, 'stopped()', self.TransportBar.stop)
        connect(self.RunControl, 'runIdChanged(int)', self.setRunId)
        self.RunControl.emit(QtCore.SIGNAL('runIdChanged(int)'),
                             self.RunControl.RunId)
        connect(self.RunControl.UpdateTimer, 'timeout()',
                self.synchronousUpdate)
        connect(self.RunControl, 'stopped()', self.synchronousUpdate)
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        if TOP_LEVEL_CONFIGURATION.get('daq.prompt-save-dialog'):
            connect(self.RunControl, 'rawDataProcessed(QString)',
                    self.promptCopyFile)
        if isinstance(self.RunControl, gArduinoRunControl):
            connect(self.RunControl.ArduinoManager, 'fatalError(QString)',
                    self.popupFatalError)
            connect(self.RunControl.ArduinoManager, 'connectedToArduino()', 
                    self.addArduinoMenu)
        connect(self.RunControl, 'started()', self.disableMenuBar)
        connect(self.RunControl, 'stopped()', self.enableMenuBar)

    def promptCopyFile(self, filePath):
        """ Prompt a save dialog to create a copy of a given file.
        """
        caption = 'Save a copy of the processed data?'
        dest = getSaveFilePath([FILE_TYPE_TXT], self, caption)
        if dest is not None:
            __utils__.cp(filePath, dest)

    def show(self):
        QtGui.QMainWindow.show(self)
        self.MenuBar.setMinimumWidth(self.width())

    def disableControls(self):
        """ Disable all the controls on the GUI.
        
        This is done, e.g., when the device manager is unable to connect to the
        arduino board and therefore there's no point in starting the data
        acquisition.

        Here we prompt a message on the main gui status bar.
        """
        self.showMessage('All controls disabled.', -1)

    def close(self):
        """ Overloaded close() method.
        """
        QtGui.QMainWindow.close(self)

    def showMessage(self, message, timeout = 5):
        """ Show a message in the status bar.
        """
        self.StatusBar.showMessage(message, 1000*timeout)

    def popupInfo(self, msg):
        """ Pop up a generic message box.

        And, before we actually do that, we have to make sure that the
        window is visible, otherwise we get an orphane message box with no
        parent window.
        """
        if not self.isVisible():
            self.show()
        gInfoBox(self, text = msg).exec_()

    def popupError(self, msg):
        """ Pop up an error message box.

        And, before we actually do that, we have to make sure that the
        window is visible, otherwise we get an orphane message box with no
        parent window.
        """
        if not self.isVisible():
            self.show()
        gErrorBox(self, text = msg).exec_()

    def popupFatalError(self, msg):
        """ Disable all controls and pop up a fatal error message box.
        """
        self.disableControls()
        if not self.isVisible():
            self.show()
        msg += '\n\nNote that all the controls (with the exception of the ' \
            'quit button) have been disabled.'
        gFatalErrorBox(self, text = msg).exec_()

    def addWidget(self, widget, row = None, col = 0, rowSpan = 1, colSpan = 1,
                  align = QtCore.Qt.AlignCenter):
        """ Add a widget to the main grid layout.
        """
        if row is None:
            row = self.MainGridLayout.rowCount()
        self.MainGridLayout.addWidget(widget, row, col, rowSpan, colSpan, align)

    def addItem(self, item, row = None, col = 0):
        """ Add a generic item to the main grid layout.
        """
        if row is None:
            row = self.MainGridLayout.rowCount()
        self.MainGridLayout.addItem(item, row, col)

    def addMenu(self, menuName):
        """ Add a new menu to the menu bar.
        """
        menu = QtGui.QMenu(menuName, self)
        self.MenuBar.addMenu(menu)
        self.MenuDict[menuName] = menu
        return menu

    def getMenu(self, menuName):
        """ Retrieve an existing menu by name.
        """
        try:
            return self.MenuDict[menuName]
        except KeyError:
            return None

    def addMenuAction(self, menuName, actionName, function):
        """ Add an action to an existing menu.
        """
        menu = self.getMenu(menuName)
        if menu is not None:
            key = (menuName, actionName)
            self.MenuActionDict[key] = menu.addAction(actionName, function)

    def insertMenuAction(self, menuName, actionName, function, position = 0):
        """ Insert a menu action at a given position.
        """
        menu = self.getMenu(menuName)
        if menu is not None:
            before = menu.actions()[position]
            action = QtGui.QAction(actionName, menu)
            connect(action, 'triggered()', function)
            menu.insertAction(before, action)
            key = (menuName, actionName)
            self.MenuActionDict[key] = action

    def insertMenu(self, menuName, prevMenuName):
        """ Insert a menu in the menu bar.
        """
        menu = QtGui.QMenu(menuName, self)
        self.MenuBar.insertMenu(self.getMenu(prevMenuName).menuAction(), menu)
        self.MenuDict[menuName] = menu
        return menu

    def getMenuAction(self, menuName, actionName):
        """ Return a specific menu action.
        """
        try:
            return self.MenuActionDict[(menuName, actionName)]
        except KeyError:
            return None

    def changeSettings(self):
        """
        """
        from plasduino.gui.gConfigurationEditor import gConfigurationEditor
        if gConfigurationEditor(self).exec_():
            msg = 'You need to restart plasduino in order for the changes ' +\
            'to have effect.'
            self.popupInfo(msg)

    def restoreDefaults(self):
        """ In order to restore the default configuration file we just create
        a gArgumentParser with no file path (which triggers the generation of a
        new file with the default values).
        """
        from plasduino.__cfgparse__ import gArgumentParser
        parser = gArgumentParser(filePath = None)

    def about(self):
        """ Menu Help -> About: show the start message in a message box.
        """
        aboutPlasduinoMessageBox(self).exec_()

    def onlineHelp(self):
        """ Menu Help -> Online help: show the html documentation.
        """
        if self.DocBrowser is None:
            self.DocBrowser = gHtmlBrowser()
        self.DocBrowser.show()

    def license(self):
        """ Menu Help -> Licence: show the licence.
        """
        licenseMessageBox(self).exec_()

    def releaseNotes(self):
        """ Menu Help -> Release Notes: show the release notes.
        """
        releaseNotesMessageBox(self).exec_()

    def addArduinoMenu(self):
        """ Add the arduino menu.

        It is responsibility of the derived classes to call this method
        explicitely *after* the gRunController object has been instantiated.

        Note the use of lambda function (through the loadSketchCallback method)
        to pass extra arguments to the pyqt slot.
        """
        from plasduino.arduino.__arduino__ import listSketches
        menu = self.addMenu('Arduino')
        menu.addAction('Who\'s connected?', self.aboutArduino)
        sketchMenu = menu.addMenu('Load a sketch')
        sketchList = listSketches()
        for (i, sketchName) in enumerate(sketchList):
            action = sketchMenu.addAction(sketchName)
            action.triggered.connect(self.loadSketch(sketchName))

    def aboutArduino(self):
        """ Pop up a message box with some information about the current
        serial connection.
        """
        self.popupInfo('%s' % self.RunControl.ArduinoManager)

    def loadSketch(self, sketchName):
        """ Load a sketch to arduino.

        This is mutuated by:
        http://eli.thegreenplace.net/2011/04/25/passing-extra-arguments-to-pyqt-slot/
        Note that you really need this callback in order to have the lambda
        evaluated at each step in the loop rather than at the end (look at the
        comments in the page above for more detils).
        """
        return lambda: self.RunControl.ArduinoManager.loadSketch(sketchName)
        


if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gMainWindow()
    w.show()
    application.exec_() 
