#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.gui.gAcquisitionDataDisplay import gAcquisitionDataDisplay



class gCounterDataDisplay(gAcquisitionDataDisplay):

    """ Specialized data display for counting applications.
    """

    EFFECTIVE_COUNTING_INTERVAL_FIELD_NAME = 'effective_counting_interval'
    COUNTER_FIELD_NAME = 'counter_%d'
 
    def __init__(self, parent = None, inputPins = [2, 3], **kwargs):
        gAcquisitionDataDisplay.__init__(self, parent, **kwargs)
        self.addField(self.EFFECTIVE_COUNTING_INTERVAL_FIELD_NAME,
                      'Last effective interval [ms]', '%d')
        for inputPin in inputPins:
            self.addField(self.COUNTER_FIELD_NAME % inputPin,
                          'Counts on D%s' % inputPin, '%d')

    def setEffectiveTimeInterval(self, value):
        """
        """
        self.setFieldValue(self.EFFECTIVE_COUNTING_INTERVAL_FIELD_NAME, value)

    def setCounts(self, inputPin, value):
        """
        """
        self.setFieldValue(self.COUNTER_FIELD_NAME % inputPin, value)



if __name__ == '__main__':
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    d = gCounterDataDisplay()
    d.setEffectiveTimeInterval(10002)
    d.setCounts(2, 3452)
    d.show()
    application.exec_() 
