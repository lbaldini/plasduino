#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.__startmsg__ import MSG
print(MSG)

""" Here is the basic mechanism to customize the configuration parser: if the
module defines a top level variable in the __main__ namespace called
TOP_LEVEL_MODULE_OPTIONS before the __cfgparse__ module is imported, then the
variable is used to configure the option parser object.
"""
try:
    from __main__ import TOP_LEVEL_MODULE_OPTIONS
except ImportError:
    TOP_LEVEL_MODULE_OPTIONS = None


def groupRequired(group):
    """ Return True if a configuration group is required, according to
    the TOP_LEVEL_MODULE_OPTIONS global variable.

    If TOP_LEVEL_MODULE_OPTIONS is None we assume *all* the groups are
    necessary.
    """
    if TOP_LEVEL_MODULE_OPTIONS is None:
        return True
    return group in [opt.split('.')[0] for opt in TOP_LEVEL_MODULE_OPTIONS]

def argumentRequired(group, argument):
    """Return True if a configuration option is required, according to
    the TOP_LEVEL_MODULE_OPTIONS global variable.

    If TOP_LEVEL_MODULE_OPTIONS is None we assume *all* the groups and options
    are necessary. Note that we also accept the group.* form (faking a wildcard)
    to enable entire groups.
    """
    if TOP_LEVEL_MODULE_OPTIONS is None:
        return True
    elif '%s.*' % group in TOP_LEVEL_MODULE_OPTIONS:
        return True
    return '%s.%s' % (group, argument) in TOP_LEVEL_MODULE_OPTIONS


import sys
import os
import argparse
try:
    # 2.x name
    import ConfigParser
except ImportError:
    # 3.x name
    import configparser as ConfigParser

from plasduino.__logging__ import logger, abort
from plasduino.__plasduino__ import PACKAGE_NAME
from plasduino.__install__ import CONFIG_FILE_PATH, RUN_ID_FILE_PATH_DEFAULT,\
    PLASDUINO_DATA_DEFAULT, PLASDUINO_LOG_DEFAULT, PLASDUINO_TMP_DEFAULT


""" System-wise string indicating an automatic setting configuration.
"""
AUTOCONFIG = 'automatic'


""" System-wise configuration facility.

The basic strategy is the following:
(*) all the options can be set by command line through a custom argparse
object. The command line arguments always override the default value;
(*) the argparse object defaults are set through a .ini configuration file.
Ideally the configuration file contains values for all the keyword arguments
(and a warning message is instantiated if that's not the case);
(*) the argparse objects has its own defaults (having the least possible
priority) defined so that in principle can be run with no configuration file.
"""


class gConfigParser(ConfigParser.ConfigParser):

    """ Specialized parser for the .ini configuration file.
    """

    def __init__(self, filePath = CONFIG_FILE_PATH):
        """
        """
        ConfigParser.ConfigParser.__init__(self)
        if not os.path.exists(filePath):
            abort('Could not find file %s' % filePath)
        logger.info('Parsing configuration file %s...' % filePath)
        self.read(filePath)
        



class gArgumentGroup(argparse._ArgumentGroup):

    """ Specialized argument group class.

    Given the subtleties of the internals we prefer not overloading class
    methods (beyond the constructor), but rather define new ones.
    """

    def __init__(self, container, title = None, description = None, **kwargs):
        """ Overloaded constructor.

        The only additional thing that we do here, beyond calling the
        base class constructor, is keeping a reference to the container,
        so that it can be effectively used in the new addArgument() method.

        Also we keep track of the actions as a list of (action, default) values
        in order to make easy to generate a fresh configuration file with the
        default values---see gArgumentParser.write().
        """
        self.Container = container
        argparse._ArgumentGroup.__init__(self, container, title, description,
                                         **kwargs)
        self.Actions = []

    def setName(self, name):
        """ Set the group name.

        (Needed if we want to use the addArgument method.)
        """
        self.Name = name

    def addArgument(self, *args, **kwargs):
        """ alternative to the add_argument() method (which is eventually
        called).

        Here we are essentially doing some book-keeping and trying to enforce
        some consistency. Particularly:

        (*) the 'dest' kwarg is set to groupName.longSyntax;
        (*) the 'default' provided by the argparse container object is
        overridden according to the configuration file. We try and make sure
        the two are in synch by issuing warning messages when the appropriate
        information cannot be retrieved from the configuration file;
        (*) we try and cast the string read from the configuration file to the
        appropriate type defined in the argparse container object;
        (*) we insert the default value in the help string.
        """
        shortSyntax, longSyntax = args
        shortSyntax = shortSyntax.lstrip('-')
        longSyntax = longSyntax.lstrip('-')
        if not argumentRequired(self.Name, longSyntax):
            return
        kwargs['dest'] = '%s.%s' % (self.Name, longSyntax)
        if self.Container.ConfigParser is not None:
            try:
                default = self.Container.ConfigParser.get(self.Name, longSyntax)
                if default in ['None', 'True', 'False']:
                    default = eval(default)
                else:
                    default = kwargs['type'](default)
                kwargs['default'] = default
            except Exception as e:
                logger.warn('%s.' % e)
                logger.info('Using argparse default (%s) instead.' %\
                                kwargs['default'])
                self.Container.ConfigFileUpToDate = False
        if not 'action' in kwargs:
            kwargs['metavar'] = longSyntax.upper()
        helpString = kwargs.get('help')
        default = kwargs.get('default')
        self.Actions.append((longSyntax, default))
        if helpString is not None:
            kwargs['help'] = '%s [%s]' % (helpString, default)
        argparse._ArgumentGroup.add_argument(self, *args, **kwargs)



class gArgumentParser(argparse.ArgumentParser):

    """ Specialized plasduino argument parser.
    """
    
    def __init__(self, filePath = CONFIG_FILE_PATH, **kwargs):
        """ Constructor.
        """
        self.ConfigFileUpToDate = True
        if not filePath or not os.path.exists(filePath):
            self.ConfigParser = None
        else:
            self.ConfigParser = gConfigParser(filePath)
        if not 'prog' in kwargs:
            kwargs['prog'] = PACKAGE_NAME
        if not 'description' in kwargs:
            kwargs['description'] = 'Run %s' % kwargs['prog']
        kwargs['epilog'] = 'Have fun with open source data acquisition!'
        argparse.ArgumentParser.__init__(self, **kwargs)
        self.populate()
        if self.ConfigParser is None:
            self.writeDefaultConfig()

    def writeDefaultConfig(self, filePath = CONFIG_FILE_PATH):
        """ Write the default configuration to file.

        This is actually done the first time plasduino is run, and it
        effectively generates a proper configuration file with default
        values.
        """
        import time
        logger.info('Writing default configuration to %s...' % filePath)
        outputFile = open(filePath, 'w')
        outputFile.writelines('; Writtent by %s on %s.\n' %\
                                  (__name__, time.asctime()))
        outputFile.writelines('; Do not edit by hand '
                              '(use the configuration editor instead).\n')
        for group in self.ArgumentGroups:
            outputFile.writelines('\n[%s]\n' % group.Name)
            for action in group.Actions:
                outputFile.writelines('%s: %s\n' % action)
        outputFile.close()
        logger.info('Configuration file succesfully written.')

    def addArgumentGroup(self, name, *args, **kwargs):
        """ Overloaded method.

        We are subclassing argparse._ArgumentGroup in order to enforce some
        basic consistency when populating the options.
        """
        if not groupRequired(name):
            return None
        group = gArgumentGroup(self, *args, **kwargs)
        self._action_groups.append(group)
        self.ArgumentGroups.append(group)
        group.setName(name)
        return group

    def parse_args(self):
        """ Overloaded method.

        Here we return a plain dict rather than a Namespace object.

        Also, if the configuration file is missing any of the arguments
        (e.g., if a new __cfgparse__ version has arguments that did not
        belong to the configuration file of a previous release, we do our
        best at merging stuff).
        """
        if not self.ConfigFileUpToDate:
            logger.info('Bringing up to speed the configuration file...')
            self.writeDefaultConfig()
        return vars(argparse.ArgumentParser.parse_args(self))

    def populate(self):
        """ Populate the argument parser.

        This can possibly be subclassed by derived classes when a different
        behavior is needed.
        """
        self.ArgumentGroups = []
        self.gui()
        self.daq()
        self.arduino()
        self.sensors()
        self.video()
        self.audio()

    def gui(self):
        """ Setup the GUI configuration group.
        """
        group = self.addArgumentGroup(
            'gui',
            'GUI options',
            'Graphical user interface configuration'
            )
        if group is None:
            return
        group.addArgument('-S', '--style', type = str,
                          default = AUTOCONFIG,
                          help = 'style for the main gui')
        group.addArgument('-s', '--skin', type = str,
                          default = 'cold',
                          help = 'skin for the main gui')
        group.addArgument('-r', '--run-control-params', action = 'store_true',
                          default = False,
                          help = 'show the run control configuration ' 
                          'parameters in the main GUI')
        group.addArgument('-l', '--language', type = str,
                          default = 'en',
                          help = 'language for the main gui')
        
    def daq(self):
        """ Setup the DAQ configuration group.
        """
        group = self.addArgumentGroup(
            'daq',
            'DAQ options',
            'Data acquisition configuration'
            )
        if group is None:
            return
        group.addArgument('-t', '--max-acquisition-time', type = float,
                          default = None,
                          help = 'maximum data acquisition time')
        group.addArgument('-N', '--max-num-events', type = int,
                          default = None,
                          help = 'maximum number of events to be acquired')
        group.addArgument('-i', '--run-id-file-path', type = str,
                          default = RUN_ID_FILE_PATH_DEFAULT,
                          help = 'the path to the runId configuration file')
        group.addArgument('-D', '--data-file-dir', type = str,
                          default = PLASDUINO_DATA_DEFAULT,
                          help = 'the path to the folder for the data files')
        group.addArgument('-c', '--prompt-save-dialog', action = 'store_true',
                          default = False,
                          help = 'prompt a file dialog at the run stop to '
                          'save a copy of the processed data')
        group.addArgument('-L', '--log-file-dir', type = str,
                          default = PLASDUINO_LOG_DEFAULT,
                          help = 'the path to the folder for the log files')
        group.addArgument('-T', '--tmp-file-dir', type = str,
                          default = PLASDUINO_TMP_DEFAULT,
                          help = 'the path to the folder for temporary files')
        group.addArgument('-q', '--disable-log-file', action = 'store_true',
                          default = False,
                          help = 'disable the generation of log files')
        group.addArgument('-A', '--advanced-processing', action = 'store_true',
                          default = False,
                          help = 'enable advanced processing, if available')

    def arduino(self):
        """ Setup the arduino configuration group.
        """
        group = self.addArgumentGroup(
            'arduino',
            'Arduino options',
            'Arduino configuration'
            )
        if group is None:
            return
        group.addArgument('-p', '--arduino-port', type = str,
                          default = AUTOCONFIG,
                          help = 'the usb port arduino is connected to')
        group.addArgument('-b', '--arduino-board', type = str,
                          default = AUTOCONFIG,
                          help = 'the arduino board connected to the computer')
        group.addArgument('-V', '--vendor-id', type = str,
                          default = AUTOCONFIG,
                          help = 'the arduino usb interface vendor id')
        group.addArgument('-P', '--product-id', type = str,
                          default = AUTOCONFIG,
                          help = 'the arduino usb interface product id')

    def sensors(self, numInputs = 6):
        """ Setup the sensor configuration group.
        """
        group = self.addArgumentGroup(
            'sensors',
            'Sensors settings',
            'Sensors and analog inputs'
            )
        if group is None:
            return
        for i in range(numInputs):
            group.addArgument('-%d' % i, '--a%d-sensor' % i, type = str,
                              default = None,
                              help = 'the sensor attached on input A%d' % i)

    def video(self):
        """ Setup the video configuration group.
        """
        group = self.addArgumentGroup(
            'video',
            'Video settings',
            'Video and image configuration'
            )
        if group is None:
            return
        group.addArgument('-v', '--video-decoder', type = str,
                          default = 'ffmpeg',
                          help = 'the video decoder for processing clips')
        group.addArgument('-f', '--frame-rate', type = float,
                          default = None,
                          help = 'override the video frame rate')
        group.addArgument('-I', '--frame-format', type = str,
                          default = 'png',
                          help = 'the image format for the frame extraction')
        group.addArgument('-o', '--num-objects', type = int,
                          default = 2,
                          help = 'the number of objects to track in a video')

    def audio(self):
        """ Setup the audio configuration group.
        """
        group = self.addArgumentGroup(
            'audio',
            'Audio settings',
            'Audio configuration'
            )
        if group is None:
            return
        group.addArgument('-R', '--sample-rate', type = int,
                          default = 1024,
                          help = 'the audio stream sample rate')
        group.addArgument('-B', '--buffer-size', type = int,
                          default = 128,
                          help = 'the audio stream buffer size')
        group.addArgument('-F', '--prescale-factor', type = int,
                          default = 16,
                          help = 'the audio stream prescale factor')


TOP_LEVEL_CONFIGURATION = gArgumentParser().parse_args()


def getapp():
    """
    """
    from PyQt4 import QtGui
    application = QtGui.QApplication(sys.argv)
    style = TOP_LEVEL_CONFIGURATION['gui.style']
    if style != AUTOCONFIG:
        if application.setStyle(style) is None:
            default = application.style().objectName()
            logger.error('Unknown style "%s". Falling back to "%s".' %
                         (style, default))
    return application



if __name__ == '__main__':
    print(TOP_LEVEL_CONFIGURATION)
