#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import pyaudio

from PyQt4 import QtCore
from plasduino.daq.gRunControl import gRunControl
from plasduino.__logging__ import logger


class gAudioStereoRunControl(gRunControl):

    """ Specialized RunControl class for the audio modules.

    In the constructor we create a pyaudio.PyAudio() object that is used
    throughout the acquisition to control the audio stream.
    """

    def __init__(self, eventBuffer, maxSeconds = None):
        """ Overloaded constructor.
        """
        gRunControl.__init__(self, eventBuffer, maxSeconds)
        logger.info('Initializing pyaudio (disregard the error messages)...')
        self.AudioManager = pyaudio.PyAudio()
        logger.info('Done.')
        self.AudioStream = None
        
    def _start(self):
        """ Overloaded method.
        """
        self.AudioStream = \
            self.AudioManager.open(format = pyaudio.paInt16, channels = 2,
                                   rate = self.SampleRate, input = True,
                                   frames_per_buffer = self.BufferSize)
        gRunControl._start(self)

    def _stop(self):
        """ Overloaded method.
        """
        if self.AudioStream is not None:
            logger.info('Closing audio stream...')
            self.Mutex.lock()
            self.AudioStream.stop_stream()
            self.AudioStream.close()
            self.AudioStream = None
            self.Mutex.unlock()
            logger.info('Done (mutex released).')
        gRunControl._stop(self)

    def poll(self):
        """ Do-nothing poll method.

        Note that it is responsibility of deriving classe to lock/unlock the
        mutex when/if needed.
        """
        pass
