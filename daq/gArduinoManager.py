#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import serial
import struct
import sys
import os
import time

import plasduino.arduino.protocol_h as ptcl

from plasduino.daq.__serial__ import getArduinoInfo
from plasduino.daq.gDigitalTransition import gDigitalTransition 
from plasduino.daq.gAnalogReadout import gAnalogReadout
from plasduino.__logging__ import logger, abort
from plasduino.arduino.plasduino_h import BAUD_RATE, EOL_CHAR
from plasduino.arduino.__arduino__ import pulseDTR
from PyQt4 import QtCore


MSG_USB_AUTODETECT_FAILED = 'Could not autodetect USB port. This might indicate that you do not have an arduino board plugged in or you need to manually configure the connection through the configuration editor.'

MSG_SERIAL_CONNECTION_ERROR =  'Could not connect to the serial device over the %s port (the pyserial exception is "%s").'

MSG_SERIAL_ERROR_16 = 'This might just indicate that you need to wait a little bit for the communication to be established (on old machines it might take as long as 30+ s from the time you plug arduino). Do not disconnect the board and try launching the application in a little while.'

MSG_SERIAL_ERROR_13 = 'This might indicate that you need to add yourself to the dialout group (sudo usermod -a -G dialout your_username)'

MSG_SKETCH_AUTOUPLOAD_FAILED = 'Autoupload of the sketch. I have no idea whatsoever about what happened.'

SERIAL_DATA_SIZE_DICT = {
    'c': 1,
    'b': 1, 
    'B': 1, 
    '?': 1, 
    'h': 2, 
    'H': 2, 
    'i': 4, 
    'I': 4, 
    'l': 4, 
    'L': 4, 
    'q': 8, 
    'Q': 8, 
    'f': 4,
    'd': 8
    }


class gArduinoManager(QtCore.QObject):

    """ Class managing an arduino board connected through the serial interface.

    In a previous implementation this was inheriting from serial.Serial,
    but then we run into the infamous metaclass conflict when trying to
    inherit from QObject (which we wanted in order to be able to emit
    signal). We therefore switched to composition, as opposed to inheritance,
    for the serial interface.

    We provide here all the facilities to exchange information with the arduino
    board.

    The signals provided by this class are:
    (*) fatalError(QString) when problems are encountered while trying
    to establish communication with the board, the parameter being the
    text of the error message;
    (*) connectedToArduino() upon successfull connection to the board.
    """

    def __init__(self):
        """ Basic constructor.
        """
        QtCore.QObject.__init__(self)
        self.SketchId = None
        self.SketchVersion = None
        self.__SerialInterface = serial.Serial()

    def connect(self, targetSketch = None, timeout = None, autoUpload = True):
        """ Connect to arduino, handshake and try and autoupload the target
        sketch.

        Return 1 upon connection error, 0 otherwise.
        """
        self.Port, self.Model, self.VID, self.PID = getArduinoInfo()
        if self.Port is None:
            self.__fatalError(MSG_USB_AUTODETECT_FAILED)
            return 1
        self.__SerialInterface.port = self.Port
        self.__SerialInterface.baudrate = BAUD_RATE
        try:
            pulseDTR(self.Port)
            self.__openConnection()
        except Exception as e:
            msg = MSG_SERIAL_CONNECTION_ERROR % (self.Port, e)
            if '[Errno 16]' in msg:
                msg += ' %s' % MSG_SERIAL_ERROR_16
            elif '[Errno 13]' in msg and os.name == 'posix':
                msg += ' %s' % MSG_SERIAL_ERROR_13
            self.__fatalError(msg)
            return 1
        if not self.connected():
            return 1
        if timeout is not None:
            self.setTimeout(timeout)
        if self.handshake(targetSketch):
            if autoUpload:
                if self.loadSketch(targetSketch):
                    self.__fatalError(MSG_SKETCH_AUTOUPLOAD_FAILED)
                    return 1
        self.emit(QtCore.SIGNAL('connectedToArduino()'))
        return 0

    def setTimeout(self, timeout):
        """ Set a read timeout value.

        Possible values for the parameter timeout:
        timeout = None: wait forever
        timeout = 0: non-blocking mode (return immediately on read)
        timeout = x: set timeout to x seconds (float allowed)
        """
        if not self.connected():
            logger.error('Device manager not connected, cannot set timeout.')
            return
        if timeout is None:
            logger.info('Setting serial connection timeout to blocking...')
        else:
            logger.info('Setting serial connection timeout to %.2f s...' %\
                            timeout)
        self.__SerialInterface.timeout = timeout

    def setNonBlockingRead(self):
        """ Set non blocking read mode.
        """
        self.setTimeout(0)

    def __fatalError(self, msg):
        """ Log an error message to the terminal and emit a signal that can
        be intercepted by the parent GUI, is any.
        """
        logger.error(msg)
        self.emit(QtCore.SIGNAL('fatalError(QString)'), msg)

    def __openConnection(self):
        """ Open a connection through the serial port.
        """
        if not self.connected():
            logger.info('Opening serial connection to %s (baudrate = %d)...' %\
                            (self.Port, BAUD_RATE))
            self.__SerialInterface.open()

    def __closeConnection(self):
        """ Close the current connection.
        """
        if self.connected():
            logger.info('Closing serial connection to %s...' % self.Port)
            self.__SerialInterface.close()

    def connected(self):
        """ Return true if properly connected with the serial device.
        """
        return self.__SerialInterface.isOpen()

    def getInfo(self):
        """ Return some information about the device manager.

        This returns in order:
        (*) the port name;
        (*) the model of the arduino board connected;
        (*) the name of the sketch uploaded on the board at the time of the last
        handshaking;
        (*) the version of the sketch uploaded on the board at the time of the
        last handshaking.
        """
        return (self.Port, self.Model, self.SketchId, self.SketchVersion)

    def handshake(self, targetSketchName = None, timeout = 5.0):
        """ Read the sketch information from the serial connection.

        Note that we set the timeout to a long(ish) value as the handshaking
        takes some time. Therefore we store the connection timeout at the
        beginning of the method and restore it at the end.
        """
        if not self.connected():
            logger.error('Device manager not connected, cannot handshake.')
            return
        prevTimeout = self.__SerialInterface.timeout
        self.setTimeout(timeout)
        logger.info('Trying hand-shaking...')
        try:
            self.SketchId = self.sreaduint8()
            self.SketchVersion = self.sreaduint8()
            logger.info('Sketch %d version %d preloaded on the board.' %\
                            (self.SketchId, self.SketchVersion))
        except:
            logger.warn('Could not determine the sketch preloaded.')
        if targetSketchName is not None:
            exec('from plasduino.arduino.%s import *' % targetSketchName)
            if self.SketchId == SKETCH_ID and \
                    self.SketchVersion == SKETCH_VERSION:
                logger.info('It looks like the sketch is right, ready to go!')
                status = 0
            else:
                logger.info('Sketch %d version %d needed, instead.' %\
                                (SKETCH_ID, SKETCH_VERSION))
                status = 1
        else:
            logger.info('No target sketch defined.')
            status = 0
        self.setTimeout(prevTimeout)
        return status

    def loadSketch(self, sketchName):
        """ Load a sketch to arduino.

        This actually involves quote a few steps:
        (*) close the serial connection;
        (*) instantiate a gSketchLoader object and upload the sketch;
        (*) re-open the serial connection;
        (*) do a hand-shaking with the new sketch.
        """
        from plasduino.arduino.gSketchLoader import gSketchLoader
        self.__closeConnection()
        loader = gSketchLoader(self.Port, self.Model)
        loader.upload(sketchName)
        self.__openConnection()
        return self.handshake(sketchName)

    def sreadline(self, stripeol = True):
        """ Read a line from the serial connection. 
        
        Note that the eol is necessary for pyserial to tell that the line
        is finished.

        By default the eol character is stripped from the line.
        """
        line = self.__SerialInterface.readline(None)
        if stripeol:
            line = line.strip(EOL_CHAR)
        return line
    
    def sread(self, size = 1):
        """ Read a given number of bytes from the serial interface.
        
        Mind the return value is not casted to a python type and None is
        returned upon timeout.
        """
        return self.__SerialInterface.read(size)

    def sreadfmt(self, fmt):
        """ Read data from the serial port and convert the actual value to the
        proper python type.
        """
        size = SERIAL_DATA_SIZE_DICT[fmt]
        return struct.unpack('>%s' % fmt, self.sread(size))[0]

    def sreaduint8(self):
        """ Read an 8-bit unsigned unsigned integer from the serial port.
        """
        return struct.unpack('>B', self.sread(1))[0]

    def sreaduint16(self):
        """ Read a 16-bit unsigned unsigned integer from the serial port.
        """
        return struct.unpack('>H', self.sread(2))[0]

    def sreaduint32(self):
        """ Read a 32-bit unsigned unsigned integer from the serial port.
        """
        return struct.unpack('>L', self.sread(4))[0]

    def sreadCtrlByte(self):
        """ Read a control byte from the serial port.
        
        This implies reading exactly one byte from the port. If the payload
        is not None (e. g., the serial read did not time-out), the one byte
        of data is unpacked and returned.
        """
        payload = self.sread(1)
        if payload:
            return struct.unpack('>B', payload)[0]
        return None

    def sreadDigitalTransition(self):
        """ Read a digital transition from the serial port.

        Watch out: return None upon timeout.
        """
        payload = self.sread(gDigitalTransition.LENGTH)
        if payload:
            return gDigitalTransition(payload)
        return None

    def sreadAnalogReadout(self):
        """ Read an analog readout from the serial port.

        Watch out: return None upon timeout.
        """
        payload = self.sread(gAnalogReadout.LENGTH)
        if not payload:
            return None
        return gAnalogReadout(payload)

    def swrite(self, value, fmt):
        """ Write a c struct to the serial port. 
        
        For convenience, here are the basic format strings.
        Format  C type             Size
        x       pad byte
        c       char               1
        b       signed char        1
        B       unsigned char      1
        ?       _Bool              1
        h       short              2
        H       unsigned short     2
        i       int                4
        I       unsigned int       4
        l       long               4
        L       unsigned long      4
        q       long long          8
        Q       unsigned long long 8
        f       float              4
        d       double             8
        s       char[]
        p       char[]
        P       void *
        """
        payload = struct.pack(fmt, value)
        logger.info('Writing [%s]%s (0x%x, %d byte) to the serial port...' %\
                        (fmt, value, value, len(payload)))
        self.__SerialInterface.write(payload)

    def swriteuint8(self, value):
        """ Write a uint8_t to the serial port.
        """
        self.swrite(value, 'B')

    def swriteuint16(self, value):
        """ Write a uint16_t to the serial port.
        """
        self.swrite(value, 'H')

    def swriteuint32(self, value):
        """ Write a uint32_t to the serial port.
        """
        self.swrite(value, 'I')

    def swritecmd(self, opcode, fmt, payload):
        """ Write a command to the arduino board.

        This implies writing the opcode to the serial port, writing the actual
        payload and, finally, reading back the arduino response and making
        sure the communication went fine.

        The function return 0 if the command is successful, 1 otherwise.
        The exit code can be used downstream to control the flux of the
        execution.
        """
        logger.info('Sending serial command (opcode = %d, payload = [%s]%s)' %\
                        (opcode, fmt, payload))
        self.swriteuint8(opcode)
        self.swrite(payload, fmt)
        logger.info('Waiting for arduino closing the loop...')
        targetOpcode = self.sreaduint8()
        actualOpcode = self.sreaduint8()
        actualPayload = self.sreadfmt(fmt)
        return self.cmdlog(targetOpcode, actualOpcode, payload, actualPayload)

    def cmdlog(self, targetOpcode, actualOpcode, payload, actualPayload):
        """ Debug information about cmd execution.
        """
        if targetOpcode == actualOpcode and payload == actualPayload:
            logger.info('Looks good, command succesfully received.')
            return 0
        else:
            logger.error('Ooops... something apparently went wrong.')
            if targetOpcode != actualOpcode:
                logger.error('Sketch expecting opcode %d, got %d.' %\
                                 (targetOpcode, actualOpcode))
            if payload != actualPayload:
                logger.error('The actual payload is %s, not %s.' %\
                                 (actualPayload, payload))
            return 1

    def swriteStartRun(self):
        """ Write a start run command to the serial port.
        """
        self.swriteuint8(ptcl.OP_CODE_START_RUN)

    def swriteStopRun(self):
        """ Write a stop run command to the serial port.
        """
        self.swriteuint8(ptcl.OP_CODE_STOP_RUN)

    def setupAnalogSamplingSketch(self, pinList, samplingInterval):
        """ Setup the sktchAnalogSampling sketch.
        """
        self.swritecmd(ptcl.OP_CODE_SELECT_NUM_ANALOG_PINS, 'B', len(pinList))
        for pin in pinList:
            self.swritecmd(ptcl.OP_CODE_SELECT_ANALOG_PIN, 'B', pin)
        self.swritecmd(ptcl.OP_CODE_SELECT_SAMPLING_INTERVAL, 'I',
                       samplingInterval)

    def setupDigitalTimerSketch(self, mode0, mode1):
        """ Setup the sktchDigitalTimer sketch.
        """
        self.swritecmd(ptcl.OP_CODE_SELECT_INTERRUPT_MODE, 'B', mode0)
        self.swritecmd(ptcl.OP_CODE_SELECT_INTERRUPT_MODE, 'B', mode1)

    def __str__(self):
        """ String formatting.
        """
        text = 'Arduino %s (vid %s, pid %s) connected on %s (baudrate = %s).' %\
            (self.Model, self.VID, self.PID, self.Port, BAUD_RATE)
        text += '\nSketch uploaded at last handshaking: sketch %s version %s' %\
            (self.SketchId, self.SketchVersion)
        return text



if __name__ == '__main__':
    m = gArduinoManager()
    print(m)
