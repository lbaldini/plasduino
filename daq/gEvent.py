#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2014 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import struct

import plasduino.arduino.protocol_h as ptcl


STANDARD_SIZE_DICT = {
    'c': 1, # char (-> string of length 1)
    'b': 1, # signed char (-> integer)
    'B': 1, # unsigned char (-> integer)
    '?': 1, # _Bool (-> bool)
    'h': 2, # short (-> integer)
    'H': 2, # unsigned short (-> integer)
    'i': 4, # int (-> integer)
    'I': 4, # unsigned int (-> integer)
    'l': 4, # long (-> integer)
    'L': 4, # unsigned long (-> integer)
    'q': 8, # long long (-> integer)
    'Q': 8, # unsigned long long (-> integer)
    'f': 4, # float (-> float)
    'd': 8  # double (-> float)
    }


class gEvent(dict):

    """ Generic event descriptor.

    Generic interface to define a series a byte, with a specific layout,
    representing a data structure. An event can be a transition of a digital
    input, an analog readout among other things.

    The class inherits from the dict type so that the field values can be
    retrieved by field name.

    The derived classes have the responsibility of defining the data members:
    * LENGTH: the lenght (in bytes) of the data block.
    * LAYOUT: the layout of the fields within the data block, in the form 
    of tuples (fieldName, fieldType, firstByte).
    """
    
    HEADER = ptcl.NO_OP_HEADER
    LENGTH = 0
    LAYOUT = []

    def __init__(self, payload):
        """ Constructor. 

        The event is typically assembled on the arduino side and transferred
        through the serial interface as a series of byte (we call this
        binary word the payload).

        The payload is decode on the python side and the proper data fields are
        filled.
        """
        self.Payload = payload
        self.decode()
        
    def decode(self):
        """ Decode the payload and fill the data members.

        This involved looping over the LAYOUT, unpacking the bytes and
        filling the appropriate data members.
        
        The byte order (big endian, of '>' in the format string, is prepended
        automatically while decoding the data block).
        """
        for (fieldName, fieldType, firstByte) in self.LAYOUT:
            length = STANDARD_SIZE_DICT[fieldType]
            data = self.Payload[firstByte:firstByte + length]
            self[fieldName] = struct.unpack('>%s' % fieldType, data)[0]

    def format(self):
        """ Format the data block for being written to an output file.

        In the base class we just return whatever the __str__() method
        provides. Derived classes should overload this with the desired
        behavior.
        """
        return '%s' % self

    def __str__(self):
        """ String formatting.
        """
        return repr(self.Payload)



if __name__ == '__main__':
    db = gEvent('')
    print(db)
