#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math


class gPoint2d:

    """ Small utility class representing a point on 2-dimensional plane.

    We do support the basic operations, here, such as sum, subtraction
    and multiplication by a scalar.
    """
    
    def __init__(self, x, y):
        """ Constructor.

        Here we assign the x and y coordinates.
        """
        self.X = x
        self.Y = y

    def distToPoint(self, other):
        """ Return the distance to another point.
        """
        return math.sqrt((self.X - other.X)**2 + (self.Y - other.Y)**2)

    def __eq__(self, other):
        """ Position comparison.

        The try/except block is not as weird as it might seem at first sight.
        If the basic expression cannot be evaluated, then other is not
        an instance of the gPoint2d class or one of its subclasses. Therefore
        the objects *are* different. And this allow to write stuff such as
        "p is not None".
        """
        try:
            return self.X == other.X and self.Y == other.Y
        except AttributeError:
            return False
 
    def __mul__(self, other):
        """ Multiply the coordinates by a scale factor.
        """
        return gPoint2d(other*self.X, other*self.Y)
    
    def __add__(self, other):
        """ Add points.
        """
        return gPoint2d(self.X + other.X, self.Y + other.Y)

    def __sub__(self, other):
        """ Subtract points.
        """
        return gPoint2d(self.X - other.X, self.Y - other.Y)

    def __str__(self):
        """ String formatting.
        """
        return '(%d, %d)' % (self.X, self.Y)



if __name__ == '__main__':
    p1 = gPoint2d(1, 2)
    p2 = gPoint2d(3, 4)
    print(p1)
    print(p2)
    print(p1 == p2)
    print(p1 == p1)
    print(p1*2)
    print(p1 + p2)
    print(p1 - p2)
    
