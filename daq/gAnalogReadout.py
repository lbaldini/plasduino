#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2014 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import plasduino.arduino.protocol_h as ptcl

from plasduino.daq.gEvent import gEvent
from plasduino.__logging__ import logger



class gAnalogReadout(gEvent):

    """ An arduino analog readout is a 7-bit binary array containing,
    from the MSB to the LSB:
    * byte(s) 0  : the analog pin number;
    * byte(s) 1-4: the timestamp of the readout from millis();
    * byte(s) 5-6: the actual adc value.

    The corresponding write function on the arduino side is defined
    in arduino/plasduino.h and arduino/plasduino.c.
    """
    
    HEADER = ptcl.ANALOG_READOUT_HEADER
    LENGTH = 7
    LAYOUT = [('pinNumber', 'B', 0),
              ('seconds'  , 'L', 1),
              ('adc'      , 'h', 5)
              ]

    def __init__(self, payload):
        """ Constructor.
        """
        gEvent.__init__(self, payload)
        self['seconds'] *= 1e-3

    def __str__(self):
        """ String representation.
        """
        return '%s -> pin %d readout @ t = %.3f: %d adc counts' %\
            (repr(self.Payload), self['pinNumber'], self['seconds'],
             self['adc'])



if __name__ == '__main__':
    r = gAnalogReadout('\xf1\x01\x00\x00\t\x16\x01\x1d')
    print(r)

