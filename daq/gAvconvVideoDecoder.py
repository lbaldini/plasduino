#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import re
import os
import plasduino.__utils__ as __utils__

from plasduino.daq.gVideoDecoder import gVideoDecoder
from plasduino.__logging__ import logger


class gAvconvVideoDecoder(gVideoDecoder):

    """ Derived class using ffmpeg.
    """
    
    ENGINE_NAME = 'avconv'
    
    def __init__(self, frameFormat = 'png'):
        """ Constructor
        """
        gVideoDecoder.__init__(self, frameFormat)

    def getInfo(self):
        """ Retrieve video information.
        """
        cmd = '%s -i %s' % (self.ENGINE_NAME, self.FilePath)
        info = __utils__.getcmdoutput(cmd)
        try:
            regexp = '(?<=Duration: )[:\.0-9]*(?=, )'
            duration = re.search(regexp, info).group(0)
            h, m, s = [float(item) for item in duration.split(':')]
            self.Duration = s + 60*m + 3600*h
        except:
            logger.error('Could not retrieve clip duration.')
            return
        try:
            regexp = '(?<=, )[0-9]*(?= tbr)'
            self.FrameRate = float(re.search(regexp, info).group(0))
        except:
            logger.error('Could not retrieve frame rate.')

    def extract(self):
        """
        """
        self.cleanup()
        logger.info('Extracting frames from %s...' % self.FilePath)
        filePath = os.path.join(self.FrameFolder, self.FRAME_BASE_NAME)
        cmd = '%s -i %s -f image2 %s.%s' %\
            (self.ENGINE_NAME, self.FilePath, filePath, self.FrameFormat)
        statusCode = __utils__.cmd(cmd)
        if not statusCode:
            logger.info('Done.')

    def formatFrameRate(self):
        """ Use this engine to build a new avi file with frame rate
            forced to 25 fps, and start allover again.
            NOT TESTED for this engine!!!!!
        """
        logger.info('Forcing frame rate for %s...' % self.FilePath)
        fileoutPath = os.path.abspath(self.FrameFolder).replace('tmp', '')
        fileoutPath = os.path.join(fileoutPath,"tmp_25fps.avi")
        cmd = '%s -i %s -r 25 -f avi %s' %\
            (self.ENGINE_NAME, self.FilePath, fileoutPath)
        statusCode = __utils__.cmd(cmd)
        if not statusCode:
            logger.info('Done. File %s written.' % fileoutPath)
        return fileoutPath


if __name__ == '__main__':
    decoder = gAvconvVideoDecoder()
    print(decoder.installed())
    decoder.open('data/airtable_sample_cut.avi')
    decoder.extract()
    print(decoder.progress())
    
