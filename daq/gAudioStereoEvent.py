#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import struct

from plasduino.daq.gEvent import gEvent, STANDARD_SIZE_DICT
from plasduino.__logging__ import logger


class gAudioStereoEvent(gEvent):

    """ Class describing an audio event (i.e. an audio sample from the
    audiocard).

    An audio sample, at least as returned by the PyAudio (PulseAudio wrapper)
    module is essentially a string with two shor int values---one for the left
    and one for the right channel. 
    """

    LENGTH = 4
    LAYOUT = [('right', 'h', 0),
              ('left' , 'h', 2)
              ]

    def __init__(self, payload):
        """ Constructor.

        Note that the raw binary word only contains the information about the
        right/left values, while the time information can only be assigned
        a posteriori, based on the sequential sample number.
        """
        gEvent.__init__(self, payload)
        self['seconds'] = None

    def decode(self):
        """ Decode the payload and fill the data members.

        We need this overloaded method here because, unlike the arduino
        serial data, the byte order here is not of big-endian type.
        """
        for (fieldName, fieldType, firstByte) in self.LAYOUT:
            length = STANDARD_SIZE_DICT[fieldType]
            data = self.Payload[firstByte:firstByte + length]
            self[fieldName] = struct.unpack('%s' % fieldType, data)[0]

    def __str__(self):
        """ String representation.
        """
        return '%s -> audio readout @ t = %.4f: right = %d, left = %d' %\
            (repr(self.Payload), self['seconds'], self['right'], self['left'])



if __name__ == '__main__':
    pass

