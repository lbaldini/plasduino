#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.daq.gFfmpegVideoDecoder import gFfmpegVideoDecoder
from plasduino.daq.gAvconvVideoDecoder import gAvconvVideoDecoder
from plasduino.__logging__ import logger


FRAME_FORMATS = ['png', 'jpg']
VIDEO_DECODERS = ['ffmpeg', 'avconv']


def getDecoder(decoderName, frameFormat):
    """ Return a video decoder object.
    """
    if frameFormat not in FRAME_FORMATS:
        defaultFrameFormat = FRAME_FORMATS[0]
        logger.error('Unknown frame format %s, falling back to %s.' %\
                         (frameFormat, defaultFrameFormat))
        frameFormat = defaultFrameFormat
    if decoderName == 'ffmpeg':
        return gFfmpegVideoDecoder(frameFormat)
    elif decoderName == 'avconv':
        return gAvconvVideoDecoder(frameFormat)
    else:
        logger.error('Unknown video decoder %s.' % decoderName)
        return None
