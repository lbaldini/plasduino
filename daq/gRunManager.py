#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import time
import plasduino.__utils__ as __utils__

from plasduino.__logging__ import logger



class gRunManager:

    """ Small convenience class handling the run number.
    """

    def __init__(self):
        """ Constructor.
        """
        self.RunId = None
        self.readRunId()

    def readRunId(self):
        """ Read the last run number from the configuration file.

        The file is created with a runId 0 if it does not exist.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        runIdFilePath = TOP_LEVEL_CONFIGURATION['daq.run-id-file-path']
        if not os.path.exists(runIdFilePath):
            self.RunId = 0
            self.writeRunId()
        else:
            logger.info('Reading run number from %s...' % runIdFilePath)
            self.RunId = int(open(runIdFilePath, 'r').readline())

    def writeRunId(self):
        """ Write the last run number to the configuration file.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        runIdFilePath = TOP_LEVEL_CONFIGURATION['daq.run-id-file-path']
        logger.info('Writing run number to %s...' % runIdFilePath)
        open(runIdFilePath, 'w').writelines('%d\n' % self.RunId)

    def incrementRunId(self):
        """ Increment the run number (called when the RunControlle is started).

        The run number is written to file and a 'runIdChanged(int)' signal is
        emitted that can be intercepted by the main GUI to update its widgets.
        """
        self.RunId += 1
        self.writeRunId()

    def getRawDataFilePath(self):
        """ Return the path to the raw data file path.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        folderPath = TOP_LEVEL_CONFIGURATION['daq.data-file-dir']
        __utils__.createFolder(folderPath)
        return os.path.join(folderPath, '%06d_raw.dat' % self.RunId)
        
    def getProcessedDataFilePath(self):
        """ Return the path to the processed data file path.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        folderPath = TOP_LEVEL_CONFIGURATION['daq.data-file-dir']
        __utils__.createFolder(folderPath)
        return os.path.join(folderPath, '%06d_processed.txt' % self.RunId)

    def getLogFilePath(self):
        """ Return the path to the log file path.
        """
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        folderPath = TOP_LEVEL_CONFIGURATION['daq.log-file-dir']
        __utils__.createFolder(folderPath)
        return os.path.join(folderPath, '%06d.log' % self.RunId)
        


if __name__ == '__main__':
    rm = gRunManager()
    print('Run number: %d' % rm.RunId)
