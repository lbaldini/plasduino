#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import time
import os

import plasduino.arduino.protocol_h as ptcl

from PyQt4 import QtCore
from plasduino.daq.gRunControl import gRunControl
from plasduino.__logging__ import logger


class gArduinoRunControl(gRunControl):

    """ Specialized RunControl class for the application using the arduino
    board.
    """

    SKETCH_NAME = None

    def __init__(self, eventBuffer, maxSeconds = None):
        """ Constructor.
        """
        gRunControl.__init__(self, eventBuffer, maxSeconds)
        self.__RunEndMarkReceived = False
        from plasduino.daq.gArduinoManager import gArduinoManager
        self.ArduinoManager = gArduinoManager()
        
    def connectToArduino(self, timeout = None, autoUpload = True):
        """ Connect to the arduino board.

        Return 1 upon connection error, 0 otherwise.
        
        The connection to the arduino board is deferred to a separate
        class method bacause for the main window to intercept the signals we
        need to call the connectRunControl() method of the main window after
        the RunControl has been instantiated and before we attempt to connect
        to arduino.
        """
        errCode = self.ArduinoManager.connect(self.SKETCH_NAME, timeout,
                                              autoUpload)
        if not errCode:
            self.EventBuffer.setProducerInfo(self.ArduinoManager.SketchId,
                                             self.ArduinoManager.SketchVersion)
        return errCode

    def _start(self):
        """ Start the RunControl (STOPPED -> RUNNING).

        Note we do set to False the self.__RunEndMarkReceived flag, here,
        that will be set to True in the polling cycle as soon as the run
        end mark actually arrives.
        """
        self.__RunEndMarkReceived = False
        gRunControl._start(self)
        self.ArduinoManager.swriteStartRun()

    def _pause(self):
        """ Pause the RunController (RUNNING -> PAUSED).
        """
        gRunControl._pause(self)
        self.ArduinoManager.swriteStopRun()

    def _resume(self):
        """ Resume the run controller (PAUSED -> RUNNING).
        """
        gRunControl._resume(self)
        self.ArduinoManager.swriteStartRun()

    def _stop(self):
        """ Stop the RunControl (RUNNING -> STOPPED).
        """
        self.ArduinoManager.swriteStopRun()
        self.flushInput()
        gRunControl._stop(self)

    def flushInput(self):
        """ Make sure all the input data wating to be transferred through the
        serial connection have been actually processed.

        This is meant to account for the events that might possibly be collected
        on the arduino in the finite time it takes for the stop signal
        to be tranferred and detected by the microcontroller. At low rate the
        chance of this happening are fairly low, but already at ~500 Hz input
        rate this happens from time to time. We want to be sure all the data
        are collected---oterwise they might end up in the following run if the
        data acquisition is restarted.

        This is not very elegant, as we just sleep for a couple hundred ms
        in order to wait for arduino to push out the last stuff, if any.
        """
        logger.info('Flushing input data stream...')
        time.sleep(0.2)
        n0 = self.getNumEvents()
        while not self.__RunEndMarkReceived:
            self.poll()
        dn = self.getNumEvents() - n0
        if dn:
            logger.info('Done, %s more event(s) transferred.' % dn)
        else:
            logger.info('Done, no events were waiting to be transferred.')

    def readCtrlByte(self):
        """ Read a control byte from the serial port.

        This is the function where we do look at the control byte and set the
        self.__RunEndMarkReceived flag to true if it is the run end mark.
        In order for the flushInput() method to work correctly it is
        important that the subclasses use this wrapper function, as opposed to
        self.ArduinoManager.sreadCtrlByte().
        """
        ctrl = self.ArduinoManager.sreadCtrlByte()
        if ctrl == ptcl.RUN_END_MARKER:
            self.__RunEndMarkReceived = True
            logger.info('Received run end mark (0x%x) from the serial port.' %\
                        ctrl)
        return ctrl

    def pollAnalogReadout(self):
        """ Typical polling cycle for reading the analog inputs.

        Here we read analog readouts from the board.
        """
        ctrl = self.readCtrlByte()
        if ctrl != ptcl.ANALOG_READOUT_HEADER:
            return ctrl
        readout = self.ArduinoManager.sreadAnalogReadout()
        self.addEvent(readout)
        return ctrl

    def pollDigitalTransition(self):
        """ Typical polling cycle for the time measurements.

        Here we essentially read the transitions from the board.
        """
        ctrl = self.readCtrlByte()
        if ctrl != ptcl.DIGITAL_TRANSITION_HEADER:
            return ctrl
        transition = self.ArduinoManager.sreadDigitalTransition()
        self.addEvent(transition)
        return ctrl

    def pollAlternateDigitalTransitions(self):
        """ Typical polling cycle for the time measurements.

        Here we essentially read the transitions from the board and we ensure on
        the fly that each time measurement is on the opposite edge with
        respect to the previous one. If that's not the case the DAQ has really
        lost a transition and we do stop the run (as this would cause spurious
        data to appear in the processed data stream).
        """
        ctrl = self.readCtrlByte()
        if ctrl != ptcl.DIGITAL_TRANSITION_HEADER:
            return ctrl
        transition = self.ArduinoManager.sreadDigitalTransition()
        previous = self.getLastEvent()
        if self.getNumEvents() and transition['edge'] == previous['edge']:
            logger.error('%s detected 2 consecutive transitions on the '
                         'same edge.' % self.__class__.__name__)
            logger.info('(%d) %s' % (self.getNumEvents(), transition))
            logger.info('(%d) %s' % (self.getNumEvents() - 1, previous))
            dt = transition['seconds'] - previous['seconds']
            logger.info('Delta time: %.6f' % dt)
            logger.error('At least one transition got lost, '
                         'forcing stop run...')
            self.setStopped()
        else:
            self.addEvent(transition)
        return ctrl

