#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2013 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import sys
import time
import datetime
import atexit

from PyQt4 import QtCore
from plasduino.gui.gQt import connect
from plasduino.daq.gFiniteStateMachine import gFiniteStateMachine
from plasduino.daq.gRunManager import gRunManager
from plasduino.__logging__ import logger, gFileHandler, abort


class gRunControl(gFiniteStateMachine, gRunManager, QtCore.QThread):

    """ Class implementing the RunControl.

    This class encapsulates all the basic functionalities for the control of
    data acquisision, including:
    (*) reading/writing/incrementing the run number;
    (*) starting the data collection loop on a separate thread;
    (*) storing the data collected.

    The class accept a gEventBuffer object as the only input parameters and
    has some built-in control infrastructure to stop the data acquisition
    after a fixed (i) time or (ii) number of events. For (i) we use an
    additional single-shot QTimer that is set up via the setMaxSeconds()
    class method; for (ii) we do a check on self.DataBuffer.full() in the
    main event loop. Before you start implementing (ii) with a signal/slot
    mechanism take a look at the documentation for the gDataBuffer class.
    
    Among the facilities provided are:
    (*) a QTimer object (self.UpdateTimer) ticking during the data acquisition
    at a "human" rate (typically 0.1 s) that can be used by the main window
    to refresh the widgets displaying data-acquisition-related parameters.
    (*) a QMutex object that can be used to serialize the operations across
    the two threads. Note that we don't do anything with it in the base class
    and it's responsibility of the derived classes to make (proper) use of it.

    The gRunControl objects emit the following signals:
    (*) runIdChanged(int) whenever the run number is changed, the parameter
    being the new run number;
    (*) started() during the execution of the _start() method (note that this
    is automatically emitted by the QThread class);
    (*) stopped() at the beginning of the _stop() method;
    (*) done() at the end of the _stop() method.
    (*) rawDataWritten(QString) at the end of the writeRawData() class method,
    the parameter being the path to the raw data file we just wrote;
    (*) rawDataProcessed(QString) at the end of the processRawData() class
    method, the parameter being the path to the processed data file we just
    wrote.
    """
    
    TIMER_UPDATE_INTERVAL = 100
    
    def __init__(self, eventBuffer, maxSeconds = None):
        """ Constructor.

        Add documentation here.
        """
        gRunManager.__init__(self)
        self.startLoggingToFile()
        gFiniteStateMachine.__init__(self)
        QtCore.QThread.__init__(self)
        self.EventBuffer = eventBuffer
        self.StopRunTimer = QtCore.QTimer(self)
        self.StopRunTimer.setSingleShot(True)
        self.setMaxSeconds(maxSeconds)
        self.UpdateTimer = QtCore.QTimer(self)
        self.UpdateTimer.setInterval(self.TIMER_UPDATE_INTERVAL)
        self.Mutex = QtCore.QMutex()
        self.StartTime = None
        self.StopTime = None
        self.StartDateTime = None
        self.StopDateTime = None
        self.setStopped()
        atexit.register(self.setStopped)

    def getEventBuffer(self):
        """ Return the event buffer.
        """
        return self.EventBuffer

    def getNumEvents(self):
        """ Return the length of the data buffer.
        """
        return len(self.EventBuffer)

    def getLastEvent(self):
        """ Return the last event in the event buffer.

        Return None if the event buffer is empty.
        """
        if len(self.EventBuffer):
            return self.EventBuffer[-1]
        return None

    def getEvent(self, eventNumber):
        """ Return a generic event in the event buffer.

        Return None in case of index error.
        """
        try:
            return self.EventBuffer[eventNumber]
        except IndexError:
            return None

    def addEvent(self, event):
        """ Add an event to the event buffer.
        """
        self.EventBuffer.fill(event)

    def setMaxSeconds(self, seconds = None):
        """ Set the maximum data acquisition time.
        """
        self.__MaxSeconds = seconds
        if seconds is None:
            logger.info('Setting RunControl in free-run mode (time-wise)...')
            if self.StopRunTimer.receivers(QtCore.SIGNAL('timeout()')):
                disconnect(self.StopRunTimer, 'timeout()', self.setStopped)
                logger.info('RunControl stop timer disconnected.')
        else:
            seconds = int(seconds)
            logger.info('Setting maximum data acquisition time to %d s...' %\
                            seconds)
            self.StopRunTimer.setInterval(seconds*1000)
            if not self.StopRunTimer.receivers(QtCore.SIGNAL('timeout()')):
                connect(self.StopRunTimer, 'timeout()', self.setStopped)
                logger.info('RunControl stop timer connected.')

    def getMaxSeconds(self):
        """ Return the current value of the the maximum data acquisition time.
        """
        return self.__MaxSeconds

    def __del__(self):
        """ Destructor.
        """
        if self.LogFileHandler is not None:
            self.stopLoggingToFile()

    def startLoggingToFile(self):
        """ Start directing the log messages to the log file for the run.
        """
        self.LogFileHandler = gFileHandler(self.getLogFilePath())

    def stopLoggingToFile(self):
        """ Stop logging to the log file.
        """
        self.LogFileHandler.close()
        self.LogFileHandler = None

    def writeRawData(self):
        """ Write the raw data to a binary file.
        """
        filePath = self.getRawDataFilePath()
        self.EventBuffer.write(filePath)
        self.emit(QtCore.SIGNAL('rawDataWritten(QString)'), filePath)

    def processRawData(self):
        """ Process the raw data and write the results to file.

        If the proper top-level option is set, a file dialog is prompted
        to the user asking whether he/she wants to save a copy of the
        processed data file.
        """
        if self.getNumEvents() < self.EventBuffer.MIN_NUM_EVENTS:
            logger.info('%d events are not enough to be processed '
                        '(at least %d needed).' %\
                            (self.getNumEvents(),
                             self.EventBuffer.MIN_NUM_EVENTS))
        else:
            filePath = self.getProcessedDataFilePath()
            self.EventBuffer.process(filePath)
            self.emit(QtCore.SIGNAL('rawDataProcessed(QString)'), filePath)

    def getElapsedTime(self):
        """ Return the elapsed time since the beginning of the run.
        """
        if self.StartTime is not None:
            if self.StopTime is None:
                return time.time() - self.StartTime
            else:
                return self.StopTime - self.StartTime
        else:
            return None
  
    def getAverageRate(self):
        """ Return the average rate of events acquired since the
        beginning of the run.
        """
        elapsedTime = self.getElapsedTime()
        if elapsedTime is not None:
            return self.getNumEvents()/elapsedTime
        else:
            return None

    def _setup(self):
        """ Do-nothing overloaded method.
        """
        pass

    def _start(self):
        """ Start the RunControl (STOPPED -> RUNNING).
        
        Here we do quite a few things, namely:
        (*) increment the run number and signal it (for the parent GUI);
        (*) clear the data buffer;
        (*) emit the 'started()' signal;
        (*) start the polling thread (overloaded by the derived classes);
        (*) write ONE to the serial port (set the arduino running).

        Note that the started() signal is implicitely emitted when the
        QtCore.QThread.start() method is called. Maybe this is sub-optimal in
        terms of code transparency and should be changed?
        """
        self.stopLoggingToFile()
        self.incrementRunId()
        self.emit(QtCore.SIGNAL('runIdChanged(int)'), self.RunId)
        self.startLoggingToFile()
        self.EventBuffer.clear()
        QtCore.QThread.start(self)
        self.UpdateTimer.start()
        self.StopRunTimer.start()
        self.StartDateTime = datetime.datetime.now()
        self.StartTime = time.time()
        self.StopTime = None
        logger.info('RunControl started on %s.' % time.asctime())

    def _pause(self):
        """ Pause the RunController (RUNNING -> PAUSED).
        """
        QtCore.QThread.wait(self)
        logger.info('RunControl paused on %s.' % time.asctime())

    def _resume(self):
        """ Resume the run controller (PAUSED -> RUNNING).
        """
        logger.info('RunControl restarted on %s.' % time.asctime())
        QtCore.QThread.start(self)

    def _teardown(self):
        """ Stop the RunControl (PAUSED -> STOPPED).
        """
        self._stop()

    def _stop(self):
        """ Stop the RunControl (RUNNING -> STOPPED).

        Here we:
        (*) wait for the polling thread to exit;
        (*) emit the 'done()' signal.
        """
        self.UpdateTimer.stop()
        self.StopRunTimer.stop()
        self.StopDateTime = datetime.datetime.now()
        self.StopTime = time.time()
        self.emit(QtCore.SIGNAL('stopped()'))
        logger.info('RunControl stopped on %s.' % time.asctime())
        self.writeRawData()
        self.processRawData()
        self.emit(QtCore.SIGNAL('done()'))
        

    def run(self):
        """ Polling loop running on a separate thread.
        """
        while self.isRunning():
            self.poll()
            if self.EventBuffer.full():
                self.setStopped()

    def poll(self):
        """ Actual implementation of the polling cycle.

        This particular one is actually limited to printing on the terminal and
        need to be implemented by the derived classes.
        """
        logger.info('Polling...')
        time.sleep(0.5)



if __name__ == '__main__':
    from plasduino.daq.gEventBuffer import gEventBuffer
    from plasduino.gui.gAcquisitionWindow import gAcquisitionWindow
    from plasduino.__cfgparse__ import getapp
    application = getapp()
    w = gAcquisitionWindow()
    w.RunControl = gRunControl(gEventBuffer())
    w.connectRunControl()
    w.show()
    application.exec_() 
