#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012--2014 Luca Baldini (luca.baldini@pi.infn.it)   *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import plasduino.arduino.protocol_h as ptcl

from plasduino.daq.gEvent import gEvent
from plasduino.__logging__ import logger



class gDigitalTransition(gEvent):

    """ An arduino digital transition is a 5-bit binary array:
    * byte(s) 0  : a header (encapsulating the pin and polarity information);
    * byte(s) 1-4: the actual time in us from micros().

    The corresponding write function on the arduino side is defined
    in arduino/plasduino.h and arduino/plasduino.c.
    """

    HEADER = ptcl.DIGITAL_TRANSITION_HEADER
    LENGTH = 5
    LAYOUT = [('info'   , 'B', 0),
              ('seconds', 'L', 1)
              ]
    RISING_EDGE = 1
    FALLING_EDGE = 0

    def __init__(self, payload):
        """ Constructor.

        Notice that we convert ms to s and we check the header.
        """
        gEvent.__init__(self, payload)
        self['seconds'] *= 1e-6
        self['pinNumber'] = self['info'] & 0x7F
        self['edge'] = (self['info'] >> 7) & 0x1

    def rising(self):
        """ Returns whether the transition is on the rising egde.
        """
        return self['edge'] == self.RISING_EDGE

    def falling(self):
        """ Returns whether the transition is on the falling egde.
        """
        return self['edge'] == self.FALLING_EDGE

    def __str__(self):
        """ String representation.
        """
        return '%s -> %.6f s (pin = %d, edge = %d)' %\
            (repr(self.Payload), self['seconds'], self['pinNumber'],
             self['edge'])



if __name__ == '__main__':
    t0 = gDigitalTransition('\xf0\xa3\x00,O\xe0')
    print(t0)
