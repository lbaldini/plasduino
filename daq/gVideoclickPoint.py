#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.daq.gPoint2d import gPoint2d


class gVideoclickPoint(gPoint2d):

    """ Small utility class representing the position of an object within a
    pixmap for the videoclick module.

    We do support the conversion between pixel and physical coordinates.
    """

    def getRawCoordinates(self):
        """ Return a tuple with the raw coordinates.
        """
        return self.X, self.Y

    def getPhysicalCoordinates(self, origin, scale):
        """ Retrieve the position in physical units (if the scale is available)
        with respect to a given origin for the reference system.
        """
        point = self - origin
        if scale is not None:
            point *= scale
        return point.X, point.Y

    def __str__(self):
        """ String formatting.
        """
        return '(%d, %d)' % (self.X, self.Y)


if __name__ == '__main__':
    p1 = gVideoclickPoint(1, 2)
    p2 = gVideoclickPoint(3, 4)
    print(p1)
    print(p2)
    print(p1 == p2)
    print(p1 == p1)
    print(p1*2)
    print(p1 + p2)
    print(p1 - p2)
    print(p1.getPhysicalCoordinates(gVideoclickPoint(0, 0), 0.1))
