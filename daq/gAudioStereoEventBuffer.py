#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.daq.gEventBuffer import gEventBuffer
from plasduino.daq.gAudioStereoEvent import gAudioStereoEvent
from plasduino.daq.gDataTable import gDataTable
from plasduino.__logging__ import logger


class gAudioStereoEventBuffer(gEventBuffer):

    """ Derived class representing a buffer of (stereo) audio data.
    """

    EVENT_LENGTH = gAudioStereoEvent.LENGTH
    PROCESSED_DATA_FIELDS = ['Time', 'R chan', 'L chan']
    PROCESSED_DATA_UNITS = ['s', 'a.u.', 'a.u.']
    PROCESSED_DATA_FORMAT = ['%.4f', '%.1f', '%.1f']
    MIN_NUM_EVENTS = 1

    def __init__(self, sampleRate, prescaleFactor = 1, maxNumEvents = None):
        """ Constructor.
        """
        gEventBuffer.__init__(self, maxNumEvents)
        self.TimeStep = 1./sampleRate
        self.PrescaleFactor = prescaleFactor

    def clear(self):
        """ Overloaded clear() method.
        """
        gEventBuffer.clear(self)
        self.CurrentIndex = 0

    def fill(self, data):
        """ Fill the event buffer with data from the audiocard.

        Note that the audio data are buffered, therefore we are effectively
        filling a whole bunch of events in one shot, here. To this end we
        return the index of the first event inserted and the total number
        of events inserted to be used by the parent RunControl object and
        the entire downstream processing.
        """
        startIndex = len(self)
        numSamples = 0
        for index in xrange(0, len(data), self.EVENT_LENGTH):
            if (self.CurrentIndex % self.PrescaleFactor == 0):
                event = gAudioStereoEvent(data[index:index + self.EVENT_LENGTH])
                event['seconds'] = self.TimeStep*(self.CurrentIndex)
                gEventBuffer.fill(self, event)
                numSamples += 1
            self.CurrentIndex += 1
        return (startIndex, numSamples)

    def write(self, filePath):
        """ Do-nothing write method.
        
        We might want to save to file the binary data (maybe in .wav format),
        but for long acquisitions this might be a lot of data---and what could
        we possibly do with it? For the time being we only write processed
        data.
        """
        pass

    def fillDataTable(self):
        """ Overload method from the base class.
        """
        for event in self:
            row = (event['seconds'], event['right'], event['left'])
            self.DataTable.addRow(row)
