#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import time
import struct

from plasduino.daq.gDataTable import gDataTable
from plasduino.daq.gRunningStat import gRunningStat
from plasduino.arduino.sketches_h import SKETCH_ID_NO_OP
from plasduino.__logging__ import logger


class gEventBuffer(list):

    """ Class describing an event buffer.

    This is essentially a list with some additional utilities (e.g., to dump
    the content into a file.)

    The class also provides a full() hook that can be used downstream for
    interrupting the data acquisition after a given number of events.
    Consumer applications should be use the full() class method in the event
    loop, in conjunction with the setMaxNumEvents() method.

    We put some thought about enforcing the maximum number of events by means
    of a full() signal to be emitted in the fill() method and to be intercepted
    by the downstream code. This had the drawback that by the time the signal
    is actually intercepted, more events (and in some applications, like the
    plasduino_dice module, *many* more events) could be acquired, so that
    the strategy was ineffective. It wasn't even any faster, as it still
    required one check in the inner event loop.

    Finally, note that if we want to go for a signal/slot paradigm, we need
    to make gEventBuffer inherit from QObject and, to avoid the conflicting
    metaclass issue, we can't inherit from list (i.e. we have to use
    composition rather than inheritance).
    """

    PROCESSED_DATA_FIELDS = None
    PROCESSED_DATA_UNITS = None
    PROCESSED_DATA_FORMAT = None
    MIN_NUM_EVENTS = 1

    def __init__(self, maxNumEvents = None):
        """ Constructor.
        """
        list.__init__(self)
        self.setMaxNumEvents(maxNumEvents)
        self.setProducerInfo()
        self.clear()
        if self.PROCESSED_DATA_FIELDS is not None:
            self.DataTable = gDataTable(self.__class__.__name__,
                                        self.PROCESSED_DATA_FIELDS,
                                        self.PROCESSED_DATA_UNITS,
                                        self.PROCESSED_DATA_FORMAT)
        else:
            self.DataTable = None

    def setProducerInfo(self, producerId = SKETCH_ID_NO_OP,
                        producerVersion = 0):
        """ Set the producer ID and version.
        """
        self.ProducerId = producerId
        self.ProducerVersion = producerVersion

    def clear(self):
        """ Clear the buffer.
        """
        del self[:]

    def full(self):
        """ Return True if the buffer is full.
        """
        return len(self) >= self.__MaxNumEvents

    def getStat(self, field):
        """ Return the average and RMS (in the form of a gRunningStat object)
        for a given field.
        """
        stat = gRunningStat()
        for event in self:
            stat.fill(event[field])
        return stat

    def setMaxNumEvents(self, numEvents = None):
        """ Set the maximum number of events to be acquired.

        If None is passed as an argument, the self.__MaxNumEvents class member
        is set to a very large number. This is to avoid a second condition
        to be checked in the full() class method, which is called in the inner
        loop and therefore should be as fast as possible.
        """
        if numEvents is None:
            numEvents = 1000000000000000
        logger.info('Setting the data buffer maximum size to %d.' % numEvents)
        self.__MaxNumEvents = numEvents

    def fill(self, event):
        """ Append an event to the buffer.

        Note that, for minimizing any overhead, we don't perform any check on
        self.__MaxNumEvents, here. It is responsibility of consumer applications
        to prevent the data buffer to grow beyond limits.
        """
        self.append(event)

    def write(self, filePath):
        """ Write the buffer to file in binary format.

        Under normal condition this dumps into a file what has been read out
        from arduino through the serial interface.
        """
        logger.info('Writing raw data to binary file %s...' % filePath)
        outputFile = open(filePath, 'wb')
        logger.info('Writing header (producer id %s, version %s)...' %\
                    (self.ProducerId, self.ProducerVersion))
        outputFile.write(struct.pack('B', self.ProducerId))
        outputFile.write(struct.pack('B', self.ProducerVersion))
        logger.info('Writing events...')
        for event in self:
            outputFile.write(struct.pack('B', event.HEADER))
            outputFile.writelines(event.Payload)
        outputFile.close()
        logger.info('Done, %d event(s) written.' % len(self))

    def fillDataTable(self):
        """ Process the event buffer and fill the output flat data table.

        The base class method is a do-nothing one. The derived classes have the
        responsibility to implement the desired behavior.
        """
        pass

    def process(self, filePath = None):
        """ Process the event buffer and write a 'baked' output file.

        In the standard base flavour this just fills the event buffer
        data table (calling the fillDataTable() hook that derive classes can
        reimplement) and writes it to the output file.
        """
        if self.DataTable is not None:
            self.DataTable.clear()
            self.fillDataTable()
            if filePath is not None:
                self.DataTable.write(filePath)
            


if __name__ == '__main__':
    b = gEventBuffer()
    b.fill(3)
    b.fill(973256)
    print(b)
    b.clear()
    print(b)
    b.fill(32)
    print(b)
    b.clear()
    import random
    for i in range(1000):
        b.fill({'dummy': random.gauss(10, 1)})
    stat = b.getStat('dummy')
    print(stat)
