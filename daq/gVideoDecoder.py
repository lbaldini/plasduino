#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import plasduino.__utils__ as __utils__

from PyQt4 import QtCore
from plasduino.gui.gQt import connect
from plasduino.__logging__ import logger, abort


class gVideoDecoder(QtCore.QThread):

    """ Base class for a video decoder.

    The basic functionalities that the actual derived classes should
    implement are:
    (*) retrieve the basic video information (frame rate, duration and alike);
    (*) extract the video frames to a given folder.
    The second functionalities should be executed on a separate thread to
    not to loose the capability of interacting with the main GUI.

    Under linux there are several options for video encoding/decoding,
    including ffmpeg, mplayer and avconv. I'm honestly not sure myself which is
    the best/most portable one. When invoking ffmpeg on Ubuntu it says it is
    deprecated (meaning that they'll stop shipping it some time in the
    future?). Fedora 16/17 apparently only ships ffmpeg, so at the very least
    we have to support both. And who knows on windows?

    As a basic strategy we do system calls and parse the command output to get
    the video information. Admittedly, this is not particularly elegant.
    """

    FRAME_BASE_NAME = 'tmp_frame_%0000d'
    ENGINE_NAME = None

    def __init__(self, frameFormat = 'png'):
        """ Constructor.
        """
        QtCore.QThread.__init__(self)
        self.FrameFormat = frameFormat
        logger.info('Initializing video decoder %s (frame format set to %s).' %\
                        (self.ENGINE_NAME, self.FrameFormat))
        from plasduino.__cfgparse__ import TOP_LEVEL_CONFIGURATION
        self.FrameFolder = TOP_LEVEL_CONFIGURATION.get('daq.tmp-file-dir')
        __utils__.createFolder(self.FrameFolder)
        self.clearInfo()
        self.ExtractTimer = QtCore.QTimer()
        self.ExtractTimer.setInterval(50)
        connect(self.ExtractTimer, 'timeout()', self.notifyProgress)

    def clearInfo(self):
        """ Reset the video information.
        """
        self.FilePath = None
        self.Duration = None
        self.FrameRate = None

    def installed(self):
        """ Return True if the underlying decoder is correctly installed.

        TODO: this won't work under Windows.
        """
        return __utils__.programInstalled(self.ENGINE_NAME)[0]

    def open(self, filePath):
        """ Retrieve the basic video information for a video file.
        """
        self.clearInfo()
        logger.info('Retrieving video information for %s...' % filePath)
        self.FilePath = filePath
        self.getInfo()
        if not self.hasInfo():
            return 
        logger.info('Done: duration = %s s, frame rate = %s fps (%d frames).' %\
                        (self.Duration, self.FrameRate, self.getNumFrames()))

    def hasInfo(self):
        """ Return True of we have complete video information.
        """
        return self.Duration is not None and self.FrameRate is not None

    def getInfo(self):
        """ Do-nothing method, to be implemented in derived classes.
        """
        pass

    def formatFrameRate(self):
        """ Do-nothing method, to be implemented in derived classes.
        """
        pass

    def forceFrameRate(self, frameRate):
        """ Set the frame rate, irrespectively of the value returned by the
        underlying decoder.

        The frame rate (tbr) inferred from the video stream is not always
        accurate (not sure why, we should do some research, but most likely
        it has to do with the codecs), therefore we leave the option of
        overriding it after the entire clip information has been parsed.
        """
        logger.info('Forcing frame rate to %.2d...' % frameRate)
        self.Duration *= (self.FrameRate/frameRate)
        self.FrameRate = frameRate
        logger.info('Done: duration = %s s, frame rate = %s fps (%d frames).' %\
                        (self.Duration, self.FrameRate, self.getNumFrames()))

    def getNumFrames(self):
        """ Return the number of frames.
        """
        if not self.hasInfo():
            return None
        return int(self.FrameRate*self.Duration)

    def extract(self):
        """ Do-nothing method, to be implemented in derived classes.
        """
        pass

    def notifyProgress(self):
        """ Notify the progress in the frame-extraction process.
        """
        self.emit(QtCore.SIGNAL('progress(int)'), self.progress())

    def run(self):
        """ Extract the frames on a separate thread.
        """
        self.ExtractTimer.start()
        self.extract()
        self.ExtractTimer.stop()
        self.emit(QtCore.SIGNAL('done()'))

    def cleanup(self, obj = None):
        """ Clean up the single-frame images.
        """
        __utils__.cleanup(self.FrameFolder)
        if obj != None:
            __utils__.rm(obj)
            

    def progress(self):
        """ Return the progress (between 0 and 100) in the frame extraction.
        """
        if not self.hasInfo():
            return 0
        return 100*len(os.listdir(self.FrameFolder))/self.getNumFrames()

    def getFrameFilePath(self, i):
        """ Return the full file path to the i-th frame.

        For the file to be there, it is assumed that the extract() function
        has been called and the cleanup has not been performed, yet.
        """
        fileName = self.FRAME_BASE_NAME.replace('%0000d', '%s' % i)
        fileName = '%s.%s' % (fileName, self.FrameFormat)
        return os.path.join(self.FrameFolder, fileName)
