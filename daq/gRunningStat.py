#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math


class gRunningStat:

    """ Small utility class to calculate the running sample average and
    variance.

    
    This method is described in paper by B. P. Welford dating back to 1962 and
    is presented in Donald Knuth's Art of Computer Programming (3rd edition),
    Vol 2, page 232.
    
    See also http://www.johndcook.com/standard_deviation.html for additional
    information.
    """

    def __init__(self):
        """ Constructor.
        """
        self.reset()

    def reset(self):
        """ Reset everything.
        """
        self.__NumValues = 0
        self.__CurM = 0.
        self.__CurS = 0.
        self.__PrevM = 0.
        self.__PrevS = 0.

    def fill(self, value):
        """ Fill one value into the running stat.

        Note that we do need to cast value to float, otherwise the
        average is screwed if we only pass integer values.
        """
        value = float(value)
        self.__NumValues += 1
        if self.__NumValues == 1:
            self.__CurM = self.__PrevM = value
        else:
            self.__CurM = self.__PrevM + (value - self.__PrevM)/self.__NumValues
            self.__CurS = self.__PrevS + (value - self.__PrevM)*\
                (value - self.__CurM)
            self.__PrevM = self.__CurM
            self.__PrevS = self.__CurS

    def getNumValues(self):
        """ Return the number of valued fed into the running stat.
        """
        return self.__NumValues

    def getAverage(self):
        """ Return the sample (i.e. arithmetic) average.
        """
        if self.__NumValues >= 1:
            return self.__CurM
        else:
            return None

    def getVariance(self):
        """ Return the sample variance.
        """
        if self.__NumValues >= 2:
            return self.__CurS/(self.__NumValues - 1)
        else:
            return None

    def getStandardDeviation(self):
        """ Return the square root of the sample variance.
        """
        if self.__NumValues >= 2:
            return math.sqrt(self.getVariance())
        else:
            return None

    def __str__(self):
        """ String representation.
        """
        return 'Sample mean = %f, sample standard deviation = %f '\
            '(%d entries)' % (self.getAverage(), self.getStandardDeviation(),
                               self.getNumValues())



if __name__ == '__main__':
    import random
    s = gRunningStat()
    for i in range(10000):
        s.fill(random.gauss(10, 1))
    print(s)
