#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from plasduino.__logging__ import logger


class gFiniteStateMachine:

    """Class implementing a finite state machine.

    The finite state machine is essentially defined by a set of possible
    states and transitions between those states. Not all the transitions are
    allowed at any moment (i.e. RUNNING -> RESET). The forbidden transitions
    are rejected by returning None.
    """

    __RESET   = 0x0
    __STOPPED = 0x1
    __RUNNING = 0x2
    __PAUSED  = 0x3

    def __init__(self):
        """ Constructor.
        """        
        self.__Status = self.__RESET

    def getStatus(self):
        """
        """
        return self.__Status

    def isReset(self):
        """ Return whether the machine is in the RESET state.
        """
        return (self.__Status == self.__RESET)

    def isStopped(self):
        """ Return whether the machine is in the STOPPED state.
        """
        return (self.__Status == self.__STOPPED)

    def isRunning(self):
        """ Return whether the machine is in the RUNNING state.
        """
        return (self.__Status == self.__RUNNING)

    def isPaused(self):
        """ Return whether the machine is in the PAUSED state.
        """
        return (self.__Status == self.__PAUSED)

    def setRunning(self):
        """
        """
        if self.__Status == self.__STOPPED:
            self.__Status = self.__RUNNING
            self._start()
        elif self.__Status == self.__PAUSED:
            self.__Status = self.__RUNNING
            self._resume()
        else:
            return None
        
    def setStopped(self):
        """
        """
        if self.__Status == self.__RESET:
            self.__Status = self.__STOPPED
            self._setup()
        elif self.__Status == self.__RUNNING:
            self.__Status = self.__STOPPED
            self._stop()
        elif self.__Status == self.__PAUSED:
            self.__Status = self.__STOPPED
            self._teardown()
        else:
            return None
        
    def setPaused(self):
        """
        """
        if self.__Status == self.__RUNNING:
            self.__Status = self.__PAUSED
            self._pause()
        else:
            return None
        
    def setReset(self):
        """
        """
        if self.__Status == self.__STOPPED:
            self.__Status = self.__RESET
            self._reset()
        else:
            return None
        
    def _setup(self):
        """ Do-nothing function called from RESET -> STOPPED.

        To be overloaded in the derived classes.
        """
        logger.info('gFiniteStateMachine._setup() not overloaded.')

    def _start(self):
        """ Do-nothing function called from STOPPED -> RUNNING.

        To be overloaded in the derived classes.
        """
        logger.info('gFiniteStateMachine._start() not overloaded.')
    
    def _stop(self):
        """ Do-nothing function called from RUNNING -> STOPPED.

        To be overloaded in the derived classes.
        """
        logger.info('gFiniteStateMachine._stop() not overloaded.')

    def _pause(self):
        """ Do-nothing function called from RUNNING -> PAUSED.

        To be overloaded in the derived classes.
        """
        logger.info('gFiniteStateMachine._pause() not overloaded.')

    def _resume(self):
        """ Do-nothing function called from PAUSED -> RUNNING.

        To be overloaded in the derived classes.
        """
        logger.info('gFiniteStateMachine._resume() not overloaded.')

    def _teardown(self):
        """ Do-nothing function called from PAUSED -> STOPPED.

        To be overloaded in the derived classes.
        """
        logger.info('gFiniteStateMachine._teardown() not overloaded.')

    def _reset(self):
        """ Do-nothing function called from STOPPED -> RESET.

        To be overloaded in the derived classes.
        """
        logger.info('gFiniteStateMachine._reset() not overloaded.')


