#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import time

from plasduino.__logging__ import logger
from plasduino.gui.gFileDialog import FILE_TYPE_TXT, FILE_TYPE_CSV,\
    isFileType, getFileExtension


class gDataTable(list):

    """ Small utility class representing a table of data (i.e., a flat
    numeric ntuple).

    The aim is to provide all methods to write/read/convert to/from file in
    different formats.
    """

    EOL_LF = '\n'
    EOL_CR = '\r'
    EOL_CRLF = '%s%s' % (EOL_CR, EOL_LF)
    DEFAULT_FORMAT = '%.2f'
    DEFAULT_UNITS = 'a.u.'
    COMMENT_MARKER = '#'
    TXT_SEP = '\t'
    TXT_EOL = EOL_LF
    CSV_SEP = ','
    CSV_EOL = EOL_CRLF

    def __init__(self, creator, header, units = None, fmtStrings = None,
                 creationTime = None):
        """ Constructor.
        """
        list.__init__(self)
        self.Creator = creator
        self.CreationTime = creationTime or time.asctime()
        self.Header = header
        self.Units = units
        if self.Units is not None and len(self.Units) != len(self.Header):
            logger.warn('Data table length mismatch between header and units '
                        '(setting units to None).')
            logger.info('Header : %s' % header)
            logger.info('Units  : %s' % units)
            self.Units = None
        if self.Units is None:
            self.Units = [self.DEFAULT_UNITS]*len(self.Header)
        self.FormatStrings = fmtStrings
        if self.FormatStrings is not None and \
                len(self.FormatStrings) != len(self.Header):
            logger.warn('Data table length mismatch between header and formats '
                        '(setting formats to None).')
            logger.info('Header : %s' % header)
            logger.info('Formats: %s' % units)
            self.FormatStrings = None
        if self.FormatStrings is None:
            self.FormatStrings = [self.DEFAULT_FORMAT]*len(self.Header)

    def getNumRows(self):
        """ Return the number of rows.
        """
        return len(self)

    def getNumColumns(self):
        """ Return the number of columns.
        """
        return len(self.Header)

    def clear(self):
        """ Clear the table contents.
        """
        del self[:]

    def addRow(self, row):
        """ Add a row to the table.
        """
        self.append(row)

    def getRow(self, row):
        """ Return a given table row.
        """
        return self[row]

    def getColumn(self, col):
        """ Return a data column.
        """
        return [row[col] for row in self]

    def getColumns(self):
        """ Return all the data column.
        """
        return [[row[i] for row in self] for i in range(self.getNumColumns())]

    def getValue(self, row, col):
        """ Return a given data point in the table.
        """
        return self[row][col]

    def toText(self, separator, eol, maxEntries = None):
        """ Basic formatting engine.
        """
        text = '%sCreator: %s%s' % (self.COMMENT_MARKER, self.Creator, eol)
        text += '%sCreation time: %s%s' %\
            (self.COMMENT_MARKER, self.CreationTime, eol)
        text += self.COMMENT_MARKER
        for item in self.Header:
            text += '%s%s' % (item, separator)
        text = '%s%s%s' % (text.rstrip(separator), eol, self.COMMENT_MARKER)
        for item in self.Units:
            text += '[%s]%s' % (item, separator)
        text = '%s%s' % (text.rstrip(separator), eol)
        # Standard mode, i.e., with no limits on the number of entries.
        if maxEntries is None or len(self) <= maxEntries:
            for row in self:
                for (i, value) in enumerate(row):
                    fmtString = '%s%s' % (self.FormatStrings[i], separator)
                    text += fmtString % value
                text = '%s%s' % (text.rstrip(separator), eol)
        # __str__() mode, i.e., limiting the number of entries to be written
        # out.
        else:
            for row in self[:int(maxEntries/2)]:
                for (i, value) in enumerate(row):
                    fmtString = '%s%s' % (self.FormatStrings[i], separator)
                    text += fmtString % value
                text = '%s%s' % (text.rstrip(separator), eol)
            text += '...%s' % eol
            for row in self[-int(maxEntries/2):]:
                for (i, value) in enumerate(row):
                    fmtString = '%s%s' % (self.FormatStrings[i], separator)
                    text += fmtString % value
                text = '%s%s' % (text.rstrip(separator), eol)
        return text

    def toTxt(self):
        """ Format the table as plain text.
        """
        return self.toText(self.TXT_SEP, self.TXT_EOL)

    def toCsv(self):
        """ Format the table as cvs.
        """
        return self.toText(self.CSV_SEP, self.CSV_EOL)

    def write(self, filePath):
        """ Write the data table to file.
        """
        if isFileType(filePath, FILE_TYPE_TXT):
            self.writeTxt(filePath)
        elif isFileType(filePath, FILE_TYPE_CSV):
            self.writeCsv(filePath)
        else:
            extension = getFileExtension(filePath)
            logger.error('File format %s not supported for gDataTable.')

    def writeTxt(self, filePath):
        """ Write the data table to file in text format.
        """
        logger.info('Writing data table to %s in text format...' % filePath)
        open(filePath, 'w').write(self.toTxt())
        logger.info('Done (short dump following).\n%s' % self)

    def writeCsv(self, filePath):
        """ Write the data table to file in csv format.
        """
        logger.info('Writing data table to %s in csv format...' % filePath)
        open(filePath, 'w').write(self.toCsv())
        logger.info('Done (short dump following).\n%s' % self)

    def __str__(self):
        """ String formatting.
        """
        separator = self.TXT_SEP
        eol = self.TXT_EOL
        text = self.toText(separator, eol, 4)
        return text.rstrip(eol)


""" Utility functions to read table data from files.
"""

def __header(line, formatter):
    """ Parse the table header from a line of text.
    """
    line = line.lstrip(gDataTable.COMMENT_MARKER)
    return formatter(line)

def __units(line, formatter):
    """ Parse the table units from a line of text.
    """
    line = line.lstrip(gDataTable.COMMENT_MARKER)
    line = line.replace('[', '').replace(']', '')
    return formatter(line)

def __formatStrings(line, formatter):
    """ Parse the format strings from a line of text.
    """
    fmtStrings = []
    for item in formatter(line):
        if not '.' in item:
            fmtStrings.append('%d')
        else:
            decimals = len(item.split('.')[-1])
            fmtStrings.append('%%.%df' % decimals)
    return fmtStrings

def __row(line, formatter):
    """ Parse a table row from file from a line of text.
    """
    row = []
    for (i, item) in enumerate(formatter(line)):
        if '.' not in item:
            value = int(item)
        else:
            value = float(item)
        row.append(value)
    return row

def __table(data, formatter):
    """ Parse an entire table from the text in a file.
    """
    creator = data.next()
    if not 'Creator' in creator:
        logger.error('Missing creator field, returning None...')
        return None
    creator = creator.split(':', 1)[1].strip()
    creationTime = data.next()
    if not 'Creation time' in creationTime:
        logger.error('Missing creation time field, returning None...')
        return None
    creationTime = creationTime.split(':', 1)[1].strip()
    header = __header(data.next(), formatter)
    units = __units(data.next(), formatter)
    firstLine = data.next()
    fmtStrings = __formatStrings(firstLine, formatter)
    table = gDataTable(creator, header, units, fmtStrings, creationTime)
    table.addRow(__row(firstLine, formatter))
    for line in data:
        row = __row(line, formatter)
        table.addRow(row)
    return table



def readDataTableTxt(filePath):
    """ Read the data table from a text file.
    """
    
    def formatter(line):
        """ Format a line.
        """
        line = line.rstrip(gDataTable.TXT_EOL)
        return line.split(gDataTable.TXT_SEP)

    data = open(filePath)
    return __table(data, formatter)



def readDataTableCsv(filePath):
    """ Read the data table from csv file.
    """

    def formatter(line):
        """ Format a line.
        """
        line = line.rstrip(gDataTable.CSV_EOL)
        return line.split(gDataTable.CSV_SEP)

    data = open(filePath)
    return __table(data, formatter)



def readDataTable(filePath, verbose = True):
    """ Read the data table from file.
    """
    logger.info('Reading data table from %s...' % filePath)
    if isFileType(filePath, FILE_TYPE_TXT):
        table = readDataTableTxt(filePath)
    elif isFileType(filePath, FILE_TYPE_CSV):
        table = readDataTableCsv(filePath)
    else:
        extension = getFileExtension(filePath)
        logger.error('File format %s not supported for gDataTable.')
        table = None
    if table is not None:
        if verbose:
            logger.info('Done (short dump following).\n%s' % table)
        else:
            logger.info('Done.')
        return table



def main():
    """ Test program.
    """
    creator = 'gDataTable'
    txtFilePath = 'table.txt'
    csvFilePath = 'table.csv'
    header = ['n', 't' , 'x']
    units = ['', 's', 'cm']
    fmtStrings = ['%d', '%.4f', '%.1f']
    table = gDataTable(creator, header, units, fmtStrings)
    for i in range(10):
        table.addRow([i, 2*i, 3*i])
    print(table)
    table.write(txtFilePath)
    table.write(csvFilePath)
    txtTable = readDataTable(txtFilePath)
    print(txtTable)
    csvTable = readDataTable(csvFilePath)
    print(csvTable)


if __name__ == '__main__':
    main()
