#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2012 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

from plasduino.__logging__ import logger


""" Basic environment for the package.
"""

PACKAGE_NAME = 'plasduino'
PLASDUINO_WEB_PAGE = 'http://pythonhosted.org/plasduino/'
PLASDUINO_ROOT = os.path.abspath(os.path.dirname(__file__))
PLASDUINO_ANALYSIS = os.path.join(PLASDUINO_ROOT, 'analysis')
PLASDUINO_ARDUINO = os.path.join(PLASDUINO_ROOT, 'arduino')
PLASDUINO_ARTWORK = os.path.join(PLASDUINO_ROOT, 'artwork')
PLASDUINO_DAQ = os.path.join(PLASDUINO_ROOT, 'daq')
PLASDUINO_DEVICES = os.path.join(PLASDUINO_ROOT, 'devices')
PLASDUINO_DIST = os.path.join(PLASDUINO_ROOT, 'dist')
PLASDUINO_DOC = os.path.join(PLASDUINO_ROOT, 'doc')
PLASDUINO_DATASHEETS = os.path.join(PLASDUINO_DOC, 'datasheets')
PLASDUINO_GUI = os.path.join(PLASDUINO_ROOT, 'gui')
PLASDUINO_MODULES = os.path.join(PLASDUINO_ROOT, 'modules')
PLASDUINO_MODULEMANAGER = os.path.join(PLASDUINO_ROOT, 'modulemanager')
PLASDUINO_SENSORS = os.path.join(PLASDUINO_ROOT, 'sensors')
PLASDUINO_SKINS = os.path.join(PLASDUINO_ARTWORK, 'skins')
PLASDUINO_WEB = os.path.join(PLASDUINO_DOC, 'webpage')
PLASDUINO_WEB_PAGES = os.path.join(PLASDUINO_WEB, 'pages')
PLASDUINO_WEB_CSS = os.path.join(PLASDUINO_WEB, 'css')
PLASDUINO_WEB_IMAGES = os.path.join(PLASDUINO_WEB, 'images')
PLASDUINO_WEB_HTML = os.path.join(PLASDUINO_DIST, 'doc', 'html')


""" Release notes file path.
"""
RELEASE_NOTES_PATH = os.path.join(PLASDUINO_DOC, 'release.notes')

""" File containing the package tag and build date.
"""

TAG_FILE_PATH = os.path.join(PLASDUINO_ROOT, '__tag__.py')

def getTagInfo():
    """ Read the tag and build date straight from the appropriate file.
    
    Use this when you don't want to import the module (i.e., at release time,
    when the file is changed), so that you don't have to bother with
    reloading stuff.
    """
    for line in open(TAG_FILE_PATH).readlines():
        exec(line.strip('\n'))
    return TAG, BUILD_DATE


""" Path to the pickle file containing the relevant information for the
modules shipped with the distribution.

The file is created at install time through a post-installation script.
"""
PLASDUINO_DIST_MODULEINFO_FILE_PATH = os.path.join(PLASDUINO_MODULEMANAGER,
                                                   'dist_modules.pickle')

if __name__ == '__main__':
    for item in dir():
        if item.isupper():
            logger.info('%s = %s' % (item, eval(item)))
    print getTagInfo()
