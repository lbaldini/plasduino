plasduino (2.3.0) UNRELEASED; urgency=low

  * Unknown changes

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sat, 23 Jan 2016 08:46:16 -0800

plasduino (2.2.0) UNRELEASED; urgency=low

  * Unknown changes

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sat, 16 Jan 2016 08:20:28 -0800

plasduino (2.1.2) UNRELEASED; urgency=low

  * Unknown changes

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 02 Jul 2014 11:27:37 +0200

plasduino (2.1.1) UNRELEASED; urgency=low

  * Bug fix in the penduluview and pendulumdrive modules (issue #223).
  * Copyright notice updated (issue #220).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 18 Mar 2014 16:57:13 +0100

plasduino (2.1.0) UNRELEASED; urgency=low

  * Removed the obsolete file setup.cfg (issue #218).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 11 Feb 2014 21:15:59 +0100

plasduino (2.0.2) UNRELEASED; urgency=low

  * Minor changes to the web page.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 06 Feb 2014 14:45:12 +0100

plasduino (2.0.1) UNRELEASED; urgency=low

  * Mac OS installation instructions on the web (issue #214).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 06 Feb 2014 14:37:54 +0100

plasduino (2.0.0) UNRELEASED; urgency=low

  * Removed plasduino logo from the main application windows (issue #213).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 06 Feb 2014 12:51:32 +0100

plasduino (1.8.0) UNRELEASED; urgency=low

  * Minor change to the headers in the protocol definition.
  * Version bumped for all the sketches.
  * Sketch id and version added in the output binary file (issue #208).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 06 Feb 2014 12:42:16 +0100

plasduino (1.7.1) UNRELEASED; urgency=low

  * Input pin for the pendulumdrive module changed from 4 to 5 (issue #183).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 06 Feb 2014 11:18:50 +0100

plasduino (1.7.0) UNRELEASED; urgency=low

  * Obsolete plasduino_help module removed.
  * _config() method added to all the sketches (this is where we parse the
  configuration commands from the serial port).
  * arduino/opcodes.h moved to arduino/protocol.h, with all the relevant
  #define statements moved in there.
  * A couple of tweaks to the web page (including two brand new pictures).
  * First complete version of the NCC paper for the SIF congress proceedings.
  * Shield schematics removed from the distribution packages (they are
  available on the web and linked from the web page in a proper way).
  * Pendulum post-processing routine changed (issue #204).
  * Encoding of basic data structures (digital transition and analog readout)
  changed (issue #207).
  * Version bumped for the relevant sketches.
  * Added a few bits of documentations for the issue of having the user added
  to the dialout group.
  * Added a command-line option to arduino/compile.py to skip selected
  sketched when recompiling everything.
  * Subtle bug fix in arduino/upload.py.
  * Run endmark mechanism implemented (issues #216 and #198). 

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 06 Feb 2014 11:15:20 +0100

plasduino (1.6.1) UNRELEASED; urgency=low

  * Some more work on the NCC paper.
  * Facilities to retrieve the temperature measurement errors added to
  the thermistor class.
  * Added a small utility test/testBinary.py to playback binary data files.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 26 Nov 2013 13:53:31 +0100

plasduino (1.6.0) UNRELEASED; urgency=low

  * "Load data from file" functionality implemented for the tempmonitor,
  pendulumview and pendulumdrive modules (issue #202).
  * Name mismatch fixed in the (obsolete) pendulumaudio module.
  * A few classes slightly refactored to support the "Load data from file"
  functionality in the analog monitor applications (issue #202).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 25 Nov 2013 14:02:39 +0100

plasduino (1.5.1) UNRELEASED; urgency=low

  * Minor change to the web page.
  * Bug fix for the tempmonitor module (issue #203).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 25 Nov 2013 11:30:39 +0100

plasduino (1.5.0) UNRELEASED; urgency=low

  * Initial work on the PPS calibration sketch (issue #197).
  * Cleanup of __NXFT15XH103FA2B__.py (issue #201).
  * Old folder misc removed (issue #200).
  * Menu entry added to the debian package (issue #70).
  * Old folder doc/writeups removed.
  * analysis folder added to collect all the ROOT macros we use to produce
  performance and calibration plots; first round of plots for the NCC paper
  produced.
  * gDataTable.__str__() method speeded up *a lot*. This was actually the
  bottleneck in quite a few situation, so we probably want to deploy this
  as soon as possible (issue #199). 
  * New board definition files downloaded and parsed (issue #196).
  * Initial work on the new counting sketches (issue #74).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 25 Nov 2013 11:16:55 +0100

plasduino (1.4.0) UNRELEASED; urgency=low

  * Reference system added to the main pixmap on the videclick module.
  * Implemented an option in the videclick module to display all the registered
  positions (issue #187).
  * Bug fix in the videoclick module (issue #193).
  * Display of the object coordinates in physical units for the
  videoclick module implemented (issue #172).
  * All the relevant classes factored out the videoclick module (issue #192).
  * Video Click window modified to support visualization options
  (issues #172 and #187).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 15 Nov 2013 17:07:19 +0100

plasduino (1.3.3) UNRELEASED; urgency=low

  * Final revision of the lab1 datasheet (issue #181).
  * First draft of the paper for the special issue of Nuovo Cimento.
  * timeResolutionPlot.py script fully revamped.
  * First version of the GPS 610F pitch adapter (issue #189).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 13 Nov 2013 21:10:38 +0100

plasduino (1.3.2) UNRELEASED; urgency=low

  * Typo fixed in the (now old) presentation for the SIF 2013 conference.
  * Minimum value set for the pwm in the pendulumdrive module (issue #184).
  * Small tweaks to the lab1 datasheet (issue #181).
  * Added a new section on the web home page.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 06 Nov 2013 22:27:43 +0100

plasduino (1.3.1) UNRELEASED; urgency=low

  * Remove all the html documentation from the distribution packages
  (issue #177).
  * "Help->Online Help" entry removed from the main menu.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 06 Nov 2013 00:32:39 +0100

plasduino (1.3.0) UNRELEASED; urgency=low

  * Spell-check of the web pages.
  * Compilation sctipts modified to use the new board infrastructure.
  * All sketches recompiled with the new board naming scheme.
  * Basic functionalities verified with the three boards supported at this
  time, i.e. uno, mega and diecimila (issue #179).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 05 Nov 2013 19:51:46 +0100

plasduino (1.2.1) UNRELEASED; urgency=low

  * vid, pid for the arduino diecimila put in by hand.
  * Slight refactoring of the code related to the __boards__.py generation
  to handle the hard-coded hacks more sensibly.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 04 Nov 2013 17:24:09 +0100

plasduino (1.2.0) UNRELEASED; urgency=low

  * Small utility to grep for a generic piece of code in the repository added.
  * Old boards.txt file removed, along with a bunch of functions dealing with
  it, in favor of the new __boards__.py module, which is used throughout
  (issue #174).
  * Old BOARD_DICT dictionary defined in __usbid__.py removed in favor of
  of the new dictionary automatically created in the __bords__.py
  module (issue #176).
  * One minor bug fix in gArduinoManager.py.
  * daq.__usbid__.py moved to daq.__serial__.py.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 04 Nov 2013 15:27:38 +0100

plasduino (1.1.9) UNRELEASED; urgency=low

  * Added a command-line switch for the branch in the boards2py script.
  * Dictionary of vid and pid for the serial interfaces dumped in the
  __boards__.py file (issue #176).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 03 Nov 2013 21:50:18 +0100

plasduino (1.1.8) UNRELEASED; urgency=low

  * New __boards__.py module, along with all the necessary machinery to generate
  it from the standard arduino boards.txt file, now in place, though
  not yet used (issue #174).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 03 Nov 2013 08:37:31 +0100

plasduino (1.1.7) UNRELEASED; urgency=low

  * All relevant links to bitbucket files on the web page pointing to the tip
  (issue #166).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 01 Nov 2013 23:20:11 +0100

plasduino (1.1.6) UNRELEASED; urgency=low

  * Minor change in a web page.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 31 Oct 2013 23:13:42 +0100

plasduino (1.1.5) UNRELEASED; urgency=low

  * Minor tweaks to the web-based documentation about the lab1 shield.
  * Cleanup enforcing consistency between the various names (Lab1, Lab 1, LabI,
  labI, Lab I) used for the lab1 shield.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 31 Oct 2013 23:05:10 +0100

plasduino (1.1.4) UNRELEASED; urgency=low

  * First decent version of the datasheets for the lab1 shield (issue #167).
  * Revamped the web pages for the shields (issue #168).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 31 Oct 2013 17:18:11 +0100

plasduino (1.1.3) UNRELEASED; urgency=low

  * Commited and pushed to the main branch a necessary orphane file.
  * vid/pid added for the arduino due (issue #164).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 30 Oct 2013 17:49:46 +0100

plasduino (1.1.2) UNRELEASED; urgency=low

  * Fix for the narrowing conversion warnings from the compiler (issue #147).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 30 Oct 2013 13:31:38 +0100

plasduino (1.1.1) UNRELEASED; urgency=low

  * Added a small utility module to scan the serial interfaces.
  * A python syntax warning fixed.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 30 Oct 2013 00:27:55 +0100

plasduino (1.1.0) UNRELEASED; urgency=low

  * arduino mega2560 added to the list of supported boards (issue #135).
  * vid and pid for the mega2560 added to the proper __usb__.py file
  (issue #135).
  * Added full support for the mega2560 board to the current list of sketches
  (issue #135).
  * Cleanup script added to the arduino directory.
  * Committed all the files for the frozen versions of the shields (i.e., lab1)
  we have produced so far (issue #168).
  * Initial version of the data sheets for the shields (issue #167).
  * Some data and macros for the timing tests with the GPS.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 29 Oct 2013 18:51:43 +0100

plasduino (1.0.2) UNRELEASED; urgency=low

  * Target explicitly set to noarch for the rpmbuild command in the release
  script, so that we can trigger the generation of an rpm on both 32 and 64
  bit machines.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 25 Oct 2013 16:39:32 +0200

plasduino (1.0.1) UNRELEASED; urgency=low

  * Minor change to the release script.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 25 Oct 2013 16:23:29 +0200

plasduino (1.0.0) UNRELEASED; urgency=low

  * Fix for issue #160.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 22 Oct 2013 23:23:22 +0200

plasduino (0.16.0) UNRELEASED; urgency=low

  * Meta data on pypi updated in view of the first 1x release.
  * Fix for the long standing issue #152.
  * Pin assignment for the analog pendulum modules updated to the final lab1
  version 2 shield.
  * A few more minor changes.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 22 Oct 2013 22:56:55 +0200

plasduino (0.15.4) UNRELEASED; urgency=low

  * Add a method to the gRunningStat class to calculate the average and
  standard deviation of a given field.
  * Zero offset routine reworked for the pendulum modules (issue #128).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 27 Jun 2013 15:22:37 +0200

plasduino (0.15.3) UNRELEASED; urgency=low

  * Minor tweak to the pendulumdrive GUI.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 24 Jun 2013 23:46:47 +0200

plasduino (0.15.2) UNRELEASED; urgency=low

  * Got rid of the frame in the plot legends.
  * Added a push button to enable/disable the PWM output in the pendulumdrive
  module (issue #154).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 24 Jun 2013 23:39:46 +0200

plasduino (0.15.1) UNRELEASED; urgency=low

  * __cfgparse__ facility used in autodetecting the usb port (issue #18).
  * One orphane web page added to the repository.
  * Installation targets removed from the top-level Makefile, which is not
  shipped with the distribution anymore (issue #30).
  * Installation web pages tweaked a little bit.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 23 Jun 2013 16:28:13 +0200

plasduino (0.15.0) UNRELEASED; urgency=low

  * First implementation of the sketch and module for the driven pendulum
  (issue #154).
  * More cleanup and a couple of minor bug fixes.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 23 Jun 2013 16:02:34 +0200

plasduino (0.14.2) UNRELEASED; urgency=low

  * Cleanup of the pendulumview module (remove hardcoded pin numbers and treat
  the offset as a float).
  * Added access by pin to the gStripChartGroup class (issue #156).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sat, 22 Jun 2013 16:48:38 +0200

plasduino (0.14.1) UNRELEASED; urgency=low

  * Some cleanup.
  * Sketch information added to the module web pages (issue #140).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 21 Jun 2013 21:14:09 +0200

plasduino (0.14.0) UNRELEASED; urgency=low

  * Sketch name and version strings for the handshaking replaced with
  uint8_t---now sketch ID and version (issue #144).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 21 Jun 2013 14:56:00 +0200

plasduino (0.13.0) UNRELEASED; urgency=low

  * Too large window size issue resolved (issue #148).
  * Folder data renamed to misc.
  * gTimestamp class renamed to gDigitalTransition (issue #145).
  * Updated screenshots put on the webpage (issue #151).
  * Analog sampling sketch modified to make the analog readouts evenly spaced
  through the round robin loop over the analog pins (issue #150). Note that
  for the corresponding modules now we keep track of the readout times
  for each single pin separately (which is more correct than before
  anyway).
  * Appearance of the pendulumview module tweaked a little bit (issue #153).
  * Got rid of all the t0 subtraction in the event buffers for the relevant
  modules, now that we keep track of the start run in the RunControl class
  running on arduino (issue #115).
  * Medium code refactoring to use the new gStripChartGroup class and proper
  handling of the x-range for the modules using the analog monitor window.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 21 Jun 2013 12:37:31 +0200

plasduino (0.12.0) UNRELEASED; urgency=low

  * Some refactoring of the arduino code, with the opcodes moved to a separate
  file and all the #define statements in the C/C++ header files translated
  into python modules, rather than parsed at run time (issue #132)
  * Major refactoring of the gArduinoManager and gArduinoRunControl classes
  (issue #134).
  * First full implementation of the RunControl class (issue #116).
  * First full implementation of the new communication protocol (issue #132).
  * Regenerate the python modules from the C/C++ header file at each
  compilation to keep stuff in synch (issue #139).
  * Show compilation warnings by default when compiling sketches (issue #142).
  * Brand new logo and icon (issue #146).
  * Major refactoring of the sketches (issue #143).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 14 Jun 2013 20:28:41 +0200

plasduino (0.11.1) UNRELEASED; urgency=low

  * Some minor tweaks to the wavegen module.
  * Added PyQwt to the list of prerequisites on the web page (issue #137).
  * All module dependencies reviewed (issue #137).
  * Added a try/except in the release.py script for preventing the script
  itself from crashing when the $PYTHONPATH environmental variable is not
  defined (issue #136).
  * Call to plasduino_updatemoduleinfo.py added in the install target of the
  Makefile (issue #136).
  * Installation instructions updated (issue #136).
  * Output temporary folder for the html documentation changed from
  doc/html to dist/doc/html.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 24 May 2013 15:33:35 +0200

plasduino (0.11.0) UNRELEASED; urgency=low

  * Minor changes to the webpage.
  * Created a new module to drive the pendulum view setup with arduino.
  The old audiocard-based module has been renamed as
  plasduino_pendulumaudio.py, with no __moduleinfo__ so that it won't show up
  in the launcher (issue #118).
  * Initial functional implementation of the wave generator module (issue #119).
  * Initial functional implementation of a rudimental communication protocol
  (issue #132).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 21 May 2013 09:53:39 +0200

plasduino (0.10.4) UNRELEASED; urgency=low

  * dumpModuleInfo() called in the website deploy script, so that it does not
  crash when called with a new module added (issue #130).
  * Minor changes to the web site.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sat, 27 Apr 2013 19:25:15 +0200

plasduino (0.10.3) UNRELEASED; urgency=low

  * Some improvements in the reactiontime module.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 25 Apr 2013 21:18:23 +0200

plasduino (0.10.2) UNRELEASED; urgency=low

  * First stub at a module to measure the human reaction time (issue #126).
  * Minor change in the formatting of the gRunningStat class (there was not
  enough significant digits).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 25 Apr 2013 10:38:26 +0200

plasduino (0.10.1) UNRELEASED; urgency=low

  * Shield added to the module info (issue #120).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 21 Apr 2013 17:12:37 +0200

plasduino (0.10.0) UNRELEASED; urgency=low

  * One-line fix for issue #121.
  * One-line fix for issue #125 (fixes #123, too).
  * One-line fix for issue #124.
  * Fritzing project for the scope shield added (issue #119).
  * Fritzing project for the PMT shield added (issue #117).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 21 Apr 2013 14:49:16 +0200

plasduino (0.9.13) UNRELEASED; urgency=low

  * Created the doc/images folder to hold the largish pics that we don't want
  to end up in the distribution.
  * Anchors to large figures removed from the web page.
  * Team page updated.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 02 Apr 2013 18:52:16 +0200

plasduino (0.9.12) UNRELEASED; urgency=low

  * Minor changes on the webpage.
  * Minor refactoring of the images for the website.
  * Wrong url corrected in the module dependencies.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sat, 30 Mar 2013 16:52:03 +0100

plasduino (0.9.11) UNRELEASED; urgency=low

  * A few minor changes on the webpage.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 29 Mar 2013 06:49:46 +0100

plasduino (0.9.10) UNRELEASED; urgency=low

  * Added material about the Lab I shield on the web page (issue #113).
  * Added material about the scaler shield on the web page (issue #113).
  * Minor changes to the copyright notice in the debian folder (issue #105).
  * Document type for the webpage changed from html 4.01 to html 5 (issue #112).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 28 Mar 2013 21:17:37 +0100

plasduino (0.9.9) UNRELEASED; urgency=low

  * Parsing of the metadata for the webpages implemented (issue #109).
  * Keywords and description added to all the web pages.
  * Added google sitemap to the web site (issue #108).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 28 Mar 2013 15:07:42 +0100

plasduino (0.9.8) UNRELEASED; urgency=low

  * More work on the web page.
  * Google verification file added to the webpage.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 27 Mar 2013 23:45:50 +0100

plasduino (0.9.7) UNRELEASED; urgency=low

  * More work on the documentation and the web page.
  * Minor improvement to the release script, which is now documented on the
  web page.
  * Major refactoring of the webpage classes.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 27 Mar 2013 15:37:32 +0100

plasduino (0.9.6) UNRELEASED; urgency=low

  * Online help menu added (issue #92).
  * Run control cleanup (issue #103).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 24 Mar 2013 08:15:38 +0100

plasduino (0.9.5) UNRELEASED; urgency=low

  * Default webpage for the package changed to:
    http://pythonhosted.org/plasduino/

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sat, 23 Mar 2013 23:00:36 +0100

plasduino (0.9.4) UNRELEASED; urgency=low

  * Dependencies added to the moduleinfo (issue #104).
  * First try on registering the new tag on PyPi automagically (issue #102).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sat, 23 Mar 2013 22:51:30 +0100

plasduino (0.9.3) UNRELEASED; urgency=low

  * Automatic update of plasduino.cfg implemented (issue #107).
  * Some more work on the web page (issue #99).
  * Credits added.
  * Experimental modules removed from the launcher (issue #101).
  * Created the plasduino_help module (issue #92).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sat, 23 Mar 2013 02:13:09 +0100

plasduino (0.9.2) UNRELEASED; urgency=low

  * Minor refactoring to let the build process of the we page pick the
  right tag information.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 22 Mar 2013 15:52:37 +0100

plasduino (0.9.1) UNRELEASED; urgency=low

  * More work on the web page (issue #99).
  * Default url pointing to the new webpage hosted by python.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 22 Mar 2013 10:02:19 +0100

plasduino (0.9.0) UNRELEASED; urgency=low

  * Compiling the html documentation as part of the release process.
  * Copyright modified (issue #105).
  * Initial implementation of the html web page (issue #99).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 21 Mar 2013 15:34:33 +0100

plasduino (0.8.1) UNRELEASED; urgency=low

  * Prevent the save dialog from being shown after the zero offset calibration
  in the pendulum view module (issue #100).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 03 Mar 2013 11:44:11 +0100

plasduino (0.8.0) UNRELEASED; urgency=low

  * Some refactoring of the pendulum view module, with the main plot now
  being a circular buffer only representing a subset of points (issue #97).
  * preun target removed from the rpm spec file (issue #96).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 03 Mar 2013 11:23:04 +0100

plasduino (0.7.3) UNRELEASED; urgency=low

  * Minor bug fix in the message logger file handler.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 15 Feb 2013 10:13:37 +0100

plasduino (0.7.2) UNRELEASED; urgency=low

  * RunControl for the pendulum view module extensively rewritten, now we have
  a much better handling on the audio buffer post-processing.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 06 Feb 2013 15:44:08 +0100

plasduino (0.7.1) UNRELEASED; urgency=low

  * Import statement changed in setup.py (pladuino.__tag__ to __tag__).
  * A few small improvements and bug fixes.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 05 Feb 2013 10:18:31 +0100

plasduino (0.7.0) UNRELEASED; urgency=low

  * Added a slot in the videoclick module to cancel the frame extraction
  (issue #68).
  * Disable the menu bar in the main windows while the data acquisition is
  running (issue #94).
  * Added the possibility of loading data from file in the pendulum view
  module (issue #75).
  * All event buffer incarnations refactored to make use of the new gDataTable
  class (issue #85).
  * A few small bug fixes.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 29 Jan 2013 17:02:09 +0100

plasduino (0.6.0) UNRELEASED; urgency=low

  * videoclick module largely refactored to use the new gDataTable class
  (should be transparent from the point of view of the functionalities).
  * Added context menu in the videclick module to clear the current frame or
  all frames in the event buffer (issue #78).
  * Proper cleanup upon rpm uninstall (issue #93).
  * Menu entry added at install time for the rpm package (issue #70).
  * Missing dependencies added to the debian package.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 28 Jan 2013 21:02:30 +0100

plasduino (0.5.8) UNRELEASED; urgency=low

  * Obsolete package.sh script and corresponding Makefile target removed
  (the release script now takes care of generating packages in all
  different flavors).
  * Obsolete post_install script removed.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 27 Jan 2013 22:51:45 +0100

plasduino (0.5.7) UNRELEASED; urgency=low

  * Release script largely refactored, now the rpm generation is based on
  a custom-made spec file (issues #36, 70, 71, 93).
  * data/*.csv files added to the distribution in the setup.py script.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 27 Jan 2013 22:41:15 +0100

plasduino (0.5.6) UNRELEASED; urgency=low

  * Double click on the main launcher starts the selected module (issue #89).
  * Warn users when manually leaving frames with incomplete data with a
  popup window (issue #91).
  * Bug fix in the videoclick module; not it should gracefully handle the case
  in which the video decoder cannot read the input file (issue #90).
  * Bug fix for spaces in input file names (issue #88).
  * A couple of data files updated.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Sun, 27 Jan 2013 10:25:40 +0100

plasduino (0.5.5) UNRELEASED; urgency=low

  * Ops fixed in the release process.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 23 Jan 2013 22:15:01 +0100

plasduino (0.5.4) UNRELEASED; urgency=low

  * videocapture module blacklisted from the module info.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 23 Jan 2013 22:12:32 +0100

plasduino (0.5.3) UNRELEASED; urgency=low

  * ffmpeg removed from the rpm requirements: it lives in rpmfusion (it's
  not readily available from the basic repositories) and it's only needed for
  the videoclick module.
  * Added option in the videoclik module to save data in csv format (with
  CR+LF EOL marker).
  * Bug fix when forcing the frame rate to a specified value for a video clip
  (propagate the change to the clip duration).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 23 Jan 2013 21:49:33 +0100

plasduino (0.5.2) UNRELEASED; urgency=low

  * Bug fix in the zoom pixmap widget for the videoclick module (issue #82).
  * Bug fix preventing the videoclick module to advance when clicking on the
  last frame (issue #84).
  * Module information added to the videcapture module (issue #83).
  * Release script modified to include a check on the post-installation script.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Wed, 23 Jan 2013 12:17:32 +0100

plasduino (0.5.1) UNRELEASED; urgency=low

  * Some work on the graphical interface for the videoclick module (issue #81).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 22 Jan 2013 06:56:02 +0100

plasduino (0.5.0) UNRELEASED; urgency=low

  * Improved diagnostics when connection to the arduino board fails (issue #72).
  * First stub at a video capture module (issue #73).
  * Bug fix in the main launcher (issue #77).
  * Some improvements in the videoclick module (issues #80 and #81).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 21 Jan 2013 20:15:29 +0100

plasduino (0.4.1) UNRELEASED; urgency=low

  * Minor bug fix in the release script.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 11 Jan 2013 18:35:32 +0100

plasduino (0.4.0) UNRELEASED; urgency=low

  * Configuration parser/editor is now application aware---i.e. for each module
  you only have the command-line options that are relevant for that module and
  only those options are available in the configuration editor (issue #46).
  * ffmpeg added to the requirements for the rpm.
  * More cleanup of the imports (issue #65).
  * Zoom in/out capability added to the gPlotWidget class (issue #67).
  * Module information strategy refactored to separate the modules shipped with
  the distribution from the custom modules (issue #50).
  * Fully revised release manager with complete automation of the process
  producing rpm and deb packages (issue #48).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 11 Jan 2013 18:28:17 +0100

plasduino (0.3.1) UNRELEASED; urgency=low

  * doc/release.notes now shipped with the distribution (issue #69).
  * installation prefix for the rpm change from /usr/local to /usr (issue #48).
  * commands (deprecated in python 3.x) module replaced with subprocess
  everywhere (issue #11).
  * All print statements converted to the python 3.x syntax (issue #11).
  * Some cleanup in the import statements (issue #65).
  * All file() occurrences replaced with open() (issue #11).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 08 Jan 2013 14:50:41 +0100

plasduino (0.3.0) UNRELEASED; urgency=low

  * Bug fix in the configuration editor (issue #57).
  * Semi-major refactoring of the gRunControl class, propagated to all the
  modules. The gRunControl class has no ParentWindow class member anymore and
  all the interaction with the main GUI happens via signals/slots (issue #51
  and issue #59).
  * Minor bug fix in gMainWindow.
  * Added option to change the path to the file with the runId (issue #58).
  * Some improvement in the plasduino_doctor module (issue #55).
  * gDeviceManager class renamed as gArduinoManager and change propagated
  throughout (issue #51).
  * Arduino part of the RunControl factored out in a separate class
  gArduinoRunControl, inheriting from gRunControl (issue #51).
  * test and calibration folders cleaned up (issue #61).
  * Bug fix in the arduino.__arduino__ module (issue #40).
  * Bug fix in gMainWindow (issue# 63).
  * Small popup message box added to the plasduino_doctor module (issue #54).
  * RunControl and EventBuffer (in all their incarnations) refactored to improve
  the innermost event loop (issue #62).
  * Sample average and stdev written in the dice module (issue #64).
  * New video decoding classes used throughout (issue #49).
  * Some work into standardizing the release process (issue #48).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 07 Jan 2013 22:50:13 +0100

plasduino (0.2.4) UNRELEASED; urgency=low

  * process() method for the audio event buffer implemented.
  * Mutex added to the audio run control in order to make the audio readout
  thread-safe (issue #53).
  * A bunch of missing files added to setup.py.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 04 Jan 2013 14:20:45 +0100

plasduino (0.2.3) UNRELEASED; urgency=low

  * Minor refactoring of gModule info (formatting of the abstract text).
  * launch() hook added to all the modules to facilitate calling applications
  from the main plasduino launcher.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 04 Jan 2013 10:42:45 +0100

plasduino (0.2.2) UNRELEASED; urgency=low

  * Main plasduino laucher now inheriting from QDialog, so that we don't need to
  spawn another thread for the main application (the dialog is hidden and the
  main application is launched in the same thread).
  * Minor changes to the plasduino launcher appearance (the right part is
  slightly larger and the four buttons in the bottom now line up).
  * Minor changes to the module info section for most of the modules.
  * Added new plasduino_config module.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 04 Jan 2013 01:17:41 +0100

plasduino (0.2.1) UNRELEASED; urgency=low

  * Initial prototype for the pendulumview module.
  * Initial prototype for an audiomonitor application to be used as a
  replacement of the pendulumView program.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Thu, 03 Jan 2013 13:50:05 +0100

plasduino (0.2.0) UNRELEASED; urgency=low

  * Fully-fledged brand new videoclick module in (issue #45).
  * Added option to prompt a save-file dialog at the end of the run to create
  a copy of the processed data file (issue #41).
  * Implemented command line switches and widgets in the configuration editor
  to select the sensors attached to the analog inputs and changes propagated
  to the tempmonitor module (issue #42).
  * tempmonitor module now working with a single thermistor (issue #34).
  * Leave the acquisition window open in case of communication error over the
  serial connection and pop up a diagnostic message from the main GUI
  (issue #43).

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Tue, 01 Jan 2013 21:43:21 +0100

plasduino (0.1.3) UNRELEASED; urgency=low

  * Command-line options added to the release.py script.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 12 Oct 2013 14:23:42 +0100

plasduino (0.1.2) UNRELEASED; urgency=low

  * modulinfo part reworked (please expand, Jacopo).
  * Some work put into the packaging (please expand, Jacopo).
  * Bug fix for issue #37.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 12 Oct 2013 11:29:41 +0100

plasduino (0.1.1) UNRELEASED; urgency=low

  * Added some information (to be used by the launcher window) to each module.
  * All modules renamed (the "-", which was preventing from importing the
  modules themselves from other python applications has been changed to "_", see
  issue #31).
  * Initial version of the new laucher window (modules/plasduino), which is
  supposed to be the main entry point for the modules (see issue #28).
  * Initial version of the release manager (see issue #29).
  * A few bug fixes in the setup.py distutils script.
  * Lots of minor changes/bug fixes.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Fri, 03 Aug 2013 15:07:38 +0100

plasduino (0.1.0) UNRELEASED; urgency=low

  * Initial tag, mainly to start exercising the functionality of our rudimentary
  release system.
  * The basic functionalities are implemented and working.

 -- Jacopo Nespolo <j.nespolo@gmail.com>  Mon, 30 Jul 2013 13:07:28 +0100

